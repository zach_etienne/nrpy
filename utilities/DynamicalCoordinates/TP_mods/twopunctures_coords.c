// reading a text file
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

const int NG = 1; // Number of ghost zones
const int NA = 32; // Number of points in A
const int NB = 32; // Number of points in B

const double b = 1.0; // Distance from the origin to the puncture
const double SF1 = 1.0; // Strength factor acting on the puncture at x = +b
const double SF2 = 1.0; // Strength factor acting on the puncture at x = -b
const double L1 = 0.1; // Lengthscale acting on the puncture at x = +b
const double L2 = 0.2; // Lengthscale acting on the puncture at x = +b

// Grid index function
static inline int IDX(const int NBT, const int i, const int j)
{
  return j + NBT * i;
}

// Uniform spacing in A
double A(const int i)
{
  double dA = 1.0 / ((double)NA + NG);
  return 0.0 + ((double)i - NG + 0.5) * dA;
}

// Uniform spacing in B
double B(const int j)
{ 
  double dB = 2.0 / ((double)NB);
  return -1.0 + ((double)j - NG + 0.5) * dB;
}

// TwoPunctures x-coordinate
double x(const int i, const int j)
{ 
  double AA = A(i);
  double BB = B(j);
  return b * (AA * AA + 1.0) / (AA * AA - 1.0) * (2.0 * BB) / (1.0 + BB * BB); 
}

// TwoPunctures y-coordinate
double y(const int i, const int j)
{ 
  double AA = A(i);
  double BB = B(j);
  return b * (2.0 * AA) / (1.0 - AA * AA) * (1.0 - BB * BB) / (1.0 + BB * BB);
}

// Magnify grid points about x = +b
void xyp(const int i, const int j, double *xp, double *yp)
{
  // Translate x = +b to the origin
  double xxmb = x(i, j) - b;
  double yy = y(i, j);

  // Transform to spherical coordinates
  double rrmb = sqrt(xxmb * xxmb + yy * yy);

  // Magnify the coordinate distribution
  double f1 = rrmb * (SF1 * exp(-pow(rrmb / L1, 2)) + 1);
  double rrmbp = f1;

  // Transform back to Cartesian, and translate the origin back to x = +b
  *xp = rrmbp * xxmb / rrmb + b;
  *yp = rrmbp * yy / rrmb;
}

// Magnify grid points about x = -b
void xypp(const int i, const int j, double *xpp, double *ypp)
{
  // Apply the transformation about x = +b
  double xxp, yyp;
  xyp(i,j,&xxp,&yyp);

  // Translate x = -b to the origin
  double xxpb = xxp + b;
  double yy = yyp;
  
  // Transform to spherical coordinates
  double rrpb = sqrt(xxpb * xxpb + yy * yy);
  
  // Magnify the coordinate distribution
  double f2 = rrpb * (SF2 * exp(-pow(rrpb / L2, 2)) + 1);
  double rrpbp = f2;
  
  // Transform back to Cartesian, and translate the origin back to x = -b
  *xpp = rrpbp * xxpb / rrpb - b;
  *ypp = rrpbp * yy / rrpb;
}

/* Coord xformation workbook; outputs (partial xpp)/(partial A), "fully simplified". Easy for Mathematica to compute. Other partials straightforward.
   x = b*(A*A + 1)/(A*A - 1)*(2*B)/(1 + B*B);
   y = b*(2*A)/(1 - A*A)*(1 - B*B)/(1 + B*B);
   xxmb = x - b;
   rrmb = Sqrt[xxmb^2 + y^2];
   rrmbp = f1[A, B]*rrmb;
   xp = rrmbp/rrmb*xxmb + b;
   yp = rrmbp/rrmb*y;
   xxpb = xp + b;
   rrpb = Sqrt[xxpb^2 + yp^2];
   rrpbp = f2[A, B]*rrpb;
   xpp = rrpbp/rrpb*xxpb - b;
   ypp = rrpbp/rrpb*yp
   FullSimplify[D[xpp, AA]]
*/

int main()
{
  // File output
  FILE *outfile;
  outfile = fopen("coord_output.txt", "w");

  // Total number of grid points, uncluding ghost zones
  const int NAT = NA + 2 * NG;
  const int NBT = NB + 2 * NG;

  // Output lines of constant A
  for(int i = NG; i < NAT - NG; i++)
    { 
      for(int j = NG; j < NBT - NG; j++)
	{
	  double xp, yp;
	  xypp(i, j, &xp, &yp);
	  fprintf(outfile, "%e %e %e %e %e %e\n", A(i), B(j), xp, yp, x(i, j), y(i, j));
	}

      fprintf(outfile, "\n");
    }

  // Output lines of constant B
  for(int j = NG; j < NBT - NG; j++)
    {
      for(int i = NG; i < NAT - NG; i++)
	{ 
	  double xp, yp;
	  xypp(i, j, &xp, &yp);
	  fprintf(outfile, "%e %e %e %e %e %e\n", A(i), B(j), xp, yp, x(i, j), y(i, j));
	}

      fprintf(outfile, "\n");
    }

  // Allocate memory to store TwoPunctures coordinate distribution
  double *xval = (double *)malloc(sizeof(double) * NAT * NBT);
  double *yval = (double *)malloc(sizeof(double) * NAT * NBT);

  // Store TwoPunctures coordinate distribution
  for(int i = 0; i < NAT; i++)
    { 
      for(int j = 0; j < NBT; j++)
	{
	  const int idx = IDX(NBT, i, j);
	  double xi, yi;
	  xypp(i, j, &xi, &yi);
	  xval[idx] = xi;
	  yval[idx] = yi;
	}
    }

  // Domain boundaries
  int imin = 0, jmin = 0;
  double dmin = 1.0e10, Amin = 0.0, Bmin = 0.0, xmin = 0.0, ymin = 0.0;

  // Find smallest step size
  for(int i = NG; i < NAT - NG; i++)
    { 
      for(int j = NG; j < NBT - NG; j++)
	{
	  const int idx = IDX(NBT, i, j);
	  const int ip1 = IDX(NBT, i + 1, j);
	  const int im1 = IDX(NBT, i - 1, j);
	  const int jp1 = IDX(NBT, i, j + 1);
	  const int jm1 = IDX(NBT, i, j - 1);

	  // Distances between point (i, j) and its nearest neighbors
	  const double dxip1 = xval[idx] - xval[ip1];
	  const double dxim1 = xval[idx] - xval[im1];
	  const double dxjp1 = xval[idx] - xval[jp1];
	  const double dxjm1 = xval[idx] - xval[jm1];

	  const double dyip1 = yval[idx] - yval[ip1];
	  const double dyim1 = yval[idx] - yval[im1];
	  const double dyjp1 = yval[idx] - yval[jp1];
	  const double dyjm1 = yval[idx] - yval[jm1];

	  const double drip1 = sqrt(dxip1 * dxip1 + dyip1 * dyip1);
	  const double drim1 = sqrt(dxim1 * dxim1 + dyim1 * dyim1);
	  const double drjp1 = sqrt(dxjp1 * dxjp1 + dyjp1 * dyjp1);
	  const double drjm1 = sqrt(dxjm1 * dxjm1 + dyjm1 * dyjm1);

	  const double dri = (drip1 < drim1) ? drip1 : drim1;
	  const double drj = (drjp1 < drjm1) ? drjp1 : drjm1;
	  const double dr = (dri < drj) ? dri : drj;

	  // Find the minimum
	  if(dmin > dr)
	    {
	      dmin = dr;
	      imin = i;
	      jmin = j;
	      Amin = A(i);
	      Bmin = B(j);
	      xmin = xval[idx];
	      ymin = yval[idx];
	    }
	}
    }

  printf("\ndmin = %e\nAmin = %e\nBmin = %e\nxmin = %e\nymin = %e\nimin = %d\njmin = %d\n", dmin, Amin, Bmin, xmin, ymin, imin, jmin);

  // Close file output
  fclose(outfile);

  // Free memory
  free(xval);
  free(yval);

  return 0;
}
