## mclachlan tov_static
ActiveThorns = "Time MoL"
ActiveThorns = "Coordbase CartGrid3d Boundary StaticConformal"
ActiveThorns = "SymBase ADMBase"
ActiveThorns = "IOUtil"
ActiveThorns = "CoordGauge Constants LocalReduce aeilocalinterp LoopControl"
ActiveThorns = "Carpet CarpetLib CarpetReduce CarpetRegrid2 CarpetInterp"
ActiveThorns = "CarpetIOASCII CarpetIOScalar CarpetIOHDF5 CarpetIOBasic QuasiLocalMeasures TmunuBase ADMCoupling ADMMacros"

ActiveThorns = "AHFinderDirect SpaceMask"
SpaceMask::use_mask = "yes"
AHFinderDirect::find_every                             = 1
AHFinderDirect::geometry_interpolator_name             = "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars             = "order=4"
AHFinderDirect::initial_guess__coord_sphere__radius[1] = 0.25
AHFinderDirect::initial_guess_method[1]                = "coordinate sphere"
AHFinderDirect::max_Newton_iterations__initial         = 50
AHFinderDirect::max_Newton_iterations__subsequent      = 10
AHFinderDirect::N_horizons                             = 1
AHFinderDirect::output_BH_diagnostics                  = "yes"
AHFinderDirect::reset_horizon_after_not_finding[1]     = "yes"
AHFinderDirect::set_mask_for_individual_horizon[1]     = "no"
AHFinderDirect::surface_interpolator_name              = "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars              = "order=4"
#AHFinderDirect::verbose_level                          = "algorithm details"
AHFinderDirect::which_surface_to_store_info[1]         = 0


ActiveThorns = "ET__SENR_bridge__ET_thorn"

ET__SENR_bridge__ET_thorn::Nx = 80
ET__SENR_bridge__ET_thorn::Ny = 80
ET__SENR_bridge__ET_thorn::Nz = 80

# grid parameters
driver::ghost_size = 1
CoordBase::boundary_size_x_lower = 1
CoordBase::boundary_size_y_lower = 1
CoordBase::boundary_size_z_lower = 1
CoordBase::boundary_size_x_upper = 1
CoordBase::boundary_size_y_upper = 1
CoordBase::boundary_size_z_upper = 1

grid::avoid_origin = "no"
CartGrid3D::type         = "coordbase"
CoordBase::domainsize = "minmax"
CoordBase::xmin = -0.6
CoordBase::ymin = -0.6
CoordBase::zmin = -0.6
CoordBase::xmax =  0.6
CoordBase::ymax =  0.6
CoordBase::zmax =  0.6
CoordBase::dx   =   .01518987341772151898
CoordBase::dy   =   .01518987341772151898
CoordBase::dz   =   .01518987341772151898

ADMBase::metric_type     = "physical"

ADMBase::initial_data = "ET__SENR_bridge__ET_thorn"
ADMBase::initial_lapse = "ET__SENR_bridge__ET_thorn"
ADMBase::initial_shift = "ET__SENR_bridge__ET_thorn"

cactus::cctk_itlast = 100000
#Cactus::terminate           = "time"
#Cactus::cctk_final_time     = 10000

Carpet::domain_from_coordbase = "yes"
Carpet::enable_all_storage       = no
Carpet::use_buffer_zones         = "yes"

Carpet::poison_new_timelevels    = "yes"
Carpet::check_for_poison         = "no"

Carpet::init_3_timelevels        = no
Carpet::init_fill_timelevels     = "yes"

CarpetLib::poison_new_memory = "yes"
CarpetLib::poison_value      = 114

# system specific Carpet paramters
Carpet::max_refinement_levels    = 1
Carpet::prolongation_order_space = 5
Carpet::prolongation_order_time  = 0

CarpetRegrid2::regrid_every = 0
CarpetRegrid2::num_centres  = 1

CarpetRegrid2::num_levels_1 = 1

time::dtfac = 0.5
#MoL::ODE_Method             = "rk4"
MoL::ODE_Method             = "euler"
MoL::MoL_Intermediate_Steps = 1
#MoL::MoL_Num_Scratch_Levels = 1

# check all physical variables for NaNs
ActiveThorns = "NaNChecker"
NaNChecker::check_every = 1
NaNChecker::action_if_found = "just warn" #"terminate", "just warn", "abort"
NaNChecker::check_vars = "ADMBase::metric ADMBase::lapse ADMBase::shift"

## Lapse Condition:  \partial_t alpha = - alpha K
## Shift Condition:  \partial_t beta^i = 0

ActiveThorns = "SphericalSurface"

SphericalSurface::nsurfaces = 1
#SphericalSurface::maxntheta = 79
#SphericalSurface::maxnphi = 160
#SphericalSurface::ntheta      [0] = 79
#SphericalSurface::nphi        [0] = 160

SphericalSurface::maxntheta = 155
SphericalSurface::maxnphi = 304
SphericalSurface::ntheta      [0] = 155
SphericalSurface::nphi        [0] = 304
SphericalSurface::nghoststheta[0] = 2
SphericalSurface::nghostsphi  [0] = 2

# Parameters of thorn QuasiLocalMeasures (implementing QuasiLocalMeasures)
QuasiLocalMeasures::interpolator         = "Lagrange polynomial interpolation"
QuasiLocalMeasures::interpolator_options = "order=4"
QuasiLocalMeasures::num_surfaces         = 1
QuasiLocalMeasures::spatial_order        = 2
QuasiLocalMeasures::surface_index[0]     = 0
#QuasiLocalMeasures::verbose              = "yes"


# init parameters
#InitBase::initial_data_setup_method = "init_some_levels"

# I/O

IOUtil::checkpoint_dir          = ABE-$parfile
IOUtil::checkpoint_every_walltime_hours = 1000.95
IOUtil::checkpoint_keep         = 2
IOUtil::checkpoint_on_terminate = "no"
IOUtil::out_dir                 = ABE-$parfile
IOUtil::out_fileinfo            = "none"
IOUtil::parfile_write           = "generate"
IOUtil::recover                 = "autoprobe"
IOUtil::recover_dir             = ABE-$parfile

IOBasic::outInfo_every = 1
#IOBasic::outInfo_vars  = "Carpet::physical_time_per_hour"
IOBasic::outInfo_vars  = "Carpet::physical_time_per_hour ADMBase::gxx ADMBase::kxx"

IOScalar::outScalar_every    = 4
CarpetIOScalar::outScalar_reductions = "maximum minimum norm2 norm1"
IOScalar::one_file_per_group = no
IOScalar::outScalar_vars     = "
  ADMBase::gxx
  QuasiLocalMeasures::qlm_spin[0]
  QuasiLocalMeasures::qlm_radius[0]
  QuasiLocalMeasures::qlm_mass[0]
  QuasiLocalMeasures::qlm_3det[0]
"

CarpetIOASCII::out0D_every            = 4
CarpetIOASCII::out0D_vars             = "
  QuasiLocalMeasures::qlm_spin[0]
  QuasiLocalMeasures::qlm_radius[0]
  QuasiLocalMeasures::qlm_mass[0]
  QuasiLocalMeasures::qlm_3det[0]
  Carpet::physical_time_per_hour
"

IOASCII::out1D_every        = -1
#IOASCII::one_file_per_group = no
IOASCII::output_symmetry_points = no
IOASCII::out3D_ghosts           = no
IOASCII::out3D_outer_ghosts     = no
IOASCII::out1D_vars         = "
  ADMBase::gxx
  ADMBase::gxy
  ADMBase::gxz
  ADMBase::gyy
  ADMBase::gyz
  ADMBase::gzz
  ADMBase::kxx
  ADMBase::kxy
  ADMBase::kxz
  ADMBase::kyy
  ADMBase::kyz
  ADMBase::kzz
"

IOASCII::out2D_every        = -1
IOASCII::out2D_vars         = "
  ADMBase::gxx
  ADMBase::gxy
  ADMBase::gxz
  ADMBase::gyy
  ADMBase::gyz
  ADMBase::gzz
  ADMBase::kxx
  ADMBase::kxy
  ADMBase::kxz
  ADMBase::kyy
  ADMBase::kyz
  ADMBase::kzz
"
