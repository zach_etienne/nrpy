#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include <gsl/gsl_sf_hyperg.h>

int main(int argc, char *argv[]) {
  if(argc!=3) {
    printf("ERROR: need 2 commmand line arguments. Usage: ./spin_compute [time] [polar-to-circumferential ratio]\n");
    exit(1);
  }
  double time = strtod(argv[1],NULL);
  double ratio = strtod(argv[2],NULL); //E.g., try 0.7350906615;

  if(ratio > 1.0) {
    printf("%e %.15e # WARNING: input polar-to-circumferential ratio = %e is greater than 1.0. Assuming spin=0.\n",time,0.0,ratio);
    return 0;
  }
  
  // Initial guess for spin.
  double spin = 0.9;

  // Empirically, 30 iterations is sufficient for convergence.
  for(int i=0;i<30;i++) {
    double sqrt__one_minus_spin_sq = sqrt(1 - spin*spin);
    double Elliptic_integral_arg = - (spin*spin) / ( (1.0 + sqrt__one_minus_spin_sq) * (1.0 + sqrt__one_minus_spin_sq) );

    // GOTTA BE REALLY CAREFUL HERE. GSL DEFINES COMPLETE ELLIPTIC INTEGRAL OF SECOND KIND DIFFERENTLY THAN Mathematica & Eq 5.2 of https://arxiv.org/pdf/gr-qc/0411149.pdf
    double Elliptic_integral = 0.5*M_PI*gsl_sf_hyperg_2F1(-0.5, 0.5, 1.0, Elliptic_integral_arg);

    double new__sqrt__one_minus_spin_sq = ratio*M_PI / Elliptic_integral - 1.0;
    double new__one_minus_spin_sq = new__sqrt__one_minus_spin_sq*new__sqrt__one_minus_spin_sq;

    // 1 - s^2 = n -> s = sqrt(1-n)
    double new_spin = sqrt(1 - new__one_minus_spin_sq);
    spin = new_spin;
  }
  printf("%e %.15e\n",time,spin);

  return 0;
}
