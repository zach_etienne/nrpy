#!/bin/bash

rm -f spin.txt

cat $1 | awk '{if(NF==40) print "./spin_compute ",$2,$24 " >> spin.txt"}' > spincompute_script.sh; chmod 755 spincompute_script.sh; ./spincompute_script.sh

rm -f spincompute_script.sh
