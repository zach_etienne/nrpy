%\documentclass[twocolumn,showpacs,aps,prd,nobibnotes,floatfix]{revtex4-1}
\documentclass[11pt]{article}
%\documentclass[aps,prd]{revtex4-1}
\usepackage[hmargin=1.5cm,vmargin=1.5cm,bindingoffset=0.0cm]{geometry}
%\usepackage[hmargin=1.2cm,vmargin=1.2cm,bindingoffset=0.0cm]{geometry}

\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{epsf}
\usepackage{longtable}
\usepackage{graphics,epsfig,placeins,subfigure,wrapfig}
\usepackage[usenames]{color}
\usepackage{amsmath}
\usepackage{soul}
\newcommand{\beq}{\begin{equation*}}
\newcommand{\eeq}{\end{equation*}}
\newcommand{\beqn}{\begin{eqnarray*}}
\newcommand{\eeqn}{\end{eqnarray*}}
\newcommand{\ve}[1]{\mbox{\boldmath $#1$}}
%\renewcommand*{\arraystretch}{2}
\linespread{0.935}

% Turns off section numbering:
\setcounter{secnumdepth}{0} % <-- Turns off section numbering
\begin{document}
\title{\vspace{-1cm}Notes on upwinded and downwinded finite difference stencils}
\author{Zachariah B.~Etienne}
\date{}
\maketitle
\vspace{-0.5cm}

It is assumed that you have already read the scalarwave tutorial,
which is housed in the same directory as this file.

\section{Upwind \& Downwind Derivatives}

Let's try upwind derivatives first. 
Expanding neighboring points by their Taylor series approximation, we
get
\beqn
u_{j-1} &=& u_j - (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j - \frac{(\Delta x)^3}{3!} u'''_j +\mathcal{O}((\Delta x)^4)\\
u_{j} &=& u_j \\
u_{j+1} &=& u_j + (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j + \frac{(\Delta x)^3}{3!} u'''_j +\mathcal{O}((\Delta x)^4)\\
u_{j+2} &=& u_j + (2 \Delta x) u'_j + \frac{(2 \Delta x)^2}{2} u''_j + \frac{(2 \Delta x)^3}{3!} u'''_j +\mathcal{O}((\Delta x)^4).
\eeqn
Our goal is to compute coefficients $a_j$ such that
$(a_{j-1} u_{j-1} + a_{j} u_{j}+...)/(\Delta x) = u'_j +\mathcal{O}((\Delta x)^3)$. 

So we get
\beqn
&& \frac{a_{j-1} u_{j-1} + a_j u_j + a_{j+1} u_{j+1} +a_{j+2} u_{j+2}}{\Delta x} \\
= && + \left( u_j - (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j - \frac{(\Delta x)^3}{3!} u'''_j+\frac{(\Delta x)^4}{4!} u^{(4)}_j \right) a_{j-1} \\
&& + \left( u_j \right) a_{j} \\
&& + \left( u_j + (\Delta x) u'_j + \frac{(\Delta x)^2}{2} u''_j + \frac{(\Delta x)^3}{3!} u'''_j+\frac{(\Delta x)^4}{4!} u^{(4)}_j \right) a_{j+1} \\
&& + \left( u_j + (2 \Delta x) u'_j + \frac{(2 \Delta x)^2}{2} u''_j + \frac{(2 \Delta x)^3}{3!} u'''_j+\frac{(2 \Delta x)^4}{4!} u^{(4)}_j \right) a_{j+2}
\eeqn

First notice that each time we take a derivative in the Taylor
expansion, we multiply by a $\Delta x$. Notice that this helps to keep
the units consistent (e.g., if $x$ were in units of meters). Let's
just agree to absorb those $\Delta x$'s into the derivatives and next
rearrange terms:
\beqn
&& a_{j-1} u_{j-1} + a_j u_j + a_{j+1} u_{j+1} + a_{j+2} u_{j+2} \\
= && \left(+a_{j-1} + 1*a_j + a_{j+1} + 2^0*a_{j+2} \right) u_j \\
  && \left(-a_{j-1} + 0*a_j + a_{j+1} + 2^1*a_{j+2} \right) u_j'/1! \\
  && \left(+a_{j-1} + 0*a_j + a_{j+1} + 2^2*a_{j+2} \right) u_j''/2! \\
  && \left(-a_{j-1} + 0*a_j + a_{j+1} + 2^3*a_{j+2} \right) u_j'''/3! \\
= && u'_j
\eeqn

In order for the above to hold true for any values 
$\left\{ u_j,u'_j,u''_j,u'''_j,u^{(4)}_j\right\}$, the following set of equations must also
hold:
\beqn
0 &=& (-1)^0*a_{j-1} + 1*a_j + a_{j+1} + a_{j+2}\\
1 &=& (-1)^1*a_{j-1} + 0*a_j + a_{j+1} + 2 a_{j+2}\\
0 \times 2! &=& (-1)^2*a_{j-1} + 0*a_j + a_{j+1} + 2^2 a_{j+2}\\
0 \times 3! &=& (-1)^3*a_{j-1} + 0*a_j + a_{j+1} + 2^3 a_{j+2} \\
\eeqn

In matrix form, this becomes ({\bf NOTE THAT WE SET $0^0=1$ FOR
  CONVENIENCE OF NOTATION}):
\beq
\left(
\begin{array}{c}
0 \\
1 \\
0\times 2! \\
0\times 3! \\
\end{array}
\right)
=
\left(
\begin{array}{cccc}
(-1)^0 & 0^0 & (1)^0 & (2)^0 \\
(-1)^1 & 0^1 & (1)^1 & (2)^1 \\
(-1)^2 & 0^2 & (1)^2 & (2)^2 \\
(-1)^3 & 0^3 & (1)^3 & (2)^3 \\
\end{array}
\right)
\left(
\begin{array}{c}
a_{j-1} \\
a_{j} \\
a_{j+1} \\
a_{j+2} \\
\end{array}
\right)
\eeq

\end{document}
