\documentclass[notitlepage,11pt,floatfix,tightenlines]{report}
%\documentclass[notitlepage,11pt,floatfix,tightenlines]{report}

%%%%%\setcounter{secnumdepth}{3}
\usepackage[hmargin=1.2cm,vmargin=0.9cm,bindingoffset=0.0cm,includefoot,footskip=0.7cm]{geometry}

\usepackage{listings}

\usepackage{hyperref}
\usepackage{url}
\makeatletter
\g@addto@macro{\UrlBreaks}{\UrlOrds}

\usepackage{graphicx}
\usepackage{epsf}
\usepackage{longtable}
\usepackage{graphics,epsfig,placeins,subfigure,wrapfig}
\usepackage[usenames]{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{soul}

% Enable inline multi-column text
\usepackage{multicol}

% For colored, rounded text boxes.
\usepackage{tcolorbox}
\tcbuselibrary{skins}
\tcbuselibrary{breakable}

% Approximately proportional to: https://tex.stackexchange.com/questions/33538/how-to-get-an-approximately-proportional-to-symbol
\newcommand{\appropto}{\mathrel{\vcenter{
  \offinterlineskip\halign{\hfil$##$\cr
    \propto\cr\noalign{\kern2pt}\sim\cr\noalign{\kern-2pt}}}}}

\newcommand{\beqNL}{\begin{equation*}}
\newcommand{\eeqNL}{\end{equation*}}
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\beqn}{\begin{eqnarray}}
\newcommand{\eeqn}{\end{eqnarray}}
\newcommand{\beqnNL}{\begin{eqnarray*}}
\newcommand{\eeqnNL}{\end{eqnarray*}}
\newcommand{\ve}[1]{\mbox{\boldmath $#1$}}
\renewcommand*{\arraystretch}{1}



\newenvironment{packed_itemize}{
\begin{itemize}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}{\end{itemize}}
\newenvironment{packed_enumerate}{
\begin{enumerate}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}{\end{enumerate}}

\usepackage{titlesec}

\titlespacing*{\chapter}{0pt}{-0.5cm}{0.25cm}
\titleformat{\chapter}[display]
{\normalfont\bfseries\filcenter}
{}
{1ex}
{\ifnum\value{chapter}>0\titlerule[2pt]
\vspace{2ex}%
\LARGE Chapter \thechapter: 
\else 
\titlerule[2pt]
\vspace{2ex}
\LARGE\fi}
[\vspace{1ex}%
{\titlerule[2pt]}]

\begin{document}

\title{{\Large Study of ``Nearly Exponential'' Convergence with Increased
  Finite Difference Order}}
\author{Zachariah B. Etienne}
\date{January 2018}
\vspace{-0.5cm}
\maketitle

These notes explain why we can expect nearly exponential convergence
of smooth functions with increased finite difference order, keeping
numerical grids fixed at a moderate resolution.

%% Mathematica notebook used to generate below table:
%% Do[deriv = 1;
%%  stencil = 
%%   Inverse[Transpose[
%%     Table[If[i == 0, 1, (j - ORDER/2)^i], {j, 0, ORDER}, {i, 0, 
%%       ORDER}]]];
%%  (*MatrixForm[stencil]*)
 
%%  sum = 0;
%%  Do[f[i - ORDER/2] := 0;
%%   Do[power = If[j == 0, 1, dx^(j - 1)*(i - ORDER/2)^j];
%%    f[i - ORDER/2] += uj[j]*power/Factorial[j], {j, 0, ORDER + 2}];
%%   (*Print[f[i-ORDER/2]];*)
  
%%   FDcoeff = Extract[stencil, {i + 1, deriv + 1}]*Factorial[deriv];
%%   sum += FDcoeff*f[i - ORDER/2], {i, 0, ORDER}];
%%  error[ORDER] = FullSimplify[sum - uj[deriv]];
%%  errorcoeff[ORDER] = error[ORDER]/(dx^ORDER*uj[ORDER + 1]);
%%  If[ORDER == 2, coeffname = "C_2 ", 
%%   coeffname = 
%%    "\\frac{C_{" <> ToString[(ORDER - 2)] <> "}}{" <> 
%%     ToString[coeffname = 1. errorcoeff[ORDER - 2]/errorcoeff[ORDER]] <>
%%      "} \\times "];
%%  Print[ORDER, " & $", TeXForm[errorcoeff[ORDER]], 
%%   "(\\Delta x)^{", (ORDER), "} f^{(", (ORDER + 1), ")}(\\xi)$ & $", 
%%   coeffname, "(\\Delta x)^{", (ORDER), "} f^{(", (ORDER + 1), 
%%   ")}(\\xi)$ \\\\"]
%%  (*If[ORDER>2,Print["Error coefficient ratio for \
%% ORDER=",ORDER,"/ORDER=",(ORDER-2)": \
%% ",1.errorcoeff[ORDER-2]/errorcoeff[ORDER]]]*), {ORDER, 2, 20, 2}]

\begin{table}[h!]
\begin{center}
\begin{tabular}{c c c}
Finite Difference Order & Dominant Error Term &
Error Term, Alternative Form\\
\hline
2 & $\frac{1}{6}(\Delta x)^{2} f^{(3)}(\xi)$ & $C_2 (\Delta x)^{2} f^{(3)}(\xi)$ \\
4 & $-\frac{1}{30}(\Delta x)^{4} f^{(5)}(\xi)$ & $\frac{C_{2}}{-5.}  (\Delta x)^{4} f^{(5)}(\xi)$ \\
6 & $\frac{1}{140}(\Delta x)^{6} f^{(7)}(\xi)$ & $\frac{C_{4}}{-4.66667}  (\Delta x)^{6} f^{(7)}(\xi)$ \\
8 & $-\frac{1}{630}(\Delta x)^{8} f^{(9)}(\xi)$ & $\frac{C_{6}}{-4.5}  (\Delta x)^{8} f^{(9)}(\xi)$ \\
10 & $\frac{1}{2772}(\Delta x)^{10} f^{(11)}(\xi)$ & $\frac{C_{8}}{-4.4}  (\Delta x)^{10} f^{(11)}(\xi)$ \\
12 & $-\frac{1}{12012}(\Delta x)^{12} f^{(13)}(\xi)$ & $\frac{C_{10}}{-4.33333}  (\Delta x)^{12} f^{(13)}(\xi)$ \\
14 & $\frac{1}{51480}(\Delta x)^{14} f^{(15)}(\xi)$ & $\frac{C_{12}}{-4.28571}  (\Delta x)^{14} f^{(15)}(\xi)$ \\
16 & $-\frac{1}{218790}(\Delta x)^{16} f^{(17)}(\xi)$ & $\frac{C_{14}}{-4.25}  (\Delta x)^{16} f^{(17)}(\xi)$ \\
18 & $\frac{1}{923780}(\Delta x)^{18} f^{(19)}(\xi)$ & $\frac{C_{16}}{-4.22222}  (\Delta x)^{18} f^{(19)}(\xi)$ \\
20 & $-\frac{1}{3879876}(\Delta x)^{20} f^{(21)}(\xi)$ & $\frac{C_{18}}{-4.2}  (\Delta x)^{20} f^{(21)}(\xi)$ \\
\vdots & \vdots & \vdots \\
200 &  & $\frac{C_{198}}{-4.02}  (\Delta x)^{200} f^{(201)}(\xi)$ \\
\vdots & \vdots & \vdots \\
$N$ &  & $\frac{C_{N-2}}{-4 (1 + N^{-1})}  (\Delta x)^{N} f^{(N+1)}(\xi)$ \\
\end{tabular}
\caption{Dominant finite difference error terms (``truncation
  error''), when computing $f'(x)$ with uniformly spaced, centered
  finite derivative stencils. $N$th order was computed by fitting all
  data from $N=2$ through $N=200$, where $N$ is even.}
\label{Errorterms}
\end{center}
\end{table}

Table~\ref{Errorterms} shows the dominant error term at varying finite
difference order, for the first derivative of some function
$f(x)$. Notice the $N$th-order-accurate finite difference stencil has
a dominant error term that is proportional to $f^{(N+1)}(\xi) (\Delta
x)^N$, times some coefficient that drops by between a
factor of 4 and 5 (see rightmost column) with each increment in finite difference
order. Here, $f^{(N+1)}(\xi)$ is the $N+1$st derivative of $f(x)$ at
the point $\xi$ in the finite difference stencil that maximizes the
value of $f^{(N+1)}(\xi)$.

\vspace{1in}\begin{center} {\Large \it Continued on next page} \end{center}


\begin{tcolorbox}[width=\textwidth,colback=red!5!white,colframe=blue!25,title={\bf
      Conditions Required to be Truncation-Error Dominated},coltitle=black]
We say that truncation error dominates so long as all of the following
conditions hold:
\begin{packed_enumerate}
\item $\Delta x$ is large enough such that roundoff error is not dominant
\item $\Delta x$ is small enough such that the function is not
  undersampled
\item $\Delta x$ {\it and} the finite difference stencil is small
  enough such that Runge's phenomenon\footnote{Strictly speaking,
    Runge's phenomenon refers to the error when {\it interpolating}
    a function with rapidly growing $f^{(N+1)}(\xi)$ versus $N$ using
    Lagrange interpolation with uniform sampling. The notion is still
    valid in the context of finite difference (FD) derivatives because
    the FD operators we use are equivalent to fitting a function 
    to a Lagrange interpolating polynomial at the sampled points and
    then setting the derivative of the function to the derivative of
    the interpolating polynomial at a given point $x$.} does not
  contaminate convergence. That is to say, $\Delta x$ and
  $f^{(N+1)}(\xi)$ are small enough such that
  $f^{(N+1)}(\xi) (\Delta x)^N$ drops by a nearly constant factor with
  each increase in $N$, but large enough so that Condition 1 is
  satisfied.
\end{packed_enumerate}
\end{tcolorbox}

So long as these three conditions are satisfied, we can expect {\it
  nearly exponential} convergence, keeping $\Delta x$ fixed and
increasing the finite difference order. As we will show, nearly
exponential convergence is observed for a range of $\Delta x$ even for
pathological functions, including the Runge function (Runge's
Phenomenon's namesake).

As a corollary, we can expect that functions whose derivatives
$f^{(N+1)}(\xi)$ are {\it bounded} within the finite difference
stencil, like $e^x$ or $\sin(x)$, will exhibit {\it nearly perfect}
exponential convergence. We use the adverb {\it nearly} because of the
{\it non-constant}, but {\it slowly-varying} $1/-4(1+N^{-1}$ factor
that multiplies the error term at each subsequent finite difference
order.

For example, consider evaluating the first derivative of the function 
\beq
f(x) = \sin\left(x+\frac{\pi}{6}\right)
\eeq
at $x=0$. Clearly, all derivatives $f^{(N+1)}(\xi)$ of this function will be
bounded, so that $|f^{(N+1)}(\xi)|\le 1$. Thus so long as we choose
$\Delta x$ so that truncation error dominates, we would expect the
error at a $N$th finite difference order to satisfy
\beq
E_N \propto \frac{C_{N-2}}{R} (\Delta x)^{N},
\eeq
where $4<R\le 5$. Thus we expect {\it nearly perfect} exponential
convergence in this case as we increase finite difference order in the
trunction error dominated regime.

Now let's take a look at whether the errors match our expectations.

\begin{figure}[h!]
\begin{center}
\includegraphics[angle=270,width=0.5\textwidth]{figures/sin__x_plus_pi_over_6__FD_rel_error__vs__delta_x__loglog.ps}
\caption{log-log plot of relative error 
  $E_{\rm rel}=(f'(0)_{\rm num} - f'(0)_{\rm exact})/f'(0)_{\rm exact}$ 
  of finite difference representation of the derivative of
  $f(x) = \sin\left(x+\frac{\pi}{6}\right)$, versus grid spacing
  $\Delta x$, at various finite difference orders.}
\label{fig1:sinxFDerror}
\end{center}
\end{figure}

Figure~\ref{fig1:sinxFDerror} plots the relative error versus 
$\Delta x$ for the finite difference derivative of this function. The first
condition listed above is violated at 12th order at 
$\Delta x < 10^{-1}$, the second condition is violated at 
$\Delta x\gtrsim 10^{0.5}$, and the third condition is never violated due
to the fact that the function's derivatives are bounded. Thus we would
expect nearly perfect exponential convergence in the range where
truncation error dominates---i.e., the range 
$10^{-1} < \Delta x \lesssim 10^{0.5}$. 

If indeed we are observing nearly perfect exponential convergence,
then the logarithmic difference between consecutive finite difference
orders $N$ and $N+2$ for any given $\Delta x$ in the
truncation-dominated region  $10^{-1} < \Delta x \lesssim 10^{0.5}$
should be {\it the same}, regardless of the chosen $N$.

\begin{figure}[h!]
\begin{center}
\includegraphics[angle=270,width=0.5\textwidth]{figures/sin__x_plus_pi_over_6__delta_FD_rel_error__vs__delta_x__loglog.ps}
\caption{Logarithmic factor by which the relative error drops at a
  given $\Delta x$ at consecutive finite difference orders, when
  computing the finite difference derivative of 
  $f(x) = \sin\left(x+\frac{\pi}{6}\right)$ at $x=0$.}
\label{fig2:deltasinxFDerror}
\end{center}
\end{figure}

Figure~\ref{fig2:deltasinxFDerror} demonstrates that indeed we observe
nearly perfect exponential convergence in this case for a range of
$\Delta x$. For example, at $\Delta x \approx 10^{-1}$,
$\Delta \log_{10} E_{\rm rel} \approx 2.6$ at all finite difference
orders, meaning that if we were to set $\Delta x$ to this value, we
can expect each incremental increase in finite difference order to
drop the relative error by a factor of $10^{2.6} \approx$ {\it 400}.

As another, more pathological example, let's take the derivative of
the function
\beq
f(x) = \frac{1}{x+4}
\eeq
at $x=0$. The derivative of this function is {\it not} bounded at
$x=0$, and grows in magnitude uncontrollably as $x\to -4$ or
$N\to\infty$. Thus we expect that the third condition will not be
well-satisfied in some range of $\Delta x$ due to Runge's Phenomenon,
though in ranges where it is approximately satisfied (and conditions
one and two hold), we should expect {\it nearly exponential}
convergence.

\vspace{1in}\begin{center} {\Large \it Continued on next page} \end{center}
\pagebreak 

\begin{figure}[h!]
\begin{center}
\includegraphics[angle=270,width=0.5\textwidth]{figures/one_over__x_plus_4__FD_rel_error__vs__delta_x__loglog.ps}
\caption{log-log plot of relative error 
  $E_{\rm rel}=(f'(0)_{\rm num} - f'(0)_{\rm exact})/f'(0)_{\rm exact}$ 
  of finite difference representation of the derivative of
  $f(x) = 1/(x+4)$, versus grid spacing
  $\Delta x$, at various finite difference orders.}
\label{fig3:1overxp4FDerror}
\end{center}
\end{figure}

Figure~\ref{fig3:1overxp4FDerror} confirms this expectation; it
appears that at $10^{-0.25} \lesssim \Delta x < 10^{0.5}$, we observe
Runge's phenomenon at some or all finite difference orders, causing
anti-convergence in the relative error with finite difference order at
these values of $\Delta x$.

%
%\pagebreak

However, in the range $10^{-1.2} \lesssim \Delta x < 10^{-0.75}$, we
apparently observe some degree of exponential convergence. Let's
analyze this more closely.

\begin{figure}[h!]
\begin{center}
\includegraphics[angle=270,width=0.5\textwidth]{figures/one_over__x_plus_4__delta_FD_rel_error__vs__delta_x__loglog.ps}
\caption{Logarithmic factor by which the relative error drops at a
  given $\Delta x$ at consecutive finite difference orders, when
  computing the finite difference derivative of 
  $f(x) = 1/(x+4)$ at $x=0$.}
\label{fig4:delta1overxp4FDerror}
\end{center}
\end{figure}

Figure~\ref{fig4:delta1overxp4FDerror} demonstrates that when we
choose to finite difference a function with monotonically increasing
derivatives $f^{(N+1)}(\xi)$, we still observe {\it
  nearly-exponential} convergence in some range of $\Delta x$. For
example, at $\Delta x = 10^{-1}$, we appear to be in the
truncation-error-dominated regime at all FD orders. 

However, the relative error at this gridspacing does drop by between
1.6 and 2.6 orders of magnitude as the finite difference order is
incremented from $N$ to $N+2$ for all FD orders attempted, and in the
usual range of $2^{\rm nd}$ to $8^{\rm th}$ order, the relative error
drops by  $2.3 \pm 0.3$ orders of magnitude at each $N$ to $N+2$
increment in finite difference order, with not-as-rapid convergence as
finite difference order is increased beyond this level.

We also performed this analysis on the Runge function itself:
$f(x)=1/(x^2+25)$ (Runge's Phenomenon's namesake function), and
observed the relative error drops by $2.35 \pm 0.25$ orders of
magnitude at each $N$ to $N+2$ increment in finite differencing order
in the usual range of $N$ adopted for our simulations (between $N=2$
and $N=8$).

\end{document}
