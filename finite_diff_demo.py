import time
from sympy import sympify, symbols
# Generate finite difference stencils
#from finite_difference_stencil_generator import *
# Define the reference metric ghatDD & rescaling quantities ReU and ReDD. xyzCart,dxdr,drdx used for ADM integrands.
from reference_metric import protected_varnames
# Define reference metric derived ("hatted") quantities:
from ref_metric__hatted_quantities import ghatUU,GammahatUDD
from NRPy_file_output import NRPy_file_output
from NRPy_functions import declarerank1, declarerank2, get_parameter_value
from parameters_compiletime import params_varname, params_value

def func_fdiff_demo_gf_define_alias(gfname):
    # FIXME: compatibility mode:
    return gfname.replace("D", "").replace("U", "").replace("2", "3").replace("1", "2").replace("0", "1").upper()


# Set precision to, e.g., double or long double
PRECISION = get_parameter_value("PRECISION", params_varname, params_value)
OUTDIR = get_parameter_value("Evol_scheme", params_varname, params_value)

#############################
#   FDIFF_DEMO PARAMETERS   #
#############################

fdiff_demo_evolved_variable_list = [ "f" ]
fdiff_demo_evolved_variable_list.sort()

fdiff_demo_aux_gridfunctions = []  # Put other gridfunctions here

# Write the file gridfunction_defines.h, which stores #define aliases to gridfunctions.
#  This makes NRPy-generated C codes easier to read and understand.
with open(OUTDIR+"/gridfunction_defines-NRPyGEN.h", "w") as output:
    output.write("#define NUM_EVOL_GFS "+str(len(fdiff_demo_evolved_variable_list))+"\n")
    for i in range(len(fdiff_demo_evolved_variable_list)):
        output.write("#define "+func_fdiff_demo_gf_define_alias(fdiff_demo_evolved_variable_list[i])+" "+str(i)+"\n")
    output.write("// AUXILIARY GRIDFUNCTIONS: (These could in principle re-use space stored for other gridfunction RHSs, though they do not.)\n")
    for i in range(len(fdiff_demo_aux_gridfunctions)):
        output.write("#define "+func_fdiff_demo_gf_define_alias(fdiff_demo_aux_gridfunctions[i])+" "+str(i)+"\n")

fdiff_demo_protected_variables = protected_varnames

##################################
#   DEFINE FDIFF_DEMO EQUATION   #
##################################
# Start timer, for benchmarking
starttimer_fdiff_demoEqs = time.time()
print("Generating C code for fdiff_demo equations...")

# Declare f as a proper SymPy symbol, so that expressions containing it are properly processed by SymPy
fdD  = [  sympify(0) for i in range(3)]; declarerank1(fdD,"fdD")
fdDD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(fdDD,"fdDD","sym12","DeclTrue")

rhs_vars = []

# First derivative in all three directions
first_deriv  = [sympify(0) for i in range(3)]
for i in range(3):
    first_deriv[i] = fdD[i]
    rhs_vars += [first_deriv[i],"first_deriv["+str(i)+"]"]

# Second derivatives in all six directions
second_deriv  = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        second_deriv[i][j] = fdDD[i][j]
        if i <= j:
            rhs_vars += [second_deriv[i][j],"second_deriv["+str(i)+"]["+str(j)+"]"]

# Append auxiliary (not-evolved) gridfunctions to end of list of fdiff_demo_gfs. Currently fdiff_demo_aux_gridfunctions is empty.
list_of_fdiff_demo_gfs = fdiff_demo_evolved_variable_list + fdiff_demo_aux_gridfunctions

NRPy_file_output(OUTDIR+"/NRPy_codegen/NRPy_fdiff_demo.h",func_fdiff_demo_gf_define_alias,list_of_fdiff_demo_gfs,
                 [], fdiff_demo_protected_variables, [], rhs_vars)

stoptimer_fdiff_demoEqs = time.time()
print("Completed finite diff demo code generation in \t\t"+str(round(stoptimer_fdiff_demoEqs-starttimer_fdiff_demoEqs,2))+" seconds")
