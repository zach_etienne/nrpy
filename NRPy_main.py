import sys
PYTHONVERSION3 = False
if sys.version_info[0]==2:
    if sys.version_info[1]<4:
        print("Sorry, NRPy won't work with Python < 2.4; sorting functs won't work. See https://docs.python.org/3/howto/sorting.html for details.")
        exit()
if sys.version_info[0]==3:
    PYTHONVERSION3 = True

# Read parameters from command line (if wish to overwrite definitions in parameters_compiletime.py)
from NRPy_functions import set_parameter, get_parameter_value
from parameters_compiletime import params_type, params_varname, params_value, \
    runtime_params_type, runtime_params_varname, FDUPWINDINPLACE

import time
from NRPy_functions import print_logo
print_logo()

# Choose either "double" or "long double"
PRECISION = "double"
set_parameter("char", "PRECISION", PRECISION, params_type, params_varname, params_value)

# Choose either "None", "256bit" (AVX/FMA), or "512bit" (AVX-512)
SIMD = "None"
set_parameter("char", "SIMD", SIMD, params_type, params_varname, params_value)
if SIMD != "None":
    if PRECISION != "double":
        print("Sorry, SIMD supports only double precision at the moment.")
        exit(1)
    if SIMD != "256bit" and SIMD != "512bit":
        print("Sorry, NRPy+ supports only 256-bit and 512-bit double precision SIMD compiler intrinsics at the moment.")
        exit(1)

if FDUPWINDINPLACE == "False":
    set_parameter("int", "UPWIND", +1, params_type, params_varname, params_value)
elif FDUPWINDINPLACE == "True":
    set_parameter("int", "UPWIND", -1, params_type, params_varname, params_value)


if(len(sys.argv) > 1):
    if len(sys.argv) != 6:
        print("Usage (defaults are in parentheses; only common parameters are steerable at command line; see parameters_compiletime.py for more complete list):")
        print("   python NRPy_main.py [Run Mode (Everything)] [Coord system (SinhSpherical)] [Evolution scheme (SENR)] [Initial data scheme (UIUC)] [FD order (4)]")
        exit()
    set_parameter("char", "NRPyRunMode",                str(sys.argv[1]),  params_type, params_varname, params_value)
    set_parameter("char", "CoordSystem",                str(sys.argv[2]),  params_type, params_varname, params_value)
    set_parameter("char", "Evol_scheme",                str(sys.argv[3]),  params_type, params_varname, params_value)
    set_parameter("char", "ID_scheme",                  str(sys.argv[4]),  params_type, params_varname, params_value)
    set_parameter("int",  "FDCENTERDERIVS_FDORDER", int(str(sys.argv[5])), params_type, params_varname, params_value)
#    FDSTENCILSIZE=FDCENTERDERIVS_FDORDER + 1
else:
    set_parameter("char", "NRPyRunMode",            "Everything",    params_type, params_varname, params_value)
    set_parameter("char", "CoordSystem",            "SinhSpherical", params_type, params_varname, params_value)
    set_parameter("char", "Evol_scheme",            "SENR",          params_type, params_varname, params_value)
    set_parameter("char", "ID_scheme",              "UIUC",          params_type, params_varname, params_value)
    set_parameter("int",  "FDCENTERDERIVS_FDORDER", 4,               params_type, params_varname, params_value)


# Output parameters
print("#########################################################")
print("\033[92m\033[1m NRPyRunMode -={ "+get_parameter_value("NRPyRunMode", params_varname, params_value)+" }=-\n"+
      " CoordSystem -={ "+get_parameter_value("CoordSystem", params_varname, params_value)+" }=-\n"+
      " Evol scheme -={ "+get_parameter_value("Evol_scheme", params_varname, params_value)+" }=-\n"+
      " ID scheme   -={ "+get_parameter_value("ID_scheme", params_varname, params_value)+" }=-\n"+
      " FD order    -={ "+str(get_parameter_value("FDCENTERDERIVS_FDORDER", params_varname, params_value))+" }=-\033[0m")
print("#########################################################")

starttimer = time.time()

RunMode = get_parameter_value("NRPyRunMode", params_varname, params_value)

if get_parameter_value("Evol_scheme", params_varname, params_value) == "SENR":
    # BSSN_RHSs depends on reference_metric.py and ref_metric__hatted_quantities.py.
    # reference_metric.py and ref_metric__hatted_quantities.py depend on file_output.py
    # file_output.py depends on finite_difference_stencil_generator.py, so that finite difference derivatives can be automatically computed
    # Thus when BSSN_RHSs is called below, the following happens:
    # 1) Finite difference stencils are computed, populating arrays like coeffs_1stderiv
    # 2) The reference metric is defined
    # 3) Hatted quantities depending on the reference metric are defined
    # 4) Algebraic expressions BSSN RHS quantities are generated
    # 5a) if RHSonly/Riccionly or Everything: BSSN RHS/Ricci quantities are output to C code
    # 5b) if Diagnostics or Everything: BSSN Diagnostic quantities are output to C code
    # 5c) if IDonly or Everything: BSSN Initial data quantities are output to C code
    if RunMode == "Everything" or RunMode == "RHSonly" or RunMode == "Riccionly"  or RunMode == "Diagnostics" or RunMode == "IDonly":
        from BSSN_RHSs import *
    if RunMode == "Everything" or RunMode == "Diagnostics":
        # BSSN_Diagnostics depends on BSSN_RHS quantities being evaluated first.
        from BSSN_Diagnostics import *
    if RunMode == "Everything" or RunMode == "IDonly":
        # BSSN_initial_data depends on BSSN runtime parameters being set (e.g., "CFEvolution"), which are declared at top of BSSN_RHS
        from BSSN_initial_data import *
elif get_parameter_value("Evol_scheme", params_varname, params_value) == "finite_diff_demo":
    if RunMode == "Everything":
        from finite_diff_demo import *
elif get_parameter_value("Evol_scheme", params_varname, params_value) == "scalarwave":
    if RunMode == "Everything" or RunMode == "RHSonly":
        from scalarwave_RHSs import *
    if RunMode == "Everything" or RunMode == "IDonly":
        from scalarwave_initial_data import *
elif get_parameter_value("Evol_scheme", params_varname, params_value) == "ETK_thorns":
    if RunMode == "FishboneMoncriefID":
        from FishboneMoncrief_ETK_initial_data import *
    else:
        print("IllinoisGRMHD_ID_ETK_thorns RunMode = "+RunMode+" not recognized!")
        exit(1)
else:
    print("Evol_scheme = "+str(get_parameter_value("Evol_scheme", params_varname, params_value))+" not recognized!")
    exit(1)

# Automatically generate C code for setting runtime+grid+RHS+etc parameters
if RunMode == "Everything" or RunMode == "RHSonly":
    from autogen_param_struct_code import *

stoptimer  = time.time()
print("NRPy+ completed all tasks in \t\t\t\t" + str(round(stoptimer-starttimer,2)) + " seconds")
