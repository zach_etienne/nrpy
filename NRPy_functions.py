from sympy import sympify, symbols, count_ops, ccode
import re
from mathematica_output import *
from parameters_compiletime import params_type, params_varname, params_value



# --={ NRPY FUNCTIONS }=--

# NRPy
def runtime_parameter(input_type, input_varname, runtime_params_type, runtime_params_varname):
    runtime_params_type.extend([input_type])
    runtime_params_varname.extend([input_varname])

def set_parameter(input_type, input_varname, input_value, params_type, params_varname, params_value):
    params_type.extend([input_type])
    params_varname.extend([input_varname])
    params_value.extend([input_value])

def get_parameter_value(input_str, params_varname, params_value):
    return params_value[params_varname.index(input_str)]

def root_directory(params_varname, params_valuel):
    return get_parameter_value("Evol_scheme", params_varname, params_value)

# *** Functions for Declaring Variables ***
# Functions having to do with setting variable names.
#  The correct behavior is to set the variable name,
#  and then if the variable is an indexed quantity
#  with symmetry between the indices, call the
#  appropriate applysym_... function above to ensure
#  that, e.g., gDD12 and gDD21 are not both defined,
#  adopting gDD12 for both indices.
# *** Regarding symmetries ***
# These functions ensure that for tensors symmetric
#  about indices i and j, where i is to the left of j,
#  that the variable defined always chooses the i<=j
#  component as convention. For example for g_{ij},
#  where g_{ij}=g_{ji}, we set g_{21} = g_{12} = gDD12
#  I.e., the two components of the tensor map to the
#  *same variable*. This makes it natural for a
#  computer algebra system (like SymPy) to handle,
#  and the C code generated will adopt this convention
def declarescalar( namee, nameestring ):
    namee = nameestring
    namee = symbols(nameestring, real=True)
    return
def declarerank1( obj, objstring ):
    for i in range(3):
        obj[i] = sympify(objstring + str(i))
    return
def declarerank2( obj, objstring, Sym, declare ):
    for i in range(3):
        for j in range(3):
            if(declare == "DeclTrue"):
                obj[i][j] = sympify(objstring + str(i) + str(j))
            if (Sym == "sym12"):
                if (j < i):
                    # j<i in g_{ij} would indicate, e.g., g_{21}.
                    #  By convention defined above, we must set
                    #  g_{21} = g_{12}:
                    obj[i][j] = obj[j][i]
    return
def declarerank3( obj, objstring, Sym, declare ):
    for i in range(3):
        for j in range(3):
            for k in range(3):
                if(declare == "DeclTrue"):
                    obj[i][j][k] = sympify(objstring + str(i) + str(j) + str(k))
                if (Sym == "sym12"):
                    if (j < i):
                        obj[i][j][k] = obj[j][i][k]
                if (Sym == "sym23"):
                    if (k < j):
                        obj[i][j][k] = obj[i][k][j]
    return

def declarerank4( obj, objstring, Sym, declare ):
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    if(declare == "DeclTrue"):
                        obj[i][j][k][l] = sympify(objstring + str(i) + str(j) + str(k) + str(l))
                    if (Sym == "sym12" or Sym == "sym12_sym34"):
                        if(j < i):
                            obj[i][j][k][l] = obj[j][i][k][l]
                    if (Sym == "sym23"):
                        if(k < j):
                            obj[i][j][k][l] = obj[i][k][j][l]
                    if (Sym == "sym34" or Sym == "sym12_sym34"):
                        if(l < k):
                            obj[i][j][k][l] = obj[i][j][l][k]
    return

# super fast 'uniq' function:
# f8() function from https://www.peterbe.com/plog/uniqifiers-benchmark
def superfast_uniq(seq): # Author: Dave Kirby
    # Order preserving
    seen = set()
    return [x for x in seq if x not in seen and not seen.add(x)]

def generic_matrix_inverter3x3(in2Darray, outINV):
    outDET = -in2Darray[0][2]*in2Darray[1][1]*in2Darray[2][0] + in2Darray[0][1]*in2Darray[1][2]*in2Darray[2][0] + \
              in2Darray[0][2]*in2Darray[1][0]*in2Darray[2][1] - in2Darray[0][0]*in2Darray[1][2]*in2Darray[2][1] - \
              in2Darray[0][1]*in2Darray[1][0]*in2Darray[2][2] + in2Darray[0][0]*in2Darray[1][1]*in2Darray[2][2]

    outINV[0][0] = -in2Darray[1][2]*in2Darray[2][1] + in2Darray[1][1]*in2Darray[2][2]
    outINV[0][1] =  in2Darray[0][2]*in2Darray[2][1] - in2Darray[0][1]*in2Darray[2][2]
    outINV[0][2] = -in2Darray[0][2]*in2Darray[1][1] + in2Darray[0][1]*in2Darray[1][2]
    outINV[1][0] =  in2Darray[1][2]*in2Darray[2][0] - in2Darray[1][0]*in2Darray[2][2]
    outINV[1][1] = -in2Darray[0][2]*in2Darray[2][0] + in2Darray[0][0]*in2Darray[2][2]
    outINV[1][2] =  in2Darray[0][2]*in2Darray[1][0] - in2Darray[0][0]*in2Darray[1][2]
    outINV[2][0] = -in2Darray[1][1]*in2Darray[2][0] + in2Darray[1][0]*in2Darray[2][1]
    outINV[2][1] =  in2Darray[0][1]*in2Darray[2][0] - in2Darray[0][0]*in2Darray[2][1]
    outINV[2][2] = -in2Darray[0][1]*in2Darray[1][0] + in2Darray[0][0]*in2Darray[1][1]

    for i in range(3):
        for j in range(3):
            outINV[i][j] /= outDET

    return outDET

def symm_matrix_inverter3x3(in2Darray, outINV):
    # It is far more efficient to write out the matrix determinant and inverse by hand instead of using built-in functions,
    #   since the matrix is symmetric.
    outDET = -in2Darray[0][2]**2*in2Darray[1][1] + 2*in2Darray[0][1]*in2Darray[0][2]*in2Darray[1][2] - \
                in2Darray[0][0]*in2Darray[1][2]**2 - in2Darray[0][1]**2*in2Darray[2][2] + \
                in2Darray[0][0]*in2Darray[1][1]*in2Darray[2][2]

    #gPhysDET = simplify(gPhysDET)
    # First fill in the upper-triangle of the gPhysINV matrix...
    outINV[0][0] = (-in2Darray[1][2]**2              + in2Darray[1][1]*in2Darray[2][2])/outDET
    outINV[0][1] = (in2Darray[0][2]*in2Darray[1][2]  - in2Darray[0][1]*in2Darray[2][2])/outDET
    outINV[0][2] = (-in2Darray[0][2]*in2Darray[1][1] + in2Darray[0][1]*in2Darray[1][2])/outDET
    outINV[1][1] = (-in2Darray[0][2]**2              + in2Darray[0][0]*in2Darray[2][2])/outDET
    outINV[1][2] = (in2Darray[0][1]*in2Darray[0][2]  - in2Darray[0][0]*in2Darray[1][2])/outDET
    outINV[2][2] = (-in2Darray[0][1]**2              + in2Darray[0][0]*in2Darray[1][1])/outDET
    outINV[1][0] = outINV[0][1]
    outINV[2][0] = outINV[0][2]
    outINV[2][1] = outINV[1][2]
    return outDET


# *** Functions for code output
def nrpy_ccode_parser(string):
    PRECISION = get_parameter_value("PRECISION", params_varname, params_value)

    # string2 = re.sub('pow\(([^,]+),[ ]*2\)', '((\\1)*(\\1))', string); string = string2
    # string2 = re.sub('pow\(([^,]+),[ ]*3\)', '((\\1)*(\\1)*(\\1))', string); string = string2
    # string2 = re.sub('pow\(([^,]+),[ ]*4\)', '((\\1)*(\\1)*(\\1)*(\\1))', string); string = string2
    # string2 = re.sub('pow\(([^,]+),[ ]*5\)', '((\\1)*(\\1)*(\\1)*(\\1)*(\\1))', string); string = string2
    #
    # string2 = re.sub('pow\(([^,]+),[ ]*-2\)', '(1/((\\1)*(\\1)))', string); string = string2
    # string2 = re.sub('pow\(([^,]+),[ ]*-3\)', '(1/((\\1)*(\\1)*(\\1)))', string); string = string2
    # string2 = re.sub('pow\(([^,]+),[ ]*-4\)', '(1/((\\1)*(\\1)*(\\1)*(\\1)))', string); string = string2
    # string2 = re.sub('pow\(([^,]+),[ ]*-5\)', '(1/((\\1)*(\\1)*(\\1)*(\\1)*(\\1)))', string); string = string2
    # string2 = re.sub('pow\(([^,]+),[ ]*-6\)', '(1/((\\1)*(\\1)*(\\1)*(\\1)*(\\1)*(\\1)))', string); string = string2

    if PRECISION=="long double":
        string2 = re.sub('pow\(', 'powl(', string); string = string2
        string2 = re.sub('sqrt\(', 'sqrtl(', string); string = string2
        string2 = re.sub('sin\(', 'sinl(', string); string = string2
        string2 = re.sub('cos\(', 'cosl(', string); string = string2
        string2 = re.sub('sinh\(', 'sinhl(', string); string = string2
        string2 = re.sub('cosh\(', 'coshl(', string); string = string2
        string2 = re.sub('tan\(', 'tanl(', string); string = string2
        string2 = re.sub('exp\(', 'expl(', string); string = string2
        string2 = re.sub('log\(', 'logl(', string); string = string2

    # FIXME: not robust:
    # string2 = re.sub('1/2', ' (1.0/2.0)', string); string = string2
    # string2 = re.sub('3/2', ' (3.0/2.0)', string); string = string2
    # string2 = re.sub('5/2', ' (5.0/2.0)', string); string = string2

    # Fix indexed quantities to be C-arrays
    # string2 = re.sub('([DU])([0-9])([0-9])([0-9])([0-9])', '\\1[\\2][\\3][\\4][\\5]', string); string = string2
    # string2 = re.sub('([DU])([0-9])([0-9])([0-9])', '\\1[\\2][\\3][\\4]', string); string = string2
    # string2 = re.sub('([DU])([0-9])([0-9])', '\\1[\\2][\\3]', string); string = string2
    # string2 = re.sub('([DU])([0-9])', '\\1[\\2]', string); string = string2

    # Fix long-double fractions, so vectorizer doesn't complain
    if PRECISION!="long double":
        string2 = re.sub('([0-9.]+)L/([0-9.]+)L', '(\\1 / \\2)', string); string = string2

    return string


def apply_function__readonly_variables(_function_,varlist, arg1, arg2):
    loop3 = range(3)
    for variable in varlist:
        if not isinstance(variable, list):
            # rank-0 object
            _function_(variable, arg1, arg2)
        elif isinstance(variable, list) and not isinstance(variable[0], list):
            # rank-1 object
            # Employ list comprehension to simplify. Much cleaner than for() loops!
            [_function_(variable[i], arg1, arg2) for i in loop3]
        elif isinstance(variable[0], list) and not isinstance(variable[0][0], list):
            # rank-2 object
            [_function_(variable[i][j], arg1, arg2) for i in loop3 for j in loop3]
        elif isinstance(variable[0][0], list) and not isinstance(variable[0][0][0], list):
            # rank-3 object
            [_function_(variable[i][j][k], arg1, arg2) for i in loop3 for j in loop3 for k in loop3]
        elif isinstance(variable[0][0][0], list) and not isinstance(variable[0][0][0][0], list):
            # rank-4 object
            loop3 = range(3)
            [_function_(variable[i][j][k][l], arg1,arg2) for i in loop3 for j in loop3 for k in loop3 for l in loop3]
        else:
            print("ERROR in unset_variables__store_list_new(): Rank currently unsupported!")
            exit()
    return

def shorten_varnames(varname,oldname,newname):
    for i in range(len(newname)):
        varname = varname.subs(oldname[i],newname[i])
    return varname

def apply_function__writeto_listvars(_function_,varlist, arg1, arg2):
    for variable in varlist:
        if not isinstance(variable, list):
            print("Error: This function does not support variables that are not lists. I.e., if variable a cannot be indexed as a[i], this function won't work.\n")
            exit()
        elif not isinstance(variable[0], list):
            # rank-1 object
            for i in range(3):
                variable[i] = _function_(variable[i], arg1, arg2)
        elif isinstance(variable[0], list) and not isinstance(variable[0][0], list):
            # rank-2 object
            for i in range(3):
                for j in range(3):
                    variable[i][j] =  _function_(variable[i][j], arg1, arg2)
        elif isinstance(variable[0][0], list) and not isinstance(variable[0][0][0], list):
            # rank-3 object
            for i in range(3):
                for j in range(3):
                    for k in range(3):
                        variable[i][j][k] = _function_(variable[i][j][k], arg1, arg2)
        elif isinstance(variable[0][0][0], list) and not isinstance(variable[0][0][0][0], list):
            # rank-4 object
            for i in range(3):
                for j in range(3):
                    for k in range(3):
                        for l in range(3):
                            variable[i][j][k][l] = _function_(variable[i][j][k][l], arg1, arg2)
        else:
            print("ERROR in unset_variables__store_list_new(): Rank currently unsupported!")
            exit()
    return

def compute_cost(expression):
    return count_ops(expression)
    # Below gives an example of how to override the count_ops function. There's no reason to use the below code--doesn't result in significant perf gain.
    # The below measures are rough at best. The C compiler will do whatever she wants in the end...
    # http://nicolas.limare.net/pro/notes/2014/12/16_math_speed/
    # POW = Symbol('POW')
    # ADD = Symbol('ADD')
    # SUB = Symbol('SUB')
    # COS = Symbol('COS')
    # SIN = Symbol('SIN')
    # SINH = Symbol('SINH')
    # COSH = Symbol('COSH')
    # TANH = Symbol('TANH')
    # DIV = Symbol('DIV')
    # LOG = Symbol('LOG')
    # EXP = Symbol('EXP')
    # MUL = Symbol('MUL')
    # NEG = Symbol('NEG')
    # TAN = Symbol('TAN')
    # return count_ops(expression, visual=True).subs(POW, 178/10.7).subs(ADD, 1).subs(SUB, 1).subs(COS,178 / 9.235).subs(SIN,178 / 9.235).subs(DIV, 178./80.).subs(MUL, 1).subs(NEG,1).subs(TAN,178 / 7.118).subs(LOG,16.3).subs(EXP,177/63.8).subs(SINH,177/63.8).subs(COSH,177/63.8).subs(TANH,177/63.8)
def read_sympy_expr__return_ccode_string(sympy_expr):
    PRECISION = get_parameter_value("PRECISION", params_varname, params_value)

    blah = symbols('blah')
    string = str(ccode(sympy_expr,blah)).replace("blah = ","").replace(";","")
    if(PRECISION!="long double"):
        string2 = re.sub('([0-9.]+)L/([0-9.]+)L', '(\\1 / \\2)', string); string = string2
    return string

def base_gf_name_and_type(inputstring,outputstring):
    outputstring[0] = re.sub('dupD([0-9]*)[0-9]$', '\\1', str(inputstring))
    if outputstring[0]!=inputstring:
        return "Upwind"
    outputstring[0] = re.sub('dD([0-9]*)[0-9]$', '\\1', str(inputstring))
    if outputstring[0] != inputstring:
        return "First"
    outputstring[0] = re.sub('dDKO([0-9]*)[0-9]$', '\\1', str(inputstring))
    if outputstring[0]!=inputstring:
        return "KreissOliger"
    outputstring[0] = re.sub('dDD([0-9]*)[0-9][0-9]$', '\\1', str(inputstring))
    if outputstring[0]!=inputstring:
        return "Second"
    outputstring[0]=inputstring
    return "Zeroth"

def get_full_list_evol_gfs(already_defined_noevol_variables,list_of_defs, list_of_expressions, output_evol_gfs):
    for i in range(len(already_defined_noevol_variables)):
        basegfname = [""]
        dummytype = base_gf_name_and_type(already_defined_noevol_variables, basegfname)
        output_evol_gfs.extend(basegfname)
    superfast_uniq(output_evol_gfs)
    output_evol_gfs.sort()



def print_diff(var,mathematica_varname):
    string = mathematica_code(var)
    string2 = re.sub('([DU])([0-9])([0-9])([0-9])([0-9])', '\\1[\\2][\\3][\\4][\\5]', string); string = string2
    string2 = re.sub('([DU])([0-9])([0-9])([0-9])', '\\1[\\2][\\3][\\4]', string); string = string2
    string2 = re.sub('([DU])([0-9])([0-9])', '\\1[\\2][\\3]', string); string = string2
    string2 = re.sub('([DU])([0-9])', '\\1[\\2]', string); string = string2
    string2 = re.sub('tmpgammabar', 'gammabar', string); string = string2
    string2 = re.sub('tmpRbar', 'Rbar', string); string = string2

    # FIXME: Assumes SymTP coords; WARNING: substitutions may be different!
    string2 = re.sub('M_PI', 'Pi', string); string = string2

    # FIXME: Assumes log radial spherical polar coords; WARNING: substitutions may be different!
    # string2 = re.sub('tmpPC0', '(DRGF^(2*(Nx1)*x1 - 1))', string); string = string2
    # string2 = re.sub('tmpPC1', '(DRGF^((Nx1)*x1 - 1/2))', string); string = string2
    # string2 = re.sub('tmpPC2', '(Log[DRGF])', string); string = string2
    # string2 = re.sub('tmpPC3', '(Sin[x2])', string); string = string2
    # string2 = re.sub('tmpPC4', '(Cos[x2])', string); string = string2
    # string2 = re.sub('tmpPC5', '(DRGF^(-(Nx1)*x1 + 1/2))', string); string = string2
    # string2 = re.sub('tmpPC6', '(DRGF^(4*(Nx1)*x1 - 2))', string); string = string2
    # string2 = re.sub('tmpPC7', '(DRGF^(-2*(Nx1)*x1 + 1))', string); string = string2
    # string2 = re.sub('tmpPC8', '(Sin[2*x2])', string); string = string2
    # string2 = re.sub('tmpPC9', '(Cos[2*x2])', string); string = string2


    # FIXME: Assumes spherical polar coords; WARNING: substitutions may be different!
    # string2 = re.sub('tmpPC0', 'Sin[x2]', string); string = string2
    # string2 = re.sub('tmpPC1', 'Cos[x2]', string); string = string2
    # string2 = re.sub('tmpPC2', 'Sin[2*x2]', string); string = string2
    # string2 = re.sub('tmpPC3', 'Cos[2*x2]', string); string = string2


    print("FullSimplify[" + string + "-" + mathematica_varname + "]")

def print_logo():
    print("ooooo      ooo ooooooooo.   ooooooooo.                 88\n"
          "`888b.     `8' `888   `Y88. `888   `Y88.             888888  \n"
          " 8 `88b.    8   888   .d88'  888   .d88' oooo    ooo   88\n"
          " 8   `88b.  8   888ooo88P'   888ooo88P'   `88.  .8'\n"
          " 8     `88b.8   888`88b.     888           `88..8'\n"
          " 8       `888   888  `88b.   888            `888'\n"
          "o8o        `8  o888o  o888o o888o            .8'\n"
          "                                         .o..P'\n"
          "  NRPy+: Python-based Code Generation    `Y8P'\n"
          "   for Numerical Relativity... and Beyond!\n")
