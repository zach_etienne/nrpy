REAL L2_Norm_Over_Entire_Grid(const int gf, REAL *in_gfs, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

  REAL L2 = 0.0;

  //#pragma omp parallel for reduction(+:L2)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	      const REAL val = in_gfs[IDX4(gf,i, j, k)];
	      L2 += val * val;
	    }
    }
  }

  return sqrt(L2);
}

REAL L2_Norm_Over_Chunk_of_Grid(const int ijkmin[3], const int ijkmax[3], REAL *in_gf, REAL *in_gf_volume, paramstruct params, REAL *outputvolume) {
#include "../parameters_readin-NRPyGEN.h"

  REAL L2 = 0.0;
  *outputvolume = 0.0;

  //#pragma omp parallel for reduction(+:L2)
  for(int k = ijkmin[2]; k < ijkmax[2]; k++) {
    for(int j = ijkmin[1]; j < ijkmax[1]; j++) {
      for(int i = ijkmin[0]; i < ijkmax[0]; i++) {
        const REAL val = in_gf[IDX3(i, j, k)];
        L2 += val * val;
        *outputvolume += in_gf_volume[IDX3(i, j, k)];
      }
    }
  }

  return sqrt(L2);
}

REAL RMS_Norm_Over_Entire_Grid(const int gf, REAL *in_gfs, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

  // Number of grid points, not counting ghost zones
  const int Npts_RMS = Nx1 * Nx2 * Nx3;
  const REAL L2 = L2_Norm_Over_Entire_Grid(gf, in_gfs, params);

  return sqrt(L2 * L2 / Npts_RMS);
}

REAL MeanLog_Norm_Over_Entire_Grid(const int gf, REAL *in_gfs, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

  // Number of grid points, not counting ghost zones
  const int Npts_meanlog = Nx1 * Nx2 * Nx3;

  REAL meanlog = 0.0;

#pragma omp parallel for reduction(+: meanlog)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	      const REAL val = in_gfs[IDX4(gf,i, j, k)];
	      meanlog += log10(val);
	    }
    }
  }

#ifdef USE_LONG_DOUBLE
  const REAL result = powl(10.0L, meanlog / (REAL)Npts_meanlog);
#else
  const REAL result = pow(10.0, meanlog / (REAL)Npts_meanlog);
#endif

  return result;
}

REAL Max_Over_Entire_Grid(const int gf, REAL *in_gfs, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

  REAL max = 0.0;

  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	      const REAL val = fabs(in_gfs[IDX4(gf,i, j, k)]);

	      if(val > max) {
          max = val;
	      }
	    }
    }
  }

  return max;
}
