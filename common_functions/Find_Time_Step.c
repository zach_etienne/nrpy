// Find the bounds of the physical grid (y1, y2, y3) and the CFL time step dt
REAL Find_Time_Step(REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, REAL y_min[DIM], REAL y_max[DIM], paramstruct params)
{
#include "parameters_readin-NRPyGEN.h" // Located in ../common_functions/

  // Minimum proper conformal distance
  REAL ds2_min = DBL_MAX;

  for(int dirn = 0; dirn < DIM; dirn++)
    {
      y_min[dirn] = DBL_MAX;
      y_max[dirn] = 0.0;
    }

  // Loop over all points (excluding ghost zones)
  for(int kk = NGHOSTS; kk < Npts3 - NGHOSTS; kk++)
    for(int jj = NGHOSTS; jj < Npts2 - NGHOSTS; jj++)
      for(int ii = NGHOSTS; ii < Npts1 - NGHOSTS; ii++) {
        const REAL x1 = x1G[ii];
        const REAL x2 = x2G[jj];
        const REAL x3 = x3G[kk];

        // Current grid point
        const int idx = IDX3(ii, jj, kk);

#include "reference_metric/NRPy_codegen/gammahatDD.h"
        const REAL ds2[3] = { gammahatDD00 * del[0] * del[0],
                              gammahatDD11 * del[1] * del[1],
                              gammahatDD22 * del[2] * del[2] };

        // Calculate proper conformal distance in each coordinate direction
        for(int dirn = 0; dirn < DIM; dirn++)
          {
            // Find minimum proper distance
            if(ds2[dirn] < ds2_min) { ds2_min = ds2[dirn]; }

            // Find coordinate bounds
            if(yy[IDX4pt(dirn,idx)] < y_min[dirn]) { y_min[dirn] = yy[IDX4pt(dirn,idx)]; }
            if(yy[IDX4pt(dirn,idx)] > y_max[dirn]) { y_max[dirn] = yy[IDX4pt(dirn,idx)]; }
          }
      } // END for(ii,jj,kk)

#ifdef USE_LONG_DOUBLE
  return sqrtl(ds2_min);
#else
  return sqrt(ds2_min);
#endif
}
