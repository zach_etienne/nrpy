// Spherical coordinates (r, th, ph)

// A mod function that handles negative numbers as expected
// e.g., (-1) % 8 == -1, but MOD(-1, 8) == 7
#define MOD(a, b) ( ( ( (a) % (b) ) + (b) ) % (b) )
#define K_PLUS_PI(k) (NGHOSTS + MOD((k) + Npts3 / 2 - 2 * NGHOSTS, Npts3 - 2 * NGHOSTS)) // ph -> pi + ph MOD 2 pi

void Apply_out_sym_bcs_Spherical(const int gf, const REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

  // Outer BC on r
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
#if defined OUTER_BOUNDARY_EXACT
        // Exact outer boundary conditions
        REAL soln[NUM_GFS];
        Exact_soln(t, x1G[i], x2G[j], x3G[k], soln); // Use exact outer boundary condition
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
#elif defined OUTER_BOUNDARY_LIN_EXTRAP
        // Linear extrapolation outer boundary conditions
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
#elif defined OUTER_BOUNDARY_QUAD_EXTRAP
        // Linear extrapolation outer boundary conditions
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
#endif
      }
    }
  }

  // Vector and tensor parity conditions. See Table 1 in Baumgarte et al (2012) arXiv:1211.6632.
  int parity_center = 1;
  int parity_axis = 1;

#ifdef BSSN_EVOLUTION
  if(gf == VET1 || gf == LAMB1 || gf == BET1 || gf == H23 || gf == A23) {
    // Vector component r or tensor component th-ph
    parity_center = -1;
  }
  else if(gf == VET2 || gf == LAMB2 || gf == BET2 || gf == H13 || gf == A13) {
    // Vector component th or tensor component r-ph
    parity_axis = -1;
  }
  else if(gf == VET3 || gf == LAMB3 || gf == BET3 || gf == H12 || gf == A12) {
    // Vector component ph or tensor component r-th
    parity_center = -1;
    parity_axis = -1;
  }
#endif

  // Apply r => -r parity condition, near r = 0
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      const int pi_minus_j = Npts2 - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
        // Points mirrored across r = 0
        const int i_lower_ghost = NGHOSTS - 1 - i;
        const int i_lower_phys = NGHOSTS + i;

        // f(-r, th, ph) = f(+r, pi - th, ph + pi)
        in_gfs[IDX4(gf, i_lower_ghost, j, k)] = parity_center * in_gfs[IDX4(gf, i_lower_phys, pi_minus_j, k_plus_pi)];
      }
    }
  }

  // Apply BC in theta direction
  for(int k = 0; k < Nx3 / 2; k++) {
    // Points mirrored across the symmetry axis
    const int k_phys = NGHOSTS + k;
    const int k_plus_pi = K_PLUS_PI(k_phys);

    for(int j = 0; j < NGHOSTS; j++) {
      // Points mirrored across th = 0
      const int j_lower_ghost = NGHOSTS - 1 - j;
      const int j_lower_phys = NGHOSTS + j;

      // Points mirrored across th = pi
      const int j_upper_ghost = Npts2 - NGHOSTS + j;
      const int j_upper_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = 0; i < Npts1; i++) {
        // f(r, -th, ph) = f(r, +th, ph + pi)
        in_gfs[IDX4(gf, i, j_lower_ghost, k_phys)] = parity_axis * in_gfs[IDX4(gf, i, j_lower_phys, k_plus_pi)];

        // f(r, -th, pi + ph) = f(r, +th, 2 pi + ph)
        in_gfs[IDX4(gf, i, j_lower_ghost, k_plus_pi)] = parity_axis * in_gfs[IDX4(gf, i, j_lower_phys, k_phys)];

        // f(r, pi + th, ph) = f(r, pi - th, pi + ph)
        in_gfs[IDX4(gf, i, j_upper_ghost, k_phys)] = parity_axis * in_gfs[IDX4(gf, i, j_upper_phys, k_plus_pi)];

        // f(r, pi + th, pi + ph) = f(r, pi - th, 2 pi + ph)
        in_gfs[IDX4(gf, i, j_upper_ghost, k_plus_pi)] = parity_axis * in_gfs[IDX4(gf, i, j_upper_phys, k_phys)];
      }
    }
  }

  // Periodic condition on ph
  for(int k = 0; k < NGHOSTS; k++) {
    // ph -> 2 * pi + ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = 0; j < Npts2; j++) {
      for(int i = 0; i < Npts1; i++) {
        // f(r, th, -ph) = f(r, th, 2 pi - ph)
        in_gfs[IDX4(gf, i, j, k)] = in_gfs[IDX4(gf, i, j, k_plus_2pi)];
      }
    }
  }

  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> 2 * pi + ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = 0; j < Npts2; j++) {
      for(int i = 0; i < Npts1; i++) {
        // f(r, th, 2 pi + ph) = f(r, th, ph)
        in_gfs[IDX4(gf, i, j, k)] = in_gfs[IDX4(gf, i, j, k_plus_2pi)];
      }
    }
  }

  return;
}

#if defined OUTER_BOUNDARY_SOMMERFELD
// Sommerfeld outgoing wave condition, copied from the Baumgarte et al code fill_outerboundary() in gridfunction.h
void Sommerfeld_Outer_BC_Spherical(const int gf, REAL *xx, REAL *yy, REAL *func_old, REAL *func, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

  const REAL background = (gf == ALPHA) ? 1.0 : 0.0;
  //const REAL wave_speed = (gf == ALPHA) ? sqrt(2.0) : 1.0;
  const REAL wave_speed = 1.0;

  int fall_off = 1;

  if(gf == VET1 || gf == VET2 || gf == VET3 || gf == LAMB1 || gf == LAMB2 || gf == LAMB3 || gf == BET1 || gf == BET2 || gf == BET3) {
    fall_off = 2;
  }

  for(int k = 0; k < Npts3; k++) {
    for(int j = 0; j < Npts2; j++) {
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        const REAL Delta_r = fabs(yy[IDX4(0,i, j, k)] - yy[IDX4(0,i - 1, j, k)]);
        const REAL Courant = wave_speed * dt / Delta_r;
        const REAL r0 = yy[IDX4(0,i - 2, j, k)];
        const REAL r1 = yy[IDX4(0,i - 1, j, k)];
        const REAL r2 = yy[IDX4(0,i, j, k)];
        const REAL r_last = Courant * r1 + (1.0 - Courant) * r2;

        REAL factor = r_last / r2;

        if(fall_off == 2) {
          factor *= factor;
        }
        //else if(fall_off == 3) {
        //  factor *= factor * factor;
        //}

        const REAL P0 = func_old[IDX4(gf, i - 2, j, k)];
        const REAL P1 = func_old[IDX4(gf, i - 1, j, k)];
        const REAL P2 = func_old[IDX4(gf, i, j, k)];
        const REAL P01 = ((r_last - r1) * P0 + (r0 - r_last) * P1) / (r0 - r1);
        const REAL P12 = ((r_last - r2) * P1 + (r1 - r_last) * P2) / (r1 - r2);
        const REAL P012 = ((r_last - r2) * P01 + (r0 - r_last) * P12) / (r0 - r2);
        func[IDX4(gf, i, j, k)] = factor * (P012 - background) + background;
      }
    }
  }

  return;
}
#endif // OUTER_BOUNDARY_SOMMERFELD

#undef MOD
#undef K_PLUS_PI
