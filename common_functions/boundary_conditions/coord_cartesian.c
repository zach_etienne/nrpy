// Cartesian coordinates (x, y, z)

// Apply boundary conditions
void Apply_out_sym_bcs_Cartesian(const int gf, const REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, paramstruct params) {
#include "../parameters_readin-NRPyGEN.h"

#if defined OUTER_BOUNDARY_EXACT
  // Set upper and lower x=constant faces of the cube
  for(int k = 0; k < Npts3; k++) for(int j = 0; j < Npts2; j++) {
      for(int i = 0; i < NGHOSTS; i++) { /* lower x-face */
        REAL soln[2];Exact_soln(t, x1G[i], x2G[j], x3G[k], soln); // u = soln[0], v = soln[1]
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
      }
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) { /* upper x-face */
        REAL soln[2];Exact_soln(t, x1G[i], x2G[j], x3G[k], soln); // u = soln[0], v = soln[1]
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
      }
    }

  /* Set upper and lower y=constant faces of the cube.
   *   Note we've already updated the upper and lower
   *   x=constant faces, so do not update them again. */
  for(int k = 0; k < Npts3; k++) for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
      for(int j = 0; j < NGHOSTS; j++) { /* lower y-face. */
        REAL soln[2];Exact_soln(t, x1G[i], x2G[j], x3G[k], soln); // u = soln[0], v = soln[1]
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
      }
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) { /* upper y-face */
        REAL soln[2];Exact_soln(t, x1G[i], x2G[j], x3G[k], soln); // u = soln[0], v = soln[1]
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
      }
    }

  /* Set upper and lower z=constant faces of the cube.
   *   Note we've already updated the upper and lower
   *   x=constant and y=constant faces, so don't update them again */
  for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int k = 0; k < NGHOSTS; k++) { /* lower z-face. */
        REAL soln[2];Exact_soln(t, x1G[i], x2G[j], x3G[k], soln); // u = soln[0], v = soln[1]
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
      }
      for(int k = Npts3 - NGHOSTS; k < Npts3; k++) { /* upper z-face. */
        REAL soln[2];Exact_soln(t, x1G[i], x2G[j], x3G[k], soln); // u = soln[0], v = soln[1]
        in_gfs[IDX4(gf, i, j, k)] = soln[gf];
      }
    }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#elif defined OUTER_BOUNDARY_LIN_EXTRAP
  /* LINEAR EXTRAPOLATION OUTER BOUNDARY CONDITIONS */

  /* Set upper and lower x=constant faces of the cube.
   * We do not include ghostzones in j and k directions,
   *  since this would require reading data that have not
   *  yet been set. */
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i; /* lower x-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, ii, j, k)] = 2.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - in_gfs[IDX4(gf, ii + 2, j, k)];
      }
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) { /* upper x-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
      }
    }

  /* Set upper and lower y=constant faces of the cube.
   * We do not include ghostzones in i and k directions,
   *  since this would require reading data that have not
   *  yet been set. */
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j; /* lower y-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, jj, k)] = 2.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - in_gfs[IDX4(gf, i, jj + 2, k)];
      }
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) { /* upper y-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i, j - 1, k)] - in_gfs[IDX4(gf, i, j - 2, k)];
      }
    }

  /* Set upper and lower z=constant faces of the cube.
   * We do not include ghostzones in i and j directions,
   *  since this would require reading data that have not
   *  yet been set. */
  for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int k = 0; k < NGHOSTS; k++) {
        const int kk = NGHOSTS - 1 - k; /* lower z-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, j, kk)] = 2.0 * in_gfs[IDX4(gf, i, j, kk + 1)] - in_gfs[IDX4(gf, i, j, kk + 2)];
      }
      for(int k = Npts3 - NGHOSTS; k < Npts3; k++) { /* upper z-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i, j, k - 1)] - in_gfs[IDX4(gf, i, j, k - 2)];
      }
    }

  /* Set the four lower z=constant edges of the cube. */
  for(int k = 0; k < NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 2.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - in_gfs[IDX4(gf, ii + 2, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
      }
    }
    for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
      // lower y edge: start inward and work our way outward
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j;
        in_gfs[IDX4(gf, i, jj, k)] = 2.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - in_gfs[IDX4(gf, i, jj + 2, k)];
      }
      // upper y edge: start inward and work our way outward
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i, j - 1, k)] - in_gfs[IDX4(gf, i, j - 2, k)];
      }
    }
  }

  /* Set the four upper z=constant edges of the cube. */
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 2.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - in_gfs[IDX4(gf, ii + 2, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
      }
    }

    for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
      // lower y edge: start inward and work our way outward
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j;
        in_gfs[IDX4(gf, i, jj, k)] = 2.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - in_gfs[IDX4(gf, i, jj + 2, k)];
      }
      // upper y edge: start inward and work our way outward
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i, j - 1, k)] - in_gfs[IDX4(gf, i, j - 2, k)];
      }
    }
  }

  /* Set remaining two edges on lower y=constant face of the cube. */
  for(int j = 0; j < NGHOSTS; j++) for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 2.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - in_gfs[IDX4(gf, ii + 2, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
      }
    }

  /* Set remaining two edges on upper y=constant face of the cube. */
  for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 2.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - in_gfs[IDX4(gf, ii + 2, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
      }
    }

  /* Set all that remains: the 8 corners of the cube */
  // Corner (0,0,0)
  for(int k = 0; k < NGHOSTS; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 2.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - in_gfs[IDX4(gf, ii + 2, j, k)];
      }
  // Corner (0,0,1)
  for(int k = 0; k < NGHOSTS; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
      }
  // Corner (0,1,0)
  for(int k = 0; k < NGHOSTS; k++) for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 2.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - in_gfs[IDX4(gf, ii + 2, j, k)];
      }
  // Corner (0,1,1)
  for(int k = 0; k < NGHOSTS; k++) for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
      }
  // Corner (1,0,0)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 2.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - in_gfs[IDX4(gf, ii + 2, j, k)];
      }
  // Corner (1,0,1)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
      }
  // Corner (1,1,0)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 2.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - in_gfs[IDX4(gf, ii + 2, j, k)];
      }
  // Corner (1,1,1)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 2.0 * in_gfs[IDX4(gf, i - 1, j, k)] - in_gfs[IDX4(gf, i - 2, j, k)];
      }

#elif defined OUTER_BOUNDARY_QUAD_EXTRAP
  /* QUADRATIC EXTRAPOLATION OUTER BOUNDARY CONDITIONS */

  /* Set upper and lower x=constant faces of the cube.
   * We do not include ghostzones in j and k directions,
   *  since this would require reading data that have not
   *  yet been set. */
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i; /* lower x-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
      }
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) { /* upper x-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
      }
    }

  /* Set upper and lower y=constant faces of the cube.
   * We do not include ghostzones in i and k directions,
   *  since this would require reading data that have not
   *  yet been set. */
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j; /* lower y-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, jj, k)] = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];
      }
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) { /* upper y-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i, j - 1, k)] - 3.0 * in_gfs[IDX4(gf, i, j - 2, k)] + in_gfs[IDX4(gf, i, j - 3, k)];
      }
    }

  /* Set upper and lower z=constant faces of the cube.
   * We do not include ghostzones in i and j directions,
   *  since this would require reading data that have not
   *  yet been set. */
  for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int k = 0; k < NGHOSTS; k++) {
        const int kk = NGHOSTS - 1 - k; /* lower z-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, j, kk)] = 3.0 * in_gfs[IDX4(gf, i, j, kk + 1)] - 3.0 * in_gfs[IDX4(gf, i, j, kk + 2)] + in_gfs[IDX4(gf, i, j, kk + 3)];
      }
      for(int k = Npts3 - NGHOSTS; k < Npts3; k++) { /* upper z-face: start from the inside and work our way outward */
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i, j, k - 1)] - 3.0 * in_gfs[IDX4(gf, i, j, k - 2)] + in_gfs[IDX4(gf, i, j, k - 3)];
      }
    }

  /* Set the four lower z=constant edges of the cube. */
  for(int k = 0; k < NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
      }
    }
    for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
      // lower y edge: start inward and work our way outward
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j;
        in_gfs[IDX4(gf, i, jj, k)] = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];
      }
      // upper y edge: start inward and work our way outward
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i, j - 1, k)] - 3.0 * in_gfs[IDX4(gf, i, j - 2, k)] + in_gfs[IDX4(gf, i, j - 3, k)];
      }
    }
  }

  /* Set the four upper z=constant edges of the cube. */
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
      }
    }

    for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
      // lower y edge: start inward and work our way outward
      for(int j = 0; j < NGHOSTS; j++) {
        const int jj = NGHOSTS - 1 - j;
        in_gfs[IDX4(gf, i, jj, k)] = 3.0 * in_gfs[IDX4(gf, i, jj + 1, k)] - 3.0 * in_gfs[IDX4(gf, i, jj + 2, k)] + in_gfs[IDX4(gf, i, jj + 3, k)];
      }
      // upper y edge: start inward and work our way outward
      for(int j = Npts2 - NGHOSTS; j < Npts2; j++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i, j - 1, k)] - 3.0 * in_gfs[IDX4(gf, i, j - 2, k)] + in_gfs[IDX4(gf, i, j - 3, k)];
      }
    }
  }

  /* Set remaining two edges on lower y=constant face of the cube. */
  for(int j = 0; j < NGHOSTS; j++) for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
      }
    }

  /* Set remaining two edges on upper y=constant face of the cube. */
  for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
      // lower x edge: start inward and work our way outward
      for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
      }
      // upper x edge: start inward and work our way outward
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
      }
    }

  /* Set all that remains: the 8 corners of the cube */
  // Corner (0,0,0)
  for(int k = 0; k < NGHOSTS; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
      }
  // Corner (0,0,1)
  for(int k = 0; k < NGHOSTS; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
      }
  // Corner (0,1,0)
  for(int k = 0; k < NGHOSTS; k++) for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
      }
  // Corner (0,1,1)
  for(int k = 0; k < NGHOSTS; k++) for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
      }
  // Corner (1,0,0)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
      }
  // Corner (1,0,1)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) for(int j = 0; j < NGHOSTS; j++) for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
      }
  // Corner (1,1,0)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int i = 0; i < NGHOSTS; i++) {
        const int ii = NGHOSTS - 1 - i;
        in_gfs[IDX4(gf, ii, j, k)] = 3.0 * in_gfs[IDX4(gf, ii + 1, j, k)] - 3.0 * in_gfs[IDX4(gf, ii + 2, j, k)] + in_gfs[IDX4(gf, ii + 3, j, k)];
      }
  // Corner (1,1,1)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) for(int j = Npts2 - NGHOSTS; j < Npts2; j++) for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        in_gfs[IDX4(gf, i, j, k)] = 3.0 * in_gfs[IDX4(gf, i - 1, j, k)] - 3.0 * in_gfs[IDX4(gf, i - 2, j, k)] + in_gfs[IDX4(gf, i - 3, j, k)];
      }
#endif

  return;
}
