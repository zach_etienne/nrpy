// SymTP coordinates (rho, ph, z) = (yy[0], yy[1], yy[2])
// (A, B, ph) = (xx[0], xx[1], xx[2])

// A mod function that handles negative numbers as expected
// e.g., (-1) % 8 == -1, but MOD(-1, 8) == 7
#define MOD(a, b) ( ( ( (a) % (b) ) + (b) ) % (b) )
#define K_PLUS_PI(k) (NGHOSTS + MOD((k) + Npts3 / 2 - 2 * NGHOSTS, Npts3 - 2 * NGHOSTS)) // ph -> pi + ph MOD 2 pi

void Apply_out_sym_bcs_SymTP(const int gf, const REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *func, paramstruct params)
{
#include "../parameters_readin-NRPyGEN.h"

  // Denote lower ghost zone by "-", upper ghost zone by "+", and the grid proper by "0"

  // (i, j, k) ~ (+, 0, 0)
  // First, set the outer boundary directly adjacent to the grid proper (i, j, k) ~ (0, 0, 0)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
#if defined OUTER_BOUNDARY_EXACT
        REAL soln[NUM_GFS];
        Exact_soln(t, x1G[i], x2G[j], x3G[k], soln);
        func[IDX4(gf, i, j, k)] = soln[gf];
#elif defined OUTER_BOUNDARY_LIN_EXTRAP
        // Linear extrapolation outer boundary conditions
        func[IDX4(gf, i, j, k)] = 2.0 * func[IDX4(gf, i - 1, j, k)] - func[IDX4(gf, i - 2, j, k)];
#elif defined OUTER_BOUNDARY_QUAD_EXTRAP
        // Linear extrapolation outer boundary conditions
        func[IDX4(gf, i, j, k)] = 3.0 * func[IDX4(gf, i - 1, j, k)] - 3.0 * func[IDX4(gf, i - 2, j, k)] + func[IDX4(gf, i - 3, j, k)];
#endif
      }
    }
  }

  // Initialize vector and tensor parity conditions
  REAL parity_P1 = 1.0, parity_P2 = 1.0, parity_P3 = 1.0;

#ifdef BSSN_EVOLUTION
  if(gf == VET1 || gf == LAMB1 || gf == BET1 || gf == H23 || gf == A23) {
    // Vector component A or tensor component B-ph
    parity_P2 = -1.0;
    parity_P3 = -1.0;
  }
  else if(gf == VET2 || gf == LAMB2 || gf == BET2 || gf == H13 || gf == A13) {
    // Vector component B or tensor component A-ph
    parity_P1 = -1.0;
    parity_P3 = -1.0;
  }
  else if(gf == VET3 || gf == LAMB3 || gf == BET3 || gf == H12 || gf == A12) {
    // Vector component ph or tensor component A-B
    parity_P1 = -1.0;
    parity_P2 = -1.0;
  }
#endif

  // Ghost zone (i, j, k) ~ (-, -, -)
  // f(-A, -1 - B, -ph) = f(+A, -1 + B, 2 * pi - ph)
  for(int k = 0; k < NGHOSTS; k++) {
    // -ph -> 2 * pi - ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = 0; j < NGHOSTS; j++) {
      // -1 - B -> -1 + B
      const int j_ghost = NGHOSTS - 1 - j;
      const int j_phys = NGHOSTS + j;

      for(int i = 0; i < NGHOSTS; i++) {
        // -A -> A
        const int i_ghost = NGHOSTS - 1 - i;
        const int i_phys = NGHOSTS + i;

        func[IDX4(gf, i_ghost, j_ghost, k)] = parity_P3 * func[IDX4(gf, i_phys, j_phys, k_plus_2pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, -, 0)
  // f(-A, -1 - B, ph) = f(+A, -1 + B, ph)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = 0; j < NGHOSTS; j++) {
      // -1 - B -> -1 + B
      const int j_ghost = NGHOSTS - 1 - j;
      const int j_phys = NGHOSTS + j;

      for(int i = 0; i < NGHOSTS; i++) {
        // -A -> A
        const int i_ghost = NGHOSTS - 1 - i;
        const int i_phys = NGHOSTS + i;

        func[IDX4(gf, i_ghost, j_ghost, k)] = parity_P3 * func[IDX4(gf, i_phys, j_phys, k)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, -, +)
  // f(-A, -1 - B, 2 pi + ph) = f(+A, -1 + B, ph)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> 2 * pi + ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = 0; j < NGHOSTS; j++) {
      // -1 - B -> -1 + B
      const int j_ghost = NGHOSTS - 1 - j;
      const int j_phys = NGHOSTS + j;

      for(int i = 0; i < NGHOSTS; i++) {
        // -A -> A
        const int i_ghost = NGHOSTS - 1 - i;
        const int i_phys = NGHOSTS + i;

        func[IDX4(gf, i_ghost, j_ghost, k)] = parity_P3 * func[IDX4(gf, i_phys, j_phys, k_plus_2pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, 0, -)
  // f(-A, B, -ph) = f(+A, B, pi - ph)
  for(int k = 0; k < NGHOSTS; k++) {
    // -ph -> pi - ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
        // -A -> A
        const int i_ghost = NGHOSTS - 1 - i;
        const int i_phys = NGHOSTS + i;

        func[IDX4(gf, i_ghost, j, k)] = parity_P2 * func[IDX4(gf, i_phys, j, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, 0, 0)
  // f(-A, B, ph) = f(+A, B, pi + ph)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
        // -A -> A
        const int i_ghost = NGHOSTS - 1 - i;
        const int i_phys = NGHOSTS + i;

        func[IDX4(gf, i_ghost, j, k)] = parity_P2 * func[IDX4(gf, i_phys, j, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, 0, +)
  // f(-A, B, 2 pi + ph) = f(+A, B, pi + ph)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = 0; i < NGHOSTS; i++) {
        // -A -> A
        const int i_ghost = NGHOSTS - 1 - i;
        const int i_phys = NGHOSTS + i;

        func[IDX4(gf, i_ghost, j, k)] = parity_P2 * func[IDX4(gf, i_phys, j, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, +, -)
  // f(-A, 1 + B, -ph) = f(+A, 1 - B, 2 pi - ph)
  for(int k = 0; k < NGHOSTS; k++) {
    // -ph -> 2 * pi - ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = 0; j < NGHOSTS; j++) {
      // 1 + B -> 1 - B
      const int j_ghost = Npts2 - NGHOSTS + j;
      const int j_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
        // -A -> A
        const int i_ghost = NGHOSTS - 1 - i;
        const int i_phys = NGHOSTS + i;

        func[IDX4(gf, i_ghost, j_ghost, k)] = parity_P3 * func[IDX4(gf, i_phys, j_phys, k_plus_2pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, +, 0)
  // f(-A, 1 + B, ph) = f(+A, 1 - B, ph)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    for(int j = 0; j < NGHOSTS; j++) {
      // 1 + B -> 1 - B
      const int j_ghost = Npts2 - NGHOSTS + j;
      const int j_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
        // -A -> A
        const int i_ghost = NGHOSTS - 1 - i;
        const int i_phys = NGHOSTS + i;

        func[IDX4(gf, i_ghost, j_ghost, k)] = parity_P3 * func[IDX4(gf, i_phys, j_phys, k)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (-, +, +)
  // f(-A, 1 + B, +ph) = f(+A, 1 - B, 2 pi + ph)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> 2 * pi + ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = 0; j < NGHOSTS; j++) {
      // 1 + B -> 1 - B
      const int j_ghost = Npts2 - NGHOSTS + j;
      const int j_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = 0; i < NGHOSTS; i++) {
        // -A -> A
        const int i_ghost = NGHOSTS - 1 - i;
        const int i_phys = NGHOSTS + i;

        func[IDX4(gf, i_ghost, j_ghost, k)] = parity_P3 * func[IDX4(gf, i_phys, j_phys, k_plus_2pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, -, -)
  // f(A, -1 - B, -ph) = f(A, -1 + B, pi - ph)
  for(int k = 0; k < NGHOSTS; k++) {
    // -ph -> pi - ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // -1 - B -> -1 + B
      const int j_ghost = NGHOSTS - 1 - j;
      const int j_phys = NGHOSTS + j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, -, 0)
  // f(A, -1 - B, ph) = f(A, -1 + B, pi + ph)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // -1 - B -> -1 + B
      const int j_ghost = NGHOSTS - 1 - j;
      const int j_phys = NGHOSTS + j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, -, +)
  // f(A, -1 - B, 2 pi + ph) = f(A, -1 + B, pi + ph)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // -1 - B -> -1 + B
      const int j_ghost = NGHOSTS - 1 - j;
      const int j_phys = NGHOSTS + j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, 0, -)
  // f(A, B, -ph) = f(A, B, 2 pi - ph)
  for(int k = 0; k < NGHOSTS; k++) {
    // ph -> 2 * pi + ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
        func[IDX4(gf, i, j, k)] = func[IDX4(gf, i, j, k_plus_2pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, 0, 0)
  // This is the grid proper. Nothing to do, here.

  // Ghost zone (i, j, k) ~ (0, 0, +)
  // f(A, B, 2 pi + ph) = f(A, B, ph)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> 2 * pi + ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
        func[IDX4(gf, i, j, k)] = func[IDX4(gf, i, j, k_plus_2pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, +, -)
  // f(A, 1 + B, -ph) = f(A, 1 - B, pi - ph)
  for(int k = 0; k < NGHOSTS; k++) {
    // -ph -> pi - ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // 1 + B -> 1 - B
      const int j_ghost = Npts2 - NGHOSTS + j;
      const int j_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, +, 0)
  // f(A, 1 + B, ph) = f(A, 1 - B, pi + ph)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // 1 + B -> 1 - B
      const int j_ghost = Npts2 - NGHOSTS + j;
      const int j_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (0, +, +)
  // f(A, 1 + B, 2 pi + ph) = f(A, 1 - B, pi + ph)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // 1 + B -> 1 - B
      const int j_ghost = Npts2 - NGHOSTS + j;
      const int j_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, -, -)
  // f(A, -1 - B, -ph) = f(A, -1 + B, pi - ph)
  for(int k = 0; k < NGHOSTS; k++) {
    // -ph -> pi - ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // -1 - B -> -1 + B
      const int j_ghost = NGHOSTS - 1 - j;
      const int j_phys = NGHOSTS + j;

      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, -, 0)
  // f(A, -1 - B, ph) = f(A, -1 + B, pi + ph)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // -1 - B -> -1 + B
      const int j_ghost = NGHOSTS - 1 - j;
      const int j_phys = NGHOSTS + j;

      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, -, +)
  // f(A, -1 - B, 2 pi + ph) = f(A, -1 + B, pi + ph)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // -1 - B -> -1 + B
      const int j_ghost = NGHOSTS - 1 - j;
      const int j_phys = NGHOSTS + j;

      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, 0, -)
  // f(A, B, -ph) = f(A, B, 2 pi - ph)
  for(int k = 0; k < NGHOSTS; k++) {
    // ph -> 2 * pi + ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        func[IDX4(gf, i, j, k)] = func[IDX4(gf, i, j, k_plus_2pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, 0, 0)
  // This is the outer boundary, which is the first region filled at the top.

  // Ghost zone (i, j, k) ~ (+, 0, +)
  // f(A, B, 2 pi + ph) = f(A, B, ph)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> 2 * pi + ph
    const int k_plus_2pi = K_PLUS_PI(K_PLUS_PI(k));

    for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) {
      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        func[IDX4(gf, i, j, k)] = func[IDX4(gf, i, j, k_plus_2pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, +, -)
  // f(A, 1 + B, -ph) = f(A, 1 - B, pi - ph)
  for(int k = 0; k < NGHOSTS; k++) {
    // -ph -> pi - ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // 1 + B -> 1 - B
      const int j_ghost = Npts2 - NGHOSTS + j;
      const int j_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, +, 0)
  // f(A, 1 + B, ph) = f(A, 1 - B, pi + ph)
  for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // 1 + B -> 1 - B
      const int j_ghost = Npts2 - NGHOSTS + j;
      const int j_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  // Ghost zone (i, j, k) ~ (+, +, +)
  // f(A, 1 + B, 2 pi + ph) = f(A, 1 - B, pi + ph)
  for(int k = Npts3 - NGHOSTS; k < Npts3; k++) {
    // ph -> pi + ph
    const int k_plus_pi = K_PLUS_PI(k);

    for(int j = 0; j < NGHOSTS; j++) {
      // 1 + B -> 1 - B
      const int j_ghost = Npts2 - NGHOSTS + j;
      const int j_phys = Npts2 - NGHOSTS - 1 - j;

      for(int i = Npts1 - NGHOSTS; i < Npts1; i++) {
        func[IDX4(gf, i, j_ghost, k)] = parity_P1 * func[IDX4(gf, i, j_phys, k_plus_pi)];
      }
    }
  }

  return;
}
#undef MOD
#undef K_PLUS_PI
