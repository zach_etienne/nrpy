#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <sys/time.h>

// Allocation and free macros for grid functions
#define ALLOCATE_2D_GENERIC(type,array,ni,nj) type **array=(type **)malloc(ni * sizeof(type *)); \
  for(int cc = 0; cc < ni; cc++)                 array[cc]=(type * )malloc(nj * sizeof(type));
#define FREE_2D_GENERIC(type,array,ni,nj) for(int cc = 0; cc < ni;cc++) free((void *)array[cc]); \
  /**/                                                                  free((void *)array);

void get_w(const int N,double w[N],double x[N]) {
  for(int i=0;i<N;i++) {
    double w_tmp_inv = 1.0;
    for(int j=0;j<i;j++)   w_tmp_inv *= (x[i] - x[j]);
    for(int j=i+1;j<N;j++) w_tmp_inv *= (x[i] - x[j]);
    w[i] = 1.0 / w_tmp_inv;
    //printf("%d %.15e\n",i,w[i]);
  }
}

double ff(double x,double y,double z) {
  return  x*x*x - 1.1*x*y*y*y + 0.2 - 3.1*y*y*y - 1.4*y - 12.*z*z*z + 4*x*x*x*y*y*y*z*z*z;
}

int main(int argc, const char *argv[]) {
  const int N=9;

  if(argc != 2) {
    printf("Usage: ./interpolate [number of points]\n");
    exit(1);
  }
  const int numpts = atoi(argv[1]);
  /*
# Inputs: x[i] = x_i: Points at which underlying function f(x) is evaluated
#         f[i] = f_i = f(x_i): Values of function at points x_i
#         xx[i]: Points xx_i at which we evaluate P_N(xx)
# Output: P_N_xx[point]: P_N(xx[point])
do i=0,N
   w_tmp = 1.0
   do j=0,N
      if(j not equal to i)
         w_tmp = w_tmp / (x[i] - x[j])
      end if
   end do
   w[i] = w_tmp
end do

do point=1,M
   P_N_xx[point] = 0.0
   l_of_x = 1.0
   do i=0,N
      # "l_of_x" in this loop over i is *independent* of "P_N_xx[point]".
      l_of_x = l_of_x * (xx[point] - x[i])
      P_N_xx[point] = P_N_xx[point] + f[i]*w[i] / (xx[point] - x[i])
   end do
   P_N_xx[point] = P_N_xx[point] * l_of_x
end do
  */

  double x[N];
  double y[N];
  double z[N];
  double f[N][N][N];
  double dx = 1.0 / ((double)N);
  double w_x[N];
  double dy = 2.2 / ((double)N);
  double w_y[N];
  double dz = 3.1415 / ((double)N);
  double w_z[N];

  for(int i=0;i<N;i++) {
    x[i] = -1.3 + (double)i * dx;
    y[i] = -12. + (double)i * dy;
    z[i] = 33.1 + (double)i * dz;
  }
  for(int i=0;i<N;i++) for(int j=0;j<N;j++) for(int k=0;k<N;k++) {
      f[i][j][k] = ff(x[i],y[j],z[k]);
    }


  double *xx_pts = (double *)malloc(sizeof(double)*numpts);
  double *yy_pts = (double *)malloc(sizeof(double)*numpts);
  double *zz_pts = (double *)malloc(sizeof(double)*numpts);
  for(int ll=0;ll<numpts;ll++) {
    xx_pts[ll] = -1.3 + (drand48()*(double)(N-1)*dx);
    yy_pts[ll] = -12. + (drand48()*(double)(N-1)*dy);
    zz_pts[ll] = 33.1 + (drand48()*(double)(N-1)*dz);
  }

  get_w(N,w_x,x);
  get_w(N,w_y,y);
  get_w(N,w_z,z);

  double P_N_x_y_z,xx,yy,zz;

  for(int ll=0;ll<numpts;ll++) {
    xx = xx_pts[ll];
    yy = yy_pts[ll];
    zz = zz_pts[ll];

    P_N_x_y_z=0.0;

    double l_of_x = 1.0;
    double l_of_y = 1.0;
    double l_of_z = 1.0;

    double w_x_i_times_xdiff_inv[N];
    double w_y_i_times_ydiff_inv[N];
    double w_z_i_times_zdiff_inv[N];
    for(int i=0;i<N;i++) {
      double xdiff = xx - x[i];
      double ydiff = yy - y[i];
      double zdiff = zz - z[i];
      w_x_i_times_xdiff_inv[i] = w_x[i] / xdiff;
      w_y_i_times_ydiff_inv[i] = w_y[i] / ydiff;
      w_z_i_times_zdiff_inv[i] = w_z[i] / zdiff;
      l_of_x *= xdiff;
      l_of_y *= ydiff;
      l_of_z *= zdiff;
    }

    //P_N_x_y = 0.0;
    for(int i=0;i<N;i++) for(int j=0;j<N;j++) for(int k=0;k<N;k++) {
          P_N_x_y_z += f[i][j][k]*w_x_i_times_xdiff_inv[i]*w_y_i_times_ydiff_inv[j]*w_z_i_times_zdiff_inv[k];
        }
    P_N_x_y_z *= l_of_x*l_of_y*l_of_z;

    printf("%e %e %e | %.15e %.15e\n",xx,yy,zz,(P_N_x_y_z-ff(xx,yy,zz))/ff(xx,yy,zz),P_N_x_y_z);
    
  }

  free(xx_pts);
  free(yy_pts);
  free(zz_pts);

  return 0;
}
