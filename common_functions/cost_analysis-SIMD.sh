#!/bin/bash

ADD=`grep -c "AddSIMD" $1`
SUB=`grep -c "SubSIMD" $1`
MUL=`grep -c "MulSIMD" $1`
FMA=`grep -c "FusedMulAddSIMD" $1`
FMS=`grep -c "FusedMulSubSIMD" $1`
SQRT=`grep -c "SqrtSIMD" $1`
CBRT=`grep -c "CbrtSIMD" $1`
DIV=`grep -c "DivSIMD" $1`
POW=`grep -c "PowSIMD" $1`
EXP=`grep -c "ExpSIMD" $1`

# Tried to synthesize info from http://sleef.org/benchmark.xhtml and https://software.intel.com/sites/landingpage/IntrinsicsGuide/ whereever possible.
# Many of the below are wild guesses:
COSTADD=`echo $ADD*1|bc`
COSTSUB=`echo $SUB*1|bc`
COSTMUL=`echo $MUL*0.5|bc`
COSTFMA=`echo $FMA*0.5|bc`
COSTFMS=`echo $FMS*0.5|bc`
COSTSQRT=`echo $SQRT*20|bc`
COSTCBRT=`echo $CBRT*20|bc`
COSTDIV=`echo $DIV*20|bc`
COSTPOW=`echo $POW*20|bc` # wild guess
COSTEXP=`echo $EXP*4|bc` # wild guess

echo Add: $ADD $COSTADD
echo Sub: $SUB $COSTSUB
echo Mul: $MUL $COSTMUL
echo FMA: $FMA $COSTFMA
echo FMS: $FMS $COSTFMS
echo Sqrt: $SQRT $COSTSQRT
echo Cbrt: $CBRT $COSTCBRT
echo Div: $DIV $COSTDIV
echo Pow: $POW $COSTPOW
echo Exp: $EXP $COSTEXP

echo Total cost: `echo $COSTADD+$COSTSUB+$COSTMUL+$COSTFMA+$COSTFMS+$COSTSQRT+$COSTCBRT+$COSTDIV+$COSTPOW+$COSTEXP|bc`

echo Integer_m1: `grep -c "_Integer_m1" $1`
