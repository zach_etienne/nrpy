#define NOGZ_IDX4(g,i,j,k)   ( (i) + Nx1 * ( (j) + Nx2 * ( (k) + Nx3 * (g) ) ) )

// Write grid functions at iteration n to checkpoint file
void Write_Checkpoint(const int n, const REAL dt, REAL *in_gfs, REAL *gfs_tmp, paramstruct params) {
  const int Npts1 = params.Npts1;
  const int Npts2 = params.Npts2;
  const int Npts3 = params.Npts3;
  const int NGHOSTS = params.NGHOSTS;

  // If we want to keep every checkpoint, then first rename the LAST checkpoint from checkpoint.bin to checkpoint_it*.bin
  // A run is always restarted from checkpoint.bin, which will be the name of the current checkpoint
  if(KEEP_ALL_CHECKPOINTS == 1) {
    // Create checkpoint filename pattern with enough leading 0's to accommodate the iteration number
    char checkpoint_filename_pattern[128];
    sprintf(checkpoint_filename_pattern, "checkpoint_it%%0%dd.bin", (int)(log10(N_FRAMES * FRAME_JUMP) + 1));

    // Checkpoint filename for iteration n - FRAME_JUMP, the last time a checkpoint was dumped
    char checkpoint_filename[128];
    sprintf(checkpoint_filename, checkpoint_filename_pattern, n - CHECKPOINT_EVERY);

    rename("checkpoint.bin", checkpoint_filename);
  }

  // Open checkpoint file to write in binary format
  FILE *checkpoint_file;
  checkpoint_file = fopen("checkpoint.bin", "wb");

  if(!checkpoint_file) {
    printf("Unable to open checkpoint file\n");
    exit(1);
  }

  printf("%c[2K", 27); // Clear the line
  printf("Writing checkpoint at iteration %d\n", n);

  const int Nx1 = params.Nx1;
  const int Nx2 = params.Nx2;
  const int Nx3 = params.Nx3;

  // Write grid parameters
  fwrite(&Nx1, sizeof(int), 1, checkpoint_file);
  fwrite(&Nx2, sizeof(int), 1, checkpoint_file);
  fwrite(&Nx3, sizeof(int), 1, checkpoint_file);

  // Write iteration number
  fwrite(&n, sizeof(int), 1, checkpoint_file);

  // Write time step
  fwrite(&dt, sizeof(REAL), 1, checkpoint_file);

#pragma omp parallel for
  for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
    for(int k = 0; k < Nx3; k++) {
      for(int j = 0; j < Nx2; j++) {
        for(int i = 0; i < Nx1; i++) {
          gfs_tmp[NOGZ_IDX4(gf, i, j, k)] = in_gfs[IDX4(gf, i + NGHOSTS, j + NGHOSTS, k + NGHOSTS)];
        }
      }
    }
  }

  // Write grid functions
  fwrite(gfs_tmp, sizeof(REAL), NUM_EVOL_GFS * Nx1 * Nx2 * Nx3, checkpoint_file);

  fclose(checkpoint_file);

  return;
}

// Read the grid functions at iteration n from checkpoint file
void Read_Checkpoint(char *checkpoint_filename, int *n, REAL *dt, REAL *in_gfs, REAL *gfs_tmp, paramstruct params) {
  const int Npts1 = params.Npts1;
  const int Npts2 = params.Npts2;
  const int Npts3 = params.Npts3;
  const int NGHOSTS = params.NGHOSTS;

  // Open checkpoint file to read in binary format
  FILE *checkpoint_file;
  checkpoint_file = fopen(checkpoint_filename, "rb");

  if(!checkpoint_file) {
    printf("Didn't find checkpoint file. No biggie, just start from t=0.\n");
    return;
  }

  size_t size;

  int Nx1, Nx2, Nx3;

  // Read parameters
  size = fread(&Nx1, sizeof(int), 1, checkpoint_file);
  size = fread(&Nx2, sizeof(int), 1, checkpoint_file);
  size = fread(&Nx3, sizeof(int), 1, checkpoint_file);

  // Simple parameter check
  if(Nx1 != params.Nx1 || Nx2 != params.Nx2 || Nx3 != params.Nx3) {
    printf("ERROR: Command line parameters do not match checkpoint.bin\n");
    exit(1);
  }

  // Read iteration numbers
  size = fread(n, sizeof(int), 1, checkpoint_file);

  // Read time step
  size = fread(dt, sizeof(REAL), 1, checkpoint_file);

  // Read grid functions
  size = fread(gfs_tmp, sizeof(REAL), NUM_EVOL_GFS * Nx1 * Nx2 * Nx3, checkpoint_file);

#pragma omp parallel for
  for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
    for(int k = 0; k < Nx3; k++) {
      for(int j = 0; j < Nx2; j++) {
        for(int i = 0; i < Nx1; i++) {
          in_gfs[IDX4(gf, i + NGHOSTS, j + NGHOSTS, k + NGHOSTS)] = gfs_tmp[NOGZ_IDX4(gf, i, j, k)];

          if(isnan(in_gfs[IDX4(gf, i + NGHOSTS, j + NGHOSTS, k + NGHOSTS)])) {
            printf("ERROR: NaN read from checkpoint.bin\n");
            exit(1);
          }
        }
      }
    }
  }

  printf("Starting checkpoint from iteration n = %d\n", *n);

  fclose(checkpoint_file);

  return;
}

// Extract data from checkpoint files and write it to ASCII files using Data_File_Output()
void Extract_Checkpoint_Data(char *out0Dname, char *out1Dname, char *out2Dname, char *out3Dname, REAL *x1G, REAL *x2G, REAL *x3G, REAL *yy, REAL *gfs_n, REAL *gfs_tmp, REAL *gfs_aux, precomputed_quantities *precomp, paramstruct params) {
  // Open file output
  FILE *out0D, *out1D, *out2D, *out3D;
  out0D = fopen(out0Dname, "w");
  out1D = fopen(out1Dname, "w");
  out2D = fopen(out2Dname, "w");
  out3D = fopen(out3Dname, "w");

  // Loop over each of the N_FRAMES checkpoint files
  // Assuming the cadence of FRAME_JUMP frames between each data dump
  for(int frame_ind = 0; frame_ind < N_FRAMES; frame_ind++) {
    int iter_start = 0;
    const int frame = frame_ind * FRAME_JUMP;

    // Create checkpoint filename pattern with enough leading 0's to accommodate the iteration number
    char checkpoint_filename_pattern[128];
    sprintf(checkpoint_filename_pattern, "checkpoint_it%%0%dd.bin", (int)(log10(N_FRAMES * FRAME_JUMP) + 1));

    // Checkpoint filename for iteration frame
    char checkpoint_filename[128];
    sprintf(checkpoint_filename, checkpoint_filename_pattern, frame);

    printf("Extracting data from %s\n", checkpoint_filename);

    Read_Checkpoint(checkpoint_filename, &iter_start, &dt, gfs_n, gfs_tmp, params);

    const REAL t = dt * frame;

#pragma omp parallel for
    for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
      Apply_out_sym_bcs(gf, t, x1G, x2G, x3G, gfs_n, params);
    }

    // Output 0D, 1D, 2D, and 3D data to files
    Data_File_Output(out0D, out1D, out2D, out3D, frame_ind, frame, t, x1G, x2G, x3G, yy, gfs_n, gfs_aux, precomp, params);
  }

  // Close file IO
  fclose(out0D);
  fclose(out1D);
  fclose(out2D);
  fclose(out3D);

  printf("Done extracting checkpoint data.\n");

  return;
}

#undef NOGZ_IDX4
