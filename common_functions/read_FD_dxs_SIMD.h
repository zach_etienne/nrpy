#ifdef ENABLE_SIMD_256_INTRINSICS
const double tmpinvdx0 = 1.0 / (x1G[1] - x1G[0]); const __m256d invdx0 = _mm256_set1_pd(tmpinvdx0);
const double tmpinvdx1 = 1.0 / (x2G[1] - x2G[0]); const __m256d invdx1 = _mm256_set1_pd(tmpinvdx1);
const double tmpinvdx2 = 1.0 / (x3G[1] - x3G[0]); const __m256d invdx2 = _mm256_set1_pd(tmpinvdx2);

const double tmpinvdx0invdx0 = tmpinvdx0*tmpinvdx0; const __m256d invdx0invdx0 = _mm256_set1_pd(tmpinvdx0invdx0);
const double tmpinvdx0invdx1 = tmpinvdx0*tmpinvdx1; const __m256d invdx0invdx1 = _mm256_set1_pd(tmpinvdx0invdx1);
const double tmpinvdx0invdx2 = tmpinvdx0*tmpinvdx2; const __m256d invdx0invdx2 = _mm256_set1_pd(tmpinvdx0invdx2);

const double tmpinvdx1invdx1 = tmpinvdx1*tmpinvdx1; const __m256d invdx1invdx1 = _mm256_set1_pd(tmpinvdx1invdx1);
const double tmpinvdx1invdx2 = tmpinvdx1*tmpinvdx2; const __m256d invdx1invdx2 = _mm256_set1_pd(tmpinvdx1invdx2);
const double tmpinvdx2invdx2 = tmpinvdx2*tmpinvdx2; const __m256d invdx2invdx2 = _mm256_set1_pd(tmpinvdx2invdx2);

#else // ENABLE_SIMD_512_INTRINSICS

const double tmpinvdx0 = 1.0 / (x1G[1] - x1G[0]); const __m512d invdx0 = _mm512_set1_pd(tmpinvdx0);
const double tmpinvdx1 = 1.0 / (x2G[1] - x2G[0]); const __m512d invdx1 = _mm512_set1_pd(tmpinvdx1);
const double tmpinvdx2 = 1.0 / (x3G[1] - x3G[0]); const __m512d invdx2 = _mm512_set1_pd(tmpinvdx2);

const double tmpinvdx0invdx0 = tmpinvdx0*tmpinvdx0; const __m512d invdx0invdx0 = _mm512_set1_pd(tmpinvdx0invdx0);
const double tmpinvdx0invdx1 = tmpinvdx0*tmpinvdx1; const __m512d invdx0invdx1 = _mm512_set1_pd(tmpinvdx0invdx1);
const double tmpinvdx0invdx2 = tmpinvdx0*tmpinvdx2; const __m512d invdx0invdx2 = _mm512_set1_pd(tmpinvdx0invdx2);

const double tmpinvdx1invdx1 = tmpinvdx1*tmpinvdx1; const __m512d invdx1invdx1 = _mm512_set1_pd(tmpinvdx1invdx1);
const double tmpinvdx1invdx2 = tmpinvdx1*tmpinvdx2; const __m512d invdx1invdx2 = _mm512_set1_pd(tmpinvdx1invdx2);
const double tmpinvdx2invdx2 = tmpinvdx2*tmpinvdx2; const __m512d invdx2invdx2 = _mm512_set1_pd(tmpinvdx2invdx2);

#endif
