from math import sin, cos, sqrt
import numpy as np
import struct
import sys
import numpy.linalg as linalg
from diagnostic_utils import compute_KS_metric_sph, convertVUCart2Sph, convertTDDCart2Sph
import argparse

def calcIntegrands(r_here,th_here,ph_here,ii,jj,kk,data_clump):

    rho = data_clump[0,kk,jj,ii]
    P   = data_clump[1,kk,jj,ii]
    vx  = data_clump[2,kk,jj,ii]
    vy  = data_clump[3,kk,jj,ii]
    vz  = data_clump[4,kk,jj,ii]
    Bx  = data_clump[5,kk,jj,ii]
    By  = data_clump[6,kk,jj,ii]
    Bz  = data_clump[7,kk,jj,ii]

    #u0  = data_clump[8,kk,jj,ii]
    
    gxx   = data_clump[9,kk,jj,ii]
    gxy   = data_clump[10,kk,jj,ii]
    gxz   = data_clump[11,kk,jj,ii]
    gyy   = data_clump[12,kk,jj,ii]
    gyz   = data_clump[13,kk,jj,ii]
    gzz   = data_clump[14,kk,jj,ii]

    betax = data_clump[15,kk,jj,ii]
    betay = data_clump[16,kk,jj,ii]
    betaz = data_clump[17,kk,jj,ii]

    alpha = data_clump[18,kk,jj,ii]


    adjustBmag = 1.0/np.sqrt(4.0*np.pi)

    Bx = Bx*adjustBmag
    By = By*adjustBmag
    Bz = Bz*adjustBmag

    betar, betath, betaph = convertVUCart2Sph(r_here,th_here,ph_here,betax,betay,betaz)
    grr, grth, grph, gthth, gthph, gphph = convertTDDCart2Sph(r_here,th_here,ph_here,gxx,gxy,gxz,gyy,gyz,gzz)

    beta_r  = grr*betar  + grth*betath  + grph*betaph
    beta_th = grth*betar + gthth*betath + gthph*betaph
    beta_ph = grph*betar + gthph*betath + gphph*betaph

    betasq = beta_r*betar + beta_th*betath + beta_ph*betaph

    g4sph = [[betasq - alpha*alpha, beta_r, beta_th, beta_ph],[beta_r,grr,grth,grph],[beta_th,grth,gthth,gthph],[beta_ph,grph,gthph,gphph]]

    vr, vth, vph = convertVUCart2Sph(r_here,th_here,ph_here,vx,vy,vz)
    Br, Bth, Bph = convertVUCart2Sph(r_here,th_here,ph_here,Bx,By,Bz)

    B_x = gxx*Bx + gxy*By + gxz*Bz
    B_y = gxy*Bx + gyy*By + gyz*Bz
    B_z = gxz*Bx + gyz*By + gzz*Bz

    vHBx = (vx + betax)/alpha
    vHBy = (vy + betay)/alpha
    vHBz = (vz + betaz)/alpha

    gij_vHBi_vHBj = gxx*vHBx*vHBx + 2.0*gxy*vHBx*vHBy + 2.0*gxz*vHBx*vHBz + gyy*vHBy*vHBy + 2.0*gyz*vHBy*vHBz + gzz*vHBz*vHBz

    Bsq = B_x*Bx + B_y*By + B_z*Bz
    B_dot_v = B_x*vHBx + B_y*vHBy + B_z*vHBz

    #detg4sph = -alpha*alpha*linalg.det(gsph)
    detg4sph = linalg.det(g4sph)
#    if (abs(detg4sph) < 1.0e-8):
#        print 'at theta = ', th_here,', and phi = ', ph_here, ', determinant is too small for inversion'

    measure = np.sqrt(-detg4sph)*dth*dph
 
    GAMMA_SPEED_LIMIT = 10.0
    ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED = 1.0 - 1.0/GAMMA_SPEED_LIMIT**2

    if(gij_vHBi_vHBj > 1.0):
        print 'at theta = ', th_here,', and phi = ', ph_here, ', u0 is ill-determined; fixing'
        correction_fac = np.sqrt(ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED/gij_vHBi_vHBj)
        vr  = (vr  + betar)*correction_fac  - betar
        vth = (vth + betath)*correction_fac - betath
        vph = (vph + betaph)*correction_fac - betaph
        gij_vHBi_vHBj = ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED

    wlor = 1.0/np.sqrt(1.0-gij_vHBi_vHBj)

    u0 = wlor/alpha

    ur  = u0*vr
    uth = u0*vth
    uph = u0*vph

    smallb2 = Bsq/wlor**2 + B_dot_v**2

    b0 = wlor*B_dot_v/alpha
    br  = Br/wlor  + B_dot_v*ur
    bth = Bth/wlor + B_dot_v*uth
    bph = Bph/wlor + B_dot_v*uph

    b_0  = g4sph[0][0]*b0 + g4sph[0][1]*br + g4sph[0][2]*bth + g4sph[0][3]*bph
    b_ph = g4sph[3][0]*b0 + g4sph[3][1]*br + g4sph[3][2]*bth + g4sph[3][3]*bph

    u_0  = g4sph[0][0]*u0 + g4sph[0][1]*ur + g4sph[0][2]*uth + g4sph[0][3]*uph
    u_ph = g4sph[3][0]*u0 + g4sph[3][1]*ur + g4sph[3][2]*uth + g4sph[3][3]*uph

    Gamma_poly = 1.33333333333333333333
    hfluid = 1.0 + P/rho*Gamma_poly/(Gamma_poly-1)

    mdot_integrand = rho*ur
    phib_integrand = 0.5*abs(Br)
    ldot_integrand = (rho*hfluid + smallb2)*u_ph*ur - b_ph*br
    edot_integrand = -(rho*hfluid + smallb2)*u_0*ur + b_0*br

    return mdot_integrand, phib_integrand, ldot_integrand, edot_integrand, measure

#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.

###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

parser = argparse.ArgumentParser(description='Produce fluxes.')
parser.add_argument("-s", "--singlefile", help="use a single file for both metadata and real data", action="store_true")
parser.add_argument("metafile", help="file with meta-info at the top")
parser.add_argument("datafile", help="main data file")
parser.add_argument("Rextract", help="extraction radius",type=float)
args = parser.parse_args()

metafile = args.metafile
datafile = args.datafile
r_target=args.Rextract

print "reading from ",datafile,", with grid information from ",metafile
print "extracting at (or just outside) R = ", r_target

with open(metafile,"rb") as f:

    Nr=struct.unpack('i',f.read(4))[0]
    Nth=struct.unpack('i',f.read(4))[0]
    Nph=struct.unpack('i',f.read(4))[0]
    print "Nr,Nth,Nph:",Nr,Nth,Nph
    r_min=struct.unpack('d',f.read(8))[0]
    r_max=struct.unpack('d',f.read(8))[0]
    print r_min,"<= r <=",r_max
    th_min=struct.unpack('d',f.read(8))[0]
    th_max=struct.unpack('d',f.read(8))[0]
    print th_min,"<= th <=",th_max
    ph_min=struct.unpack('d',f.read(8))[0]
    ph_max=struct.unpack('d',f.read(8))[0]
    print ph_min,"<= ph <=",ph_max

    dr=(r_max-r_min)/(Nr-1.0);
    rs=np.arange(r_min,r_max+dr,dr)
    dth=(th_max-th_min)/(Nth-1.0);
    ths=np.arange(th_min,th_max+dth,dth)
    dph=(ph_max-ph_min)/(Nph-1.0);
    phs=np.arange(ph_min,ph_max+dph,dph)
    magicnum=0
 
    # taking from thorn interpolator code
    # data col 0 = rho
    # data col 1 = P
    # data col 2 = vx
    # data col 3 = vy
    # data col 4 = vz
    # data col 5 = Bx
    # data col 6 = By
    # data col 7 = Bz

    # data col 8   = alp
    # data col 9   = betax
    # data col 10  = betay
    # data col 11  = betaz
    # data col 12  = gxx
    # data col 13  = gxy
    # data col 14  = gxz
    # data col 15  = gyy
    # data col 16  = gyz
    # data col 17  = gzz

    #print 'theta = ', ths
    #print 'phi = ',phs
    #print 'r = ',rs

    ii_extract = 0
    while (rs[ii_extract] < r_target):
        ii_extract = ii_extract+1

    r_extract = rs[ii_extract]

    print 'ideal extraction radius = ', r_target,'; next grid radius = ',r_extract 

    endofmeta = f.tell()
    print "position in file after meta information is", endofmeta

startofdata = 0
if args.singlefile:
    print "use the same file for meta and proper data"
    startofdata = endofmeta # continue where we left off
else:
    print "use different files for meta and proper data"
    startofdata = 0 # start from position 0 in file

print "starting from file position ", startofdata


with open(datafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(startofdata, 0)

    bytechunk=f.read(8)

    Nvar = 18 # added the whole metric

    full_timeslice_size = Nvar*Nph*Nth*Nr*8
    full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

    print 'full time slice should take ', full_timeslice_size_inc_meta, ' bytes, including iter and time'

    f1outname = "fluxes_r"+str(r_extract)+".txt"
    f1out = open(f1outname,'w',0)

    header_line = "#r = "+str(r_extract)+"\n"
    f1out.write(header_line)
    header_line = "#time\tMdot\t\tphiB\t\tLdot\t\tEdot\n"
    f1out.write(header_line)

    print "len bytechunk=",len(bytechunk)
    while (bytechunk):

        num=struct.unpack('d',bytechunk)[0]
        if(magicnum==0): 
            magicnum=num  
            print "magicnum=",magicnum
        elif(not magicnum==num):
            print"magicnum mismatch"
            print "got num=",num
            # return data

        this_iter = struct.unpack('i',f.read(4))[0]
        this_time = struct.unpack('d',f.read(8))[0]
        print "reading slab it = ", this_iter," time = ", this_time

        file_place = f.tell()            
        data_left = eof - file_place
        print "only ", data_left, " bytes left in file"

        if(data_left < full_timeslice_size_inc_meta):
            print "not enough data for another time slice --- sorry!"
            break

        bytechunk=f.read(full_timeslice_size)
        buffer_res = np.frombuffer(bytechunk)
        print 'size of buffer = ',buffer_res.size

        # rather than push this into a huge data array, do analysis on the data here
        this_data = buffer_res.reshape(Nvar,Nph,Nth,Nr)

        Mdot = 0.0
        phiB = 0.0
        Ldot = 0.0
        Edot = 0.0

        for jj in range(Nth):
            th_here = ths[jj]

            costh = cos(th_here)

            for kk in range(Nph):
                ph_here = phs[kk]

                mdot_term, phib_term, ldot_term, edot_term, measure = calcIntegrands(r_extract,th_here,ph_here,ii_extract,jj,kk,this_data)

                Mdot = Mdot + mdot_term*measure 
                phiB = phiB + phib_term*measure 
                Ldot = Ldot + ldot_term*measure 
                Edot = Edot + edot_term*measure 

        data_vec = [str(this_time),str(Mdot),str(phiB),str(Ldot),str(Edot)]
        data_line = "\t".join(data_vec)
        f1out.write(data_line)
        f1out.write('\n')

        bytechunk=f.read(8)


f1out.close()


#exit()

## note format of read-in files:
## data[time_idx][field_idx,phi_idx,theta_idx,r_idx]


