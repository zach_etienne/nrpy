from math import sin, cos, sqrt
import numpy as np
import struct
import matplotlib.pyplot as plt
import sys
from pylab import *

###################################################### 
###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

parser = argparse.ArgumentParser(description='Do colored 2D plots.')
parser.add_argument("datafile", help="main data file, in ASCII format")
parser.add_argument("outfileroot", help="root of output file names")
args = parser.parse_args()

datafile = args.datafile
outfileroot = args.outfileroot


rvals, thvals, rhos, betainvs, sigmas, temps = np.loadtxt(datafile,usecols=(0,1,2,3,4,5), unpack=True)

runique  = np.unique(rvals)
thunique = np.unique(thvals)

Nr = len(runique)
Nth = len(thunique)

print 'Nr = ', Nr,', and Nth = ', Nth,', implying Nr x Nth = ', Nr*Nth,' grid points '

Ndata = len(rhos)

print 'Actual # of grid points is ', Ndata

# reshape read-in data back into 2D array format

rvals_2D = np.reshape(rvals,(Nr,Nth))
thvals_2D = np.reshape(thvals,(Nr,Nth))

rhos_2D = np.reshape(rhos,(Nr,Nth))
betainvs_2D = np.reshape(betainvs,(Nr,Nth))
sigmas_2D = np.reshape(sigmas,(Nr,Nth))
temps_2D = np.reshape(temps,(Nr,Nth))

qvals_2D = rvals_2D*sin(thvals_2D)
zvals_2D = rvals_2D*cos(thvals_2D)

#print 'reshaped r values (1st row) are :', rvals_2D[0,:]
#print 'reshaped theta values (1st row) are :', thvals_2D[0,:]


plt.figure(figsize=(13,4))

plt.subplot(1,4,1)
plt.title(r'$\log_{10}\langle\rho\rangle$; Illinois')
plt.pcolor(qvals_2D,zvals_2D,np.log10(rhos_2D),cmap='jet', vmin=-7.0, vmax=0.0)
plt.colorbar()

plt.subplot(1,4,2)
plt.title(r'$\log_{10}\langle\beta^{-1}\rangle$; Illinois')
plt.pcolor(qvals_2D,zvals_2D,np.log10(betainvs_2D),cmap='plasma', vmin=-2.0, vmax=3.0)
plt.colorbar()

plt.subplot(1,4,3)
plt.title(r'$\log_{10}\langle\sigma\rangle$; Illinois')
plt.pcolor(qvals_2D,zvals_2D,np.log10(sigmas_2D),cmap='viridis', vmin=-3.0, vmax=3.0)
plt.colorbar()

plt.subplot(1,4,4)
plt.title(r'$\langle T \rangle$; Illinois')
#plt.pcolor(qvals_2D,zvals_2D,np.log10(temps_2D),cmap='viridis', vmin=-3.0, vmax=-1.0)
plt.pcolor(qvals_2D,zvals_2D,temps_2D,cmap='jet',vmax=0.03)
plt.colorbar()

#plt.show()

plt.savefig(framename)
plt.close(fig)

exit()
