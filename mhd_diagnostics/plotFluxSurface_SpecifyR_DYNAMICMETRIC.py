import matplotlib
matplotlib.use('Agg')
import numpy as np
import struct
import sys
import numpy.linalg as linalg
from diagnostic_utils import convertVUCart2Sph, convertTDDCart2Sph
import argparse
import matplotlib.pyplot as plt
import matplotlib.colors
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


def calcIntegrands(r_here,th_here,ph_here,ii,jj,kk,data_clump):

    rho = data_clump[0,kk,jj,ii]
    P   = data_clump[1,kk,jj,ii]
    vx  = data_clump[2,kk,jj,ii]
    vy  = data_clump[3,kk,jj,ii]
    vz  = data_clump[4,kk,jj,ii]
    Bx  = data_clump[5,kk,jj,ii]
    By  = data_clump[6,kk,jj,ii]
    Bz  = data_clump[7,kk,jj,ii]

    #u0  = data_clump[8,kk,jj,ii]
    
    gxx   = data_clump[9,kk,jj,ii]
    gxy   = data_clump[10,kk,jj,ii]
    gxz   = data_clump[11,kk,jj,ii]
    gyy   = data_clump[12,kk,jj,ii]
    gyz   = data_clump[13,kk,jj,ii]
    gzz   = data_clump[14,kk,jj,ii]

    betax = data_clump[15,kk,jj,ii]
    betay = data_clump[16,kk,jj,ii]
    betaz = data_clump[17,kk,jj,ii]

    alpha = data_clump[18,kk,jj,ii]

    #vVGammax = data_clump[19,kk,jj,ii]
    #vVGammay = data_clump[20,kk,jj,ii]
    #vVGammaz = data_clump[21,kk,jj,ii]
    #tau      = data_clump[22,kk,jj,ii]
    #rhostar  = data_clump[23,kk,jj,ii]

    adjustBmag = 1.0/np.sqrt(4.0*np.pi)

    Bx = Bx*adjustBmag
    By = By*adjustBmag
    Bz = Bz*adjustBmag

    betar, betath, betaph = convertVUCart2Sph(r_here,th_here,ph_here,betax,betay,betaz)
    grr, grth, grph, gthth, gthph, gphph = convertTDDCart2Sph(r_here,th_here,ph_here,gxx,gxy,gxz,gyy,gyz,gzz)

    beta_r  = grr*betar  + grth*betath  + grph*betaph
    beta_th = grth*betar + gthth*betath + gthph*betaph
    beta_ph = grph*betar + gthph*betath + gphph*betaph

    betasq = beta_r*betar + beta_th*betath + beta_ph*betaph

    g4sph = [[betasq - alpha*alpha, beta_r, beta_th, beta_ph],[beta_r,grr,grth,grph],[beta_th,grth,gthth,gthph],[beta_ph,grph,gthph,gphph]]

    vr, vth, vph = convertVUCart2Sph(r_here,th_here,ph_here,vx,vy,vz)
    Br, Bth, Bph = convertVUCart2Sph(r_here,th_here,ph_here,Bx,By,Bz)

    B_x = gxx*Bx + gxy*By + gxz*Bz
    B_y = gxy*Bx + gyy*By + gyz*Bz
    B_z = gxz*Bx + gyz*By + gzz*Bz

    vHBx = (vx + betax)/alpha
    vHBy = (vy + betay)/alpha
    vHBz = (vz + betaz)/alpha

    gij_vHBi_vHBj = gxx*vHBx*vHBx + 2.0*gxy*vHBx*vHBy + 2.0*gxz*vHBx*vHBz + gyy*vHBy*vHBy + 2.0*gyz*vHBy*vHBz + gzz*vHBz*vHBz

    Bsq = B_x*Bx + B_y*By + B_z*Bz
    B_dot_v = B_x*vHBx + B_y*vHBy + B_z*vHBz

    #detg4sph = -alpha*alpha*linalg.det(gsph)
    detg4sph = linalg.det(g4sph)
#    if (abs(detg4sph) < 1.0e-8):
#        print 'at theta = ', th_here,', and phi = ', ph_here, ', determinant is too small for inversion'

    measure = np.sqrt(-detg4sph)*dth*dph
 
    GAMMA_SPEED_LIMIT = 10.0
    ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED = 1.0 - 1.0/GAMMA_SPEED_LIMIT**2

    if(gij_vHBi_vHBj > 1.0):
        print 'at theta = ', th_here,', and phi = ', ph_here, ', u0 is ill-determined; fixing'
        correction_fac = np.sqrt(ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED/gij_vHBi_vHBj)
        vr  = (vr  + betar)*correction_fac  - betar
        vth = (vth + betath)*correction_fac - betath
        vph = (vph + betaph)*correction_fac - betaph
        gij_vHBi_vHBj = ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED

    wlor = 1.0/np.sqrt(1.0-gij_vHBi_vHBj)

    u0 = wlor/alpha

    ur  = u0*vr
    uth = u0*vth
    uph = u0*vph

    smallb2 = Bsq/wlor**2 + B_dot_v**2

    b0 = wlor*B_dot_v/alpha
    br  = Br/wlor  + B_dot_v*ur
    bth = Bth/wlor + B_dot_v*uth
    bph = Bph/wlor + B_dot_v*uph

    b_0  = g4sph[0][0]*b0 + g4sph[0][1]*br + g4sph[0][2]*bth + g4sph[0][3]*bph
    b_ph = g4sph[3][0]*b0 + g4sph[3][1]*br + g4sph[3][2]*bth + g4sph[3][3]*bph

    u_0  = g4sph[0][0]*u0 + g4sph[0][1]*ur + g4sph[0][2]*uth + g4sph[0][3]*uph
    u_ph = g4sph[3][0]*u0 + g4sph[3][1]*ur + g4sph[3][2]*uth + g4sph[3][3]*uph

    Gamma_poly = 1.33333333333333333333
    hfluid = 1.0 + P/rho*Gamma_poly/(Gamma_poly-1)

    mdot_integrand = rho*ur
    phib_integrand = 0.5*abs(Br)
    ldot_integrand = (rho*hfluid + smallb2)*u_ph*ur - b_ph*br
    edot_integrand = -(rho*hfluid + smallb2)*u_0*ur + b_0*br

    return mdot_integrand, phib_integrand, ldot_integrand, edot_integrand, measure

#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.

###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

parser = argparse.ArgumentParser(description='Produce fluxes.')
parser.add_argument("-s", "--singlefile", help="use a single file for both metadata and real data", action="store_true")
parser.add_argument("metafile", help="file with meta-info at the top")
parser.add_argument("datafile", help="main data file")
parser.add_argument("Rextract", help="extraction radius",type=float)
args = parser.parse_args()

metafile = args.metafile
datafile = args.datafile
r_target = args.Rextract

print "reading from ",datafile,", with grid information from ",metafile
print "extracting at (or just outside) R = ", r_target

plot_every = 10

with open(metafile,"rb") as f:

    Nr=struct.unpack('i',f.read(4))[0]
    Nth=struct.unpack('i',f.read(4))[0]
    Nph=struct.unpack('i',f.read(4))[0]
    print "Nr,Nth,Nph:",Nr,Nth,Nph
    r_min=struct.unpack('d',f.read(8))[0]
    r_max=struct.unpack('d',f.read(8))[0]
    print r_min,"<= r <=",r_max
    th_min=struct.unpack('d',f.read(8))[0]
    th_max=struct.unpack('d',f.read(8))[0]
    print th_min,"<= th <=",th_max
    ph_min=struct.unpack('d',f.read(8))[0]
    ph_max=struct.unpack('d',f.read(8))[0]
    print ph_min,"<= ph <=",ph_max

    dr=(r_max-r_min)/(Nr-1.0)
    rs=np.arange(r_min,r_max+dr,dr)
    dth=(th_max-th_min)/(Nth-1.0)
    ths=np.arange(th_min,th_max+dth,dth)
    dph=(ph_max-ph_min)/(Nph-1.0)
    phs=np.arange(ph_min,ph_max+dph,dph)
    magicnum=0
 
    # taking from thorn interpolator code
    # data col 0 = rho
    # data col 1 = P
    # data col 2 = vx
    # data col 3 = vy
    # data col 4 = vz
    # data col 5 = Bx
    # data col 6 = By
    # data col 7 = Bz

    # data col 8   = alp
    # data col 9   = betax
    # data col 10  = betay
    # data col 11  = betaz
    # data col 12  = gxx
    # data col 13  = gxy
    # data col 14  = gxz
    # data col 15  = gyy
    # data col 16  = gyz
    # data col 17  = gzz

    #print 'theta = ', ths
    #print 'phi = ',phs
    #print 'r = ',rs

    ii_extract = 0
    while (rs[ii_extract] < r_target):
        ii_extract = ii_extract+1

    r_extract = rs[ii_extract]

    print 'ideal extraction radius = ', r_target,'; next grid radius = ',r_extract 

    endofmeta = f.tell()
    print "position in file after meta information is", endofmeta

startofdata = 0
if args.singlefile:
    print "use the same file for meta and proper data"
    startofdata = endofmeta # continue where we left off
else:
    print "use different files for meta and proper data"
    startofdata = 0 # start from position 0 in file

print "starting from file position ", startofdata

data_slice_num = 0
with open(datafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(startofdata, 0)

    bytechunk=f.read(8)

    Nvar = 18 # added the whole metric

    full_timeslice_size = Nvar*Nph*Nth*Nr*8
    full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

    print 'full time slice should take ', full_timeslice_size_inc_meta, ' bytes, including iter and time'


    print "len bytechunk=",len(bytechunk)
    while (bytechunk):

        num=struct.unpack('d',bytechunk)[0]
        if(magicnum==0): 
            magicnum=num  
            print "magicnum=",magicnum
        elif(not magicnum==num):
            print"magicnum mismatch"
            print "got num=",num
            # return data

        this_iter = struct.unpack('i',f.read(4))[0]
        this_time = struct.unpack('d',f.read(8))[0]
        print "reading slab it = ", this_iter," time = ", this_time

        file_place = f.tell()            
        data_left = eof - file_place
        print "only ", data_left, " bytes left in file"

        if(data_left < full_timeslice_size_inc_meta):
            print "not enough data for another time slice --- sorry!"
            break

        bytechunk=f.read(full_timeslice_size)
        buffer_res = np.frombuffer(bytechunk)
        print 'size of buffer = ',buffer_res.size

        # rather than push this into a huge data array, do analysis on the data here
        this_data = buffer_res.reshape(Nvar,Nph,Nth,Nr)

        th_grid, ph_grid = np.meshgrid(ths, phs, indexing='ij')

        x_grid = r_extract*np.sin(th_grid)*np.cos(ph_grid)
        y_grid = r_extract*np.sin(th_grid)*np.sin(ph_grid)
        z_grid = r_extract*np.cos(th_grid)

        Mdot_grid = 0.0*th_grid
        phiB_grid = 0.0*th_grid
        Ldot_grid = 0.0*th_grid
        Edot_grid = 0.0*th_grid

        if (data_slice_num % plot_every == 0):

            for jj in range(Nth):

                for kk in range(Nph):
                    th_here = th_grid[jj,kk]
                    ph_here = ph_grid[jj,kk]

                    mdot_term, phib_term, ldot_term, edot_term, measure = calcIntegrands(r_extract,th_here,ph_here,ii_extract,jj,kk,this_data)

                    Mdot_grid[jj][kk] = mdot_term
                    phiB_grid[jj][kk] = phib_term
                    Ldot_grid[jj][kk] = ldot_term
                    Edot_grid[jj][kk] = edot_term

            f_max =   0.0
            f_min =  -5.0 #-20.0 for uniform domain
            b = 1.0/(f_max-f_min)
            a = -b*f_min

            #f_grid = np.abs(Mdot_grid)
            #fnorm = f_grid/f_grid.max()
            f_norm = a + b*Mdot_grid

            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')

            timename = str(this_time)
            titlestring = " ".join(['t = ',timename])
            ax.set_title(titlestring)

            pltnorm = matplotlib.colors.Normalize(vmin=f_min, vmax=f_max) 
            ax.plot_surface(x_grid, y_grid, z_grid, rstride=1, cstride=1, facecolors=cm.plasma_r(f_norm))

            m = cm.ScalarMappable(cmap=plt.cm.plasma_r, norm=pltnorm)
            m.set_array([])
            plt.colorbar(m)

            iter_label = '{0:06d}'.format(this_iter)
            framename = "".join(['frame_',iter_label,'.png'])
            plt.savefig(framename)
            plt.close(fig)

        # end if (data_slice_num % plot_every == 0)

        data_slice_num = data_slice_num + 1
        bytechunk=f.read(8)




#exit()

## note format of read-in files:
## data[time_idx][field_idx,phi_idx,theta_idx,r_idx]


