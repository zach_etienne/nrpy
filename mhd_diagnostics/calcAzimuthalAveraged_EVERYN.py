from math import sin, cos, sqrt
import numpy as np
import struct
import sys
import numpy.linalg as linalg
import os
from diagnostic_utils import compute_KS_metric_sph, convertVUCart2Sph
import argparse

def calcIntegrands(r_here,th_here,ph_here,ii,jj,kk,data_clump,g4sph,g4sphUU):

    alphasq = -1.0/g4sphUU[0][0]
    alpha = np.sqrt(alphasq)
    betar   = alphasq*g4sphUU[0][1]
    betath  = alphasq*g4sphUU[0][2]
    betaph  = alphasq*g4sphUU[0][3]

    rho = data_clump[0,kk,jj,ii]
    P   = data_clump[1,kk,jj,ii]
    vx  = data_clump[2,kk,jj,ii]
    vy  = data_clump[3,kk,jj,ii]
    vz  = data_clump[4,kk,jj,ii]
    Bx  = data_clump[5,kk,jj,ii]
    By  = data_clump[6,kk,jj,ii]
    Bz  = data_clump[7,kk,jj,ii]

    adjustBmag = 1.0/np.sqrt(4.0*np.pi)

    Bx = Bx*adjustBmag
    By = By*adjustBmag
    Bz = Bz*adjustBmag

    Gamma_poly = 1.33333333333333333333
    hfluid = 1.0 + P/rho*Gamma_poly/(Gamma_poly-1)

    vr, vth, vph = convertVUCart2Sph(r_here,th_here,ph_here,vx,vy,vz)
    Br, Bth, Bph = convertVUCart2Sph(r_here,th_here,ph_here,Bx,By,Bz)

    B_r = g4sph[1][1]*Br + g4sph[1][2]*Bth + g4sph[1][3]*Bph
    B_th = g4sph[2][1]*Br + g4sph[2][2]*Bth + g4sph[2][3]*Bph
    B_ph = g4sph[3][1]*Br + g4sph[3][2]*Bth + g4sph[3][3]*Bph

    vHBr  = (vr  + betar)/alpha
    vHBth = (vth + betath)/alpha
    vHBph = (vph + betaph)/alpha

    gij_vHBi_vHBj = g4sph[1][1]*vHBr*vHBr + 2.0*g4sph[1][2]*vHBr*vHBth + 2.0*g4sph[1][3]*vHBr*vHBph + g4sph[2][2]*vHBth*vHBth + 2.0*g4sph[2][3]*vHBth*vHBph + g4sph[3][3]*vHBph*vHBph

    Bsq = B_r*Br + B_th*Bth + B_ph*Bph
    B_dot_v = B_r*vHBr + B_th*vHBth + B_ph*vHBph

    measure = dph # used here because not integrating over theta
 
    GAMMA_SPEED_LIMIT = 10.0
    ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED = 1.0 - 1.0/GAMMA_SPEED_LIMIT**2

    if(gij_vHBi_vHBj > 1.0):
        print 'at theta = ', th_here,', and phi = ', ph_here, ', u0 is ill-determined; fixing'
        correction_fac = np.sqrt(ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED/gij_vHBi_vHBj)
        vr  = (vr  + betar)*correction_fac  - betar
        vth = (vth + betath)*correction_fac - betath
        vph = (vph + betaph)*correction_fac - betaph
        gij_vHBi_vHBj = ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED

    wlor = 1.0/np.sqrt(1.0-gij_vHBi_vHBj)

    u0 = wlor/alpha

    uph = u0*vph

    smallb2 = Bsq/wlor**2 + B_dot_v**2
    bmag = np.sqrt(smallb2)

    Pmag = smallb2/2.0
    betainv = Pmag/P
    sigma = smallb2/rho
    temp = P/rho

    return rho, betainv, sigma, temp, measure

#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.

###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

# hard-coded mass & spin
M_BH=1
chi_BH=0.9375
#M_BH=0.97
#chi_BH=0.69


parser = argparse.ArgumentParser(description='Do azimuthal & time averaging of fields of interest.')
parser.add_argument("-s", "--singlefile", help="use a single file for both metadata and real data", action="store_true")
parser.add_argument("metafile", help="file with meta-info at the top")
parser.add_argument("datafile", help="main data file")
parser.add_argument("start_time", help="starting time",type=float)
parser.add_argument("final_time", help="ending time",type=float)
parser.add_argument("output_iter", help="output cadence",type=int)
args = parser.parse_args()

metafile = args.metafile
datafile = args.datafile
start_time = args.start_time
final_time = args.final_time
output_iter = args.output_iter

print "reading from ",datafile,", with grid information from ",metafile
print "starting at time ", start_time
print "finishing at time ", final_time
print "outputting every ",output_iter," iterations"

with open(metafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(0, 0)

    Nr=struct.unpack('i',f.read(4))[0]
    Nth=struct.unpack('i',f.read(4))[0]
    Nph=struct.unpack('i',f.read(4))[0]
    print "Nr,Nth,Nph:",Nr,Nth,Nph
    r_min=struct.unpack('d',f.read(8))[0]
    r_max=struct.unpack('d',f.read(8))[0]
    print r_min,"<= r <=",r_max
    th_min=struct.unpack('d',f.read(8))[0]
    th_max=struct.unpack('d',f.read(8))[0]
    print th_min,"<= th <=",th_max
    ph_min=struct.unpack('d',f.read(8))[0]
    ph_max=struct.unpack('d',f.read(8))[0]
    print ph_min,"<= ph <=",ph_max

    dr=(r_max-r_min)/(Nr-1.0);
    rs=np.arange(r_min,r_max+dr,dr)
    dth=(th_max-th_min)/(Nth-1.0);
    ths=np.arange(th_min,th_max+dth,dth)
    dph=(ph_max-ph_min)/(Nph-1.0);
    phs=np.arange(ph_min,ph_max+dph,dph)
    magicnum=0
 
    # taking from thorn interpolator code
    # data col 0 = rho
    # data col 1 = P
    # data col 2 = vx
    # data col 3 = vy
    # data col 4 = vz
    # data col 5 = Bx
    # data col 6 = By
    # data col 7 = Bz
    # data col 8 = u0IGM

    #print 'theta = ', ths
    #print 'phi = ',phs
    #print 'r = ',rs

    endofmeta = f.tell()
    print "position in file after meta information is", endofmeta

g4KSsph = [[[[] for kk in range(Nph)] for jj in range(Nth)] for ii in range(Nr)]

g4KSsphUU = [[[[] for kk in range(Nph)] for jj in range(Nth)] for ii in range(Nr)]

for ii in range(Nr):
    r_here = rs[ii]

    for jj in range(Nth):
        th_here = ths[jj]

        for kk in range(Nph):
            ph_here = phs[kk]

            g4KSsph[ii][jj][kk], g4KSsphUU[ii][jj][kk] = compute_KS_metric_sph(r_here,th_here,ph_here,M_BH,chi_BH)

startofdata = 0
if args.singlefile:
    print "use the same file for meta and proper data"
    startofdata = endofmeta # continue where we left off
else:
    print "use different files for meta and proper data"
    startofdata = 0 # start from position 0 in file

print "starting from file position ", startofdata

rho_azimuth     = np.zeros(shape=(Nr,Nth))
betainv_azimuth = np.zeros(shape=(Nr,Nth))
sigma_azimuth   = np.zeros(shape=(Nr,Nth))
temp_azimuth    = np.zeros(shape=(Nr,Nth))

Nt_used = 0

Nvar = 22 # was 8 before u0IGM was added

full_timeslice_size = Nvar*Nph*Nth*Nr*8
full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

print 'full time slice should take ', full_timeslice_size_inc_meta, ' bytes, including iter and time'

with open(datafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(startofdata, 0)

    bytechunk=f.read(8)
    print "len bytechunk=",len(bytechunk)

    while (bytechunk):

        num=struct.unpack('d',bytechunk)[0]
        if(magicnum==0): 
            magicnum=num  
            print "magicnum=",magicnum
        elif(not magicnum==num):
            print"magicnum mismatch"
            print "got num=",num
            # return data

        this_iter = struct.unpack('i',f.read(4))[0]
        this_time = struct.unpack('d',f.read(8))[0]
        print "reading slab it = ", this_iter," time = ", this_time

        file_place = f.tell()            
        data_left = eof - file_place
        print "only ", data_left, " bytes left in file"

        if(data_left < full_timeslice_size_inc_meta):
            print "not enough data for another time slice --- sorry!"
            break

        if( (this_time < start_time) or (this_time > final_time) or (this_iter%output_iter != 0)):
            print "too early --- skipping to next time step"
            f.seek(file_place+full_timeslice_size)
            #now want to skip processing this time step ...
        else:
            print "NOT too early --- using this time step"

            bytechunk=f.read(full_timeslice_size)
            buffer_res = np.frombuffer(bytechunk)
            print 'size of buffer = ',buffer_res.size

            # rather than push this into a huge data array, do analysis on the data here
            this_data = buffer_res.reshape(Nvar,Nph,Nth,Nr)

            for ii in range(Nr):
                r_here = rs[ii]

                for jj in range(Nth):
                    th_here = ths[jj]

                    rho_int     = 0.0
                    betainv_int = 0.0
                    sigma_int   = 0.0
                    temp_int    = 0.0

                    for kk in range(Nph):
                        ph_here = phs[kk]

                        #g4KSsph, g4KSsphUU = compute_KS_metric_sph(r_here,th_here,ph_here,M_BH,chi_BH)

                        rho, betainv, sigma, temp, measure = calcIntegrands(r_here,th_here,ph_here,ii,jj,kk,this_data,g4KSsph[ii][jj][kk],g4KSsphUU[ii][jj][kk])

                        rho_int     = rho_int     + rho*measure
                        betainv_int = betainv_int + betainv*measure
                        sigma_int   = sigma_int   + sigma*measure
                        temp_int    = temp_int    + temp*measure

                    # END OF LOOP for kk in range(Nph)

                    rho_azimuth[ii,jj]     = rho_azimuth[ii,jj]     + rho_int
                    betainv_azimuth[ii,jj] = betainv_azimuth[ii,jj] + betainv_int
                    sigma_azimuth[ii,jj]   = sigma_azimuth[ii,jj]   + sigma_int
                    temp_azimuth[ii,jj]    = temp_azimuth[ii,jj]    + temp_int

                # END OF LOOP for jj in range(Nth)

            # END OF LOOP for ii in range(Nr)

            Nt_used = Nt_used + 1

        # END OF IF (this_time < recover_time)

        bytechunk=f.read(8)

    # end of reading in data chunk by chunk

    fout = open("azimuthal_averages_2D.txt",'w',0)
    header_line = "#r\ttheta\trho_avg\t\tbetainv_avg\t\tsigma_avg\t\ttemp_avg\n"
    fout.write(header_line)

    for ii in range(Nr):
        r_here = rs[ii]

        for jj in range(Nth):
            th_here = ths[jj]

            rho_avg_here     = rho_azimuth[ii,jj]/Nt_used/2.0/np.pi
            betainv_avg_here = betainv_azimuth[ii,jj]/Nt_used/2.0/np.pi
            sigma_avg_here   = sigma_azimuth[ii,jj]/Nt_used/2.0/np.pi
            temp_avg_here    = temp_azimuth[ii,jj]/Nt_used/2.0/np.pi

            data_vec = [str(r_here),str(th_here),str(rho_avg_here),str(betainv_avg_here),str(sigma_avg_here),str(temp_avg_here)]
            data_line = "\t".join(data_vec)
            fout.write(data_line)
            fout.write('\n')

        fout.write('\n')

    fout.close()


#exit()

## note format of read-in files:
## data[time_idx][field_idx,phi_idx,theta_idx,r_idx]
