from math import sin, cos, sqrt
import numpy as np
import struct
import sys
import numpy.linalg as linalg
from diagnostic_utils import compute_KS_metric_sph, convertVUCart2Sph

def calcPrimitives(r_here,th_here,ph_here,ii,jj,kk,data_clump,g4sph,g4sphUU):

    alphasq = -1.0/g4sphUU[0][0]
    alpha = np.sqrt(alphasq)
    betar  = alphasq*g4sphUU[0][1]
    betath = alphasq*g4sphUU[0][2]
    betaph = alphasq*g4sphUU[0][3]

    rho = data_clump[0,kk,jj,ii]
    P   = data_clump[1,kk,jj,ii]
    vx  = data_clump[2,kk,jj,ii]
    vy  = data_clump[3,kk,jj,ii]
    vz  = data_clump[4,kk,jj,ii]
    Bx  = data_clump[5,kk,jj,ii]
    By  = data_clump[6,kk,jj,ii]
    Bz  = data_clump[7,kk,jj,ii]

    vr, vth, vph = convertVUCart2Sph(r_here,th_here,ph_here,vx,vy,vz)
    Br, Bth, Bph = convertVUCart2Sph(r_here,th_here,ph_here,Bx,By,Bz)

    return rho, P, vr, vth, vph, Br, Bth, Bph

#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.

###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

# hard-coded mass & spin
M_BH=1
chi_BH=0.9375
#M_BH=0.97
#chi_BH=0.69


chi_temp = 1.0+np.sqrt(1.0-chi_BH**2)
#rh_sq = 2.0*M**2*( chi**2*chi_temp + (4.0*chi_temp - chi**2*(2.0+chi_temp))*costh**2 )/( chi**2 + 2.0*(2.0-chi**2)*costh**2 + chi**2*costh**4 )
#rh = np.sqrt(rh_sq)
rh = M_BH*chi_temp
#TEMP ...
print 'rh = ', rh


metafile=sys.argv[1]
datafile=sys.argv[2]

print "reading from ",datafile,", with grid information from ",metafile

start_time = float(sys.argv[3])
end_time = float(sys.argv[4])
print "outputting in time window [", start_time,", ", end_time, "]"

with open(metafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(0, 0)

    Nr=struct.unpack('i',f.read(4))[0]
    Nth=struct.unpack('i',f.read(4))[0]
    Nph=struct.unpack('i',f.read(4))[0]
    print "Nr,Nth,Nph:",Nr,Nth,Nph
    r_min=struct.unpack('d',f.read(8))[0]
    r_max=struct.unpack('d',f.read(8))[0]
    print r_min,"<= r <=",r_max
    th_min=struct.unpack('d',f.read(8))[0]
    th_max=struct.unpack('d',f.read(8))[0]
    print th_min,"<= th <=",th_max
    ph_min=struct.unpack('d',f.read(8))[0]
    ph_max=struct.unpack('d',f.read(8))[0]
    print ph_min,"<= ph <=",ph_max

    dr=(r_max-r_min)/(Nr-1.0);
    rs=np.arange(r_min,r_max+dr,dr)
    dth=(th_max-th_min)/(Nth-1.0);
    ths=np.arange(th_min,th_max+dth,dth)
    dph=(ph_max-ph_min)/(Nph-1.0);
    phs=np.arange(ph_min,ph_max+dph,dph)
    magicnum=0
 
    # taking from thorn interpolator code
    # data col 0 = rho
    # data col 1 = P
    # data col 2 = vx
    # data col 3 = vy
    # data col 4 = vz
    # data col 5 = Bx
    # data col 6 = By
    # data col 7 = Bz

    #print 'theta = ', ths
    #print 'phi = ',phs
    #print 'r = ',rs

    ii_r1 = 0
    while (rs[ii_r1] < rh):
        ii_r1 = ii_r1+1

    r1 = rs[ii_r1]
    r2 = rs[ii_r1+1]
    r3 = rs[ii_r1+2]

    print 'horizon radius = ', rh,'; next grid radius = ',r1 

    g4_r1_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]
    g4_r2_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]
    g4_r3_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]

    g4UU_r1_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]
    g4UU_r2_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]
    g4UU_r3_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]

    for jj in range(Nth):
        th_here = ths[jj]

        for kk in range(Nph):
            ph_here = phs[kk]

            g4_r1_arr[jj][kk], g4UU_r1_arr[jj][kk] = compute_KS_metric_sph(r1,th_here,ph_here,M_BH,chi_BH)
            g4_r2_arr[jj][kk], g4UU_r2_arr[jj][kk] = compute_KS_metric_sph(r2,th_here,ph_here,M_BH,chi_BH)
            g4_r3_arr[jj][kk], g4UU_r3_arr[jj][kk] = compute_KS_metric_sph(r3,th_here,ph_here,M_BH,chi_BH)

with open(datafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(0, 0)

    bytechunk=f.read(8)

    Nvar = 8

    full_timeslice_size = Nvar*Nph*Nth*Nr*8
    full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

    print 'full time slice should take ', full_timeslice_size_inc_meta, ' bytes, including iter and time'

    print "len bytechunk=",len(bytechunk)
    while (bytechunk):

        num=struct.unpack('d',bytechunk)[0]
        if(magicnum==0): 
            magicnum=num  
            print "magicnum=",magicnum
        elif(not magicnum==num):
            print"magicnum mismatch"
            print "got num=",num
            # return data

        this_iter = struct.unpack('i',f.read(4))[0]
        this_time = struct.unpack('d',f.read(8))[0]
        print "reading slab it = ", this_iter," time = ", this_time

        file_place = f.tell()            
        data_left = eof - file_place
        print "only ", data_left, " bytes left in file"

        if(data_left < full_timeslice_size_inc_meta):
            print "not enough data for another time slice --- sorry!"
            break


        if( (this_time < start_time) or (this_time > end_time) ):
            print "time not in window --- skipping to next time step"
            f.seek(file_place+full_timeslice_size)
            #now want to skip processing this time step ...
        else:
            print "time in window --- using this time step"
            bytechunk=f.read(full_timeslice_size)
            buffer_res = np.frombuffer(bytechunk)
            print 'size of buffer = ',buffer_res.size

            # rather than push this into a huge data array, do analysis on the data here
            this_data = buffer_res.reshape(Nvar,Nph,Nth,Nr)

            f1out2Dname = "primitives_2D_it"+str(this_iter)+"_r"+str(r1)+".asc"
            f1out = open(f1out2Dname,'w',0)
            header_line = "#theta\t\tphi\t\trho\t\tP\t\tBr\t\tBth\t\tBphi\ur\t\tuth\t\tuphi\n"
            f1out.write(header_line)

            f2out2Dname = "primitives_2D_it"+str(this_iter)+"_r"+str(r2)+".asc"
            f2out = open(f2out2Dname,'w',0)
            header_line = "#theta\t\tphi\t\trho\t\tP\t\tBr\t\tBth\t\tBphi\ur\t\tuth\t\tuphi\n"
            f2out.write(header_line)

            f3out2Dname = "primitives_2D_it"+str(this_iter)+"_r"+str(r3)+".asc"
            f3out = open(f3out2Dname,'w',0)
            header_line = "#theta\t\tphi\t\trho\t\tP\t\tBr\t\tBth\t\tBphi\ur\t\tuth\t\tuphi\n"
            f3out.write(header_line)

            for jj in range(Nth):
                th_here = ths[jj]

                costh = cos(th_here)

                for kk in range(Nph):
                    ph_here = phs[kk]

                    rho_r1, P_r1, vr_r1, vth_r1, vph_r1, Br_r1, Bth_r1, Bph_r1 = calcPrimitives(r1,th_here,ph_here,ii_r1,jj,kk,this_data,g4_r1_arr[jj][kk],g4UU_r1_arr[jj][kk])
                    rho_r2, P_r2, vr_r2, vth_r2, vph_r2, Br_r2, Bth_r2, Bph_r2 = calcPrimitives(r2,th_here,ph_here,ii_r1+1,jj,kk,this_data,g4_r2_arr[jj][kk],g4UU_r2_arr[jj][kk])
                    rho_r3, P_r3, vr_r3, vth_r3, vph_r3, Br_r3, Bth_r3, Bph_r3 = calcPrimitives(r3,th_here,ph_here,ii_r1+2,jj,kk,this_data,g4_r3_arr[jj][kk],g4UU_r3_arr[jj][kk])

                    data_vec = [str(th_here),str(ph_here),str(rho_r1),str(P_r1),str(vr_r1),str(vth_r1),str(vph_r1),str(Br_r1),str(Bth_r1),str(Bph_r1)]
                    data_line = "\t".join(data_vec)
                    f1out.write(data_line)
                    f1out.write('\n')

                    data_vec = [str(th_here),str(ph_here),str(rho_r2),str(P_r2),str(vr_r2),str(vth_r2),str(vph_r2),str(Br_r2),str(Bth_r2),str(Bph_r2)]
                    data_line = "\t".join(data_vec)
                    f2out.write(data_line)
                    f2out.write('\n')

                    data_vec = [str(th_here),str(ph_here),str(rho_r3),str(P_r3),str(vr_r3),str(vth_r3),str(vph_r3),str(Br_r3),str(Bth_r3),str(Bph_r3)]
                    data_line = "\t".join(data_vec)
                    f3out.write(data_line)
                    f3out.write('\n')

                f1out.write('\n')
                f2out.write('\n')
                f3out.write('\n')

            f1out.close()
            f2out.close()
            f3out.close()

        # end if(this_time<start_time):

        bytechunk=f.read(8)


#exit()
