from math import sin, cos, sqrt
import numpy as np
import struct
import sys
import numpy.linalg as linalg
from diagnostic_utils import compute_KS_metric_sph, convertVUCart2Sph
import argparse

def calcIntegrands(r_here,th_here,ph_here,ii,jj,kk,data_clump,g4sph,g4sphUU):

    alphasq = -1.0/g4sphUU[0][0]
    alpha = np.sqrt(alphasq)
    betar   = alphasq*g4sphUU[0][1]
    betath  = alphasq*g4sphUU[0][2]
    betaph  = alphasq*g4sphUU[0][3]

    rho = data_clump[0,kk,jj,ii]
    P   = data_clump[1,kk,jj,ii]
    vx  = data_clump[2,kk,jj,ii]
    vy  = data_clump[3,kk,jj,ii]
    vz  = data_clump[4,kk,jj,ii]
    Bx  = data_clump[5,kk,jj,ii]
    By  = data_clump[6,kk,jj,ii]
    Bz  = data_clump[7,kk,jj,ii]

    adjustBmag = 1.0/np.sqrt(4.0*np.pi)

    Bx = Bx*adjustBmag
    By = By*adjustBmag
    Bz = Bz*adjustBmag

    vr, vth, vph = convertVUCart2Sph(r_here,th_here,ph_here,vx,vy,vz)
    Br, Bth, Bph = convertVUCart2Sph(r_here,th_here,ph_here,Bx,By,Bz)

    B_r  = g4sph[1][1]*Br  + g4sph[1][2]*Bth  + g4sph[1][3]*Bph
    B_th = g4sph[1][2]*Br + g4sph[2][2]*Bth + g4sph[2][3]*Bph
    B_ph = g4sph[1][3]*Br + g4sph[2][3]*Bth + g4sph[3][3]*Bph

    vHBr  = (vr  + betar)/alpha
    vHBth = (vth + betath)/alpha
    vHBph = (vph + betaph)/alpha

    gij_vHBi_vHBj = g4sph[1][1]*vHBr*vHBr + 2.0*g4sph[1][2]*vHBr*vHBth + 2.0*g4sph[1][3]*vHBr*vHBph + g4sph[2][2]*vHBth*vHBth + 2.0*g4sph[2][3]*vHBth*vHBph + g4sph[3][3]*vHBph*vHBph

    Bsq = B_r*Br + B_th*Bth + B_ph*Bph
    B_dot_v = B_r*vHBr + B_th*vHBth + B_ph*vHBph

    detg4sph = linalg.det(g4sph)
    measure = np.sqrt(-detg4sph)*dth*dph # this asymptotes to r^2 sin theta dth dph == r^2 dOmega
 
    GAMMA_SPEED_LIMIT = 10.0
    ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED = 1.0 - 1.0/GAMMA_SPEED_LIMIT**2

    if(gij_vHBi_vHBj > 1.0):
        print 'at theta = ', th_here,', and phi = ', ph_here, ', u0 is ill-determined; fixing'
        correction_fac = np.sqrt(ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED/gij_vHBi_vHBj)
        vr  = (vr  + betar)*correction_fac  - betar
        vth = (vth + betath)*correction_fac - betath
        vph = (vph + betaph)*correction_fac - betaph
        gij_vHBi_vHBj = ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED

    wlor = 1.0/np.sqrt(1.0-gij_vHBi_vHBj)

    u0 = wlor/alpha

    ur  = u0*vr
    uth = u0*vth
    uph = u0*vph

    smallb2 = Bsq/wlor**2 + B_dot_v**2

    b0 = wlor*B_dot_v/alpha
    br  = Br/wlor  + B_dot_v*ur
    bth = Bth/wlor + B_dot_v*uth
    bph = Bph/wlor + B_dot_v*uph

    b_0   = g4sph[0][0]*b0 + g4sph[0][1]*br + g4sph[0][2]*bth + g4sph[0][3]*bph
    b_r   = g4sph[1][0]*b0 + g4sph[1][1]*br + g4sph[1][2]*bth + g4sph[1][3]*bph
    b_th  = g4sph[2][0]*b0 + g4sph[1][2]*br + g4sph[2][2]*bth + g4sph[2][3]*bph
    b_ph  = g4sph[3][0]*b0 + g4sph[1][3]*br + g4sph[2][3]*bth + g4sph[3][3]*bph

    u_0  = g4sph[0][0]*u0 + g4sph[0][1]*ur + g4sph[0][2]*uth + g4sph[0][3]*uph

    Poynr_integrand = -(smallb2*ur*u_0 - br*b_0)

    return Poynr_integrand, measure


#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.

###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

M_BH = 0.97
chi_BH = 0.69

parser = argparse.ArgumentParser(description='Produce fluxes.')
parser.add_argument("-s", "--singlefile", help="use a single file for both metadata and real data", action="store_true")
parser.add_argument("metafile", help="file with meta-info at the top")
parser.add_argument("datafile", help="main data file")
parser.add_argument("Rextract", help="extraction radius",type=float)
args = parser.parse_args()

metafile = args.metafile
datafile = args.datafile
r_target=args.Rextract

print "reading from ",datafile,", with grid information from ",metafile
print "extracting at (or just outside) R = ", r_target

with open(metafile,"rb") as f:

    Nr=struct.unpack('i',f.read(4))[0]
    Nth=struct.unpack('i',f.read(4))[0]
    Nph=struct.unpack('i',f.read(4))[0]
    print "Nr,Nth,Nph:",Nr,Nth,Nph
    r_min=struct.unpack('d',f.read(8))[0]
    r_max=struct.unpack('d',f.read(8))[0]
    print r_min,"<= r <=",r_max
    th_min=struct.unpack('d',f.read(8))[0]
    th_max=struct.unpack('d',f.read(8))[0]
    print th_min,"<= th <=",th_max
    ph_min=struct.unpack('d',f.read(8))[0]
    ph_max=struct.unpack('d',f.read(8))[0]
    print ph_min,"<= ph <=",ph_max

    dr=(r_max-r_min)/(Nr-1.0);
    rs=np.arange(r_min,r_max+dr,dr)
    dth=(th_max-th_min)/(Nth-1.0);
    ths=np.arange(th_min,th_max+dth,dth)
    dph=(ph_max-ph_min)/(Nph-1.0);
    phs=np.arange(ph_min,ph_max+dph,dph)
    magicnum=0
 
    # taking from thorn interpolator code
    # data col 0 = rho
    # data col 1 = P
    # data col 2 = vx
    # data col 3 = vy
    # data col 4 = vz
    # data col 5 = Bx
    # data col 6 = By
    # data col 7 = Bz

    #print 'theta = ', ths
    #print 'phi = ',phs
    #print 'r = ',rs

    ii_extract = 0
    while (rs[ii_extract] < r_target):
        ii_extract = ii_extract+1

    r_extract = rs[ii_extract]

    print 'ideal extraction radius = ', r_target,'; next grid radius = ',r_extract 

    endofmeta = f.tell()
    print "position in file after meta information is", endofmeta

startofdata = 0
if args.singlefile:
    print "use the same file for meta and proper data"
    startofdata = endofmeta # continue where we left off
else:
    print "use different files for meta and proper data"
    startofdata = 0 # start from position 0 in file

print "starting from file position ", startofdata


with open(datafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(startofdata, 0)

    bytechunk=f.read(8)

    Nvar = 18 # added the whole metric

    full_timeslice_size = Nvar*Nph*Nth*Nr*8
    full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

    print 'full time slice should take ', full_timeslice_size_inc_meta, ' bytes, including iter and time'

    f1outname = "LPoynt_r"+str(r_extract)+".txt"
    f1out = open(f1outname,'w',0)

    header_line = "#r = "+str(r_extract)+"\n"
    f1out.write(header_line)
    header_line = "#time\tLPoynt\n"
    f1out.write(header_line)

    print "len bytechunk=",len(bytechunk)
    while (bytechunk):

        num=struct.unpack('d',bytechunk)[0]
        if(magicnum==0): 
            magicnum=num  
            print "magicnum=",magicnum
        elif(not magicnum==num):
            print"magicnum mismatch"
            print "got num=",num
            # return data

        this_iter = struct.unpack('i',f.read(4))[0]
        this_time = struct.unpack('d',f.read(8))[0]
        print "reading slab it = ", this_iter," time = ", this_time

        file_place = f.tell()            
        data_left = eof - file_place
        print "only ", data_left, " bytes left in file"

        if(data_left < full_timeslice_size_inc_meta):
            print "not enough data for another time slice --- sorry!"
            break

        bytechunk=f.read(full_timeslice_size)
        buffer_res = np.frombuffer(bytechunk)
        print 'size of buffer = ',buffer_res.size

        # rather than push this into a huge data array, do analysis on the data here
        this_data = buffer_res.reshape(Nvar,Nph,Nth,Nr)

        LPoynt = 0.0

        for jj in range(Nth):
            th_here = ths[jj]

            for kk in range(Nph):
                ph_here = phs[kk]

                g4KSsph, g4KSsphUU = compute_KS_metric_sph(r_extract,th_here,ph_here,M_BH,chi_BH)
                Poyntr_here, measure_here = calcIntegrands(r_extract,th_here,ph_here,ii_extract,jj,kk,this_data,g4KSsph,g4KSsphUU)

                LPoynt = LPoynt + Poyntr_here*measure_here 

        data_vec = [str(this_time),str(LPoynt)]
        data_line = "\t".join(data_vec)
        f1out.write(data_line)
        f1out.write('\n')

        bytechunk=f.read(8)


f1out.close()


#exit()


