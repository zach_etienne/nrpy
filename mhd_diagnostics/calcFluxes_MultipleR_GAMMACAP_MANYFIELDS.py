from math import sin, cos, sqrt
import numpy as np
import struct
import sys
import numpy.linalg as linalg
from diagnostic_utils import compute_KS_metric_sph, convertVUCart2Sph
import argparse

def calcIntegrands(r_here,th_here,ph_here,ii,jj,kk,data_clump,g4sph,g4sphUU):

    alphasq = -1.0/g4sphUU[0][0]
    alpha = np.sqrt(alphasq)
    betar   = alphasq*g4sphUU[0][1]
    betath  = alphasq*g4sphUU[0][2]
    betaph  = alphasq*g4sphUU[0][3]

    rho = data_clump[0,kk,jj,ii]
    P   = data_clump[1,kk,jj,ii]
    vx  = data_clump[2,kk,jj,ii]
    vy  = data_clump[3,kk,jj,ii]
    vz  = data_clump[4,kk,jj,ii]
    Bx  = data_clump[5,kk,jj,ii]
    By  = data_clump[6,kk,jj,ii]
    Bz  = data_clump[7,kk,jj,ii]
    #u0IGM  = data_clump[8,kk,jj,ii]

    adjustBmag = 1.0/np.sqrt(4.0*np.pi)

    Bx = Bx*adjustBmag
    By = By*adjustBmag
    Bz = Bz*adjustBmag

    Gamma_poly = 1.33333333333333333333
    hfluid = 1.0 + P/rho*Gamma_poly/(Gamma_poly-1)

    vr, vth, vph = convertVUCart2Sph(r_here,th_here,ph_here,vx,vy,vz)
    Br, Bth, Bph = convertVUCart2Sph(r_here,th_here,ph_here,Bx,By,Bz)

    B_r = g4sph[1][1]*Br + g4sph[1][2]*Bth + g4sph[1][3]*Bph
    B_th = g4sph[2][1]*Br + g4sph[2][2]*Bth + g4sph[2][3]*Bph
    B_ph = g4sph[3][1]*Br + g4sph[3][2]*Bth + g4sph[3][3]*Bph

    vHBr  = (vr  + betar)/alpha
    vHBth = (vth + betath)/alpha
    vHBph = (vph + betaph)/alpha

    gij_vHBi_vHBj = g4sph[1][1]*vHBr*vHBr + 2.0*g4sph[1][2]*vHBr*vHBth + 2.0*g4sph[1][3]*vHBr*vHBph + g4sph[2][2]*vHBth*vHBth + 2.0*g4sph[2][3]*vHBth*vHBph + g4sph[3][3]*vHBph*vHBph

    Bsq = B_r*Br + B_th*Bth + B_ph*Bph
    B_dot_v = B_r*vHBr + B_th*vHBth + B_ph*vHBph

    detg4sph = linalg.det(g4sph)
#    if (abs(detg4sph) < 1.0e-8):
#        print 'at theta = ', th_here,', and phi = ', ph_here, ', determinant is too small for inversion'

    measure = np.sqrt(-detg4sph)*dth*dph
 
    GAMMA_SPEED_LIMIT = 10.0
    ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED = 1.0 - 1.0/GAMMA_SPEED_LIMIT**2

    if(gij_vHBi_vHBj > ONE_MINUS_ONE_OVER_GAMMA_SPEED_LIMIT_SQUARED):
        print 'at theta = ', th_here,', and phi = ', ph_here, ', Lorentz limit exceeded; ignoring'
        mdot_integrand = 0.0
        phib_integrand = 0.0
        ldot_integrand = 0.0
        edot_integrand = 0.0

    else:

        wlor = 1.0/np.sqrt(1.0-gij_vHBi_vHBj)

        u0 = wlor/alpha

        ur  = u0*vr
        uth = u0*vth
        uph = u0*vph

        smallb2 = Bsq/wlor**2 + B_dot_v**2

        b0  = wlor*B_dot_v/alpha
        br  = Br/wlor  + B_dot_v*ur
        bth = Bth/wlor + B_dot_v*uth
        bph = Bph/wlor + B_dot_v*uph

        b_0  = g4sph[0][0]*b0 + g4sph[0][1]*br + g4sph[0][2]*bth + g4sph[0][3]*bph
        b_r  = g4sph[1][0]*b0 + g4sph[1][1]*br + g4sph[1][2]*bth + g4sph[1][3]*bph
        b_th = g4sph[2][0]*b0 + g4sph[2][1]*br + g4sph[2][2]*bth + g4sph[2][3]*bph
        b_ph = g4sph[3][0]*b0 + g4sph[3][1]*br + g4sph[3][2]*bth + g4sph[3][3]*bph

        u_0  = g4sph[0][0]*u0 + g4sph[0][1]*ur + g4sph[0][2]*uth + g4sph[0][3]*uph
        u_ph = g4sph[3][0]*u0 + g4sph[3][1]*ur + g4sph[3][2]*uth + g4sph[3][3]*uph

        mdot_integrand = rho*ur
        phib_integrand = 0.5*abs(Br)
        ldot_integrand = (rho*hfluid + smallb2)*u_ph*ur - b_ph*br
        edot_integrand = -(rho*hfluid + smallb2)*u_0*ur + b_0*br

    return mdot_integrand, phib_integrand, ldot_integrand, edot_integrand, measure

#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.

###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

# This version of the code assumes that small metadata file is available, and remaining datafile has NO metadata

# hard-coded mass & spin
M_BH=1
chi_BH=0.9375
#M_BH = 0.97
#chi_BH = 0.69

chi_temp = 1.0+np.sqrt(1.0-chi_BH**2)
#rh_sq = 2.0*M**2*( chi**2*chi_temp + (4.0*chi_temp - chi**2*(2.0+chi_temp))*costh**2 )/( chi**2 + 2.0*(2.0-chi**2)*costh**2 + chi**2*costh**4 )
#rh = np.sqrt(rh_sq)
rh = M_BH*chi_temp


parser = argparse.ArgumentParser(description='Produce fluxes.')
parser.add_argument("-s", "--singlefile", help="use a single file for both metadata and real data", action="store_true")
parser.add_argument("metafile", help="file with meta-info at the top")
parser.add_argument("datafile", help="main data file")
parser.add_argument("outfileroot", help="root of output file names")
parser.add_argument("start_time", help="starting time of extraction", type=float)
parser.add_argument("final_time", help="ending time of extraction", type=float)
args = parser.parse_args()

metafile = args.metafile
datafile = args.datafile
outfileroot = args.outfileroot
start_time = args.start_time
final_time = args.final_time

print "reading from ",datafile,", with grid information from ",metafile
print "starting at time ", start_time
print "finishing at time ", final_time

with open(metafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(0, 0)

    Nr=struct.unpack('i',f.read(4))[0]
    Nth=struct.unpack('i',f.read(4))[0]
    Nph=struct.unpack('i',f.read(4))[0]
    print "Nr,Nth,Nph:",Nr,Nth,Nph
    r_min=struct.unpack('d',f.read(8))[0]
    r_max=struct.unpack('d',f.read(8))[0]
    print r_min,"<= r <=",r_max
    th_min=struct.unpack('d',f.read(8))[0]
    th_max=struct.unpack('d',f.read(8))[0]
    print th_min,"<= th <=",th_max
    ph_min=struct.unpack('d',f.read(8))[0]
    ph_max=struct.unpack('d',f.read(8))[0]
    print ph_min,"<= ph <=",ph_max

    dr=(r_max-r_min)/(Nr-1.0);
    rs=np.arange(r_min,r_max+dr,dr)
    dth=(th_max-th_min)/(Nth-1.0);
    ths=np.arange(th_min,th_max+dth,dth)
    dph=(ph_max-ph_min)/(Nph-1.0);
    phs=np.arange(ph_min,ph_max+dph,dph)
    magicnum=0
 
    # taking from thorn interpolator code
    # data col 0 = rho
    # data col 1 = P
    # data col 2 = vx
    # data col 3 = vy
    # data col 4 = vz
    # data col 5 = Bx
    # data col 6 = By
    # data col 7 = Bz
    ### data col 8 = u0IGM

    #print 'theta = ', ths
    #print 'phi = ',phs
    #print 'r = ',rs

    ii_r1 = 0
    while (rs[ii_r1] < rh):
        ii_r1 = ii_r1+1

    r1 = rs[ii_r1]
    r2 = rs[ii_r1+5]
    r3 = rs[ii_r1+10]

    print 'horizon radius = ', rh,'; next grid radius = ',r1 

    endofmeta = f.tell()
    print "position in file after meta information is", endofmeta

startofdata = 0
if args.singlefile:
    print "use the same file for meta and proper data"
    startofdata = endofmeta # continue where we left off
else:
    print "use different files for meta and proper data"
    startofdata = 0 # start from position 0 in file

print "starting from file position ", startofdata

with open(datafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(startofdata, 0)

    bytechunk=f.read(8)

    Nvar = 24 #was 8, before we added u0IGM

    full_timeslice_size = Nvar*Nph*Nth*Nr*8
    full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

    print 'full time slice should take ', full_timeslice_size_inc_meta, ' bytes, including iter and time'

    f1out = open(outfileroot+"_r1.txt",'w',0)
    f2out = open(outfileroot+"_r2.txt",'w',0)
    f3out = open(outfileroot+"_r3.txt",'w',0)
    #f3out = open("fluxes_r3_CTD.txt",'w',0)

    header_line = "#r = "+str(r1)+"\n"
    f1out.write(header_line)
    header_line = "#time\tMdot\t\tphiB\t\tLdot\t\tEdot\n"
    f1out.write(header_line)
    header_line = "#r = "+str(r2)+"\n"
    f2out.write(header_line)
    header_line = "#time\tMdot\t\tphiB\t\tLdot\t\tEdot\n"
    f2out.write(header_line)
    header_line = "#r = "+str(r3)+"\n"
    f3out.write(header_line)
    header_line = "#time\tMdot\t\tphiB\t\tLdot\t\tEdot\n"
    f3out.write(header_line)

    g4_r1_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]
    g4_r2_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]
    g4_r3_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]

    g4UU_r1_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]
    g4UU_r2_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]
    g4UU_r3_arr = [[[] for kk in range(Nph)] for jj in range(Nth)]

    for jj in range(Nth):
        th_here = ths[jj]

        costh = cos(th_here)

        for kk in range(Nph):
            ph_here = phs[kk]

            g4_r1_arr[jj][kk], g4UU_r1_arr[jj][kk] = compute_KS_metric_sph(r1,th_here,ph_here,M_BH,chi_BH)
            g4_r2_arr[jj][kk], g4UU_r2_arr[jj][kk] = compute_KS_metric_sph(r2,th_here,ph_here,M_BH,chi_BH)
            g4_r3_arr[jj][kk], g4UU_r3_arr[jj][kk] = compute_KS_metric_sph(r3,th_here,ph_here,M_BH,chi_BH)

    print "len bytechunk=",len(bytechunk)
    while (bytechunk):

        num=struct.unpack('d',bytechunk)[0]
        if(magicnum==0): 
            magicnum=num  
            print "magicnum=",magicnum
        elif(not magicnum==num):
            print"magicnum mismatch"
            print "got num=",num
            # return data

        this_iter = struct.unpack('i',f.read(4))[0]
        this_time = struct.unpack('d',f.read(8))[0]
        print "reading slab it = ", this_iter," time = ", this_time

        file_place = f.tell()            
        data_left = eof - file_place
        print "only ", data_left, " bytes left in file"

        if(data_left < full_timeslice_size_inc_meta):
            print "not enough data for another time slice --- sorry!"
            break

        if( (this_time < start_time) or (this_time > final_time)):
            print "too early --- skipping to next time step"
            f.seek(file_place+full_timeslice_size)
            #now want to skip processing this time step ...
        else:
            print "NOT too early --- using this time step"

            bytechunk=f.read(full_timeslice_size)
            buffer_res = np.frombuffer(bytechunk)
            print 'size of buffer = ',buffer_res.size

            # rather than push this into a huge data array, do analysis on the data here
            this_data = buffer_res.reshape(Nvar,Nph,Nth,Nr)

            Mdot_r1 = Mdot_r2 = Mdot_r3 = 0.0
            phiB_r1 = phiB_r2 = phiB_r3 = 0.0
            Ldot_r1 = Ldot_r2 = Ldot_r3 = 0.0
            Edot_r1 = Edot_r2 = Edot_r3 = 0.0

            for jj in range(Nth):
                th_here = ths[jj]

                costh = cos(th_here)

                for kk in range(Nph):
                    ph_here = phs[kk]

                    mdot_term_r1, phib_term_r1, ldot_term_r1, edot_term_r1, measure_r1 = calcIntegrands(r1,th_here,ph_here,ii_r1,jj,kk,this_data,g4_r1_arr[jj][kk], g4UU_r1_arr[jj][kk])
                    mdot_term_r2, phib_term_r2, ldot_term_r2, edot_term_r2, measure_r2 = calcIntegrands(r2,th_here,ph_here,ii_r1+1,jj,kk,this_data,g4_r2_arr[jj][kk], g4UU_r2_arr[jj][kk])
                    mdot_term_r3, phib_term_r3, ldot_term_r3, edot_term_r3, measure_r3 = calcIntegrands(r3,th_here,ph_here,ii_r1+2,jj,kk,this_data,g4_r3_arr[jj][kk], g4UU_r3_arr[jj][kk])

                    Mdot_r1 = Mdot_r1 + mdot_term_r1*measure_r1 
                    phiB_r1 = phiB_r1 + phib_term_r1*measure_r1 
                    Ldot_r1 = Ldot_r1 + ldot_term_r1*measure_r1 
                    Edot_r1 = Edot_r1 + edot_term_r1*measure_r1 

                    Mdot_r2 = Mdot_r2 + mdot_term_r2*measure_r2 
                    phiB_r2 = phiB_r2 + phib_term_r2*measure_r2 
                    Ldot_r2 = Ldot_r2 + ldot_term_r2*measure_r2 
                    Edot_r2 = Edot_r2 + edot_term_r2*measure_r2 

                    Mdot_r3 = Mdot_r3 + mdot_term_r3*measure_r3 
                    phiB_r3 = phiB_r3 + phib_term_r3*measure_r3 
                    Ldot_r3 = Ldot_r3 + ldot_term_r3*measure_r3 
                    Edot_r3 = Edot_r3 + edot_term_r3*measure_r3 

            print 'Mdot_r1 = ', Mdot_r1
            print 'phiB_r1 = ', phiB_r1
            data_vec = [str(this_time),str(Mdot_r1),str(phiB_r1),str(Ldot_r1),str(Edot_r1)]
            data_line = "\t".join(data_vec)
            f1out.write(data_line)
            f1out.write('\n')

            data_vec = [str(this_time),str(Mdot_r2),str(phiB_r2),str(Ldot_r2),str(Edot_r2)]
            data_line = "\t".join(data_vec)
            f2out.write(data_line)
            f2out.write('\n')

            data_vec = [str(this_time),str(Mdot_r3),str(phiB_r3),str(Ldot_r3),str(Edot_r3)]
            data_line = "\t".join(data_vec)
            f3out.write(data_line)
            f3out.write('\n')


        # END OF IF (this_time < recover_time)
        bytechunk=f.read(8)

f1out.close()
f2out.close()
f3out.close()



#exit()

## note format of read-in files:
## data[time_idx][field_idx,phi_idx,theta_idx,r_idx]


