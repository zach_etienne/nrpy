from math import sin, cos, sqrt
import numpy as np
import struct
import sys
import numpy.linalg as linalg
import argparse

parser = argparse.ArgumentParser(description='Read file.')
parser.add_argument("-s", "--singlefile", help="use a single file for both metadata and real data", action="store_true")
parser.add_argument("metafile", help="file with meta-info at the top")
parser.add_argument("datafile", help="main data file")
parser.add_argument("outfileroot", help="root of output file names")
parser.add_argument("start_time", help="starting time",type=float)
parser.add_argument("final_time", help="ending time",type=float)
parser.add_argument("inner_radius", help="inner radius",type=float)
parser.add_argument("outer_radius", help="outer radius",type=float)

args = parser.parse_args()

metafile = args.metafile
datafile = args.datafile
outfileroot = args.outfileroot
start_time = args.start_time
final_time = args.final_time
inner_radius = args.inner_radius
outer_radius = args.outer_radius

print("reading from "+str(datafile)+", with grid information from "+str(metafile))
print("starting at time "+str(start_time))
print("finishing at time "+str(final_time))

with open(metafile,"rb") as f:
    f.seek(0, 2)
    eof = f.tell()
    f.seek(0, 0)

    Nr=struct.unpack('i',f.read(4))[0]
    Nth=struct.unpack('i',f.read(4))[0]
    Nph=struct.unpack('i',f.read(4))[0]
    print("Nr,Nth,Nph: "+str(Nr)+","+str(Nth)+","+str(Nph))
    r_min=struct.unpack('d',f.read(8))[0]
    r_max=struct.unpack('d',f.read(8))[0]
    print(str(r_min)+" <= r <= "+str(r_max))
    th_min=struct.unpack('d',f.read(8))[0]
    th_max=struct.unpack('d',f.read(8))[0]
    print(str(th_min)+" <= th <= "+str(th_max))
    ph_min=struct.unpack('d',f.read(8))[0]
    ph_max=struct.unpack('d',f.read(8))[0]
    print(str(ph_min)+" <= ph <= "+str(ph_max))

    dr=(r_max-r_min)/(Nr-1.0)
    rs=np.arange(r_min,r_max+dr,dr)
    dth=(th_max-th_min)/(Nth-1.0)
    ths=np.arange(th_min,th_max+dth,dth)
    dph=(ph_max-ph_min)/(Nph-1.0)
    phs=np.arange(ph_min,ph_max+dph,dph)
    magicnum=0
 
    Nr = len(rs)
    Nth = len(ths)
    Nph = len(phs)

    dth = (ths[Nth-1]-ths[0])/(Nth-1) # assumes even spacing
    dph = (phs[Nph-1]-phs[0])/(Nph-1)

    th_low = np.pi/3.0
    th_high = 2.0*np.pi/3.0

    jj_low = 0
    while( ths[jj_low] < th_low):
        jj_low = jj_low+1

    jj_high = Nth-1
    while( ths[jj_high] > th_high):
        jj_high = jj_high-1


    endofmeta = f.tell()
    print("position in file after meta information is "+str(endofmeta))

startofdata = 0
if args.singlefile:
    print("use the same file for meta and proper data")
    startofdata = endofmeta # continue where we left off
else:
    print("use different files for meta and proper data")
    startofdata = 0 # start from position 0 in file

print("starting from file position "+ str(startofdata))

# taking from thorn interpolator code
#  output_f[0] = output_Poynx
#  output_f[1] = output_Poyny
#  output_f[2] = output_Poynz
#  output_f[3] = output_smallb2
#  output_f[4] = output_vx
#  output_f[5] = output_vy
#  output_f[6] = output_vz
#  output_f[7] = output_Bx
#  output_f[8] = output_By
#  output_f[9] = output_Bz
#  output_f[10] = output_Sx
#  output_f[11] = output_Sy
#  output_f[12] = output_Sz
#  output_f[13] = output_Poynx_suborigB
#  output_f[14] = output_Poyny_suborigB
#  output_f[15] = output_Poynz_suborigB

Nvar = 16

with open(datafile,"rb") as f:

    f.seek(0, 2)
    eof = f.tell()
    f.seek(startofdata, 0)

    bytechunk=f.read(8)

    full_timeslice_size = Nvar*Nph*Nth*Nr*8
    full_timeslice_size_inc_meta = full_timeslice_size + 4 + 8 # accounts for iter and time

    #print("full time slice should take "+ str(full_timeslice_size_inc_meta)+" bytes, including iter and time")


    #print("len bytechunk = "+str(len(bytechunk)))
    while (bytechunk):
        num = struct.unpack('d', bytechunk)[0]
        actual_magicnum = 1.130814081305130e-21
        if (np.fabs(num - actual_magicnum) / actual_magicnum < 1.e-8):
            magicnum = num
            #print("magicnum successful! " + str(magicnum))
        elif (not magicnum == num):
            print("magicnum mismatch")
            print("got num = " + str(num) + ". Was expecting: " + str(actual_magicnum))
            exit(1)
            # return data

        this_iter = struct.unpack('i', f.read(4))[0]
        this_time = struct.unpack('d', f.read(8))[0]
        print("reading slab it = " + str(this_iter) + " time = " + str(this_time))

        file_place = f.tell()
        data_left = eof - file_place
        #print("only " + str(data_left) + " bytes left in file")

        if ((this_time < start_time) or (this_time > final_time)):
            print("too early --- skipping to next time step")
            f.seek(file_place + full_timeslice_size)
            # now want to skip processing this time step ...
        else:
            #print("NOT too early --- using this time step")
            with open(outfileroot + "__" + str(this_time).zfill(8) + ".txt", 'w', 0) as f1out:

                header_line = "# 1.time 2.x 3.y 4.Poynx 5.Poyny 6.Poynz 7.smallb2 8.vx 9.vy 10.vz 11.Bx 12.By 13.Bz 14.Sx 15.Sy 16.Sz 17.Poynx_suborigB 18.Poyny_suborigB 19.Poynz_suborigB 20.Poynr 21.Poynrv2 22.Poynrv3\n"
                f1out.write(header_line)

                bytechunk=f.read(full_timeslice_size)
                buffer_res = np.frombuffer(bytechunk)
                #print("size of buffer = "+str(buffer_res.size))

                # rather than push this into a huge data array, do analysis on the data here
                this_data = buffer_res.reshape(Nvar,Nph,Nth,Nr)
                data_cols = ""
                for ii in range(Nr):
                    r_here = rs[ii]

                    if r_here > inner_radius and r_here < outer_radius:

                        for jj in range(Nth):
                            th_here = ths[jj]

                            if th_here < 3.14*0.5:
                                for kk in range(Nph):
                                    ph_here = phs[kk]
                                    
                                    xx = r_here*sin(th_here)*cos(ph_here)
                                    yy = r_here*sin(th_here)*sin(ph_here)
                                    zz = r_here*cos(th_here)

                                    data_cols += str(this_time) + " "
                                    data_cols += str(xx) + " "
                                    data_cols += str(yy) + " "

                                    for ww in range(Nvar):
                                        data_cols += str(this_data[ww,kk,jj,ii]) + " "
                                        if ww == Nvar-1:
                                            Poynx = this_data[0,kk,jj,ii]
                                            Poyny = this_data[1,kk,jj,ii]
                                            Poynz = this_data[2,kk,jj,ii]
                                            Poynr = (xx * Poynx + yy * Poyny + zz * Poynz) / r_here
                                            data_cols += str(Poynr) + " "

                                            Poynxv2 = this_data[10,kk,jj,ii]
                                            Poynyv2 = this_data[11,kk,jj,ii]
                                            Poynzv2 = this_data[12,kk,jj,ii]
                                            Poynrv2 = (xx * Poynxv2 + yy * Poynyv2 + zz * Poynzv2) / r_here
                                            data_cols += str(Poynrv2) + " "
                                            
                                            Poynxv3 = this_data[13,kk,jj,ii]
                                            Poynyv3 = this_data[14,kk,jj,ii]
                                            Poynzv3 = this_data[15,kk,jj,ii]
                                            Poynrv3 = (xx * Poynxv3 + yy * Poynyv3 + zz * Poynzv3) / r_here
                                            data_cols += str(Poynrv3) + " "
                                            
                                    data_cols += "\n"
                                data_cols += "\n"
                    data_cols += "\n\n"
                if data_cols != "":
                    f1out.write(data_cols)
        # END OF IF (this_time < recover_time)
        bytechunk=f.read(8)
