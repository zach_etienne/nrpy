from math import sin, cos, sqrt
import numpy as np
import struct
import sys
import numpy.linalg as linalg

#Notes:
#Need to transform veloc and B  from cart2sph
#Basic analysis quantities include u^ph (from coord transf) and small_b2 [see code in SENR/ETK_GRMHD_initial_data.py], and \beta^-1 = small_b2/(2P)
#The latter will req. gKS4DD [draft computation above]
#Will also need certain Stress-Energy tensor components for integrals.

###################################################### 
###################################################### 
################# Main Routine ################### 
###################################################### 
###################################################### 

datafile=sys.argv[1]

print "reading 1D ASCII data from",datafile

all_r_vals, all_rho_avg_vals, all_P_avg_vals, all_uph_avg_vals, all_bmag_avg_vals, all_betainv_avg_vals = np.loadtxt(datafile,usecols=(0,1,2,3,4,5), unpack=True)

# at this stage, we have all the times together; how to split up?
# idea: get a *unique* listing of rvals, and then use this to split up

Nrows = len(all_r_vals)

print "total length of data is ", Nrows

r_vals = np.unique(all_r_vals)
Nr = len(r_vals)

print "length of unique radial points is", Nr

Nt = Nrows/Nr

print "... so we have ",Nt," time steps"

rho_avg     = np.zeros(Nr)
P_avg       = np.zeros(Nr)
uph_avg     = np.zeros(Nr)
bmag_avg    = np.zeros(Nr)
betainv_avg = np.zeros(Nr)

startidx = 0
for ll in range(Nt):
    rho_avg_vals     = all_rho_avg_vals[startidx:startidx+Nr]
    P_avg_vals       = all_P_avg_vals[startidx:startidx+Nr]
    uph_avg_vals     = all_uph_avg_vals[startidx:startidx+Nr]
    bmag_avg_vals    = all_bmag_avg_vals[startidx:startidx+Nr]
    betainv_avg_vals = all_betainv_avg_vals[startidx:startidx+Nr]

    rho_avg     = rho_avg     + rho_avg_vals
    P_avg       = P_avg       + P_avg_vals
    uph_avg     = uph_avg     + uph_avg_vals
    bmag_avg    = bmag_avg    + bmag_avg_vals
    betainv_avg = betainv_avg + betainv_avg_vals

    startidx = startidx + Nr
    # END OF LOOP for ll in range(Nt)

# now divide by Nt, and output radial results

rho_avg     = rho_avg/Nt
P_avg       = P_avg/Nt
uph_avg     = uph_avg/Nt
bmag_avg    = bmag_avg/Nt
betainv_avg = betainv_avg/Nt

# step 2: calculate standard deviations

rho_var     = np.zeros(Nr)
P_var       = np.zeros(Nr)
uph_var     = np.zeros(Nr)
bmag_var    = np.zeros(Nr)
betainv_var = np.zeros(Nr)

startidx = 0
for ll in range(Nt):
    rho_avg_vals     = all_rho_avg_vals[startidx:startidx+Nr]
    P_avg_vals       = all_P_avg_vals[startidx:startidx+Nr]
    uph_avg_vals     = all_uph_avg_vals[startidx:startidx+Nr]
    bmag_avg_vals    = all_bmag_avg_vals[startidx:startidx+Nr]
    betainv_avg_vals = all_betainv_avg_vals[startidx:startidx+Nr]

    rho_var     = rho_var     + (rho_avg_vals - rho_avg)**2
    P_var       = P_var       + (P_avg_vals - P_avg)**2
    uph_var     = uph_var     + (uph_avg_vals - uph_avg)**2
    bmag_var    = bmag_var    + (bmag_avg_vals - bmag_avg)**2
    betainv_var = betainv_var + (betainv_avg_vals - betainv_avg)**2

    startidx = startidx + Nr
    # END OF LOOP for ll in range(Nt)

rho_std     = np.sqrt(rho_var/(Nt-1))
P_std       = np.sqrt(P_var/(Nt-1))
uph_std     = np.sqrt(uph_var/(Nt-1))
bmag_std    = np.sqrt(bmag_var/(Nt-1))
betainv_std = np.sqrt(betainv_var/(Nt-1))

# output to file (one at the moment)

fout = open("disk_time_averages.txt",'w',0)
header_line = "#r\trho_avg\t\trho_std\t\tP_avg\t\tP_std\t\tuph_avg\t\tuph_std\t\tbmag_avg\t\tbmag_std\t\tbetainv_avg\t\tbetainv_std\n"
fout.write(header_line)

for ii in range(Nr):
    data_vec = [str(r_vals[ii]),str(rho_avg[ii]),str(rho_std[ii]),str(P_avg[ii]),str(P_std[ii]),str(uph_avg[ii]),str(uph_std[ii]),str(bmag_avg[ii]),str(bmag_std[ii]),str(betainv_avg[ii]),str(betainv_std[ii])]
    data_line = "\t".join(data_vec)
    fout.write(data_line)
    fout.write('\n')

    # END OF LOOP for ii in range(Nr)

fout.close()


#exit()


