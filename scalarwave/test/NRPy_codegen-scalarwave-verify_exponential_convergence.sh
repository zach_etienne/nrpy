#!/bin/bash

COORDSYSTEM="Spherical"
EVOLUTIONTYPE="scalarwave"
INITIALDATA="PlaneWave"

# Clean up any previous files that were there.
rm -f ../output/*

# Keeping the grid structure fixed, do we obtain exponential convergence with increased finite difference order?
cd ../../
for FDORDER in 2 4 6 8; do 
    python NRPy_main.py Everything $COORDSYSTEM $EVOLUTIONTYPE $INITIALDATA $FDORDER
    cd scalarwave;
    make -j
    time taskset -c 0,1,2,3 ./scalarwave 128 128 2 10.0
    cd ..
done

cd scalarwave/test/
gnuplot gnuplot_script_expon

