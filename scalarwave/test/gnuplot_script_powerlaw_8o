set term post enh color fontscale 2.0
set out "PowerLawConvergence_8o.ps"
set key left Left reverse
set title "Power-Law Convergence of Solution,\n Using 8th-Order Finite Difference, t=4.3, OB=14."
set xlabel "r coordinate"
set ylabel "(Numerical - Exact) Solution" offset 1.0

FILE1X="`ls ../output/*1D*Nx64_Ny64*`"
FILE2X="`ls ../output/*1D*Nx96_Ny96*`"
FILE4X="`ls ../output/*1D*Nx128_Ny128*`"

set log y
set format y "10^{%L}"

# Easy to confirm that index 1 at resolution dr,dth = index 4 at resolution dr/2,dth/2 = index 16 at resolution dr/4,dth/4: just look at the 7th column of data in these files (time). 
#   Also look at columns 8 and 9 (theta and phi coord of line) to confirm that theta and phi are constant across resolutions (they should be)
p [:8] [7e-10:1e-6] FILE1X i 4 u 1:(abs($2)) ti "Error(dr,d{/Symbol q})" w l,FILE2X i 9 u 1:(abs($2*1.5**8)) ti "Error(dr/1.5,d{/Symbol q}/1.5)*1.5^8" w l,FILE4X i 16 u 1:(abs($2*2**8)) ti "Error(dr/2,d{/Symbol q}/2)*2^8" w l
# We plot at t=4.3 instead of t=8.6 because cruft from outer boundary contaminates most of the grid at t=8.6 (outer boundary set to 14).

!ps2pdf PowerLawConvergence_8o.ps
!rm -f PowerLawConvergence_8o.ps