#!/bin/bash

COORDSYSTEM="Spherical"
EVOLUTIONTYPE="scalarwave"
INITIALDATA="PlaneWave"
FDORDER=4

# Clean up any previous files that were there.
rm -f ../output/*

# Keeping the grid structure fixed, do we obtain exponential convergence with increased finite difference order?
cd ../../
python NRPy_main.py Everything $COORDSYSTEM $EVOLUTIONTYPE $INITIALDATA $FDORDER
cd scalarwave
make -j
for i in "256 256" "128 128" "64 64"; do
    time taskset -c 0,1,2,3 ./scalarwave $i 2 10.0
done

cd test/
gnuplot gnuplot_script_powerlaw

