/*
  Fourth Order Runge-Kutta

  Integrate the first order ODE
  d_t y(t) = f(y(t), t)

  from time t0 to t0 + dt (iteration n to n + 1), where dt is
  the temporal step size. Let y0 = y(t0) be stored in gfs_n.

  STEP 1:
  k1 = f(y0, t0)
  y1 = y0 + k1 * dt / 2
  Apply boundary conditions to y1 at time t0 + dt / 2

  STEP 2:
  k2 = f(y1, t0 + dt / 2)
  y2 = y0 + k2 * dt / 2
  Apply boundary conditions to y2 at time t0 + dt / 2

  STEP 3:
  k3 = f(y2, t0 + dt / 2)
  y3 = y0 + k3 * dt
  Apply boundary conditions to y3 at time t0 + dt

  STEP 4:
  k4 = f(y3, t0 + dt)
  y(t0 + dt) = y0 + dt * (k1 + 2 * k2 + 2 * k3 + k4) / 6
  Apply boundary conditions to y(t0 + dt) at time t0 + dt

  Note that we store y1 in gfs_1, then overwrite with y2,
  and then with y3.
*/

const double tph = t + 0.5 * dt; // Half step
const double tp1 = t + dt; // Full step

#ifdef RK4_NAN_CHECKER
NaN_Checker(0, n, t, x1G, x2G, x3G, gfs_n, gf_name, params);
#endif

/////////////////////////////////////////////////////////////////////////////////////////
// RK4 STEP 1
// Gridfunction inputs:  gfs_n and gfs_aux.
// Gridfunction outputs: gfs_k = k_1
Evaluate_RHS(t, x1G, x2G, x3G, yy, gfs_n, gfs_aux, gfs_k, precomp, params);

// Evolve to (1)
#pragma omp parallel for
for(int gf = 0; gf < NUM_EVOL_GFS; gf++) for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	const int idx = IDX4(gf, i, j, k);
	gfs_1[idx] = gfs_n[idx] + 0.5 * dt * gfs_k[idx];
        // Original (inefficient) line of code:
        // gfs_np1[idx] = gfs_k[idx];
        // For improved performance, we instead swap pointers to 
        //   gfs_np1 and gfs_k in memory (see "SWAP POINTERS STEP 1" below)
      }

/* SWAP POINTERS STEP 1 */
{
  // To set gfs_np1 = gfs_k, simply swap pointers!
  REAL *tmp_pointer_step1 = gfs_np1;
  gfs_np1 = gfs_k;
  gfs_k = tmp_pointer_step1;
}

// Boundary (parity) conditions at (1)
#pragma omp parallel for
for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
  Apply_out_sym_bcs(gf, tph, x1G,x2G,x3G, gfs_1, params);
 }

#ifdef RK4_NAN_CHECKER
NaN_Checker(1, n, t, x1G, x2G, x3G, gfs_1, gf_name, params);
#endif

/////////////////////////////////////////////////////////////////////////////////////////
// RK4 STEP 2
// Gridfunction inputs:  gfs_1 and gfs_aux.
// Gridfunction outputs: gfs_k = k_2
Evaluate_RHS(tph, x1G, x2G, x3G, yy, gfs_1, gfs_aux, gfs_k, precomp, params);

// Evolve to (2)
#pragma omp parallel for
for(int gf = 0; gf < NUM_EVOL_GFS; gf++) for(int k=NGHOSTS;k<Npts3-NGHOSTS;k++) for(int j=NGHOSTS;j<Npts2-NGHOSTS;j++) for(int i=NGHOSTS;i<Npts1-NGHOSTS;i++) {
	const int idx = IDX4(gf, i, j, k);
        const REAL gfs_kL = gfs_k[idx];
        gfs_1[idx] = gfs_n[idx] + 0.5 * dt * gfs_kL;
	gfs_np1[idx] +=                2.0 * gfs_kL;
      }

// Boundary (parity) conditions at (2)
#pragma omp parallel for
for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
  Apply_out_sym_bcs(gf, tph, x1G,x2G,x3G, gfs_1, params);
 }

#ifdef RK4_NAN_CHECKER
NaN_Checker(2, n, t, x1G, x2G, x3G, gfs_1, gf_name, params);
#endif

/////////////////////////////////////////////////////////////////////////////////////////
// RK4 STEP 3
// Gridfunction inputs:  gfs_1 and gfs_aux.
// Gridfunction outputs: gfs_k = k_3
Evaluate_RHS(tph, x1G, x2G, x3G, yy, gfs_1, gfs_aux, gfs_k, precomp, params);

// Evolve to (3)
#pragma omp parallel for
for(int gf = 0; gf < NUM_EVOL_GFS; gf++) for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	const int idx = IDX4(gf, i, j, k);
        const REAL gfs_kL = gfs_k[idx];
        gfs_1[idx] = gfs_n[idx] + dt * gfs_kL;
	gfs_np1[idx] += 2.0 * gfs_kL;
      }

// Boundary (parity) conditions at (3)
#pragma omp parallel for
for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
  Apply_out_sym_bcs(gf, tp1, x1G,x2G,x3G, gfs_1, params);
 }

#ifdef RK4_NAN_CHECKER
NaN_Checker(3, n, t, x1G, x2G, x3G, gfs_1, gf_name, params);
#endif

/////////////////////////////////////////////////////////////////////////////////////////
// RK4 STEP 4
// Gridfunction inputs:  gfs_1 and gfs_aux.
// Gridfunction outputs: gfs_k = k_4
Evaluate_RHS(tp1, x1G, x2G, x3G, yy, gfs_1, gfs_aux, gfs_k, precomp, params);

// Evolve to n+1
const REAL dt_over_6 = dt / 6.0;
#pragma omp parallel for
for(int gf = 0; gf < NUM_EVOL_GFS; gf++) for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++) for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++) for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
	const int idx = IDX4(gf, i, j, k);
	gfs_np1[idx] = gfs_n[idx] + (gfs_np1[idx] + gfs_k[idx])*dt_over_6;
      }

// Boundary (parity) conditions at n+1
#pragma omp parallel for
for(int gf = 0; gf < NUM_EVOL_GFS; gf++) {
  Apply_out_sym_bcs(gf, tp1, x1G,x2G,x3G, gfs_np1, params);
 }

#ifdef RK4_NAN_CHECKER
NaN_Checker(4, n, t, x1G, x2G, x3G, gfs_np1, gf_name, params);
#endif
