// Evaluate scalar wave evolution equation right-hand-sides, as called by RK4_Steps.c
void Evaluate_RHS(const REAL t, REAL *x1G, REAL *x2G, REAL *x3G, REAL *yy, REAL *gfs_n, REAL *gfs_aux, REAL *gfs_k, precomputed_quantities *precomp, paramstruct params) {
  scalarwave_RHS(t, x1G,x2G,x3G, yy, gfs_n, gfs_aux, gfs_k, precomp, params);
  return;
}
