//#include "diagnostics/Grid_Function_Reduction.c" // Located in ../../common_functions/

void Data_File_Output(FILE *out0D, FILE *out1D, FILE *out2D, FILE *out3D, const int frame_ind, const int n, const REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *gfs_n, REAL *gfs_aux, precomputed_quantities *precomp, paramstruct params)
{
#include "parameters_readin-NRPyGEN.h" // Located in ../common_functions/

  //const int whichpt_x1 = Npts1 / 2;
  const int whichpt_x2 = Npts2 / 2;
  //const int whichpt_x2 = NGHOSTS;
  const int whichpt_x3 = Npts3 / 2;

  // 0D output
  int ijkmin[3],ijkmax[3];
  ijkmin[0] = ijkmin[1] = ijkmin[2] = NGHOSTS;
  int maxval0 = 0;
  REAL TARGET_RADIUS = 4.0;
  REAL min_dist=1e200;
  for(int i=0;i<Npts1;i++) {
    REAL distance_from_target = fabsl(yy[IDX4(0,i,NGHOSTS,NGHOSTS)] - TARGET_RADIUS);
    if(distance_from_target < min_dist) {
      min_dist = distance_from_target;
      maxval0=i;
    }
  }
  ijkmax[0] = maxval0;
  ijkmax[1] = Npts2-NGHOSTS;
  ijkmax[2] = Npts3-NGHOSTS;
  REAL outputvolume=0;
  const REAL drr = x1G[1]-x1G[0];
  const REAL dth = x2G[1]-x2G[0];
  const REAL dph = x3G[1]-x3G[0];
  LOOP_GZFILL(i,j,k) {
    const int idx = IDX3(i,j,k);
    const REAL rr = yy[IDX4pt(0,idx)];
    const REAL th = yy[IDX4pt(1,idx)];
    const REAL ph = yy[IDX4pt(2,idx)];

    const REAL zz = rr*cos(th);

    const REAL vol_element = rr*rr*sin(th)*drr*dth*dph;

    gfs_aux[IDX4pt(UUEXACTDIFF,idx)] = (gfs_n[IDX4pt(UU,idx)] - (cos(zz - t) + 2.0))*vol_element;
    gfs_aux[IDX4pt(VVEXACTDIFF,idx)] = (gfs_n[IDX4pt(VV,idx)] - (sin(zz - t)      ))*vol_element;

    gfs_aux[IDX4pt(VOLUME,idx)] = 1.0*vol_element;
  }
  REAL output_int = L2_Norm_Over_Chunk_of_Grid(ijkmin,ijkmax, &gfs_aux[IDX4pt(UUEXACTDIFF,0)],&gfs_aux[IDX4pt(VOLUME,0)], params, &outputvolume);
  fprintf(out0D, "%e %.15e %.15e ", (double)t,(double)output_int, (double)outputvolume);
  output_int = L2_Norm_Over_Chunk_of_Grid(ijkmin,ijkmax, &gfs_aux[IDX4pt(VVEXACTDIFF,0)],&gfs_aux[IDX4pt(VOLUME,0)], params, &outputvolume);
  fprintf(out0D, "%.15e\n", (double)output_int);

  REAL *in_gfs = gfs_n;

  // 1D output
  for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++)
    //for(int i = 0; i < Npts1; i++)
    //for(int k = 0; k < Npts3; k++)
    {
      const int idx = IDX3(i, whichpt_x2, whichpt_x3); // Output the y1 axis
      //const int idx = IDX3(NGHOSTS, whichpt_x2, k);

      //const int idx = IDX3(i, i, i); // Output the y1 = y2 = y3 diagonal
      const REAL x1 = x1G[i];
      const REAL y1 = yy[IDX4pt(0,idx)];
      const REAL y2 = yy[IDX4pt(1,idx)];
      const REAL y3 = yy[IDX4pt(2,idx)];
      const REAL u = gfs_n[IDX4pt(UU,idx)];

      const REAL rr = yy[IDX4pt(0,idx)];
      const REAL th = yy[IDX4pt(1,idx)];
      const REAL zz = rr*cos(th);
      gfs_aux[IDX4pt(UUEXACTDIFF,idx)] = (gfs_n[IDX4pt(UU,idx)] - (cos(zz - t) + 2.0));
      gfs_aux[IDX4pt(VVEXACTDIFF,idx)] = (gfs_n[IDX4pt(VV,idx)] - (sin(zz - t)      ));

      const REAL uerror = gfs_aux[IDX4pt(UUEXACTDIFF,idx)];
      const REAL v = gfs_n[IDX4pt(VV,idx)];

      fprintf(out1D, "%.15e %.15e %.15e %.15e | %d %e %e %e",(double)y1, (double)uerror,(double)u, (double)v, frame_ind,t,y2,y3);
      fprintf(out1D, "\n");
    }

  fprintf(out1D, "\n\n");

  // 2D output: SymTP
  if(strncmp(params.CoordSystem,"SymTP",100)==0) {
    for(int jj = NGHOSTS; jj < Npts2 - NGHOSTS; jj++)
      {
        for(int ii = NGHOSTS; ii < Npts1 - NGHOSTS; ii++)
          {
            const int idx = IDX3(ii, jj, whichpt_x3);
            const REAL y1 = yy[IDX4pt(0,idx)];
            const REAL y3 = yy[IDX4pt(2,idx)];
            const REAL u = gfs_n[IDX4pt(UU,idx)];
            const REAL v = gfs_n[IDX4pt(VV,idx)];

            fprintf(out2D, "%f %f ", (double)y1, (double)y3); // Coordinates
            fprintf(out2D, "%.15e %.15e | %d %e %d %d",(double)u, (double)v, frame_ind,t,ii,jj);
          }
        fprintf(out2D, "\n");
      }
    fprintf(out2D, "\n");
  } else {
    for(int jj = NGHOSTS; jj < Npts2 - NGHOSTS; jj++)
      {
        for(int ii = NGHOSTS; ii < Npts1 - NGHOSTS; ii++)
          {
            const int idx = IDX3(ii, jj, whichpt_x3);
            const REAL y1 = yy[IDX4pt(0,idx)];
            const REAL y2 = yy[IDX4pt(1,idx)];
            const REAL u = gfs_n[IDX4pt(UU,idx)];
            const REAL v = gfs_n[IDX4pt(VV,idx)];
            const REAL uerror = gfs_aux[IDX4pt(UUEXACTDIFF,idx)];

            int kk = whichpt_x3;
            const REAL x1 = x1G[ii];
            const REAL x2 = x2G[jj];
            const REAL x3 = x3G[kk];

            fprintf(out2D, "%f %f ", (double)y1, (double)y2); // Coordinates
            fprintf(out2D, "%.15e %.15e %.15e | %d %d %e %d %d \n", uerror, u,v, frame_ind, n, (double)t, ii, jj); // Grid function data
          }
        fprintf(out2D, "\n");
      }
    fprintf(out2D, "\n");
  }

  // 3D output (disabled)
  //for(int k = NGHOSTS; k < Npts3 - NGHOSTS; k++)
  /*
  for(int k = 0; k < Npts3; k++)
    {
      //for(int j = NGHOSTS; j < Npts2 - NGHOSTS; j++)
      for(int j = 0; j < Npts2; j++)
	{
	  //for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++)
	  for(int i = 0; i < Npts1; i++)
	    {
	      const int idx = IDX3(i, j, k);
	      const REAL y1 = yy[IDX4pt(0,idx)];
	      const REAL y2 = yy[IDX4pt(1,idx)];
	      const REAL y3 = yy[IDX4pt(2,idx)];
              const REAL u = gfs_n[IDX4pt(UU,idx)];
              const REAL v = gfs_n[IDX4pt(VV,idx)];

	      fprintf(out3D, "%d %d %e %d %d %d %e %e %e ", frame_ind, n, t, i, j, k, y1, y2, y3); // Coordinates
	      fprintf(out3D, "%.15e %.15e\n", u,v);
	    }

	  fprintf(out3D, "\n");
	}

      //fprintf(out3D, "\n");
      }

  */
  return;
}
