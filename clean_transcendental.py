from __future__ import print_function, division
from sympy.printing.codeprinter import CodePrinter
from sympy.printing.str import StrPrinter
from sympy.printing.precedence import precedence
from sympy import srepr, sympify

# Transcendental functions cause problems both for
#  SymPy (when simplifying expressions), and they
#  are very expensive to compute. Therefore, one
#  efficient strategy is to replace all
#  transcendental functions with variable names,
#  and pre-compute them before evaluating an
# expression.

precomputed_quantity_varname = []
precomputed_quantity_expression = []
# This Mathematica output module is from http://docs.sympy.org/dev/_modules/sympy/printing/mathematica.html
# LICENSE:
"""
Copyright (c) 2006-2017 SymPy Development Team

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  a. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
  b. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  c. Neither the name of SymPy nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.


THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

--------------------------------------------------------------------------------

Patches taken from the Diofant project (https://github.com/diofant/diofant)
are licensed as:

Copyright (c) 2006-2017 SymPy Development Team,
              2013-2017 Sergey B Kirpichev

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  a. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
  b. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  c. Neither the name of Diofant, or SymPy nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.


THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

Mathematica code printer
"""

# Used in MCodePrinter._print_Function(self)
known_functions = {
    "exp": [(lambda x: True, "exp")],
    "abs": [(lambda x: True, "fabs")],
    "log": [(lambda x: True, "log")],
    "pow": [(lambda x: True, "pow")],
    "sin": [(lambda x: True, "sin")],
    "cos": [(lambda x: True, "cos")],
    "tan": [(lambda x: True, "tan")],
    "cot": [(lambda x: True, "cot")],
    "asin": [(lambda x: True, "asin")],
    "acos": [(lambda x: True, "acos")],
    "atan": [(lambda x: True, "atan")],
    "sinh": [(lambda x: True, "sinh")],
    "cosh": [(lambda x: True, "cosh")],
    "tanh": [(lambda x: True, "tanh")],
    "coth": [(lambda x: True, "coth")],
    "sech": [(lambda x: True, "sech")],
    "csch": [(lambda x: True, "csch")],
    "sign": [(lambda x: True, "copysign")],
    "asinh": [(lambda x: True, "asinh")],
    "acosh": [(lambda x: True, "acosh")],
    "atanh": [(lambda x: True, "atanh")],
    "acoth": [(lambda x: True, "acoth")],
    "asech": [(lambda x: True, "asech")],
    "acsch": [(lambda x: True, "acsch")],

}

class CleanTranscendental(CodePrinter):
    """A printer to remove transcendental functions. Based on _mcode
    """
    printmethod = "_cleantranscendental"
    _default_settings = {
        'order': None,
        'full_prec': 'auto',
        'precision': 15,
        'user_functions': {},
        'human': True,
    }

    _number_symbols = set()
    _not_supported = set()


    def __init__(self, settings={}):
        """Register function mappings supplied by user"""
        CodePrinter.__init__(self, settings)
        self.known_functions = dict(known_functions)
        userfuncs = settings.get('user_functions', {})
        for k, v in userfuncs.items():
            if not isinstance(v, list):
                userfuncs[k] = [(lambda *x: True, v)]
                self.known_functions.update(userfuncs)

    doprint = StrPrinter.doprint

    def _print_Pow(self, expr):
        PREC = precedence(expr)
        orig_str = sympify("%s**%s" % (self.parenthesize(expr.base, PREC), self.parenthesize(expr.exp, PREC)))
        base_ =  str(self.parenthesize(expr.base, PREC))
        if expr.exp == -2:
            orig_str = "(1/((" + base_ + ")*(" + base_ + " )))"
            return orig_str
        elif expr.exp == -1:
            orig_str = "(1/(" + base_ + "))"
            return orig_str
        elif expr.exp == 2:
            orig_str = "((" + base_ + ")*(" + base_ + " ))"
            return orig_str
        elif expr.exp == 3:
            orig_str = "((" + base_ + ")*(" + base_ + ")*(" + base_ + "))"
            return orig_str
        elif expr.exp == 4:
            orig_str = "((" + base_ + ")*(" + base_ + " )*(" + base_ + ")*(" + base_ + "))"
            return orig_str
        elif expr.exp == 6:
            orig_str = "((" + base_ + ")*(" + base_ + " )*(" + base_ + ")*(" + base_ + " )*(" + base_ + ")*(" + base_ + "))"
            return orig_str
        else:
            precomputed_quantity_varname.extend([ srepr(orig_str). \
            replace(" ", "SPAC").replace("(", "BEGINPAREN"). \
            replace(")", "ENDPAREN").replace("'", "SINGQUOT").replace(",", "COMMA").replace("-", "NEG") ])
            precomputed_quantity_expression.extend(["pow(%s,%s)" % (self.parenthesize(expr.base, PREC), self.parenthesize(expr.exp, PREC))])
            return precomputed_quantity_varname[len(precomputed_quantity_varname)-1]

    def _print_Mul(self, expr):
        PREC = precedence(expr)
        c, nc = expr.args_cnc()
        res = super(CleanTranscendental, self)._print_Mul(expr.func(*c))
        if nc:
            res += '*'
            res += '**'.join(self.parenthesize(a, PREC) for a in nc)
        return res

    def _print_Pi(self, expr):
        return 'M_PI'

    def _print_Function(self, expr):
        if expr.func.__name__ in self.known_functions:
            cond_mfunc = self.known_functions[expr.func.__name__]
            for cond, mfunc in cond_mfunc:
                if cond(*expr.args):
                    orig_str = "%s(%s)" % (mfunc, self.stringify(expr.args, ", "))
                    precomputed_quantity_varname.extend([ srepr(orig_str).\
                        replace(" ", "SPAC").replace("/","DIV").replace("*","MUL").replace("(","BEGINPAREN").\
                        replace(")", "ENDPAREN").replace("'", "SINGQUOT").replace(",", "COMMA").replace("-", "NEG") ])
                    precomputed_quantity_expression.extend([ orig_str ])
                    return precomputed_quantity_varname[len(precomputed_quantity_varname)-1]
        print("UNKNOWN FUNCTION: %s" + expr.func.__name__)
        exit(1)

def clean_transcendental(expr, precomp_names_list,precomp_exprs_list,**settings):
    # """Replaces transcendental function with a variable name that encodes SymPy's internal representation of the expression.
    #
    #     Examples
    #     ========
    #
    #     >>> from sympy import *
    #     >>> clean_transcendental(sin(x),list_of_precomp_names,counter)
    #      [stuff]
    #     >>> print(list_of_precomp_names[0])
    #      sinBEGINPARENSymbolBEGINPARENSINGQUOTx2SINGQUOTENDPARENENDPAREN=sin(x2)
    # """
    expr = CleanTranscendental(settings).doprint(expr)

    precomp_names_list.extend(precomputed_quantity_varname)
    precomp_exprs_list.extend(precomputed_quantity_expression)
    return sympify(expr)