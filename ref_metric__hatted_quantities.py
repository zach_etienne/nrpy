import time
from parameters_compiletime import Optimize, params_varname, params_value

from reference_metric import xx,y1,y2,y3,ReDD,ReU,ghatDD, protected_varnames
from NRPy_functions import symm_matrix_inverter3x3,apply_function__readonly_variables,superfast_uniq,get_parameter_value,nrpy_ccode_parser
from sympy import sympify,symbols,diff,simplify,Rational
from NRPy_file_output import NRPy_file_output,list_unset_variables

#***********************************************************
#***********************************************************
# START OF REFERENCE METRIC ("HATTED QUANTITY") COMPUTATIONS

# *****************************************************
# *****************************************************
# COMPUTE HATTED (REFERENCE-METRIC-RELATED) QUANTITIES:
print("Computing hatted (reference-metric-related) quantities...")
start1 = time.time()

yy = [y1,y2,y3]

PRECISION = get_parameter_value("PRECISION", params_varname, params_value)
RunMode = get_parameter_value("NRPyRunMode", params_varname, params_value)

# -={ C code generation: output to codegen_output/NRPy_Initial_Data_*.h }=-
if RunMode == "Everything" or RunMode == "RHSonly":
    NRPy_file_output("common_functions/reference_metric/NRPy_codegen/yy.h", [],[],[], protected_varnames,
                     [],[yy[0],"yy[IDX4pt(0,idx)]",yy[1],"yy[IDX4pt(1,idx)]",yy[2],"yy[IDX4pt(2,idx)]"])

# First sanitize input:
for i in range(3):
    ReU[i] = sympify(ReU[i])
    for j in range(3):
        ReDD[i][j] = sympify(ReDD[i][j])
        ghatDD[i][j] = sympify(ghatDD[i][j])

ghatUU   =   [[ sympify(0) for i in range(3)] for j in range(3)]
detgammahat = symbols('detgammahat',positive=True)
detgammahat = symm_matrix_inverter3x3(ghatDD, ghatUU)

for i in range(3):
    for j in range(3):
        ReDD[i][j]   = (ReDD[i][j]  )
        ghatDD[i][j] = (ghatDD[i][j])
        ghatUU[i][j] = (ghatUU[i][j])

for i in range(3):
    for j in range(3):
        if ReDD[i][j] != ReDD[j][i]:
            print("Error: ReDD["+ str(i) + "][" + str(j) + "] != ReDD["+ str(j) + "][" + str(i) + ": " + str(ReDD[i][j]) + "!=" + str(ReDD[j][i]))
            exit(1)
        if ghatDD[i][j] != ghatDD[j][i]:
            print("Error: ghatDD["+ str(i) + "][" + str(j) + "] != ghatDD["+ str(j) + "][" + str(i) + ": " + str(ghatDD[i][j]) + "!=" + str(ghatDD[j][i]))
            exit(1)
        if ghatUU[i][j] != ghatUU[j][i]:
            print("Error: ghatUU["+ str(i) + "][" + str(j) + "] != ghatUU["+ str(j) + "][" + str(i) + ": " + str(ghatUU[i][j]) + "!=" + str(ghatUU[j][i]))
            exit(1)

# Compute det(ghat) and its 1st & 2nd derivatives
detgammahatdD  =  [ sympify(0) for i in range(3) ]
detgammahatdDD = [[ sympify(0) for i in range(3) ] for j in range(3)]
for i in range(3):
    detgammahatdD[i] = (diff(detgammahat, xx[i]))
    for j in range(3):
        detgammahatdDD[i][j] = diff(detgammahatdD[i], xx[j])

# Compute 1st & 2nd derivatives of rescaling vector. Needed for betaUdDD computation.
ReUdD = [[sympify(0) for i in range(3)] for j in range(3)]
ReUdDD = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
for i in range(3):
    for j in range(3):
        ReUdD[i][j] = diff(ReU[i], xx[j])
        for k in range(3):
            ReUdDD[i][j][k] = diff(ReUdD[i][j], xx[k])

# Compute 1st & 2nd derivatives of rescaling matrix
ReDDdD = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
ReDDdDD = [[[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)] for l in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            ReDDdD[i][j][k] = (diff(ReDD[i][j],xx[k]))
            for l in range(3):
                # Simplifying this doesn't appear to help overall NRPy run time.
                ReDDdDD[i][j][k][l] = diff(ReDDdD[i][j][k], xx[l])

ghatDDdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
ghatDDdDD = [[[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)] for l in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            ghatDDdD[i][j][k] = simplify(diff(ghatDD[i][j],xx[k])) # BAD: MUST BE SIMPLIFIED OR ANSWER IS INCORRECT! Must be some bug in sympy...
            for l in range(3):
                ghatDDdDD[i][j][k][l] = (diff(ghatDDdD[i][j][k],xx[l]))

GammahatUDD = [ [ [ sympify(0) for i in range(3) ] for j in range(3) ] for k in range(3) ]
for i in range(3):
    for k in range(3):
        for l in range(3):
            for m in range(3):
                GammahatUDD[i][k][l] += (Rational(1,2))*ghatUU[i][m]*(ghatDDdD[m][k][l] + ghatDDdD[m][l][k] - ghatDDdD[k][l][m])

GammahatUDDdD = [[[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)] for l in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                GammahatUDDdD[i][j][k][l] = (diff(GammahatUDD[i][j][k],xx[l]))

# Next, set up infrastructure to automatically precompute transcendental functions
# NOTE: THE PRECOMPUTATION INFRASTRUCTURE IS CURRENTLY DISABLED; PRECOMPUTATION SIMPLIFICATIONS ARE NOT QUITE OPTIMAL.
# NOTE #2: The remaining enabled infrastructure *is* needed.
unset_varnames_list = []
apply_function__readonly_variables(list_unset_variables,[detgammahat,detgammahatdD,ReU,ReUdD,
                                                         detgammahatdDD,ghatDD,ghatUU,ReDD,
                                                         GammahatUDD,ghatDDdD,ReDDdD,ReUdDD,
                                                         GammahatUDDdD, ghatDDdDD,ReDDdDD],protected_varnames, unset_varnames_list)
precomp_names_list = []
precomp_exprs_list = []

# apply_function__writeto_listvars() only works on list variables, so all scalars must call function separately:
# detgammahat = clean_transcendental(detgammahat,precomp_names_list, precomp_exprs_list)
# apply_function__writeto_listvars(clean_transcendental,[detgammahatdD,ReU,ReUdD,
#                                                        detgammahatdDD,ghatDD,ghatUU,ReDD,
#                                                        GammahatUDD,ghatDDdD,ReDDdD,ReUdDD,
#                                                        GammahatUDDdD, ghatDDdDD,ReDDdDD],precomp_names_list, precomp_exprs_list)
list_of_uniq_precomp_names0       = superfast_uniq(precomp_names_list)
list_of_uniq_precomp_expressions0 = superfast_uniq(precomp_exprs_list)
list_of_uniq_precomp_names = []
list_of_uniq_precomp_expressions = []
delete = False
for i in range(len(list_of_uniq_precomp_names0)):
    for j in range(len(list_of_uniq_precomp_names0)):
        if list_of_uniq_precomp_names0[j] in list_of_uniq_precomp_expressions0[i]:
            delete = True
            j = len(list_of_uniq_precomp_names0) + 1
    if not delete:
        list_of_uniq_precomp_names.extend([list_of_uniq_precomp_names0[i]])
        list_of_uniq_precomp_expressions.extend([list_of_uniq_precomp_expressions0[i]])
    else:
        delete = False

##print("patterns for precomputation:",list_of_uniq_precomp_expressions)

# Current variable names are designed to be completely unique and perfectly descriptive.
#  However, the names are ridiculously long. Here we shorten the variable names:
oldname = []
newname = []
for ll in range(len(list_of_uniq_precomp_names)):
#    list_of_uniq_precomp_expressions[ll] = str(sympify(list_of_uniq_precomp_expressions[ll].replace("NEG", "-").replace("COMMA", ",").replace("SINGQUOT","'").replace("ENDPAREN", ")").replace("BEGINPAREN","(").replace("SPAC"," ")))
    oldname.extend([sympify(list_of_uniq_precomp_names[ll])])
    newname.extend([sympify("tmpPC"+str(ll))])

# apply_function__writeto_listvars() only works on list variables, so all scalars must call function separately:
# detgammahat = shorten_varnames(detgammahat,oldname,newname)
# apply_function__writeto_listvars(shorten_varnames, [detgammahatdD, ReU, ReUdD,
#                                                     detgammahatdDD, ghatDD, ghatUU, ReDD,
#                                                     GammahatUDD, ghatDDdD, ReDDdD, ReUdDD,
#                                                     GammahatUDDdD, ghatDDdDD, ReDDdDD],oldname,newname)

if RunMode == "Everything" or RunMode == "RHSonly":
    # Output C code that defines these precomputed quantities.
    # First the actual code that will define the quantities before the initial data are computed:
    with open("common_functions/reference_metric/NRPy_codegen/NRPy_compute__precomputed_hatted_quantities.h", "w") as output:
        for ll in range(len(list_of_uniq_precomp_names)):
            output.write("precomp[idx].tmpPC"+str(ll)+" = "+nrpy_ccode_parser(list_of_uniq_precomp_expressions[ll])+";\n")
    # Then the definition of the struct
    with open("common_functions/reference_metric/NRPy_codegen/NRPy_struct__precomputed_hatted_quantities.h", "w") as output:
        output.write("typedef struct __precomputed_quantities__ {\n")
        for ll in range(len(list_of_uniq_precomp_names)):
            output.write("    "+PRECISION+" tmpPC"+str(ll)+";\n")
        output.write("} precomputed_quantities;\n")
    # Finally, the header file for #include'ing
    with open("common_functions/reference_metric/NRPy_codegen/NRPy_include__precomputed_hatted_quantities.h", "w") as output:
        for ll in range(len(list_of_uniq_precomp_names)):
            output.write("    const "+PRECISION+" tmpPC"+str(ll)+" = precomp[idx]."+"tmpPC"+str(ll)+";\n")

stop1 = time.time()
print("Finished hatted (ref.-metric-related) computations in \t" + str(round(stop1 - start1, 2)) + " seconds")

# FINISHED COMPUTING HATTED QUANTITIES!
# **************************************
# **************************************
# **************************************
