import sys

params_type = []
params_varname = []
params_value = []
runtime_params_type = []
runtime_params_varname = []

# Auxiliary finite difference derivative parameters
FDUPWINDINPLACE = "False"

# Force NRPy+ to work harder at simplifying expressions before performing the CSE.
#   DOES NOT HELP; cannot outsmart CSE. TODO: Remove this option.
Optimize    = "False"

EnableKreissOligerDissipation = "True"
#Setting above to "False" is equivalent to setting below EnableKO_direction = ["False","False","False"]
EnableKO_direction = ["True","True","True"]