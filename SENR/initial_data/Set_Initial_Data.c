//#define ENABLE_RANDOM_ID

// Set the BSSN initial data
void Set_Initial_Data(REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, paramstruct params) {
#include "parameters_readin-NRPyGEN.h" //Located in ../../common_functions/

#define sign(x) (((x) > 0) - ((x) < 0))

  LOOP_NOGZFILL(ii, jj, kk) {
    const int idx = IDX3(ii, jj, kk);

    const REAL t = 0.0;
    const REAL x1 = x1G[ii];
    const REAL x2 = x2G[jj];
    const REAL x3 = x3G[kk];

#include "NRPy_codegen/Initial_Data.h"
  } // END for(ii, jj, kk)

  return;
}

#ifdef ENABLE_RANDOM_ID
// Random perturbation about flat spacetime
// Used for SENR/Baumgarte comparison and Apples-with-Apples robust stability test
// http://www.appleswithapples.org/TestMethods/Tests/robust/robust.html
void Set_Random_Initial_Data(REAL *in_gfs, paramstruct params) {
#include "parameters_readin-NRPyGEN.h" //Located in ../../common_functions/

  const int convergence_factor = 4;

  //const REAL pert_mag = 2.0e-1;
  const REAL pert_mag = 1.0e-10 / (convergence_factor * convergence_factor);

  // Random number generator seeder is not threadsafe, so DON'T OPENMP THIS LOOP
  for(int kk = 0; kk < Npts3; kk++) {
    for(int jj = 0; jj < Npts2; jj++) {
      for(int ii = 0; ii < Npts1; ii++) {
        const int idx = IDX3(ii, jj, kk);
        srand48(1e6 * ii + 1e3 * jj + kk);

        for(int gf = H11; gf <= H33; gf++) in_gfs[IDX4pt(gf,idx)] = (2.0 * drand48() - 1.0) * pert_mag;
        in_gfs[CF][idx] = fabs((2.0 * drand48() - 1.0) * pert_mag);
        in_gfs[ALPHA][idx] = 1.0 - drand48() * pert_mag;
        in_gfs[TRK][idx] = (2.0 * drand48() - 1.0) * pert_mag;
        for(int gf = A11; gf <= A33; gf++) in_gfs[IDX4pt(gf,idx)] = (2.0 * drand48() - 1.0) * pert_mag;
        for(int gf = LAMB1; gf <= LAMB3; gf++) in_gfs[IDX4pt(gf,idx)] = (2.0 * drand48() - 1.0) * pert_mag;
        for(int gf = VET1; gf <= VET3; gf++) in_gfs[IDX4pt(gf,idx)] = (2.0 * drand48() - 1.0) * pert_mag;
        for(int gf = BET1; gf <= BET3; gf++) in_gfs[IDX4pt(gf,idx)] = (2.0 * drand48() - 1.0) * pert_mag;
      }
    }
  }

  return;
}
#endif
