#ifndef METRIC_INVERSE_AND_DETERMINANT_H__
#define METRIC_INVERSE_AND_DETERMINANT_H__

// The determinant of the conformal metric, normalized to the reference metric determinant
// DETG = det(gammabarDD) / det(gammahatDD)
void BSSN_Det(REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, REAL *gfs_aux, paramstruct params)
{
#include "parameters_readin-NRPyGEN.h" // Located in ../common_functions/
  
  for(int kk=0;kk<Npts3;kk++) {
    const REAL x3 = x3G[kk];
    for(int jj=0;jj<Npts2;jj++) {
      const REAL x2 = x2G[jj];
#pragma omp parallel for
#pragma ivdep
#pragma vector always
      //#pragma omp simd
      for(int ii=0;ii<Npts1;ii++) {
        const int idx = IDX3(ii, jj, kk);
        const REAL x1 = x1G[ii];
        
#include "diagnostics/NRPy_codegen/detg.h"
      }
    }
  }
  return;
}

// Rescale gammabarDD to normalized determinant
void Rescale_Metric_Det(REAL *x1G,REAL *x2G,REAL *x3G, REAL *in_gfs, REAL *gfs_aux, paramstruct params)
{
#include "parameters_readin-NRPyGEN.h" // Located in ../common_functions/

  // Get contaminated det(gammabarDD)
  //BSSN_Det(x1G,x2G,x3G, in_gfs,gfs_aux,params); // This is calculated at the end of RK4 Step 4

  //FIXME: This is some pretty egregious code duplication...
  LOOP_GZFILL(ii, jj, kk) {
    const int idx = IDX3(ii, jj, kk);

    // Scale factor to reset determinant to initial value Initial_det_gammabar
    // Initial det(gammabarDD) = 1 in NRPy:
    const REAL Initial_det_gammabar = 1.0;
#ifdef USE_LONG_DOUBLE
    REAL fac = cbrtl(Initial_det_gammabar / gfs_aux[IDX4pt(AUXDETG,idx)]);
#else
    REAL fac = cbrt(Initial_det_gammabar / gfs_aux[IDX4pt(AUXDETG,idx)]);
#endif
    // Kronecker delta
    const REAL KD[DIM][DIM] = {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}};

    // Scale out metric determinant
    int which_gf = H11;
    F2s12(i,j) { in_gfs[IDX4pt(which_gf,idx)] = fac * (KD[i][j] + in_gfs[IDX4pt(which_gf,idx)]) - KD[i][j]; which_gf++; }
  }

  // Recalculate determinant with cleaned metric
  BSSN_Det(x1G,x2G,x3G, in_gfs,gfs_aux, params);

  return;
}



// Remove residual trace from AbarDD
void Remove_Trace(REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, paramstruct params)
{
#include "parameters_readin-NRPyGEN.h" // Located in ../common_functions/

  LOOP_GZFILL(ii, jj, kk) {
    const int idx = IDX3(ii, jj, kk);

    const REAL x1 = x1G[ii];
    const REAL x2 = x2G[jj];
    const REAL x3 = x3G[kk];

    // Residual trace (divided by 3)
#include "diagnostics/NRPy_codegen/TraceAo3.h"

    // Kronecker delta
    const REAL KD[DIM][DIM] = {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}};

    // Subtract out residual trace
    int which_gf = A11;
    int which_gf_2 = H11;
    F2s12(i,j) { in_gfs[IDX4pt(which_gf,idx)] -= (KD[i][j] + in_gfs[IDX4pt(which_gf_2,idx)]) * Trace_Ao3; which_gf++; which_gf_2++; }
  }

  return;
}

#endif // METRIC_INVERSE_AND_DETERMINANT_H__
