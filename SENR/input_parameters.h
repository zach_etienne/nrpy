#ifndef INPUT_PARAMETERS_H__
#define INPUT_PARAMETERS_H__

//#define AUTO_BOUNDARY

#ifdef USE_LONG_DOUBLE
#define ERF(X, X0, W) (0.5L * (erfl( ( (X) - (X0) ) / (W) ) + 1.0L))
#else
#define ERF(X, X0, W) (0.5 * (erf( ( (X) - (X0) ) / (W) ) + 1.0))
#endif

// Gamma-driving shift damping parameter
const REAL eta = 2.0;

/* Kreiss-Oliger Dissipation Parameters */
const REAL diss_ko_strength = 0.99;
REAL KreissOligerDissipation_vs_DistFromOrigin(REAL dist_from_origin) {
  return ERF(dist_from_origin,2.0L,0.17L)*diss_ko_strength;
  //return ERF(dist_from_origin-1.0L,1.0L,0.17L)*diss_ko_strength;
}

/* UIUC puncture mass and dimensionless spin */
const REAL M = 1.0;
const REAL chi = 0.8;

/* BoostedSchwarzschild velocity */
const REAL vz = 0.5;

/* Brill-Lindquist Initial data parameters */
const REAL M1       = 0.5;
const REAL M2       = 0.5;
const REAL bScaleBL = 1.0;

/* Total number of iterations N_iters = FRAME_JUMP * N_FRAMES */
#define FRAME_JUMP 2000 /* Output one frame to data files when iteration# is a multiple of FRAME_JUMP */
#define N_FRAMES   100000 /* Output this many frames (plus the initial condition) */

/* Checkpointing */
const int CHECKPOINT_ID = 0;    // Set 1 to save checkpoint after setting up initial data. Useful if initial data take a long time to generate.
const int READ_CHECKPOINT = 0;    // Set 1 to read checkpoint
const int CHECKPOINT_EVERY = 20000; // Set negative to turn off checkpoint writing
const int KEEP_ALL_CHECKPOINTS = 1; // Set to 0 to keep only the most recent checkpoint, set to 1 to keep all checkpoints
const int EXTRACT_CHECKPOINT_DATA = 0; // Extract gridfunction data from a sequence of checkpoint files

/* WARNING: DO NOT CHANGE THIS ORDERING! */
const char *gf_name[NUM_EVOL_GFS] = {"vetU0", "vetU1", "vetU2",
                                     "hDD00", "hDD01", "hDD02", "hDD11", "hDD12", "hDD22",
                                     "cf",
                                     "alpha",
                                     "aDD00", "aDD01", "aDD02", "aDD11", "aDD12", "aDD22",
                                     "trK",
                                     "lambdaU0", "lambdaU1", "lambdaU2",
                                     "betU0", "betU1", "betU2"};

/* Outer boundary conditions */
//#define OUTER_BOUNDARY_EXACT
//#define OUTER_BOUNDARY_LIN_EXTRAP
#define OUTER_BOUNDARY_QUAD_EXTRAP
//#define OUTER_BOUNDARY_SOMMERFELD

/* Courant-Friedrichs-Lewy (CFL) factor for Runge-Kutta timestep */
const REAL CFL = 0.5;

/* Time step (in physical units) */
REAL dt;

/* Evaluate ADM mass, linear momentum, and angular momentum surface integrals */
#define EVALUATE_ADM_INTEGRALS
// Target (physical) radii for ADM surface integrals. Assumes yy[0][] stores the radial coordinate.
// Set to a negative value if you wish the ADM integrals to be computed at the outermost non-ghostzone radius.
// A radius larger than the grid extent will also default to the outermost non-ghostzone radius.
#define TARGET_RADIUS_NUM_RADII 9
const REAL TARGET_RADIUS[TARGET_RADIUS_NUM_RADII] = { 5.0, 10.0, 20.0, 40.0, 80.0, 160.0, 320.0, 640.0, -1e300 };

/* Set initial data to random perturbation about flat spacetime */
//#define ENABLE_RANDOM_ID

#endif // INPUT_PARAMETERS_H__
