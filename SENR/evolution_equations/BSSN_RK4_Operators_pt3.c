#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "loop_setup.h" // Located in ../../common_functions/
#include "SIMD_header.h" // Must be included after loop_setup.h! Located in ../../common_functions/
#include "../defines_for_BSSN_variables.h"
// Declare the struct datatype for precomputed hatted (reference-metric-related) quantities.
#include "reference_metric/NRPy_codegen/NRPy_struct__precomputed_hatted_quantities.h" // Located in ../../common_functions/

// Gamma-driving shift damping parameter
extern const REAL eta;

// RK4 operator for BSSN
void RK4_RHS_pt3(REAL t, REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux, REAL *gfs_rhs, precomputed_quantities *precomp, paramstruct params)
{

#if defined(ENABLE_SIMD_256_INTRINSICS) || defined(ENABLE_SIMD_512_INTRINSICS)
  SIMD_WARNING_MESSAGE;

#include "parameters_readin_SIMD-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs_SIMD.h" // Located in ../../common_functions/

#include "NRPy_codegen/NRPy_BSSN_RHS_pt3.h-SIMDconstants.h"

#ifdef ENABLE_SIMD_256_INTRINSICS
  __m256d etavec = _mm256_set1_pd(eta);
#else
  __m512d etavec = _mm512_set1_pd(eta);
#endif
#define eta etavec

  START_LOOP_NOGZFILL_SIMD(ii,jj,kk) {
#include "KreissOliger_header-SIMD.h"
#include "NRPy_codegen/NRPy_BSSN_RHS_pt3.h"
  } END_LOOP_NOGZFILL_SIMD;

#undef eta
  
#else

#include "parameters_readin-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs.h" // Located in ../../common_functions/

  START_LOOP_NOGZFILL(ii,jj,kk) {
#include "KreissOliger_header.h"
#include "NRPy_codegen/NRPy_BSSN_RHS_pt3.h"
  } END_LOOP_NOGZFILL;

#endif

  return;
}
