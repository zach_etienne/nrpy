const REAL yy0 = yy[IDX4pt(0,idx)];
const REAL yy1 = yy[IDX4pt(1,idx)];
const REAL yy2 = yy[IDX4pt(2,idx)];

const REAL dy0i = yy0 - yy[IDX4(0, ii - 1, jj, kk)];
const REAL dy1i = yy1 - yy[IDX4(1, ii - 1, jj, kk)];
const REAL dy2i = yy2 - yy[IDX4(2, ii - 1, jj, kk)];

const REAL dy0j = yy0 - yy[IDX4(0, ii, jj - 1, kk)];
const REAL dy1j = yy1 - yy[IDX4(1, ii, jj - 1, kk)];
const REAL dy2j = yy2 - yy[IDX4(2, ii, jj - 1, kk)];

const REAL dy0k = yy0 - yy[IDX4(0, ii, jj, kk - 1)];
const REAL dy1k = yy1 - yy[IDX4(1, ii, jj, kk - 1)];
const REAL dy2k = yy2 - yy[IDX4(2, ii, jj, kk - 1)];

#ifdef USE_LONG_DOUBLE
#define sqrt sqrtl
#endif
const REAL invdy0 = 1.0 / sqrt(dy0i * dy0i + dy1i * dy1i + dy2i * dy2i);
const REAL invdy1 = 1.0 / sqrt(dy0j * dy0j + dy1j * dy1j + dy2j * dy2j);
const REAL invdy2 = 1.0 / sqrt(dy0k * dy0k + dy1k * dy1k + dy2k * dy2k);
#ifdef USE_LONG_DOUBLE
#undef sqrt
#endif

const REAL diss_ko_strength = gfs_aux[IDX4pt(KOSTRNGH,idx)];
