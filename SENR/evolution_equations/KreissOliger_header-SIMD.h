#ifdef ENABLE_SIMD_256_INTRINSICS
const __m256d yy0 = _mm256_loadu_pd(&yy[IDX4pt(0,idx)]);
const __m256d yy1 = _mm256_loadu_pd(&yy[IDX4pt(1,idx)]);
const __m256d yy2 = _mm256_loadu_pd(&yy[IDX4pt(2,idx)]);

const __m256d dy0i = SubSIMD(yy0 , _mm256_loadu_pd(&yy[IDX4(0, ii - 1, jj, kk)]));
const __m256d dy1i = SubSIMD(yy1 , _mm256_loadu_pd(&yy[IDX4(1, ii - 1, jj, kk)]));
const __m256d dy2i = SubSIMD(yy2 , _mm256_loadu_pd(&yy[IDX4(2, ii - 1, jj, kk)]));

const __m256d dy0j = SubSIMD(yy0 , _mm256_loadu_pd(&yy[IDX4(0, ii, jj - 1, kk)]));
const __m256d dy1j = SubSIMD(yy1 , _mm256_loadu_pd(&yy[IDX4(1, ii, jj - 1, kk)]));
const __m256d dy2j = SubSIMD(yy2 , _mm256_loadu_pd(&yy[IDX4(2, ii, jj - 1, kk)]));

const __m256d dy0k = SubSIMD(yy0 , _mm256_loadu_pd(&yy[IDX4(0, ii, jj, kk - 1)]));
const __m256d dy1k = SubSIMD(yy1 , _mm256_loadu_pd(&yy[IDX4(1, ii, jj, kk - 1)]));
const __m256d dy2k = SubSIMD(yy2 , _mm256_loadu_pd(&yy[IDX4(2, ii, jj, kk - 1)]));

const double _KO_ONEd = 1.0; const __m256d _KO_ONEvec = _mm256_set1_pd(_KO_ONEd);
const __m256d invdy0 = DivSIMD(_KO_ONEvec , SqrtSIMD(FusedMulAddSIMD(dy0i , dy0i , FusedMulAddSIMD(dy1i , dy1i , MulSIMD(dy2i , dy2i)))));
const __m256d invdy1 = DivSIMD(_KO_ONEvec , SqrtSIMD(FusedMulAddSIMD(dy0j , dy0j , FusedMulAddSIMD(dy1j , dy1j , MulSIMD(dy2j , dy2j)))));
const __m256d invdy2 = DivSIMD(_KO_ONEvec , SqrtSIMD(FusedMulAddSIMD(dy0k , dy0k , FusedMulAddSIMD(dy1k , dy1k , MulSIMD(dy2k , dy2k)))));

const __m256d diss_ko_strength = _mm256_loadu_pd(&gfs_aux[IDX4pt(KOSTRNGH,idx)]);

#else // ENABLE_SIMD_512_INTRINSICS
const __m512d yy0 = _mm512_loadu_pd(&yy[IDX4pt(0,idx)]);
const __m512d yy1 = _mm512_loadu_pd(&yy[IDX4pt(1,idx)]);
const __m512d yy2 = _mm512_loadu_pd(&yy[IDX4pt(2,idx)]);

const __m512d dy0i = SubSIMD(yy0 , _mm512_loadu_pd(&yy[IDX4(0, ii - 1, jj, kk)]));
const __m512d dy1i = SubSIMD(yy1 , _mm512_loadu_pd(&yy[IDX4(1, ii - 1, jj, kk)]));
const __m512d dy2i = SubSIMD(yy2 , _mm512_loadu_pd(&yy[IDX4(2, ii - 1, jj, kk)]));

const __m512d dy0j = SubSIMD(yy0 , _mm512_loadu_pd(&yy[IDX4(0, ii, jj - 1, kk)]));
const __m512d dy1j = SubSIMD(yy1 , _mm512_loadu_pd(&yy[IDX4(1, ii, jj - 1, kk)]));
const __m512d dy2j = SubSIMD(yy2 , _mm512_loadu_pd(&yy[IDX4(2, ii, jj - 1, kk)]));

const __m512d dy0k = SubSIMD(yy0 , _mm512_loadu_pd(&yy[IDX4(0, ii, jj, kk - 1)]));
const __m512d dy1k = SubSIMD(yy1 , _mm512_loadu_pd(&yy[IDX4(1, ii, jj, kk - 1)]));
const __m512d dy2k = SubSIMD(yy2 , _mm512_loadu_pd(&yy[IDX4(2, ii, jj, kk - 1)]));

const double _KO_ONEd = 1.0; const __m512d _KO_ONEvec = _mm512_set1_pd(_KO_ONEd);
const __m512d invdy0 = DivSIMD(_KO_ONEvec , SqrtSIMD(FusedMulAddSIMD(dy0i , dy0i , FusedMulAddSIMD(dy1i , dy1i , MulSIMD(dy2i , dy2i)))));
const __m512d invdy1 = DivSIMD(_KO_ONEvec , SqrtSIMD(FusedMulAddSIMD(dy0j , dy0j , FusedMulAddSIMD(dy1j , dy1j , MulSIMD(dy2j , dy2j)))));
const __m512d invdy2 = DivSIMD(_KO_ONEvec , SqrtSIMD(FusedMulAddSIMD(dy0k , dy0k , FusedMulAddSIMD(dy1k , dy1k , MulSIMD(dy2k , dy2k)))));

const __m512d diss_ko_strength = _mm512_loadu_pd(&gfs_aux[IDX4pt(KOSTRNGH,idx)]);
#endif
