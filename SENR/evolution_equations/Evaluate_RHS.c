// Evaluate BSSN Ricci tensor components and evolution equation right-hand-sides, used in RK4_Steps.c
void Evaluate_RHS(const REAL t, REAL *x1G, REAL *x2G, REAL *x3G, REAL *yy, REAL *gfs_n, REAL *gfs_aux, REAL *gfs_k, precomputed_quantities *precomp, paramstruct params) {
  Compute_Ricci_pt1(t,x1G,x2G,x3G,yy,gfs_n,gfs_aux,precomp,params);
  Compute_Ricci_pt2(t,x1G,x2G,x3G,yy,gfs_n,gfs_aux,precomp,params);
  RK4_RHS_pt1(t, x1G,x2G,x3G, yy, gfs_n, gfs_aux, gfs_k, precomp, params); // k1 = L1(un, vn)
  RK4_RHS_pt2(t, x1G,x2G,x3G, yy, gfs_n, gfs_aux, gfs_k, precomp, params); // k1 = L1(un, vn)
  RK4_RHS_pt3(t, x1G,x2G,x3G, yy, gfs_n, gfs_aux, gfs_k, precomp, params); // k1 = L1(un, vn)
  RK4_RHS_pt4(t, x1G,x2G,x3G, yy, gfs_n, gfs_aux, gfs_k, precomp, params); // k1 = L1(un, vn)

  return;
}
