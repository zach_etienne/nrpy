// Evaluate the null expansion assuming spherical symmetry and spherical polar coordinates
void Spherical_Symmetry_Null_Expansion(REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL *gfs_aux,paramstruct params)
{
#include "parameters_readin-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs.h" // Located in ../../common_functions/

  LOOP_NOGZFILL(ii, jj, kk)
    {
      const int idx = IDX3(ii, jj, kk);
      const REAL x1 = x1G[ii];
      const REAL x2 = x2G[jj];
      const REAL x3 = x3G[kk];

#include "NRPy_codegen/NullExpansion.h"
    }
  return;
}

// Find the apparent horizon radius rAH, assuming spherical symmetry
REAL Spherical_Symmetry_Find_Horizon_Radius(REAL *yy, REAL *gfs_aux,paramstruct params)
{
#include "parameters_readin-NRPyGEN.h" // Located in ../../common_functions/

  REAL rAH = 0.0;
  
  const int jj = NGHOSTS;
  const int kk = NGHOSTS;

  for(int ii = NGHOSTS; ii < Npts[0] - NGHOSTS - 1; ii++)
    {
      const int idx0 = IDX3(ii, jj, kk);
      const int idx1 = IDX3(ii + 1, jj, kk);
      const REAL TH0 = gfs_aux[IDX4pt(NULLEXP,idx0)];
      const REAL TH1 = gfs_aux[IDX4pt(NULLEXP,idx1)];

      // Crossing \Theta = 0
      if(TH0 * TH1 < 0.0)
	{
	  const REAL r0 = yy[IDX4pt(0,idx0)];
	  const REAL r1 = yy[IDX4pt(0,idx1)];
		  
	  rAH = r0 - TH0 * (r1 - r0) / (TH1 - TH0);
	}
    }

  return rAH;
}
