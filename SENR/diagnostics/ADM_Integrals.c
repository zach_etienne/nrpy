// Evaluate the ADM mass, linear momentum, and angular momentum surface integrals
// This method assumes x1 takes the form of a radial coordinate in the far field
REAL ADM_Integrals(REAL *x1G,REAL *x2G,REAL *x3G, REAL *yy, REAL *in_gfs, REAL ADM_MPJ[7], paramstruct params, const REAL TARGET_RADIUS_i) {
#include "parameters_readin-NRPyGEN.h" // Located in ../../common_functions/
#include "read_FD_dxs.h" // Located in ../../common_functions/

  // Initial Riemann sum
  REAL integral_M = 0.0;
  REAL integral_P1 = 0.0, integral_P2 = 0.0, integral_P3 = 0.0;
  REAL integral_J1 = 0.0, integral_J2 = 0.0, integral_J3 = 0.0;

  int ii=Npts1 - 2 * NGHOSTS; // Default to the largest x1 such that the finite difference stencils are entirely in the physical domain
  double min_distance_from_TARGET_RADIUS_i=1e300;
  REAL ACTUAL_RADIUS = yy[IDX4(0,ii, NGHOSTS, NGHOSTS)];
  for(int i = NGHOSTS; i < Npts1 - NGHOSTS; i++) {
    const int idx = IDX3(i, NGHOSTS, NGHOSTS);
    if(fabs((double)(yy[IDX4pt(0,idx)]-TARGET_RADIUS_i)) < min_distance_from_TARGET_RADIUS_i) {
      ii = i;
      ACTUAL_RADIUS = yy[IDX4pt(0,idx)];
      min_distance_from_TARGET_RADIUS_i = fabs( (double)(yy[IDX4pt(0,idx)]-TARGET_RADIUS_i) );
    }
  }

  // Integrate over solid angle
#pragma omp parallel for reduction(+: integral_M,integral_P1,integral_P2,integral_P3,integral_J1,integral_J2,integral_J3)
  for(int kk = NGHOSTS; kk < Npts3 - NGHOSTS; kk++) {
    for(int jj = NGHOSTS; jj < Npts2 - NGHOSTS; jj++) {
      const int idx = IDX3(ii, jj, kk);

      const REAL x1 = x1G[ii];
      const REAL x2 = x2G[jj];
      const REAL x3 = x3G[kk];

#include "NRPy_codegen/ADM_integrand.h"

      integral_M += integrand_M;
      integral_P1 += integrand_P1;
      integral_P2 += integrand_P2;
      integral_P3 += integrand_P3;
      integral_J1 += integrand_J1;
      integral_J2 += integrand_J2;
      integral_J3 += integrand_J3;
    }
  }

  // Area element
  const REAL dA = del[1] * del[2];

  // ADM mass
  ADM_MPJ[0] = integral_M * dA;

  // ADM linear momentum
  ADM_MPJ[1] = integral_P1 * dA;
  ADM_MPJ[2] = integral_P2 * dA;
  ADM_MPJ[3] = integral_P3 * dA;

  // ADM angular momentum
  ADM_MPJ[4] = integral_J1 * dA;
  ADM_MPJ[5] = integral_J2 * dA;
  ADM_MPJ[6] = integral_J3 * dA;

  return ACTUAL_RADIUS;
}
