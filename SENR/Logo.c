// Courtesy http://www.patorjk.com/software/taag/#p=testall&f=Graffiti&t=*SENR*
// Adjusted \ --> \\ to be printf()-friendly.
printf("\x1B[36m    _         _______. _______ .__   __. .______         _    \n\x1B[0m");
printf("\x1B[36m /\\| |/\\     /       ||   ____||  \\ |  | |   _  \\     /\\| |/\\ \n\x1B[0m");
printf("\x1B[36m \\ ` ' /    |   (----`|  |__   |   \\|  | |  |_)  |    \\ ` ' / \n\x1B[0m");
printf("\x1B[36m|_     _|    \\   \\    |   __|  |  . `  | |      /    |_     _|\n\x1B[0m");
printf("\x1B[36m / , . \\ .----)   |   |  |____ |  |\\   | |  |\\  \\----./ , . \\ \n\x1B[0m");
printf("\x1B[36m \\/|_|\\/ |_______/    |_______||__| \\__| | _| `._____|\\/|_|\\/ \n\x1B[0m");
                                                              
