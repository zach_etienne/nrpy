#ifndef BSSN_H__
#define BSSN_H__

#include "loop_setup.h"
#include "grid_and_gridfunction_setup.h"

double GammahatUDD(const int i,const int j,const int k) { return 0.0; }
double Dhat_gammabarDDD   (double gammabarDDdD  [DIM][DIM][DIM]     ,const int i,const int j,const int k            ) { return gammabarDDdD[i][j][k]     ; }
double Dhat_gammabarDDD_dD(double gammabarDDd2DD[DIM][DIM][DIM][DIM],const int i,const int j,const int k,const int l) { return gammabarDDd2DD[i][j][k][l]; }
double Dhat_LambarUD(double LambarUdD[DIM][DIM], const int i,const int j) { return LambarUdD[i][j]; }
double Dhat2_betaUDD(double betaUdDD[DIM][DIM][DIM],const int i,const int j,const int k) { return betaUdDD[i][j][k]; }

void BSSN_rhs(double **in_gfs,  double **rhs_gfs) {
  /*
    At each iteration of our main loop (below), we must read in each 
    BSSN variable gridfunction at point ii,jj,kk AND MANY NEIGHBORING POINTS
    (so that we may evaluate the finite difference derivatives).
    Geometrically, the neighboring points includes the intersection of
    three planes centered at the point where the derivatives must be computed.
    
    Reading in the gridfunction data from main memory is a very expensive operation,
    as every cache miss can waste ~tens to hundreds of CPU cycles.
    (see http://igoro.com/archive/gallery-of-processor-cache-effects/ for beautiful
    illustrations and analysis.) So our top goal should be to NEVER REPEAT reading
    a gridfunction from main memory at a given ii,jj,kk point.

    Most BSSN gridfunctions need only read in data for first spatial derivatives, but
    the 3-metric, lapse, shift, & conformal factor need second derivatives. The finite 
    difference (FD) stencils for all DIM first derivatives look like DIM intersecting 
    line segments, intersecting at the point from where the deriv is computed.
    Meanwhile, the FD stencils for the six independent second derivatives look like 
    DIM-1 intersecting planes, again centered at the point where the deriv is computed.

    Thus for gridfunctions requiring only first derivatives' computation, we need to 
    read in (2*NGHOSTS)*NDIM + 1 points (the +1 might be needed for upwinded derivs)

    And for gridfunctions for which second derivatives need to be taken, we need 
    for 1D:
    (2*NGHOSTS+1)
    for 2D:
    (2*NGHOSTS+1)*(2*NGHOSTS+1)
    for 3D (there's a line: 
    (2*NGHOSTS+1)*(2*NGHOSTS+1) + (2*NGHOSTS)*(2*NGHOSTS)

    // Generate a string of gridfunction indices to prevent any gridpoint to be pulled from main memory more than once.
    */

  LOOP_GZFILL(ii,jj,kk) {
    double gammabarDD[DIM][DIM];
    int which_gf=G11;
    for(int j=0;j<DIM;j++) for(int k=j;k<DIM;k++) { gammabarDD[j][k] = in_gfs[which_gf][GFIDX(ii,jj,kk)]; which_gf++; }
    in_gfs[DETG][GFIDX(ii,jj,kk)] = determinant(gammabarDD);
  }
  //Start the timer, for benchmarking
  struct timeval start, end;
  long mtime, seconds, useconds;
  gettimeofday(&start, NULL);

  LOOP_NOGZFILL(ii,jj,kk) {
    /***************************************/
    /**** DECLARE LOCAL VARIABLE ARRAYS ****/
    /***************************************/
    // Declare array that stores the gridfunction data on the stencils
    double locGF[2*NGHOSTS+1][2*NGHOSTS+1][2*NGHOSTS+1]; // About 10KB for NGHOSTS=5.
    // Declare local variable arrays

#ifdef SHIFTADVECT
    double betaUdupD[DIM][DIM],betaUddnD[DIM][DIM];
#endif //SHIFTADVECT
#ifdef BIADVECT
    double BUdupD[DIM][DIM],BUddnD[DIM][DIM];
#endif // BIADVECT

    double gammabarDD[DIM][DIM],gammabarDDdD[DIM][DIM][DIM],gammabarDDdDD[DIM][DIM][DIM][DIM], gammabarDDdupD[DIM][DIM][DIM],gammabarDDddnD[DIM][DIM][DIM];
    double phi,phidD[DIM],phidDD[DIM][DIM], phidupD[DIM],phiddnD[DIM];
    double alpha,alphadD[DIM],alphadDD[DIM][DIM], alphadupD[DIM],alphaddnD[DIM];
    double betaU[DIM],betaUdD[DIM][DIM],betaUdDD[DIM][DIM][DIM];
    double detgammabar,detgammabardD[DIM],detgammabardDD[DIM][DIM];

    double AbarDD[DIM][DIM],AbarDDdD[DIM][DIM][DIM], AbarDDdupD[DIM][DIM][DIM],AbarDDddnD[DIM][DIM][DIM];
    double trK,trKdD[DIM], trKdupD[DIM],trKddnD[DIM];
    double LambarU[DIM],LambarUdD[DIM][DIM], LambarUdupD[DIM][DIM],LambarUddnD[DIM][DIM];
    double BU[DIM];
#ifdef BIADVECT
    double BUdD[DIM][DIM];
#endif //BIADVECT

    /**********************************************************************/
    /**** READ GRIDFUNCTIONS FROM MAIN MEMORY, TAKE FINITE DIFF DERIVS ****/
    /**********************************************************************/
    // Gridfunctions for which we'll need to take 2nd derivatives:
    double dnwind[DIM];
    double upwind[DIM];

    /* Inputs for autogenFDcode/ code generation. 
       Note that UPDOWNWIND is contingent on UPWIND being
       #define'd, and BU/betaU's UPDOWNWIND are contingent
       on SHIFTADVECT & BIADVECT being #define'd, respectively.

       To generate all finite difference code for this file,
       simply go to the autogenFDcode directory and run
       ./gen_stencil [this filename]

       DVGENSTART BSSN_rhs__input_gfdata_and_compute_derivs.h
       betaU	        ZERO	FIRST	SECOND	UPDOWNWIND
       gammabarDD	ZERO	FIRST	SECOND	UPDOWNWIND
       phi		ZERO	FIRST	SECOND	UPDOWNWIND
       alpha     	ZERO	FIRST	SECOND	UPDOWNWIND
       AbarDD		ZERO	FIRST	UPDOWNWIND
       trK		ZERO	FIRST   UPDOWNWIND
       LambarU   	ZERO	FIRST	UPDOWNWIND
       BU		ZERO	UPDOWNWIND
       detgammabar 	ZERO	FIRST   SECOND
       DVGENEND
    */

#include "autogenFDcode/BSSN_rhs__input_gfdata_and_compute_derivs.h"

    /********************************************************/
    /**** SET SYMMETRIC COMPONENTS OF SECOND DERIVATIVES ****/
    /********************************************************/
    SS3s23(betaUdDD);
    SS4s34(gammabarDDdDD);
    SS2s12(phidDD);
    SS2s12(alphadDD);
    SS2s12(detgammabardDD);

#ifdef UPWIND
    F1(j) { dnwind[j] = 1.0; upwind[j] = 0.0; }
    F1(j) { if(betaU[j]>0.0) { dnwind[j] = 0.0; upwind[j] = 1.0; } }

#ifdef SHIFTADVECT
    F2(i,j)   {  betaUdupD[i][j]         = upwind[j]*betaUdupD[i][j]         + dnwind[j]*betaUddnD[i][j]; }
#endif // SHIFTADVECT
    F3(i,j,k) {  gammabarDDdupD[i][j][k] = upwind[k]*gammabarDDdupD[i][j][k] + dnwind[k]*gammabarDDddnD[i][j][k]; }
    F1(i)     {  phidupD[i]              = upwind[i]*phidupD[i]              + dnwind[i]*phiddnD[i]; }
    F1(i)     {  alphadupD[i]            = upwind[i]*alphadupD[i]            + dnwind[i]*alphaddnD[i]; }
    F3(i,j,k) {  AbarDDdupD[i][j][k]     = upwind[k]*AbarDDdupD[i][j][k]     + dnwind[k]*AbarDDddnD[i][j][k]; }
    F1(i)     {  trKdupD[i]              = upwind[i]*trKdupD[i]              + dnwind[i]*trKddnD[i]; }
    F2(i,j)   {  LambarUdupD[i][j]       = upwind[j]*LambarUdupD[i][j]       + dnwind[j]*LambarUddnD[i][j]; }
#ifdef BIADVECT
    F2(i,j)   {  BUdupD[i][j]            = upwind[j]*BUdupD[i][j]            + dnwind[j]*BUddnD[i][j]; }
#endif // BIADVECT
#else // UPWIND
#ifdef SHIFTADVECT
    F2(i,j)   {  betaUdupD[i][j]         = betaUdD[i][j];         }
#endif // SHIFTADVECT
    F3(i,j,k) {  gammabarDDdupD[i][j][k] = gammabarDDdD[i][j][k]; }
    F1(i)     {  phidupD[i]              = phidD[i];              }
    F1(i)     {  alphadupD[i]            = alphadD[i];            }
    F3(i,j,k) {  AbarDDdupD[i][j][k]     = AbarDDdD[i][j][k];     }
    F1(i)     {  trKdupD[i]              = trKdD[i];              }
    F2(i,j)   {  LambarUdupD[i][j]       = LambarUdD[i][j];       }
#ifdef BIADVECT
    F2(i,j)   {  BUdupD[i][j]            = BUdD[i][j];            }
#endif // BIADVECT
#endif // UPWIND


    /****************************************************/
    /**** SET SYMMETRIC COMPONENTS OF RANK-2 TENSORS ****/
    /****************************************************/
    // For efficiency, will ultimately fix loops below. 
    // NOTE THAT 2nd DERIVATIVE SYMMETRIES HAVE ALREADY BEEN SET.
    SS2s12(gammabarDD); SS3s12(gammabarDDdD); SS4s12(gammabarDDdDD);
    SS2s12(AbarDD); SS3s12(AbarDDdD);

    /***********************/
    /**** gammabar^{ij} ****/
    /***********************/
    I2NI(gammabarUU); Invertgammabar(gammabarUU, gammabarDD,detgammabar);
    SS2s12(gammabarUU);
    
    /**************************************************************************************/
    /**** Gammabar^{i}_{jk}, Gammabar_{ijk}, (\Delta Gamma)^i, Abar^i_j, and Abar^{ij} ****/
    /**************************************************************************************/
 
    I3NI(GammabarDDD); I3(GammabarUDD);
    F3(i,j,k)   { GammabarDDD[i][j][k] = (1.0/2.0) * (gammabarDDdD[i][j][k] + gammabarDDdD[i][k][j] - gammabarDDdD[j][k][i]); }
    F4(i,j,k,l) { GammabarUDD[i][j][k] += gammabarUU[i][l] * GammabarDDD[l][j][k]; }

    I3NI(DGammaUDD); I3(DGammaDDD);
    F3(i,j,k)   { DGammaUDD[i][j][k]  = GammabarUDD[i][j][k] - GammahatUDD(i,j,k); }
    F4(i,j,k,l) { DGammaDDD[i][j][k] += gammabarDD[i][l]*DGammaUDD[l][j][k]; }
    
    I1(DGammaU);
    F3(i,j,k) { DGammaU[i] += gammabarUU[j][k]*DGammaUDD[i][j][k]; }
    
    I2(AbarUD); I2(AbarUU);
    F3(i,j,l) AbarUD[i][j] += gammabarUU[i][l]*AbarDD[l][j];
    F3(i,j,l) AbarUU[i][j] += gammabarUU[i][l]*AbarUD[j][l];

    /*************************/
    /**** LIE DERIVATIVES ****/
    /*************************/
    /* Lie derivatives of gammabar and Abar. 
     *    Note that gammabar and Abar are ordinary tensors, with density weight 0,
     *    unlike orig. BSSN (Eqs. 24 and 25 in http://arxiv.org/pdf/1205.5111.pdf). 
     *    Recall that in the original BSSN equations, \tilde{\gamma}_{ij} 
     *    and \tilde{A}_{ij} have tensor weight -2/3. Thus according to the 
     *    definition of Lie derivative (https://en.wikipedia.org/wiki/Lie_derivative), 
     *    the term with the  (-2/3) multiplying the divergence of the shift in
     *    Eqs. 24 and 25 in http://arxiv.org/pdf/1205.5111.pdf DISAPPEARS.
     */
    I2(Lbeta_gammabarDD); I2(Lbeta_AbarDD);
    F3(i,j,k) { Lbeta_gammabarDD[i][j] += /* First term               */ betaU[k]*gammabarDDdupD[i][j][k] + 
        /**/                              /* Second (symmetrized) term*/ gammabarDD[k][i]*betaUdD[k][j] + gammabarDD[k][j]*betaUdD[k][i];
      /**/      Lbeta_AbarDD    [i][j] += /* First term               */ betaU[k]*AbarDDdupD[i][j][k] + 
        /**/                              /* Second (symmetrized) term*/ AbarDD[k][i]*betaUdD[k][j] + AbarDD[k][j]*betaUdD[k][i]; }

    // Eq. 11.52 in Shapiro & Baumgarte:
    I0(Lbeta_trK); 
    F1(l) { Lbeta_trK += betaU[l]*trKdupD[l]; }
    // phi is simply a scalar with tensor density zero.
    I0(Lbeta_phi);
    F1(l) { Lbeta_phi += betaU[l]*phidupD[l]; }
    // For advecting lapse gauge: /**/  Lbeta_alpha += betaU[l]*alphadupD[l]; }

    /* Lie derivatives of \bar{\Lambda}^i.
     *     Again, \bar{\Lambda}^i is an ordinary tensor with density weight 0, unlike
     *     \tilde{\Gamma}^i in original BSSN. Thus the Lie derivative will be given by
     *     (see https://en.wikipedia.org/wiki/Lie_derivative):
     *     Lbeta_LambarU^i = \beta^j \partial_j \Lambar^i - \partial_j \beta^i \Lambar^j
     */
    I1(Lbeta_LambarU);
    F2(i,j) { Lbeta_LambarU[i] += betaU[j]*LambarUdupD[i][j] - betaUdD[i][j]*LambarU[j]; }
  
    /**********************************************/
    /**** COVARIANT DERIVATIVES OF PHI & ALPHA ****/
    /**********************************************/
    // lapse is a scalar, so \bar D_i's are easy to compute:
    I1NI(Dbar_alphaD); I1(Dbar_alphaU);
    F1(i)   { Dbar_alphaD[i]  = alphadD[i]; }
    F2(i,j) { Dbar_alphaU[i] += gammabarUU[i][j]*alphadD[j]; }

    // Bottom of page 56 in Shapiro & Baumgarte: phi behaves as a scalar under \bar D_i's:
    I1NI(Dbar_phiD); I1(Dbar_phiU);
    F1(i)   { Dbar_phiD[i] = phidD[i]; }
    F2(i,j) { Dbar_phiU[i] += gammabarUU[i][j]*phidD[j]; }

    I2NI(Dbar2_phiDD); I0(Dbar_phi2);
    // Dbar2_phiDD = Dbar_i Dbar_j \phi:
    F2(i,j)   { Dbar2_phiDD[i][j]  = phidDD[i][j]; }
    F3(i,j,l) { Dbar2_phiDD[i][j] -= phidD[l]*GammabarUDD[l][i][j]; }
    // Dbar_phi2 = Dbarphi^i Dbarphi_i
    F1(i)   { Dbar_phi2 += Dbar_phiD[i]*Dbar_phiU[i]; }

    // Derivatives of the lapse
    I2NI(Dbar2_alphaDD); I0(Dbar2_alpha);
    F2(i,j)   { Dbar2_alphaDD[i][j]  = alphadDD[i][j]; }
    F3(i,j,l) { Dbar2_alphaDD[i][j] -= alphadD[l]*GammabarUDD[l][i][j]; }
    F2(i,j) { Dbar2_alpha += gammabarUU[i][j]*Dbar2_alphaDD[i][j]; }

    // All terms involving derivatives of phi in Eq. 9b, http://arxiv.org/pdf/1211.6632.pdf.
    I2NI(phitermsDD);
    F2(i,j)     { phitermsDD[i][j]  = /* 1st phi term */ - 2.0*alpha*Dbar2_phiDD[i][j]
        /**/                          /* 2nd phi term */ + 4.0*alpha*Dbar_phiD[i]*Dbar_phiD[j]
        /**/                          /* 3rd phi term */ + 2.0*(Dbar_alphaD[i]*Dbar_phiD[j] + Dbar_alphaD[j]*Dbar_phiD[i]); /*FIXME: CHECK SIGN. */ }

    /***************************************************/
    /**** COVARIANT DERIVATIVES OF GAMMABAR & SHIFT ****/
    /***************************************************/
    // Eq. 27 in Baumgarte et al. http://arxiv.org/pdf/1211.6632.pdf
    I4NI(Dhat2_gammabarDDDD);
    /*                                                                    \partial_k (Dhat_l gammabar_{ij} */
    F4(i,j,k,l)   { Dhat2_gammabarDDDD[i][j][l][k]  = /* First term  */   Dhat_gammabarDDD_dD(gammabarDDdDD,i,j,l,k); }
    F5(i,j,k,l,m) { Dhat2_gammabarDDDD[i][j][k][l] += /* Second term */ - Dhat_gammabarDDD(gammabarDDdD,i,j,m)*GammahatUDD(m,l,k) 
        /**/                                          /* Third term  */ - Dhat_gammabarDDD(gammabarDDdD,m,j,l)*GammahatUDD(m,i,k)
        /**/                                          /* Fourth term */ - Dhat_gammabarDDD(gammabarDDdD,i,m,l)*GammahatUDD(m,j,k); }

    // \bar D_i \beta^j = \partial_i \beta^j + \Gammabar^j_{il} \beta^l
    I2NI(Dbar_betaUD);
    F2(i,j)   { Dbar_betaUD[i][j]  = /* First term */ betaUdD[i][j]; }
    F3(i,j,l) { Dbar_betaUD[i][j] += /* Second term*/ GammabarUDD[j][i][l] * betaU[l]; }
    // \bar D_i \beta^i:
    I0(Dbar_beta_contraction);
    F1(i) { Dbar_beta_contraction += Dbar_betaUD[i][i]; }

    // \bar{D}^i \bar{D}_j \beta^j = gammabar^{ik} [ beta^j_{,jk} + 1/2*(gammabar_{,jk} beta^j - gammabar_{,j} gammabar_{,k} beta^j/gammabar + gammabar_{,j} beta^j_{,k})/gammabar ]
    // (See notes/notes.tex for derivation of full expression.)
    I1(Dbar2_betacontractionD); I1(Dbar2_betacontractionU);
    // First compute \bar{D}_i \bar{D}_j \beta^j:
    F2(k,j) { Dbar2_betacontractionD[k] += /* First term */ betaUdDD[j][j][k] +
        /**/                               /* Second term*/ (1.0/2.0)*(detgammabardDD[j][k]*betaU[j] - detgammabardD[j]*detgammabardD[k]*betaU[j]/detgammabar + detgammabardD[j]*betaUdD[j][k])/detgammabar; }
    // Apply raising operator to contract.
    F2(i,k) { Dbar2_betacontractionU[i] += gammabarUU[i][k]*Dbar2_betacontractionD[k]; }

    /*****************************************/
    /**** RICCI TENSOR, WRT BARRED METRIC ****/
    /*****************************************/
    I2(RbarDD);
    F4(i,j,k,l)   { RbarDD[i][j] += /* First term  */ -(1.0/2.0)*gammabarUU[k][l]*Dhat2_gammabarDDDD[i][j][k][l]; }
    F3(i,j,k)     { RbarDD[i][j] += /* Second term */  (1.0/2.0)*( gammabarDD[k][i]*Dhat_LambarUD(LambarUdD,k,j) +
                                                                   gammabarDD[k][j]*Dhat_LambarUD(LambarUdD,k,i) ) +
        /**/                        /* Third term  */ DGammaU[k]*(1.0/2.0)*( DGammaDDD[i][j][k] + DGammaDDD[j][i][k] ); }
    F5(i,j,k,l,m) { RbarDD[i][j] += /* Fourth term */ gammabarUU[k][l]*( (DGammaUDD[m][k][i]*DGammaDDD[j][m][l] + /* Cancelled 0.5*2 */ 
                                                                          DGammaUDD[m][k][j]*DGammaDDD[i][m][l] )
                                                                         + DGammaUDD[m][i][k]*DGammaDDD[m][j][l] ); }

    const double deltaUD[3][3] = { { 1,0,0 }, {0,1,0}, {0,0,1} }; // FIXME: DIM=3 only.
    /*******************************************************************/
    /**** TRACE-FREE PARTS OF RbarDD, phitermsDD, and Dbar2_alphaDD ****/
    /*******************************************************************/
    // For a tensor P_{ij}, TF(P)_{ij} = P_{ij} - gammabar_{ij} gammabar^{lm} P_{lm} * (1/3)
    I2NI(RbarTFDD); I2NI(phitermsTFDD); I2NI(Dbar2_alphaTFDD);
    F2(i,j)     { RbarTFDD[i][j]        = RbarDD[i][j]; 
      /**/        phitermsTFDD[i][j]    = phitermsDD[i][j];
      /**/        Dbar2_alphaTFDD[i][j] = Dbar2_alphaDD[i][j]; }
    F4(i,j,l,m) { RbarTFDD[i][j]        -= gammabarDD[i][j] * gammabarUU[l][m] * RbarDD[l][m]       *(1.0/3.0);   // FIXME: 1/3 comes from dimension...
      /**/        phitermsTFDD[i][j]    -= gammabarDD[i][j] * gammabarUU[l][m] * phitermsDD[l][m]   *(1.0/3.0);   // FIXME: 1/3 comes from dimension...
      /**/        Dbar2_alphaTFDD[i][j] -= gammabarDD[i][j] * gammabarUU[l][m] * Dbar2_alphaDD[l][m]*(1.0/3.0); } // FIXME: 1/3 comes from dimension...

    /*******************/
    /**** BSSN RHSs ****/
    /*******************/
    // Declare RHS variables
    I2NI(gammabar_rhsDD); I2NI(Abar_rhsDD); I0NI(phi_rhs); I0NI(trK_rhs); I1NI(Lambar_rhsU); I0NI(alpha_rhs); I1NI(beta_rhsU); I1NI(B_rhsU);

    // Set RHS variables

    // Eq. 9a in http://arxiv.org/pdf/1211.6632.pdf:
    F2(i,j)   { gammabar_rhsDD[i][j]  = /* zeroth */ Lbeta_gammabarDD[i][j] + 
        /**/                            /* first  */ (-2.0/3.0)*gammabarDD[i][j]*Dbar_beta_contraction +
        /**/                            /* second */ (-2.0)*alpha*AbarDD[i][j]; }

    double psim4 = exp(-4.0*phi);

    // Eq. 9b in http://arxiv.org/pdf/1211.6632.pdf:
    F2(i,j)   { Abar_rhsDD[i][j]  = /* zeroth term: */ Lbeta_AbarDD[i][j] +
        /**/                        /* first term : */ (-2.0/3.0)*AbarDD[i][j]*Dbar_beta_contraction +
        /**/                        /* third term : */ alpha*AbarDD[i][j]*trK +
        /**/                        /* fourth term: */ psim4*(phitermsTFDD[i][j] - Dbar2_alphaTFDD[i][j] + alpha*RbarTFDD[i][j]); }
    F3(i,j,l) { Abar_rhsDD[i][j] += /* second term: */ (-2.0)*alpha*AbarDD[i][l]*AbarUD[l][j]; }

    // From Eq. 9c in http://arxiv.org/pdf/1211.6632.pdf:
    // Note that Lie derivative can be identified from: http://arxiv.org/pdf/1205.5111.pdf , Eq 23.
    // Also, chi = exp(-4*phi) -> \partial_t chi = -4*exp(-4*phi)*(\partial_t phi) = -4*chi*(\partial_t phi)
    //           -> chi_rhs = -4*chi*phi_rhs.
    /**/        phi_rhs  =               /* zeroth */ Lbeta_phi + // <- Lie derivative of a scalar
      /**/                  +(1.0/6.0)*( /* first  */ Dbar_beta_contraction + 
                                         /* second */ -alpha*trK );

    // Eq. 9d in http://arxiv.org/pdf/1211.6632.pdf:
    /**/        trK_rhs  = /* zeroth */ Lbeta_trK + /* first */ alpha/3.0 * trK*trK - /* third */ psim4*Dbar2_alpha;
    F2(i,j)   { trK_rhs += /* second */ alpha * ( AbarDD[i][j]*AbarUU[i][j] ); }
    F1(i)     { trK_rhs += /* fourth */ -psim4*2.0*Dbar_alphaU[i]*Dbar_phiD[i]; } /* FIXME: missing matter source terms */

    // Eq. 9e in http://arxiv.org/pdf/1211.6632.pdf:
    F1(i)     { Lambar_rhsU[i]  = /* zeroth */ Lbeta_LambarU[i] +
        /**/                      /* second */ (2.0/3.0)*DGammaU[i]*Dbar_beta_contraction +
        /**/                      /* third  */ (1.0/3.0)*Dbar2_betacontractionU[i]; }
    F2(i,j)   { Lambar_rhsU[i] += /* fifth  */ -(4.0/3.0)*alpha*gammabarUU[i][j]*trKdD[j] 
        /**/                      /* sixth     FIXME: MATTER EVOLUTIONS : - 16.0*M_PI*alpha*gammabarUU[i][j]*SD[j] */; }
    F3(i,j,k) { Lambar_rhsU[i] += /* first  */ gammabarUU[j][k]*Dhat2_betaUDD(betaUdDD,i,j,k) +
        /**/                      /* fourth */ -2.0*AbarUU[j][k]*(deltaUD[i][j]*alphadD[k] - 6.0*alpha*deltaUD[i][j]*phidD[k] - alpha*DGammaUDD[i][j][k]); }

    // Eq. 15 in http://arxiv.org/pdf/1211.6632.pdf, except with advecting lapse:
    alpha_rhs = /* for advecting lapse gauge: Lbeta_alpha */ +
      /**/      /* first term */ -2.0*alpha*trK;

    // Eq. 16a: non-advecting, Gamma-driving shift: beta_rhsU[i] = BU[i];
    F1(i)   { beta_rhsU[i]  = BU[i]; }
#ifdef SHIFTADVECT
    F2(i,j) { beta_rhsU[i] += betaU[j]*betaUdupD[j][i]; }
#endif
  
    //B_rhsU[i] = (3.0/4.0)*Lambar_rhsU[i] - eta*BU[i];
    // Eq. 16b: non-advecting, Gamma-driving shift:
    const double eta = 1.0; // <- FIXME.
    F1(i)   { B_rhsU[i]  = (3.0/4.0)*Lambar_rhsU[i] - eta*BU[i]; } //FIXME? INCONSISTENT: Lambar_rhsU[i] contains Lie derivative, unlike Eq. 16b.
#ifdef BIADVECT
    F2(i,j) { B_rhsU[i] += betaU[j]*BUdupD[j][i]; }
#endif

    // STORE RHSs IN GRIDFUNCTIONS
    int which_gf=0;
    const int index = GFIDX(ii, jj, kk);
    for(int j=0;j<DIM;j++)                        { rhs_gfs[which_gf][index]=beta_rhsU[j]        ; which_gf++; }
    for(int j=0;j<DIM;j++) for(int k=j;k<DIM;k++) { rhs_gfs[which_gf][index]=gammabar_rhsDD[j][k]; which_gf++; }
    /**/                                            rhs_gfs[which_gf][index]=phi_rhs             ; which_gf++;
    /**/                                            rhs_gfs[which_gf][index]=alpha_rhs           ; which_gf++;
    for(int j=0;j<DIM;j++) for(int k=j;k<DIM;k++) { rhs_gfs[which_gf][index]=Abar_rhsDD[j][k]    ; which_gf++; }
    /**/                                            rhs_gfs[which_gf][index]=trK_rhs             ; which_gf++;
    for(int j=0;j<DIM;j++)                        { rhs_gfs[which_gf][index]=Lambar_rhsU[j]      ; which_gf++; }
    for(int j=0;j<DIM;j++)                        { rhs_gfs[which_gf][index]=B_rhsU[j]           ; which_gf++; }
  }
  gettimeofday(&end, NULL);

  seconds  = end.tv_sec  - start.tv_sec;
  useconds = end.tv_usec - start.tv_usec;

  mtime = ((seconds) * 1000 + useconds/1000.0) + 0.999;  // We add 0.999 since mtime is a long int; this rounds up the result before setting the value.  Here, rounding down is incorrect.
  printf("Finished in %e seconds. %e gridpoints per second.\n",((double)mtime/1000.0),(Npts[2]-2*NGHOSTS)*(Npts[1]-2*NGHOSTS)*(Npts[0]-2*NGHOSTS) / ((double)mtime/1000.0));

}

#endif // BSSN_H__
