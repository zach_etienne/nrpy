/*
 * BSSN in arbitrary coordinates
 * By Zachariah B. Etienne, 2016.
 * Goal is to generalize reference-metric formulation 
 *   of T. Baumgarte et al. 
 *   See, e.g., http://arxiv.org/abs/1211.6632
 *
 * So far, results from this code have been validated
 * against Kranc in Cartesian coordinates for
 * a highly-spinning black hole in UIUC coordinates
 * (Liu, Etienne, & Shapiro PRD 80 121503(R) (2009); 
 *   http://arxiv.org/abs/1001.4077.)
 * The results from the BSSN right-hand-side evaluation
 *   between Kranc and this code agree to roundoff error,
 *   even with non-trivial shift & lapse initial 
 *   conditions.
 *
 * List of key features of this code
 * 1) Reference metric connection coefficients 
 *    (Christoffels) and conformal derivatives 
 *    can be inserted as desired within a function.
 *    *Preferably* these functions would be filled-in
 *    using Mathematica-generated codes.
 * 2) Automated code generation for reading
 *    gridfunctions from main memory and computing
 *    needed finite difference derivatives. With this
 *    
 *
 * To-Do list
 * 1a) Add a function that multiplies the metric, 
 *     \Lambda^i, and \bar{A}_{ij} by scaling factors, 
 *     a la Eqs. 20--22 of 
 *     http://arxiv.org/pdf/1211.6632.pdf, 
 *     to help smooth terms at coord. singularities.
 *     Can this be automated?
 * 1b) Replace gridfunctions gammabar, Lambdabar, Abar
 *     with h_{ij}, a_{ij}, and \lambda^i (they are
 *     equivalent in Cartesian coordinates).
 * 2)  Implement Mathematica-based automatic code 
 *     generation for connection coefficients & 
 *     covariant derivs wrt reference metric.
 * 3)  Implement PIRK method in the standalone BSSN solver,
 *     explore to what degree singular terms can be picked
 *     out automatically. Is there any harm in treating 
 *     even non-singular variables with the singular L_2
 *     operator (see Eqs 29--30 of 
 *     http://arxiv.org/pdf/1211.6632.pdf).
 * 4)  Implement simple outer boundary conditions for 
 *     standalone BSSN solver.
 * 5)  Plenty more in store. See grant proposal.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include "initialdata/UIUCInitialData.h"
#include "grid_and_gridfunction_setup.h"
#include "loop_setup.h"
#include "metric_inverse_and_determinant.h"
#include "BSSN.h"

/* Basic 2D array allocation/free facilities. */
#define ALLOCATE_2D_GENERIC(type,array,ni,nj) type **array=(type **)malloc(ni * sizeof(type *)); \
  for(int cc = 0; cc < ni; cc++)                 array[cc]=(type * )malloc(nj * sizeof(type));
#define FREE_2D_GENERIC(type,array,ni,nj) for(int cc = 0; cc < ni;cc++) free((void *)array[cc]); \
  /**/                                                                  free((void *)array);

/* Set up single, spinning black hole in UIUC coordinates: 
   Liu, Etienne, & Shapiro PRD 80 121503(R) (2009); 
   http://arxiv.org/abs/1001.4077. */
void initial_data(double **in_gfs) {
  LOOP_GZFILL(ii,jj,kk) {
    int idx = GFIDX(ii,jj,kk);

    /* Declare variables for "physical" (as opposed to conformal) 3-metric & extrins. curvature */
    double gphysDD[3][3];
    double KphysDD[3][3];
    double N[4];

    double x = xyzmin + del[0]*ii;
    double y = xyzmin + del[1]*jj;
    double z = xyzmin + del[2]*kk;

    UIUC_ID(x,y,z, gphysDD,KphysDD,N);

    double detphys = determinant(gphysDD);
    double phi = (1.0/12.0) * log(detphys);
    in_gfs[PHI][idx] = phi;
    double chi = exp(-4.0*phi);

    int which_gf = G11;
    F2s12(i,j) { in_gfs[which_gf][idx] = gphysDD[i][j]*chi; which_gf++; }

    in_gfs[ALPHA][idx] = N[0];
    in_gfs[BETA1][idx] = N[1];
    in_gfs[BETA2][idx] = N[2];
    in_gfs[BETA3][idx] = N[3];

    double trK=0;
    double gphysUU[3][3];
    Invertgammabar(gphysUU, gphysDD,detphys);
    trK = gphysUU[0][0]*KphysDD[0][0] + gphysUU[1][1]*KphysDD[1][1] + gphysUU[2][2]*KphysDD[2][2] + 
      2.0*(gphysUU[0][1]*KphysDD[0][1] + gphysUU[0][2]*KphysDD[0][2] + gphysUU[1][2]*KphysDD[1][2]);

    which_gf = A11;
    F2s12(i,j) { in_gfs[which_gf][idx] = chi*(KphysDD[i][j] - (1.0/3.0)*gphysDD[i][j]*trK); which_gf++; }

    in_gfs[TRK][idx] = trK;

    in_gfs[B1][idx] = 0.0;
    in_gfs[B2][idx] = 0.0;
    in_gfs[B3][idx] = 0.0;
    
    // Initialize Lambar to zero:
    in_gfs[LAMB1][idx] = 0.0;
    in_gfs[LAMB2][idx] = 0.0;
    in_gfs[LAMB3][idx] = 0.0;
   }

  LOOP_NOGZFILL(ii,jj,kk) {
    int idx = GFIDX(ii,jj,kk);

    double gammabarDD[DIM][DIM];
    double gammabarUU[DIM][DIM];

    int which_gf = G11;
    F2s12(i,j) { gammabarDD[i][j] = in_gfs[which_gf][idx]; which_gf++; }
    SS2s12(gammabarDD);
    double detgammabar = determinant(gammabarDD);
    Invertgammabar(gammabarUU, gammabarDD,detgammabar);
    SS2s12(gammabarUU);

    double gammabarDDdD[DIM][DIM][DIM];
    double locGF[2*NGHOSTS+1][2*NGHOSTS+1][2*NGHOSTS+1]; // About 10KB for NGHOSTS=5.
    /* Inputs for autogenFDcode/ code generation. 

       To generate all finite difference code for this file,
       simply go to the autogenFDcode directory and run
       ./gen_stencil [this filename]

       DVGENSTART ID__input_gfdata_and_compute_derivs.h
       gammabarDD  FIRST
       DVGENEND
    */
#include "autogenFDcode/ID__input_gfdata_and_compute_derivs.h"
    SS3s12(gammabarDDdD);

    I3NI(GammabarDDD); I3(GammabarUDD);
    F3(i,j,k)   { GammabarDDD[i][j][k] = (1.0/2.0) * (gammabarDDdD[i][j][k] + gammabarDDdD[i][k][j] - gammabarDDdD[j][k][i]); }
    F4(i,j,k,l) { GammabarUDD[i][j][k] += gammabarUU[i][l] * GammabarDDD[l][j][k]; }
    I3NI(DGammaUDD);
    F3(i,j,k)   { DGammaUDD[i][j][k]  = GammabarUDD[i][j][k] - GammahatUDD(i,j,k); }
    I1(DGammaU);
    F3(i,j,k) { DGammaU[i] += gammabarUU[j][k]*DGammaUDD[i][j][k]; }

    in_gfs[LAMB1][idx] = DGammaU[0];
    in_gfs[LAMB2][idx] = DGammaU[1];
    in_gfs[LAMB3][idx] = DGammaU[2];
  }
}

int main() {

  if(DIM!=3) { printf("Sorry non-3D grids not yet supported!\n"); exit(1); }
  
  ALLOCATE_2D_GENERIC(double,gfs,     NUM_GFS,Npts[0]*Npts[1]*Npts[2]);
  ALLOCATE_2D_GENERIC(double,gfs_rhs, NUM_EVOLGFS,Npts[0]*Npts[1]*Npts[2]);

  initial_data(gfs);

  int max_iter = 1;
  for(int iter=0;iter<max_iter;iter++) {
    
    BSSN_rhs(gfs,  gfs_rhs);

    for(int kk=0;kk<Npts[2];kk++) for(int jj=0;jj<Npts[1];jj++) for(int ii=0;ii<Npts[0];ii++) {
          double x = xyzmin + del[0]*ii;
          double y = xyzmin + del[1]*jj;
          double z = xyzmin + del[2]*kk;

          if(fabs(x-0.01) < 0.01*del[0] && fabs(y-0.01) < 0.01*del[1]) {
            int index = GFIDX(ii,jj,kk);
            printf("%e %e | %.15e %.15e %.15e %.15e %.15e %.15e | rhss\n",z,gfs[A11][index],
                   gfs_rhs[A11][index],
                   gfs_rhs[A12][index],
                   gfs_rhs[A13][index],
                   gfs_rhs[A22][index],
                   gfs_rhs[A23][index],
                   gfs_rhs[A33][index]);
          }
        }

    if(1==1 && iter==max_iter-1) {
      LOOP_GZFILL(ii,jj,kk) {
        double x = xyzmin + del[0]*ii;
        double y = xyzmin + del[1]*jj;
        double z = xyzmin + del[2]*kk;

        if(fabs(x-0.01) < 0.01*del[0] && fabs(y-0.01) < 0.01*del[1]) {
          int index = GFIDX(ii,jj,kk);
          printf("%e | ",z);
          for(int i=0;i<NUM_EVOLGFS;i++) {
            printf("%.15e ",gfs_rhs[i][index]);
          } printf("\n");
        }
      }
    }
  }

  FREE_2D_GENERIC(double,gfs,     NUM_GFS,Npts[0]*Npts[1]*Npts[2]);
  FREE_2D_GENERIC(double,gfs_rhs, NUM_EVOLGFS,Npts[0]*Npts[1]*Npts[2]);

  return 0;
}
