#ifndef GRID_AND_GRIDFUNCTION_SETUP_H__
#define GRID_AND_GRIDFUNCTION_SETUP_H__

//const double xyzmin = -0.59; // 2nd order
const double xyzmin = -0.69; // 4th order
//const double xyzmin = -0.79; // 6th order
//const int   Npts[3] = {13,13,13}; // 2nd order
const int   Npts[3] = {15,15,15}; // 4th order
//const int   Npts[3] = {17,17,17}; // 6th order
const double del[3] = { 0.1, 0.1, 0.1 };

/* Number of spatial dimensions. Warning: We do not currently support DIM != 3. */
#define DIM         3
/* Finite-differencing order. */
#define ORDER       4
/* Upwind all shift-advection terms on the RHSs of the BSSN equations? */
#define UPWIND
/* A second-order shift condition is implemented. Should we advect the shift? 
   I.e., should \partial_t \beta^i -> \partial_0 \beta^i?  */
#define SHIFTADVECT
/* Similarly, should \partial_t B^i -> \partial_0 B^i?  */
#define BIADVECT

/* Number of ghostzones */
#define NGHOSTS ((ORDER)/2+1)
#define NGM1 ((NGHOSTS)-1) // <-- NGHOSTS includes upwinding, so NGHOSTS*2 = finite difference convergence order.


// WARNING: DO NOT CHANGE THIS ORDERING!
#define BETA1  0
#define BETA2  1
#define BETA3  2
#define G11    3
#define G12    4
#define G13    5
#define G22    6
#define G23    7
#define G33    8
#define PHI    9 
#define ALPHA 10
#define A11   11
#define A12   12
#define A13   13
#define A22   14
#define A23   15
#define A33   16
#define TRK   17
#define LAMB1 18
#define LAMB2 19
#define LAMB3 20
#define B1    21
#define B2    22
#define B3    23
#define NUM_EVOLGFS 24 /* Number of gridfunctions to evolve. */
/* ADD GRIDFUNCTIONS NOT EVOLVED BELOW THIS LINE */
/* --------------------------------------------- */
#define DETG  24
#define NUM_GFS     25
#define NUM_GFSx2   50

// WARNING: DO NOT CHANGE THIS ORDERING!
char *gf_name[NUM_GFSx2] = { "betaU", "[0]",
                             "betaU", "[1]",
                             "betaU", "[2]",
                             "gammabarDD", "[0][0]",
                             "gammabarDD", "[0][1]",
                             "gammabarDD", "[0][2]",
                             "gammabarDD", "[1][1]",
                             "gammabarDD", "[1][2]",
                             "gammabarDD", "[2][2]",
                             "phi", "",
                             "alpha", "",
                             "AbarDD", "[0][0]",
                             "AbarDD", "[0][1]",
                             "AbarDD", "[0][2]",
                             "AbarDD", "[1][1]",
                             "AbarDD", "[1][2]",
                             "AbarDD", "[2][2]",
                             "trK", "",
                             "LambarU", "[0]",
                             "LambarU", "[1]",
                             "LambarU", "[2]",
                             "BU", "[0]",
                             "BU", "[1]",
                             "BU", "[2]",
                             "detgammabar", ""};

#endif // GRID_AND_GRIDFUNCTION_SETUP_H__
