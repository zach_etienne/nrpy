#!/bin/bash

# Adding the needed sort functionality and data structures in C 
# is a cumbersome and wasteful task, particularly
# given that UNIX command line offers all functionality we need.

# This function 
function parse_one_gridfunction_derivs {
    # First read in all derivatives & corresponding stencils needed by a gridfunction, 
    #  then generate a unique checksum for each gridpoint. Sort the checksums, and 
    #  output only the unique ones. This gives us the complete list of gridpoints 
    #  needed to be read in for a given gridfunction:
    grep "^$1 " out.txt | awk 'BEGIN{offset=100}{print ($4+offset)*100000 + ($3+offset)*1000 + ($2+offset),"          ",$0}'|sort -k1 -g | uniq -w 10 > list_unique_gps.txt

    # Output the C code needed to read in the gridfunction data from main memory.
    #   In particular, we are setting a small 3D array called locGF.
    cat list_unique_gps.txt | awk '{print "locGF[NGHOSTS+"$3"][NGHOSTS+"$4"][NGHOSTS+"$5"] = in_gfs["$2"][GFIDX(ii+"$3",jj+"$4",kk+"$5")];"}' | sed "s/+-/-/g" >> $2

    # Output the C code needed to compute all derivatives of the gridfunction.
    #   This is facilitated with a simple awk script. Note here and above we
    #   replace all a+-b with a-b, for purely aesthetic reasons.
    grep "^$1 " out.txt | awk '{idx1[NR]=$2;idx2[NR]=$3;idx3[NR]=$4;coeff[NR]=$5;num_pts[NR]=$6;name[NR]=$7}END{
for(i=1;i<=NR;i++) {
printf("%s = ",name[i]);
for(j=0;j<num_pts[i];j++) {
printf("%s * locGF[NGHOSTS+%d][NGHOSTS+%d][NGHOSTS+%d]",coeff[i+j],idx1[i+j],idx2[i+j],idx3[i+j]);
if(num_pts[i]>1 && j!=num_pts[i]-1) printf(" + ");
}
i+=num_pts[i]-1;
printf(";\n");
}
}' | sed "s/+-/-/g;s/+ -/- /g;s/1.0000000000000000e+00 \* //g" >> $2
    
}


# Clean up earlier files.
rm -f *-drvinput.txt
DOWEHAVEANYTHINGTODO=`cat $1 |grep DVGENSTART |wc -l`

if (( $DOWEHAVEANYTHINGTODO < 1 )); then
    echo Could not find any information in the file: $1
    exit
fi

echo "Generating finite difference derivative header file(s) for source file " $1

cat $1  |awk 'BEGIN{inside=0;}{if($1=="DVGENSTART") { inside=1; filename=$2;}   if(inside) print $0 >> filename"-drvinput.txt";  if($1=="DVGENEND") inside=0;}'

# Compile the executable.
make > /dev/null

for i in *-drvinput.txt; do
    OUTPUTFILENAME=`echo $i | sed "s/-drvinput.txt//g"`
    rm -f $OUTPUTFILENAME

    cat $i | grep -v DVGEN | awk '{print "./FDcode_generator_arb_order ",$0}' > tmpscript.sh
    chmod 755 tmpscript.sh
    ./tmpscript.sh | grep gplist > out.txt

    cat out.txt | grep ^[0-9] | awk '{print $1}'|sort -k1 -g|uniq > list_of_gfs.txt

    for gf in `cat list_of_gfs.txt`; do
        parse_one_gridfunction_derivs $gf $OUTPUTFILENAME
    done
done

# Clean up. If debugging probably want to comment out the below line.
rm -f list_unique_gps.txt out.txt *-drvinput.txt tmpscript.sh list_of_gfs.txt
