#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "../grid_and_gridfunction_setup.h"
#include "../loop_setup.h"

#define NUM_DERIV_TYPES 4
#define FIRST      1
#define SECOND     2
#define UPDOWNWIND 3
#define ZERO       4
#define NONE      -1

typedef struct stencilstruct__ {
  int    idx  [MAX_STENCIL_SIZE][DIM];
  double coeff[MAX_STENCIL_SIZE];
  char name[100];
  int size_npts;
} stencilstruct;

void set_stencil(stencilstruct *stencil,int num_st, int idx_array[][DIM],double *coeff_array) {
  for(int idx=0;idx<stencil[num_st].size_npts;idx++) { memcpy(stencil[num_st].idx[idx],idx_array[idx],sizeof(int)*3); stencil[num_st].coeff[idx] = coeff_array[idx]; }
}
void zero_idx_array(int idx_array[][DIM], int num_pts) { for(int i=0;i<num_pts;i++) for(int j=0;j<DIM;j++) idx_array[i][j]=0; }

/*
 * This code is designed to set up efficient finite difference 
 *    derivative stencils for each BSSN gridfunction.
 * The inputs are: 
 *  1) finite difference order (integer)
 *  2) whether to enable upwinding (boolean)
 * The output is a .h file containing:
 *  1) An array for each gridfunction that ensure no gridpoint 
 *        is read from main memory more than once (reading from
 *        main memory is a very expensive operation, often
 *        exceeding the computational cost of evaluating the
 *        BSSN RHSs)
 *  2) "const"-type finite difference coefficient & stencil arrays
 */

int main(int argc, char *argv[]) {

  if(DIM!=3) { printf("Sorry, non-3D grids currently unsupported!\n"); exit(1); }
  if(ORDER!=2) { printf("Error: ORDER!=2 set. Other (possibly arbitrary) orders will be added soon.\n"); exit(1); }

  const int NUM_STENCILS = 8*2 + 2; // <-- Some gridfunctions need 2nd derivs only, and some need 1st derivs only. There are a total of 8 upwinding options.
  stencilstruct *stencil = (stencilstruct *)malloc(sizeof(stencilstruct)*NUM_STENCILS);

  int deriv_types[NUM_GFS][NUM_DERIV_TYPES];
  int nderiv_types[NUM_GFS];

  /* Note that below, we refer to a "stencil" as the set of all unique gridpoints
   *  that a given function needs to evaluate a single gridpoint within the BSSN_RHS() function.
   *  E.g., if we had a 1D, 2nd-order code and the RHS contained a single centered, second derivative,
   *  the point i would depend on the neighboring points i-1, i, i+1 only.
   */

  // The derivatives needed for each gridfunction are easily seen from scanning the BSSN equations / BSSN_RHS function in BSSN.h.
  // Warning: If modifying the below, be sure that UPDOWNWIND remains the final element N of deriv_types[which_gf][N].
  int which_gf=0;
  /* SHIFT */ F1(i)      { deriv_types[which_gf][0]=ZERO; deriv_types[which_gf][1]=FIRST; deriv_types[which_gf][2]=SECOND; deriv_types[which_gf][3]=UPDOWNWIND; nderiv_types[which_gf]=4; which_gf++; }
  /* GIJ   */ F2s12(i,j) { deriv_types[which_gf][0]=ZERO; deriv_types[which_gf][1]=FIRST; deriv_types[which_gf][2]=SECOND; deriv_types[which_gf][3]=UPDOWNWIND; nderiv_types[which_gf]=4; which_gf++; }
  /* PHI   */            { deriv_types[which_gf][0]=ZERO; deriv_types[which_gf][1]=FIRST; deriv_types[which_gf][2]=SECOND; deriv_types[which_gf][3]=UPDOWNWIND; nderiv_types[which_gf]=4; which_gf++; }
  /* ALPHA */            { deriv_types[which_gf][0]=ZERO; deriv_types[which_gf][1]=FIRST; deriv_types[which_gf][2]=SECOND; deriv_types[which_gf][3]=UPDOWNWIND; nderiv_types[which_gf]=4; which_gf++; }
  /* AIJ   */ F2s12(i,j) { deriv_types[which_gf][0]=ZERO; deriv_types[which_gf][1]=FIRST; deriv_types[which_gf][2]=UPDOWNWIND; nderiv_types[which_gf]=3; which_gf++; }
  /* TRK   */            { deriv_types[which_gf][0]=ZERO; deriv_types[which_gf][1]=UPDOWNWIND;                                 nderiv_types[which_gf]=2; which_gf++; }
  /* LAMBI */ F1(i)      { deriv_types[which_gf][0]=ZERO; deriv_types[which_gf][1]=FIRST; deriv_types[which_gf][2]=UPDOWNWIND; nderiv_types[which_gf]=3; which_gf++; }
  /* BI    */ F1(i)      { deriv_types[which_gf][0]=ZERO; deriv_types[which_gf][1]=UPDOWNWIND;                                 nderiv_types[which_gf]=2; which_gf++; }
  /* DETG  */            { deriv_types[which_gf][0]=ZERO; deriv_types[which_gf][1]=FIRST; deriv_types[which_gf][2]=SECOND;     nderiv_types[which_gf]=3; which_gf++; }

  int num_gfs = which_gf;
  if(NUM_GFS != num_gfs) { printf("Assumed %d gridfunctions, but tried to set %d gridfunctions\n",NUM_GFS,num_gfs); exit(1); }

  // If SHIFT upwinding is disabled, disable the UPDOWNWIND at the end of each deriv_types[BETAI] array:
#ifndef SHIFTADVECT
  for(int i=BETA1;i<BETA1+DIM;i++) { deriv_types[i][nderiv_types[i]-1]=NONE; nderiv_types[i]--; }
#endif

  // If BI upwinding is disabled, disable the UPDOWNWIND at the end of each deriv_types[BI] array:
#ifndef BIADVECT
  for(int i=B1;i<B1+DIM;i++)         { deriv_types[i][nderiv_types[i]-1]=NONE; nderiv_types[i]--; }
#endif

  // If upwinding is disabled, disable the UPDOWNWIND at the end of each deriv_types[i] array, except DETG:
#ifndef UPWIND
  for(int i=G11;i<B1;i++)            { deriv_types[i][nderiv_types[i]-1]=NONE; nderiv_types[i]--; }
#endif

  // Next generate needed finite difference stencils for all gridfunctions.
  for(int gf=0;gf<num_gfs;gf++) {
    int which_stenc = 0;
    for(int deriv=0;deriv<nderiv_types[gf];deriv++) {
      if(deriv_types[gf][deriv] == ZERO) {
        sprintf(stencil[which_stenc].name,"%s%s",gf_name[gf*2],gf_name[gf*2+1]);
        double coeff_array[1] = { 1.0 };
        // Centered first derivative stencils, in all directions:
        stencil[which_stenc].size_npts = 1;
        int idx_array[1][DIM] = { { 0, 0, 0 } };
        set_stencil(stencil,which_stenc,  idx_array,coeff_array); which_stenc++;

      } else if(deriv_types[gf][deriv] == FIRST) {
        int idx_array[2][DIM]; 
        // Centered first derivative stencils, in all directions:
        for(int dirn=0;dirn<DIM;dirn++) {
          double coeff_array[2] = { (-1.0/2.0)/del[dirn], (+1.0/2.0)/del[dirn] };
          sprintf(stencil[which_stenc].name,"%sdD%s[%d]",gf_name[gf*2],gf_name[gf*2+1],dirn);
          stencil[which_stenc].size_npts = 2; zero_idx_array(idx_array, stencil[which_stenc].size_npts); 
          idx_array[0][dirn] = -1; idx_array[1][dirn] = +1;
          set_stencil(stencil,which_stenc,  idx_array,coeff_array); which_stenc++;
        }

      } else if(deriv_types[gf][deriv] == SECOND) {
        int idx_array[4][DIM];
        // Second derivatives wrt a single coordinate:
        for(int dirn=0;dirn<DIM;dirn++) {
          double coeff_array[3] = { 1.0/(del[dirn]*del[dirn]), -2.0/(del[dirn]*del[dirn]), 1.0/(del[dirn]*del[dirn]) };
          sprintf(stencil[which_stenc].name,"%sdDD%s[%d][%d]",gf_name[gf*2],gf_name[gf*2+1],dirn,dirn);
          stencil[which_stenc].size_npts = 3; zero_idx_array(idx_array, stencil[which_stenc].size_npts); 
          idx_array[0][dirn] = -1; idx_array[1][dirn] = 0; idx_array[2][dirn] = +1;
          set_stencil(stencil,which_stenc,  idx_array,coeff_array); which_stenc++;
        }

        // Mixed second derivatives:
        double coeff_array[4];
        // second derivative wrt x1, then x2 (or vice-versa):
        coeff_array[0]=(+1.0/4.0); coeff_array[1]=(-1.0/4.0);coeff_array[2]=(-1.0/4.0);coeff_array[3]=(+1.0/4.0);
        for(int ii=0;ii<4;ii++) coeff_array[ii] /= (del[0]*del[1]);
        sprintf(stencil[which_stenc].name,"%sdDD%s[%d][%d]",gf_name[gf*2],gf_name[gf*2+1],0,1);
        stencil[which_stenc].size_npts = 4; zero_idx_array(idx_array, stencil[which_stenc].size_npts);
        idx_array[0][0]=-1;idx_array[0][1]=-1;  idx_array[1][0]=-1;idx_array[1][1]=+1;  idx_array[2][0]=+1;idx_array[2][1]=-1;  idx_array[3][0]=+1;idx_array[3][1]=+1;
        set_stencil(stencil,which_stenc,  idx_array,coeff_array); which_stenc++;

        // second derivative wrt x1, then x3 (or vice-versa):
        coeff_array[0]=(+1.0/4.0); coeff_array[1]=(-1.0/4.0);coeff_array[2]=(-1.0/4.0);coeff_array[3]=(+1.0/4.0);
        for(int ii=0;ii<4;ii++) coeff_array[ii] /= (del[0]*del[2]);
        sprintf(stencil[which_stenc].name,"%sdDD%s[%d][%d]",gf_name[gf*2],gf_name[gf*2+1],0,2);
        stencil[which_stenc].size_npts = 4; zero_idx_array(idx_array, stencil[which_stenc].size_npts);
        idx_array[0][0]=-1;idx_array[0][2]=-1;  idx_array[1][0]=-1;idx_array[1][2]=+1;  idx_array[2][0]=+1;idx_array[2][2]=-1;  idx_array[3][0]=+1;idx_array[3][2]=+1;
        set_stencil(stencil,which_stenc,  idx_array,coeff_array); which_stenc++;

        // second derivative wrt x2, then x3 (or vice-versa):
        coeff_array[0]=(+1.0/4.0); coeff_array[1]=(-1.0/4.0);coeff_array[2]=(-1.0/4.0);coeff_array[3]=(+1.0/4.0);
        for(int ii=0;ii<4;ii++) coeff_array[ii] /= (del[1]*del[2]);
        sprintf(stencil[which_stenc].name,"%sdDD%s[%d][%d]",gf_name[gf*2],gf_name[gf*2+1],1,2);
        stencil[which_stenc].size_npts = 4; zero_idx_array(idx_array, stencil[which_stenc].size_npts);
        idx_array[0][1]=-1;idx_array[0][2]=-1;  idx_array[1][1]=-1;idx_array[1][2]=+1;  idx_array[2][1]=+1;idx_array[2][2]=-1;  idx_array[3][1]=+1;idx_array[3][2]=+1;
        set_stencil(stencil,which_stenc,  idx_array,coeff_array); which_stenc++;
      } else if(deriv_types[gf][deriv] == UPDOWNWIND) {
        int idx_array[3][DIM];
        // First define all upwinded stencils:
        for(int dirn=0;dirn<DIM;dirn++) {
          double coeff_arrayUP[3] = { (-3.0/2.0)/del[dirn],+2.0/del[dirn], (-1.0/2.0)/del[dirn] };
          sprintf(stencil[which_stenc].name,"%sdupD%s[%d]",gf_name[gf*2],gf_name[gf*2+1],dirn);
          stencil[which_stenc].size_npts = 3; zero_idx_array(idx_array, stencil[which_stenc].size_npts); 
          idx_array[0][dirn] = 0; idx_array[1][dirn] = +1; idx_array[2][dirn] = +2;
          set_stencil(stencil,which_stenc,  idx_array,coeff_arrayUP /* <- upwinded */); which_stenc++;
        }

        // First define all downwinded stencils:
        for(int dirn=0;dirn<DIM;dirn++) {
          double coeff_arrayDN[3] = { (+3.0/2.0)/del[dirn],-2.0/del[dirn], (+1.0/2.0)/del[dirn] };
          sprintf(stencil[which_stenc].name,"%sddnD%s[%d]",gf_name[gf*2],gf_name[gf*2+1],dirn);
          stencil[which_stenc].size_npts = 3; zero_idx_array(idx_array, stencil[which_stenc].size_npts); 
          idx_array[0][dirn] = 0; idx_array[1][dirn] = -1; idx_array[2][dirn] = -2;
          set_stencil(stencil,which_stenc,  idx_array,coeff_arrayDN /* <- downwinded */); which_stenc++;
        }
      }
    }
    int num_stencils = which_stenc;
    /*
     * We now have all stencils needed for our gridfunction.
     * In the code, we must be very stingy about reading data from main memory, 
     * as this is usually our #1 performance killer; 
     * one read from main memory (cache miss) can cost ~200 clock cycles!
     * (http://stackoverflow.com/questions/1126529/what-is-the-cost-of-an-l1-cache-miss#1130117)
     * 
     * So to minimize cache misses we only read data from main memory when
     *    absolutely needed, and never read twice.
     * When upwinding is enabled, we only read the data needed. There are 3 dimensions
     *    and 2 upwinding possibilities for each dimension, so for each
     *    upwinded gridfunction there are a total of 2^3 = 8 possible combinations
     *    of index sets that must be read from main memory.
     */

    int tot_num_gridpoints=0;
    for(int which_stenc=0;which_stenc<num_stencils;which_stenc++) {
      for(int which_gp_in_stencil=0;which_gp_in_stencil<stencil[which_stenc].size_npts;which_gp_in_stencil++) {
        printf("%d %d | %d %d %d %.16e %d %s gridpoint list\n",gf,which_stenc,
               stencil[which_stenc].idx[which_gp_in_stencil][0],
               stencil[which_stenc].idx[which_gp_in_stencil][1],
               stencil[which_stenc].idx[which_gp_in_stencil][2],
               stencil[which_stenc].coeff[which_gp_in_stencil],
               stencil[which_stenc].size_npts,
               stencil[which_stenc].name);
      }
    }

  }
  
  free(stencil);
  return 0;
}
