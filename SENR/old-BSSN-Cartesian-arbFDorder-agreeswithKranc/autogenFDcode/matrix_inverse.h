/* Copyright 2015 Chandra Shekhar (chandraiitk AT yahoo DOT co DOT in).
   Homepage: https://sites.google.com/site/chandraacads

   For original code, look here:
   http://chandraacads.blogspot.jp/2015/12/c-program-for-matrix-inversion.html

   Modifications for use in SENR by Zachariah B. Etienne
   Homepage: http://math.wvu.edu/~zetienne/SENR/
   * * */


/* This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
   * * */

/* This program computes inverse of a square matrix, based on LUP decomposition. */

#include <stdio.h>
#include <math.h>
#include "../grid_and_gridfunction_setup.h"

/* This function performs LUP decomposition of the to-be-inverted matrix 'A'. It is
 * defined after the function 'main()'.
 * * */
static int LUPdecompose(int size, double A[size][size], int P[size]);

/* This function decomposes the matrix 'A' into L, U, and P. If successful,
 * the L and the U are stored in 'A', and information about the pivot in 'P'.
 * The diagonal elements of 'L' are all 1, and therefore they are not stored. */
static int LUPdecompose(int size, double A[size][size], int P[size])
{
  int kd = 0, T;
  double p, t;

  /* Finding the pivot of the LUP decomposition. */
  for(int i=1; i<size; i++) P[i] = i; //Initializing.

  for(int k=1; k<size-1; k++)
    {
      p = 0.0;
      for(int i=k; i<size; i++)
        {
          t = A[i][k];
          if(t < 0.0) t *= -1; //Abosolute value of 't'.
          if(t > p)
            {
              p = t;
              kd = i;
            }
        }

      if(p == 0.0)
        {
          printf("\nLUPdecompose(): ERROR: A singular matrix is supplied.\n"\
                 "\tRefusing to proceed any further.\n");
          return -1;
        }

      /* Exchanging the rows according to the pivot determined above. */
      T = P[kd];
      P[kd] = P[k];
      P[k] = T;
      for(int i=1; i<size; i++)
        {
          t = A[kd][i];
          A[kd][i] = A[k][i];
          A[k][i] = t;
        }

      for(int i=k+1; i<size; i++) //Performing substraction to decompose A as LU.
        {
          A[i][k] = A[i][k]/A[k][k];
          for(int j=k+1; j<size; j++) A[i][j] -= A[i][k]*A[k][j];
        }
    } //Now, 'A' contains the L (without the diagonal elements, which are all 1)
      //and the U.

  return 0;
}



/* This function calculates the inverse of the LUP decomposed matrix 'LU' and pivoting
 * information stored in 'P'. The inverse is returned through the matrix 'LU' itself.
 * 'B', X', and 'Y' are used as temporary spaces. */
static int LUPinverse(int size, int P[size], double LU[size][size],\
                      double B[size][size], double X[size], double Y[size])
{
  double t;

  //Initializing X and Y.
  for(int n=1; n<size; n++) X[n] = Y[n] = 0;

  /* Solving LUX = Pe, in order to calculate the inverse of 'A'. Here, 'e' is a column
   * vector of the identity matrix of size 'size-1'. Solving for all 'e'. */
  for(int i=1; i<size; i++)
    {
      //Storing elements of the i-th column of the identity matrix in i-th row of 'B'.
      for(int j = 1; j<size; j++) B[i][j] = 0;
      B[i][i] = 1;

      //Solving Ly = Pb.
      for(int n=1; n<size; n++)
        {
          t = 0;
          for(int m=1; m<=n-1; m++) t += LU[n][m]*Y[m];
          Y[n] = B[i][P[n]]-t;
        }

      //Solving Ux = y.
      for(int n=size-1; n>=1; n--)
        {
          t = 0;
          for(int m = n+1; m < size; m++) t += LU[n][m]*X[m];
          X[n] = (Y[n]-t)/LU[n][n];
        }//Now, X contains the solution.

      for(int j = 1; j<size; j++) B[i][j] = X[j]; //Copying 'X' into the same row of 'B'.
    } //Now, 'B' is the transpose of the inverse of 'A'.

  /* Copying 'B', the transposed inverse of 'A', into 'LU'. */
  for(int i=1; i<size; i++) for(int j=1; j<size; j++) LU[i][j] = B[j][i];

  return 0;
}
