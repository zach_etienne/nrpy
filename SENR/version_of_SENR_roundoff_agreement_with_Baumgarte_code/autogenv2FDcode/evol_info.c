#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
//#include "../SENR.h"

// Defines coordinate type.
#include "../grid_setup.h"

// Need to know names of gridfunctions and whether or not to upwind!
#if defined EVOLVE_SCALAR_WAVE
#include "../Scalar_Wave__grid_and_gridfunction_setup.h"
#elif defined EVOLVE_BSSN
#include "../BSSN__grid_and_gridfunction_setup.h"
#endif
// Sets GFIDX(), needed for coord-specific .c files below.
#include "../loop_setup.h"

// Need coordinate-specific grid step sizes del[DIM]
#if defined COORD_CARTESIAN
#include "../boundary_conditions/coord_cartesian.c"
#elif defined COORD_CYLINDRICAL
#include "../boundary_conditions/coord_cylindrical.c"
#elif defined COORD_SPHERICAL
#include "../boundary_conditions/coord_spherical.c"
#elif defined COORD_SYMTP
#include "../boundary_conditions/coord_symtp.c"
#endif

/*
 * This code outputs various evolution parameters, so that
 *  finite difference stencil header files can be automatically
 *  generated. For example, the instructions

 DVGENSTART BSSN_L1rhs__input_gfdata_and_compute_derivs.h
 vetU             ZERO    FIRST   UPDOWNWIND
 detgammabar      ZERO
 DVGENEND

 * means that we want zeroth, first, and up/downwind derivs for
 *  all components of the vetU vector, and the zeroth deriv. of 
 *  detgammabar.

 * newFD_generator.c takes care of the low-level finite diff.
 *   coefficient calculation.
 * This code outputs other information needed for the actual 
 *   header file BSSN_L1rhs__input_gfdata_and_compute_derivs.h
 * For example:
 *   Outputting names of all components of vetU
 *   Determining whether UPWIND is defined
 *   Outputting grid spacing in all directions
 *   
 */

int main(int argc, char *argv[]) {

  if(argc < 2) {
    printf("At least 1 argument is required. Proper syntax is\n");
    printf("./evol_info [line of input, e.g., \"vetU ZERO FIRST UPDOWNWIND\"] \n\n");
    printf("Output: [UPWIND enabled? 0=no, 1=same-order, 2=lower-order] [FD ORDER] [dx1] [dx2] [dx3] [number of gridfunctions with this prefix] [all gridfunction names with this prefix] BREAKBREAK [derivs wanted. In this case, ZERO FIRST UPDOWNWIND\n");
    exit(1);
  }

  char gf_prefix[100];
  snprintf(gf_prefix,99,"%s",argv[1]);

  /* Now determine how many unique gridfunctions correspond to this prefix.
   * E.g., gammabarDD would have 6.
   */
  int count=0; int first_idx=0;
  for(int i=0;i<NUM_GFS;i++) {
    if(strcmp(gf_name[i*2],gf_prefix) == 0) {
      if(count==0) first_idx = i;
      count++;
    }
  }
  if(count==0) { printf("Could not find gridfunction named %s. Are you sure you #define'd the correct EVOLVE_* variable?x Exiting.\n",gf_prefix); exit(1); }

  int upwindenabled=0;
#ifdef UPWIND
  upwindenabled=1;
#endif
#ifdef UPWINDLOWERORDER
  upwindenabled=2;
#endif

  int fdorder = ORDER;

  printf("%d %d %.16e %.16e %.16e %d ",upwindenabled,fdorder,del[0],del[1],del[2],count);
  for(int i=first_idx;i<first_idx+count;i++) {
    printf("%s%s ",gf_name[i*2],gf_name[i*2+1]);
  }
  printf("BREAKBREAK ");
  for(int i=2;i<argc;i++) {
    printf("%s ",argv[i]);
  }
  printf("\n");

  return 0;
}
