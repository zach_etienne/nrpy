#ifndef GRID_SETUP_H__
#define GRID_SETUP_H__

/* Finite-differencing order. */
#define ORDER 4

/* Number of spatial dimensions. Warning: We do not currently support DIM != 3. */
#define DIM 3

/* Coordinate system */
//#define COORD_CARTESIAN
//#define COORD_CYLINDRICAL
#define COORD_SPHERICAL
//#define COORD_SYMTP

/* Outer boundary conditions */
//#define OUTER_BOUNDARY_EXACT
//#define OUTER_BOUNDARY_LIN_EXTRAP
#define OUTER_BOUNDARY_SOMMERFELD

/* Number of ghostzones */
#if defined UPWIND
#define NGHOSTS ((ORDER) / 2 + 1) // Upwinding
#else
#define NGHOSTS ((ORDER) / 2) // No upwinding
#endif

/* CFL factor for PIRK timestep */
#define CFL 0.4

/* Number of grid points */
#define Nx1 64
#define Nx2 32
#define Nx3 32

#define FRAME_JUMP 1 /* Output one frame to data files when iteration# is a multiple of FRAME_JUMP */
#define N_FRAMES   10 /* Output this many frames (plus the initial condition) */

/* Total number of grid points, including ghost zones */
const int Npts[3] = {Nx1 + 2 * NGHOSTS, Nx2 + 2 * NGHOSTS, Nx3 + 2 * NGHOSTS};

/* PIRK time step */
double dt;

#endif // GRID_SETUP_H__
