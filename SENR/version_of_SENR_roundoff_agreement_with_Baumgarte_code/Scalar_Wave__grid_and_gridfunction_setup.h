#ifndef SCALAR_WAVE__GRID_AND_GRIDFUNCTION_SETUP_H__
#define SCALAR_WAVE__GRID_AND_GRIDFUNCTION_SETUP_H__

/* WARNING: DO NOT CHANGE THIS ORDERING! */
#define U       0
#define V       1
#define NUM_EVOLGFS 2 /* Number of gridfunctions to evolve. */
/* ADD GRIDFUNCTIONS NOT EVOLVED BELOW THIS LINE */
/* Auxiliary grid functions */
#define GINV00  0
#define GINV01  1
#define GINV02  2
#define GINV03  3
#define GINV11  4
#define GINV12  5
#define GINV13  6
#define GINV22  7
#define GINV23  8
#define GINV33  9
#define GAM0   10
#define GAM1   11
#define GAM2   12
#define GAM3   13
#define ANA    14
#define RELERR 15
/* --------------------------------------------- */
#define NUM_GFS      2
#define NUM_GFSx2    4
#define NUM_AUX_GFS 16

/* WARNING: DO NOT CHANGE THIS ORDERING! */
char *gf_name[NUM_GFSx2] = {"u", "",
			    "v", ""};

#endif /* SCALAR_WAVE__GRID_AND_GRIDFUNCTION_SETUP_H__ */
