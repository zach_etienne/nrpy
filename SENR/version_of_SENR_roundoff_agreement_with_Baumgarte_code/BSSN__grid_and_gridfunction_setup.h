#ifndef BSSN__GRID_AND_GRIDFUNCTION_SETUP_H__
#define BSSN__GRID_AND_GRIDFUNCTION_SETUP_H__

/* A second-order shift condition is implemented. Should we advect the shift? 
   I.e., should \partial_t \beta^i -> \partial_0 \beta^i?  */
#define SHIFTADVECT
/* Similarly, should \partial_t B^i -> \partial_0 B^i?  */
#define BIADVECT
/* Advect the lapse, \partial_t \alpha -> \partial_0 \alpha */
#define LAPSEADVECT

#define UPWIND
#define UPWINDINPLACE

/* Finite difference lambdaU initial data */
//#define ID_FD_LAMB

/* UIUC puncture mass and dimensionless spin */
const double M = 1.0;
const double chi = 0.99;

/* SHIFT ETA PARAMETER */
const double eta = 2.0;

/* WARNING: DO NOT CHANGE THIS ORDERING! */
#define VET1   0
#define VET2   1
#define VET3   2
#define H11    3
#define H12    4
#define H13    5
#define H22    6
#define H23    7
#define H33    8
#define PHI    9
#define ALPHA 10
#define A11   11
#define A12   12
#define A13   13
#define A22   14
#define A23   15
#define A33   16
#define TRK   17
#define LAMB1 18
#define LAMB2 19
#define LAMB3 20
#define BET1  21
#define BET2  22
#define BET3  23
#define NUM_EVOLGFS 24 /* Number of gridfunctions to evolve. */
/* ADD GRIDFUNCTIONS NOT EVOLVED BELOW THIS LINE */
/* --------------------------------------------- */
#define DETG  24
/* Auxiliary grid functions */
#define HAM      0
#define INITDETG 1

/* Number of grid functions */
#define NUM_GFS     25
#define NUM_GFSx2   50
#define NUM_AUX_GFS  2

/* WARNING: DO NOT CHANGE THIS ORDERING! */
char *gf_name[NUM_GFSx2] = { "vetU", "[0]",
                             "vetU", "[1]",
                             "vetU", "[2]",
                             "hDD", "[0][0]",
                             "hDD", "[0][1]",
                             "hDD", "[0][2]",
                             "hDD", "[1][1]",
                             "hDD", "[1][2]",
                             "hDD", "[2][2]",
                             "phi", "",
                             "alpha", "",
                             "aDD", "[0][0]",
                             "aDD", "[0][1]",
                             "aDD", "[0][2]",
                             "aDD", "[1][1]",
                             "aDD", "[1][2]",
                             "aDD", "[2][2]",
                             "trK", "",
                             "lambdaU", "[0]",
                             "lambdaU", "[1]",
                             "lambdaU", "[2]",
                             "betU", "[0]",
                             "betU", "[1]",
                             "betU", "[2]",
                             "detg", ""};

#endif /* BSSN__GRID_AND_GRIDFUNCTION_SETUP_H__ */
