#ifndef BSSNL2PART3_C__
#define BSSNL2PART3_C__

#include "../loop_setup.h"
#include "../BSSN__grid_and_gridfunction_setup.h"

void BSSN_L2part3_rhs(double **in_gfs, double **xx, double **yy, double **rhs_gfs) {
  //Start the timer, for benchmarking
  //struct timeval start, end;
  //long mtime, seconds, useconds;
  //gettimeofday(&start, NULL);

  LOOP_NOGZFILL(ii,jj,kk) {
    const int idx = GFIDX(ii, jj, kk);

    const double x1 = xx[0][ii];
    const double x2 = xx[1][jj];
    const double x3 = xx[2][kk];

    /***************************************/
    /**** DECLARE LOCAL VARIABLE ARRAYS ****/
    /***************************************/
    // Declare array that stores the gridfunction data on the stencils
    double locGF[2*NGHOSTS+1][2*NGHOSTS+1][2*NGHOSTS+1]; // About 10KB for NGHOSTS=5.

    // Declare local variable arrays
    double vetU[DIM], vetUdD[DIM][DIM], vetUdDD[DIM][DIM][DIM];
    double hDD[DIM][DIM], hDDdD[DIM][DIM][DIM];
    double aDD[DIM][DIM];
    double phi, phidD[DIM];
    double alpha, alphadD[DIM];
    double trK, trKdD[DIM];
    double lambdaU[DIM], lambdaUdupD[DIM][DIM], lambdaUddnD[DIM][DIM], lambdaUdD[DIM][DIM];
    double detg, detgdD[DIM], detgdDD[DIM][DIM];

    /* Inputs for autogenFDcode/ code generation. 
       Note that UPDOWNWIND is contingent on UPWIND being
       #define'd, and betU/betaU's UPDOWNWIND are contingent
       on SHIFTADVECT & BIADVECT being #define'd, respectively.

       To generate all finite difference code for this file,
       simply go to the autogenFDcode directory and run
       ./gen_stencil [this filename]

       DVGENSTART BSSN_L2part3rhs__input_gfdata_and_compute_derivs.h
       vetU	        ZERO	FIRST   SECOND
       hDD      	ZERO	FIRST
       aDD		ZERO
       phi		ZERO    FIRST
       alpha     	ZERO    FIRST
       trK		ZERO    FIRST
       lambdaU		ZERO    FIRST   UPDOWNWIND
       detg     	ZERO	FIRST   SECOND
       DVGENEND
    */

#include "../autogenFDcode/BSSN_L2part3rhs__input_gfdata_and_compute_derivs.h"

    //double gammabarDD[DIM][DIM];

    //BSSN_gammabarDD(ii, jj, kk, in_gfs, yy, gammabarDD);

    double gammabarUU[DIM][DIM];
    //double oneoverdetgammabar = 1.0 / detgammabar;
    //Invertgammabar(gammabarUU, gammabarDD, oneoverdetgammabar);

    Compute_Inverse_Metric(ii, jj, kk, yy, in_gfs, gammabarUU);

#ifdef UPWIND
    double ReU[DIM] = RESCALING_VECTOR;

    double dnwind[DIM];
    double upwind[DIM];
    F1(j) { dnwind[j] = 1.0; upwind[j] = 0.0; }
    F1(j) { if(vetU[j]*ReU[j]>0.0) { dnwind[j] = 0.0; upwind[j] = 1.0; } }

    F2(i,j)   {  lambdaUdupD[i][j] = upwind[j]*lambdaUdupD[i][j]    + dnwind[j]*lambdaUddnD[i][j]; }
#else // UPWIND
    F2(i,j)   {  lambdaUdupD[i][j] = lambdaUdD[i][j];   }
#endif // UPWIND

    double BSSNL2Operatorpart3[NUM_GFS];

    {
#include "../autogenMathcode/BSSNL2Operatorpart3.txt-parsed.h"
    }

    int which_gf=0;
    for(int j=BET1;j<=BET3;j++) { rhs_gfs[j][idx]    =BSSNL2Operatorpart3[which_gf]; which_gf++; }

    /*if(ii == 17 && jj == 17 && kk == 17)
      {
	printf("L2 %.15e %.15e %.15e\n", BSSNL2Operatorpart3[3], BSSNL2Operatorpart3[4], BSSNL2Operatorpart3[5]);
	printf("L3 %.15e %.15e %.15e\n", BSSNL2Operatorpart3[6], BSSNL2Operatorpart3[7], BSSNL2Operatorpart3[8]);
	printf("LbetaLambarU %.15e %.15e %.15e\n", BSSNL2Operatorpart3[9], BSSNL2Operatorpart3[10], BSSNL2Operatorpart3[11]);
	printf("DGammaU %.15e %.15e %.15e\n", BSSNL2Operatorpart3[12], BSSNL2Operatorpart3[13], BSSNL2Operatorpart3[14]);
	printf("div beta = %.15e\n", BSSNL2Operatorpart3[15]);
	}*/

  }
  //gettimeofday(&end, NULL);

  //seconds  = end.tv_sec  - start.tv_sec;
  //useconds = end.tv_usec - start.tv_usec;

  //mtime = ((seconds) * 1000 + useconds/1000.0) + 0.999;  // We add 0.999 since mtime is a long int; this rounds up the result before setting the value.  Here, rounding down is incorrect.
  //printf("Finished in %e seconds. %e gridpoints per second.\n",((double)mtime/1000.0),(Npts[2]-2*NGHOSTS)*(Npts[1]-2*NGHOSTS)*(Npts[0]-2*NGHOSTS) / ((double)mtime/1000.0));

}
#endif // BSSNL2PART3_C__
