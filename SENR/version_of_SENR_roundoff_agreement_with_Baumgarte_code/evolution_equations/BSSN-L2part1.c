#ifndef BSSNL2part1_C__
#define BSSNL2part1_C__

#include "../loop_setup.h"
#include "../BSSN__grid_and_gridfunction_setup.h"

void BSSN_L2part1_rhs(double **in_gfs, double **xx, double **yy, double **rhs_gfs) {
  //Start the timer, for benchmarking
  //struct timeval start, end;
  //long mtime, seconds, useconds;
  //gettimeofday(&start, NULL);

  LOOP_NOGZFILL(ii,jj,kk) {
    const int idx = GFIDX(ii, jj, kk);

    const double x1 = xx[0][ii];
    const double x2 = xx[1][jj];
    const double x3 = xx[2][kk];

    /***************************************/
    /**** DECLARE LOCAL VARIABLE ARRAYS ****/
    /***************************************/
    // Declare array that stores the gridfunction data on the stencils
    double locGF[2*NGHOSTS+1][2*NGHOSTS+1][2*NGHOSTS+1]; // About 10KB for NGHOSTS=5.

    // Declare local variable arrays
    double hDD[DIM][DIM], hDDdD[DIM][DIM][DIM], hDDdDD[DIM][DIM][DIM][DIM];
    double phi, phidD[DIM], phidDD[DIM][DIM];
    double alpha, alphadD[DIM], alphadDD[DIM][DIM];
    double lambdaU[DIM], lambdaUdD[DIM][DIM];
    double detg;

    /* Inputs for autogenFDcode/ code generation. 
       Note that UPDOWNWIND is contingent on UPWIND being
       #define'd, and betU/betaU's UPDOWNWIND are contingent
       on SHIFTADVECT & BIADVECT being #define'd, respectively.

       To generate all finite difference code for this file,
       simply go to the autogenFDcode directory and run
       ./gen_stencil [this filename]

       DVGENSTART BSSN_L2part1rhs__input_gfdata_and_compute_derivs.h
       hDD      	ZERO	FIRST	SECOND
       phi		ZERO	FIRST	SECOND
       alpha     	ZERO	FIRST	SECOND
       lambdaU   	ZERO	FIRST
       detg   	        ZERO
       DVGENEND
    */

#include "../autogenFDcode/BSSN_L2part1rhs__input_gfdata_and_compute_derivs.h"

    //double gammabarDD[DIM][DIM];

    //BSSN_gammabarDD(ii, jj, kk, in_gfs, yy, gammabarDD);

    double gammabarUU[DIM][DIM];
    //double oneoverdetgammabar = 1.0 / detgammabar;
    //Invertgammabar(gammabarUU, gammabarDD, oneoverdetgammabar);

    Compute_Inverse_Metric(ii, jj, kk, yy, in_gfs, gammabarUU);

    double BSSNL2Operatorpart1[NUM_GFS];

    {
#include "../autogenMathcode/BSSNL2Operatorpart1.txt-parsed.h"
    }

    int which_gf=0;
    for(int j=A11;j<=A33;j++)   { rhs_gfs[j][idx]    =BSSNL2Operatorpart1[which_gf]; which_gf++; }
    /**/                          rhs_gfs[TRK][idx]  =BSSNL2Operatorpart1[which_gf]; which_gf++;
  }
  //gettimeofday(&end, NULL);

  //seconds  = end.tv_sec  - start.tv_sec;
  //useconds = end.tv_usec - start.tv_usec;

  //mtime = ((seconds) * 1000 + useconds/1000.0) + 0.999;  // We add 0.999 since mtime is a long int; this rounds up the result before setting the value.  Here, rounding down is incorrect.
  //printf("Finished in %e seconds. %e gridpoints per second.\n",((double)mtime/1000.0),(Npts[2]-2*NGHOSTS)*(Npts[1]-2*NGHOSTS)*(Npts[0]-2*NGHOSTS) / ((double)mtime/1000.0));

}

#endif // BSSNL2part1_C__
