%\documentclass[twocolumn,showpacs,aps,prd,nobibnotes,floatfix]{revtex4-1}
\documentclass[11pt]{article}
%\documentclass[aps,prd]{revtex4-1}
\usepackage[hmargin=1.5cm,vmargin=1.5cm,bindingoffset=0.0cm]{geometry}
%\usepackage[hmargin=1.2cm,vmargin=1.2cm,bindingoffset=0.0cm]{geometry}

\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{epsf}
\usepackage{longtable}
\usepackage{graphics,epsfig,placeins,subfigure,wrapfig}
\usepackage[usenames]{color}
\usepackage{amsmath}
\usepackage{soul}
\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\beqn}{\begin{eqnarray}}
\newcommand{\eeqn}{\end{eqnarray}}
\newcommand{\ve}[1]{\mbox{\boldmath $#1$}}

\def\Dflat{\hat {\mathcal D}}
\def\R{{\mathcal R}}
\def\Gammaflat{\hat \Gamma}
\def\mr{\mathring}

%\renewcommand*{\arraystretch}{2}
\linespread{0.935}

% Turns off section numbering:
\setcounter{secnumdepth}{0} % <-- Turns off section numbering
\begin{document}
\title{\vspace{-1cm}Toward a BSSN code extensible to arbitrary
  reference metrics}
\author{Zachariah B.~Etienne}
\date{}
\maketitle
\vspace{-0.5cm}

Central to Baumgarte et al's
(\url{http://arxiv.org/pdf/1211.6632v1.pdf}; Phys.~Rev.~D 87 044026, 2013)
formalism for setting up a spherical polar background metric is the
computation of the ``hatted'' quantities such as $\hat{\Gamma}^i_{jk}$
(Eq 18), $\hat{D}_i \bar{\gamma}_{jk}$ (Eq. 25),
$\hat{D}_i\bar{\Lambda}^j$ (Eq. 26), $\hat{D}_i\hat{D}_l \beta^j$
(Eq. 9e), and $\partial_l\hat{D}_i\bar{\gamma}_{jk}$ (Eq. 27).

It appears that these quantities were computed by hand in spherical
polar coordinates, most likely for the purposes of optimization. 

{\bf We are not at the moment interested in an optimized code. Instead
  our goal is to produce a ``reference'' code that is extensible to
  arbitrary reference metrics and finite difference orders.}

Note that, despite being a reference code, the fact that it supports
OpenMP means that it is still likely to significantly outperform
non-OpenMP-capable codes on multi-core CPUs.

Eq. 24 in Baumgarte et al reads $\Dflat_i \bar \gamma_{jk} = \Dflat_i
\epsilon_{jk}$, where $\epsilon_{jk}$ is given in spherical
coordinates by (Eq. 20 in Baumgarte):
\begin{equation}
\label{epsilon}
\epsilon_{ij} = 
\left( \begin{array}{ccc}
h_{rr} 				& r h_{r\theta}    		& r \sin \theta h_{r\phi} \\
r h_{r\theta} 			& r^2 h_{\theta\theta} 		& r^2 \sin \theta h_{\theta \phi} \\
r \sin \theta h_{r\phi}  	& r^2 \sin \theta h_{\theta \phi}   & r^2 \sin^2 \theta h_{\phi\phi}
\end{array}
\right).
\end{equation}

Thus $\epsilon_{jk}$ may be written component-by-component as a
multiplication between $h_{jk}$ and a ``Rescaling matrix'' $\R_{jk}$:
\beq
\label{epsilondecomp}
\epsilon_{jk} = \R_{jk} h_{jk}
\eeq
Note that repeated indices on the RHS do {\it not} imply a sum; this
is a component-by-component definition, used for notational
convenience, as we will need all partial derivatives of $\epsilon_{jk}$ up
to second order. The convenience of this rather peculiar notation
becomes quite apparent when evaluating these derivatives. Note that we
are given $h_{jk}$, $h_{jk,i}$, and $h_{jk,il}$ from our finite
difference operations:
\beq
\label{epsd1calc}
\partial_i \epsilon_{jk} = h_{jk} \partial_i \R_{jk}  + \R_{jk} \partial_i h_{jk}
\eeq
and
\beqn
\label{epsd2calc}
\partial_l \partial_i \epsilon_{jk} &=& h_{jk} \partial_l \partial_i
\R_{jk}  + \R_{jk} \partial_l \partial_i h_{jk} + \partial_l h_{jk}
\partial_i \R_{jk} + \partial_i h_{jk} \partial_l \R_{jk} \\
&=& \R_{jk,il} h_{jk} + \R_{jk} h_{jk,il} + h_{jk,l} \R_{jk,i}+ h_{jk,i} \R_{jk,l}
\eeqn

Thus we only need compute all partial derivatives of $\R_{jk}$ up to
second-order. 

In addition, we will also need to compute all ``hatted'' Christoffel
symbols for the $\Dflat_i$ covariant derivatives. In spherical polar
coordinates, there are only 6 nonzero and nontrivial Christoffels
needed.

Finally, all first derivatives of the ``hatted'' Christoffel symbols
will need to be computed, for the
$\partial_l\hat{D}_i\bar{\gamma}_{jk}$ in Eq. 27. Recall that the
nonzero and nontrivial Christoffels are given by Eq. 18:
\begin{equation} \label{flatconnection}
\begin{array}{rclrcl}
\Gammaflat^r_{\theta\theta} & = & -r  ~~~~ & ~~~~
\Gammaflat^r_{\phi\phi} & = & -r \sin^2 \theta  \\ 
\Gammaflat^\theta_{\phi\phi} & = & -\sin \theta \cos \theta ~~~~ & ~~~~ 
\Gammaflat^\theta_{r\theta} & = & r^{-1}  \\
\Gammaflat^\phi_{r\phi} & = & r^{-1}  ~~~~ & ~~~~
\Gammaflat^\phi_{\phi\theta} & = & \cot \theta.
\end{array}
\end{equation}

Based on a quick analysis of these terms, we add in parentheses the
number of nonzero, nontrivial (i.e., not given by symmetries) first derivatives:
\begin{equation} \label{flatconnection}
\begin{array}{rclrcl}
\Gammaflat^r_{\theta\theta} (1) & = & -r  ~~~~ & ~~~~
\Gammaflat^r_{\phi\phi} (2) & = & -r \sin^2 \theta  \\ 
\Gammaflat^\theta_{\phi\phi} (1) & = & -\sin \theta \cos \theta ~~~~ & ~~~~ 
\Gammaflat^\theta_{r\theta} (1) & = & r^{-1}  \\
\Gammaflat^\phi_{r\phi} (1) & = & r^{-1}  ~~~~ & ~~~~
\Gammaflat^\phi_{\phi\theta} (1) & = & \cot \theta.
\end{array}
\end{equation}

So in spherical polar coordinates, we must include 6 Christoffel
symbols and 7 nontrivial Christoffel derivatives.

In spherical polar coordinates, $\R_{jk}$ may be written
\begin{equation}
\R_{ij} = 
\left( \begin{array}{ccc}
1 	&  r 		& r \sin \theta  \\
... 	& r^2  		& r^2 \sin \theta \\
...  	& ...           & r^2 \sin^2 \theta 
\end{array}
\right),
\end{equation}
where the ``...'' terms are given by symmetry of $\R_{ij}$ under index
interchange. 

Upon analysis of this matrix, there will be a total of 8 nonzero and
nontrivial $\R_{ij,k}$ in spherical polar coordinates (the rest are
either zero or given by symmetry under index interchange). Below we
add the number of first nonzero derivatives for each nontrivial
component of $\R_{ij}$ in parentheses, and nonzero second derivatives
in square brackets:

\begin{equation}
\R_{ij} = 
\left( \begin{array}{ccc}
1 (0) [0]	&  r (1) [0]	& r \sin \theta (2) [2] \\
...     	& r^2 (1) [1]	& r^2 \sin \theta (2) [3] \\
...      	& ...           & r^2 \sin^2 \theta (2) [3]
\end{array}
\right),
\end{equation}

Also, only four elements in this upper-triangle matrix will have
nonzero second derivatives (i.e., all but the $\R_{rr}$ and
$\R_{r\theta}$ elements). $\R_{r\phi,r\theta}$,
$\R_{r\phi,\theta\theta}$, $\R_{\theta\theta,rr}$, $\R_{\theta\phi,rr}$, 
$\R_{\theta\phi,r\theta}$, $\R_{\theta\phi,\theta\theta}$,
$\R_{\phi\phi,rr}$, $\R_{\phi\phi,r\theta}$, and
$\R_{\phi\phi,\theta\theta}$ will be the only nonzero, nontrivial
second derivatives (all others will either be zero or given by the
symmetries of $\R_{ij}$ or second-derivative symmetries). This means
that we only need to compute 8 first derivatives and 9 second
derivatives of $\R_{ij}$ by hand, for spherical polar coordinates.

We will now show that once these 17 derivatives of $\R_{ij}$, 6
Christoffel symbols, and 7 nontrivial Christoffel derivatives are
computed (a straightforward differentiation exercise), we can then
write code to automatically compute all needed ``hatted'' quantities.

\section{Rescaling Vectors and Computing Derivatives}
Vectors like $B^i$, $\beta^i$, and $\hat{\Lambda}^i$ are stored as
rescaled quantities, just like $\bar{A}_{ij}$ and $\bar{\gamma}_{ij}$
as well. We call these $RB^i$, $R\beta^i$, and $R\hat{\Lambda}^i$. In
spherical polar coordinates,
\beq
\beta^i = R\beta^i \R^i = 
R\beta^i 
\left(
\begin{array}{c}
1 \\
1/r \\
1/(r \sin(\theta))
\end{array}
\right)
\eeq

Again, this notation is designed not as an implied sum on index $i$,
but for convenience in taking derivatives.

\section{Generic expressions for ``hatted'' quantities}
First the ``hatted'' covariant derivative $\hat{D}_i\bar{\gamma}_{jk}$:

\beq
\label{hatderivgamma}
\Dflat_i \bar{\gamma}_{jk} = \Dflat_i \epsilon_{jk} = 
\partial_i \epsilon_{jk}
 - \hat{\Gamma}^d_{ij} \epsilon_{dk} 
- \hat{\Gamma}^d_{ik} \epsilon_{jd},
\eeq
where $\partial_i \epsilon_{jk}$ is given by Eq.~\ref{epsd1calc},
$\hat{\Gamma}^d_{ij}$ by Eq.~\ref{flatconnection}, and
$\epsilon_{jk}$ by Eqs.~\ref{epsilon} and \ref{epsilondecomp}.

Next $\hat{D}_i V^j$, where $V=\beta$, $V=\bar{\Lambda}$, or $V=B$:
\beq
\hat{D}_i V^j = \partial_i V^j + \hat{\Gamma}^j_{ik} V^k,
\eeq
where $\hat{\Gamma}^d_{ij}$ is given by Eq.~\ref{flatconnection}.

Next $\hat{D}_k \hat{D}_i V^j$:
\beqn
\hat{D}_k \hat{D}_i V^j &=& \hat{D}_k (\hat{D}_i V^j) \\
&=& \hat{D}_k (\partial_i V^j + \hat{\Gamma}^j_{id} V^d) \\
&=& \partial_k (\partial_i V^j + \hat{\Gamma}^j_{id} V^d) +
\hat{\Gamma}^j_{k\ell} (\partial_i V^\ell + \hat{\Gamma}^\ell_{id} V^d)-
\hat{\Gamma}^\ell_{ki} (\partial_\ell V^j + \hat{\Gamma}^j_{\ell d} V^d) \\
&=& V^j{}_{,ik} + \hat{\Gamma}^j_{id,k} V^d+\hat{\Gamma}^j_{id} V^d{}_{,k} +
(\hat{\Gamma}^j_{k\ell} \hat{\Gamma}^\ell_{id} - \hat{\Gamma}^\ell_{ki} \hat{\Gamma}^j_{\ell d}) V^d+
\hat{\Gamma}^j_{k\ell} V^\ell{}_{,i} - \hat{\Gamma}^\ell_{ki} V^j{}_{,\ell} \\
&=& V^j{}_{,ik} + \hat{\Gamma}^j_{id,k} V^d+\hat{\Gamma}^j_{id} V^d{}_{,k} +
(\hat{\Gamma}^j_{\ell k} \hat{\Gamma}^\ell_{id} - \hat{\Gamma}^j_{\ell d} \hat{\Gamma}^\ell_{ik}) V^d+
\hat{\Gamma}^j_{k\ell} V^\ell{}_{,i} - \hat{\Gamma}^\ell_{ki} V^j{}_{,\ell},
\eeqn
where $\hat{\Gamma}^d_{ij}$ and $\hat{\Gamma}^j_{id,k}$ are given by Eq.~\ref{flatconnection}.

And finally $\partial_l\hat{D}_i\bar{\gamma}_{jk}$. Starting from
Eq.~\ref{hatderivgamma}, we get
\beqn
\partial_l\hat{D}_i\bar{\gamma}_{jk} &=& \partial_l \left( \partial_i \epsilon_{jk}
- \hat{\Gamma}^d_{ij} \epsilon_{dk} 
- \hat{\Gamma}^d_{ik} \epsilon_{jd}\right) \\
&=& \epsilon_{jk,il} 
- \hat{\Gamma}^d_{ij,l} \epsilon_{dk} - \hat{\Gamma}^d_{ij} \epsilon_{dk,l}
- \hat{\Gamma}^d_{ik,l} \epsilon_{jd} -\hat{\Gamma}^d_{ik} \epsilon_{jd,l} \\
&=& \epsilon_{jk,il} 
- (\hat{\Gamma}^d_{ij,l} \epsilon_{dk} + \hat{\Gamma}^d_{ik,l}\epsilon_{dj})
- (\hat{\Gamma}^d_{ij} \epsilon_{dk,l} + \hat{\Gamma}^d_{ik} \epsilon_{dj,l}),
\eeqn
where $\epsilon_{jk,il}$ is given by Eq.~\ref{epsd2calc},
$\epsilon_{dk,l}$ is given by Eq.~\ref{epsd1calc}, and
$\hat{\Gamma}^d_{ik}$ \& $\hat{\Gamma}^d_{ik,l}$ are given by Eq.~\ref{flatconnection}.


\end{document}
