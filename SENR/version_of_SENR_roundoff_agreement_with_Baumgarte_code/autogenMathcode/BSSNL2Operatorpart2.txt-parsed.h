double tmp10=1/x1;
double tmp12=x1*x1;
double tmp13=1/tmp12;
double tmp20=sin(x2);
double tmp22=cos(x2);
double tmp25=tmp20*tmp20;
double tmp31=1/tmp20;
double tmp53=aDD[0][1]*gammabarUU[0][1]*x1;
double tmp52=aDD[0][0]*gammabarUU[0][0];
double tmp54=aDD[0][2]*gammabarUU[0][2]*tmp20*x1;
double tmp55=tmp52+tmp53+tmp54;
double tmp57=aDD[0][1]*gammabarUU[0][0]*x1;
double tmp58=aDD[1][1]*gammabarUU[0][1]*tmp12;
double tmp59=aDD[1][2]*gammabarUU[0][2]*tmp12*tmp20;
double tmp60=tmp57+tmp58+tmp59;
double tmp62=aDD[0][2]*gammabarUU[0][0]*tmp20*x1;
double tmp63=aDD[1][2]*gammabarUU[0][1]*tmp12*tmp20;
double tmp64=aDD[2][2]*gammabarUU[0][2]*tmp12*tmp25;
double tmp65=tmp62+tmp63+tmp64;
double tmp46=2.*hDD[1][2]*tmp20*x1;
double tmp48=hDDdD[1][2][0]*tmp12*tmp20;
double tmp88=aDD[1][2]*gammabarUU[1][2]*tmp12*tmp20;
double tmp82=aDD[0][0]*gammabarUU[0][1];
double tmp83=aDD[0][1]*gammabarUU[1][1]*x1;
double tmp84=aDD[0][2]*gammabarUU[1][2]*tmp20*x1;
double tmp85=tmp82+tmp83+tmp84;
double tmp87=aDD[1][1]*gammabarUU[1][1]*tmp12;
double tmp89=tmp53+tmp87+tmp88;
double tmp91=aDD[0][2]*gammabarUU[0][1]*tmp20*x1;
double tmp92=aDD[1][2]*gammabarUU[1][1]*tmp12*tmp20;
double tmp93=aDD[2][2]*gammabarUU[1][2]*tmp12*tmp25;
double tmp94=tmp91+tmp92+tmp93;
double tmp103=hDDdD[0][1][2]*x1;
double tmp45=hDD[0][2]*tmp22*x1;
double tmp47=hDDdD[0][2][1]*tmp20*x1;
double tmp115=aDD[0][0]*gammabarUU[0][2];
double tmp116=aDD[0][1]*gammabarUU[1][2]*x1;
double tmp117=aDD[0][2]*gammabarUU[2][2]*tmp20*x1;
double tmp118=tmp115+tmp116+tmp117;
double tmp120=aDD[0][1]*gammabarUU[0][2]*x1;
double tmp121=aDD[1][1]*gammabarUU[1][2]*tmp12;
double tmp122=aDD[1][2]*gammabarUU[2][2]*tmp12*tmp20;
double tmp123=tmp120+tmp121+tmp122;
double tmp125=aDD[2][2]*gammabarUU[2][2]*tmp12*tmp25;
double tmp126=tmp125+tmp54+tmp88;
double tmp149=0.5*gammabarUU[0][1]*hDDdD[1][1][2]*tmp12;
double tmp150=-2.*hDD[1][2]*tmp20*x1;
double tmp151=-(hDDdD[1][2][0]*tmp12*tmp20);
double tmp152=tmp103+tmp150+tmp151+tmp45+tmp47;
double tmp153=0.5*gammabarUU[0][0]*tmp152;
double tmp154=2.*tmp12*tmp20*tmp22;
double tmp155=2.*hDD[2][2]*tmp12*tmp20*tmp22;
double tmp156=hDDdD[2][2][1]*tmp12*tmp25;
double tmp157=tmp154+tmp155+tmp156;
double tmp158=0.5*gammabarUU[0][2]*tmp157;
double tmp159=tmp149+tmp153+tmp158;
double tmp38=0.5*gammabarUU[0][0]*hDDdD[0][0][1];
double tmp39=2.*x1;
double tmp40=2.*hDD[1][1]*x1;
double tmp41=hDDdD[1][1][0]*tmp12;
double tmp42=tmp39+tmp40+tmp41;
double tmp43=0.5*gammabarUU[0][1]*tmp42;
double tmp44=-(hDDdD[0][1][2]*x1);
double tmp49=tmp44+tmp45+tmp46+tmp47+tmp48;
double tmp50=0.5*gammabarUU[0][2]*tmp49;
double tmp51=tmp38+tmp43+tmp50;
double tmp102=0.5*gammabarUU[0][0]*hDDdD[0][0][2];
double tmp104=-(hDD[0][2]*tmp22*x1);
double tmp105=-(hDDdD[0][2][1]*tmp20*x1);
double tmp106=tmp103+tmp104+tmp105+tmp46+tmp48;
double tmp107=0.5*gammabarUU[0][1]*tmp106;
double tmp108=2.*tmp25*x1;
double tmp109=2.*hDD[2][2]*tmp25*x1;
double tmp110=hDDdD[2][2][0]*tmp12*tmp25;
double tmp111=tmp108+tmp109+tmp110;
double tmp112=0.5*gammabarUU[0][2]*tmp111;
double tmp113=tmp102+tmp107+tmp112;
double tmp206=1/detg;
double tmp207=tmp12*tmp12;
double tmp208=1/tmp207;
double tmp209=1/tmp25;
double tmp210=tmp12*x1;
double tmp211=4.*detg*tmp210*tmp25;
double tmp212=detgdD[0]*tmp207*tmp25;
double tmp213=tmp211+tmp212;
double tmp215=tmp213*tmp213;
double tmp14=-(tmp13*vetU[1]);
double tmp15=tmp10*vetUdD[1][0];
double tmp16=tmp14+tmp15;
double tmp228=tmp207*x1;
double tmp229=1/tmp228;
double tmp224=2.*detg*tmp20*tmp207*tmp22;
double tmp225=detgdD[1]*tmp207*tmp25;
double tmp226=tmp224+tmp225;
double tmp32=-(tmp13*tmp31*vetU[2]);
double tmp33=1.*tmp10*tmp31*vetUdD[2][0];
double tmp34=tmp32+tmp33;
double tmp251=detgdD[2]*detgdD[2];
double tmp241=4.*detgdD[2]*tmp210*tmp25;
double tmp242=detgdDD[0][2]*tmp207*tmp25;
double tmp243=tmp241+tmp242;
double tmp231=8.*detg*tmp20*tmp210*tmp22;
double tmp232=2.*detgdD[0]*tmp20*tmp207*tmp22;
double tmp233=4.*detgdD[1]*tmp210*tmp25;
double tmp234=detgdDD[0][1]*tmp207*tmp25;
double tmp235=tmp231+tmp232+tmp233+tmp234;
double tmp280=tmp226*tmp226;
double tmp284=tmp22*tmp22;
double tmp264=2.*detgdD[2]*tmp20*tmp207*tmp22;
double tmp265=detgdDD[1][2]*tmp207*tmp25;
double tmp266=tmp264+tmp265;
double tmp296=tan(x2);
double tmp297=1/tmp296;
double tmp295=1.*tmp10*tmp31*vetUdD[2][1];
double tmp298=-(tmp10*tmp297*tmp31*vetU[2]);
double tmp299=tmp295+tmp298;
double tmp202=-(tmp13*vetUdD[1][1]);
double tmp203=tmp10*vetUdDD[1][0][1];
double tmp321=1/tmp210;
double tmp272=tmp10*vetUdDD[1][1][1];
double tmp172=2.*hDD[0][1];
double tmp173=-hDDdD[0][0][1];
double tmp174=2.*hDDdD[0][1][0]*x1;
double tmp175=tmp172+tmp173+tmp174;
double tmp177=-hDDdD[0][0][2];
double tmp178=2.*hDD[0][2]*tmp20;
double tmp179=2.*hDDdD[0][2][0]*tmp20*x1;
double tmp180=tmp177+tmp178+tmp179;
double tmp166=gammabarUU[0][0]*tmp55;
double tmp167=gammabarUU[0][1]*tmp60;
double tmp168=gammabarUU[0][2]*tmp65;
double tmp169=tmp166+tmp167+tmp168;
double tmp186=gammabarUU[0][0]*tmp85;
double tmp187=gammabarUU[0][1]*tmp89;
double tmp188=gammabarUU[0][2]*tmp94;
double tmp189=tmp186+tmp187+tmp188;
double tmp98=gammabarUU[0][2]*tmp55;
double tmp99=gammabarUU[1][2]*tmp60;
double tmp100=gammabarUU[2][2]*tmp65;
double tmp101=tmp100+tmp98+tmp99;
double tmp194=gammabarUU[0][0]*tmp118;
double tmp195=gammabarUU[0][1]*tmp123;
double tmp196=gammabarUU[0][2]*tmp126;
double tmp197=tmp194+tmp195+tmp196;
double tmp356=0.5*gammabarUU[0][1]*hDDdD[0][0][2];
double tmp357=0.5*gammabarUU[1][1]*tmp106;
double tmp358=0.5*gammabarUU[1][2]*tmp111;
double tmp359=tmp356+tmp357+tmp358;
double tmp119=gammabarUU[0][2]*tmp118;
double tmp124=gammabarUU[1][2]*tmp123;
double tmp127=gammabarUU[2][2]*tmp126;
double tmp128=tmp119+tmp124+tmp127;
double tmp131=2.*hDDdD[0][2][2]*tmp20*x1;
double tmp132=-2.*tmp25*x1;
double tmp133=-2.*hDD[2][2]*tmp25*x1;
double tmp134=-(hDDdD[2][2][0]*tmp12*tmp25);
double tmp135=tmp131+tmp132+tmp133+tmp134;
double tmp137=2.*hDDdD[1][2][2]*tmp12*tmp20;
double tmp138=-2.*tmp12*tmp20*tmp22;
double tmp139=-2.*hDD[2][2]*tmp12*tmp20*tmp22;
double tmp140=-(hDDdD[2][2][1]*tmp12*tmp25);
double tmp141=tmp137+tmp138+tmp139+tmp140;
double tmp145=gammabarUU[0][2]*tmp85;
double tmp146=gammabarUU[1][2]*tmp89;
double tmp147=gammabarUU[2][2]*tmp94;
double tmp148=tmp145+tmp146+tmp147;
double tmp56=gammabarUU[0][1]*tmp55;
double tmp61=gammabarUU[1][1]*tmp60;
double tmp66=gammabarUU[1][2]*tmp65;
double tmp67=tmp56+tmp61+tmp66;
double tmp170=-6.*alpha*phidD[0];
double tmp350=0.5*gammabarUU[0][1]*hDDdD[0][0][1];
double tmp351=-tmp10;
double tmp352=0.5*gammabarUU[1][1]*tmp42;
double tmp353=0.5*gammabarUU[1][2]*tmp49;
double tmp354=tmp350+tmp351+tmp352+tmp353;
double tmp86=gammabarUU[0][1]*tmp85;
double tmp90=gammabarUU[1][1]*tmp89;
double tmp95=gammabarUU[1][2]*tmp94;
double tmp96=tmp86+tmp90+tmp95;
double tmp190=-6.*alpha*phidD[1];
double tmp70=-2.*x1;
double tmp71=-2.*hDD[1][1]*x1;
double tmp72=2.*hDDdD[0][1][1]*x1;
double tmp73=-(hDDdD[1][1][0]*tmp12);
double tmp74=tmp70+tmp71+tmp72+tmp73;
double tmp76=-(hDDdD[1][1][2]*tmp12);
double tmp77=2.*hDD[1][2]*tmp12*tmp22;
double tmp78=2.*hDDdD[1][2][1]*tmp12*tmp20;
double tmp79=tmp76+tmp77+tmp78;
double tmp161=gammabarUU[0][1]*tmp118;
double tmp162=gammabarUU[1][1]*tmp123;
double tmp163=gammabarUU[1][2]*tmp126;
double tmp164=tmp161+tmp162+tmp163;
double tmp198=-6.*alpha*phidD[2];
double tmp368=0.5*gammabarUU[1][1]*hDDdD[1][1][2]*tmp12;
double tmp369=0.5*gammabarUU[0][1]*tmp152;
double tmp370=0.5*gammabarUU[1][2]*tmp157;
double tmp371=tmp368+tmp369+tmp370;
double tmp204=-(tmp13*tmp31*vetUdD[2][2]);
double tmp205=1.*tmp10*tmp31*vetUdDD[2][0][2];
double tmp214=tmp213*vetUdD[0][0];
double tmp216=-(tmp206*tmp208*tmp209*tmp215*vetU[0]);
double tmp217=12.*detg*tmp12*tmp25;
double tmp218=8.*detgdD[0]*tmp210*tmp25;
double tmp219=detgdDD[0][0]*tmp207*tmp25;
double tmp220=tmp217+tmp218+tmp219;
double tmp221=tmp220*vetU[0];
double tmp222=tmp214+tmp216+tmp221;
double tmp223=0.5*tmp206*tmp208*tmp209*tmp222;
double tmp227=tmp16*tmp226;
double tmp230=-(tmp206*tmp209*tmp213*tmp226*tmp229*vetU[1]);
double tmp236=tmp10*tmp235*vetU[1];
double tmp237=tmp227+tmp230+tmp236;
double tmp238=0.5*tmp206*tmp208*tmp209*tmp237;
double tmp239=detgdD[2]*tmp207*tmp25*tmp34;
double tmp240=-(detgdD[2]*tmp10*tmp206*tmp213*tmp31*vetU[2]);
double tmp244=1.*tmp10*tmp243*tmp31*vetU[2];
double tmp245=tmp239+tmp240+tmp244;
double tmp246=0.5*tmp206*tmp208*tmp209*tmp245;
double tmp247=tmp202+tmp203+tmp204+tmp205+tmp223+tmp238+tmp246+vetUdDD[0][0][0];
double tmp249=tmp10*vetUdDD[1][1][2];
double tmp250=1.*tmp10*tmp31*vetUdDD[2][2][2];
double tmp252=-(tmp20*tmp206*tmp210*tmp251*vetU[2]);
double tmp253=detgdDD[2][2]*tmp20*tmp210*vetU[2];
double tmp254=detgdD[2]*tmp20*tmp210*vetUdD[2][2];
double tmp255=tmp252+tmp253+tmp254;
double tmp256=0.5*tmp206*tmp208*tmp209*tmp255;
double tmp257=-(detgdD[2]*tmp206*tmp213*vetU[0]);
double tmp258=tmp213*vetUdD[0][2];
double tmp259=tmp243*vetU[0];
double tmp260=tmp257+tmp258+tmp259;
double tmp261=0.5*tmp206*tmp208*tmp209*tmp260;
double tmp262=-(detgdD[2]*tmp10*tmp206*tmp226*vetU[1]);
double tmp263=tmp10*tmp226*vetUdD[1][2];
double tmp267=tmp10*tmp266*vetU[1];
double tmp268=tmp262+tmp263+tmp267;
double tmp269=0.5*tmp206*tmp208*tmp209*tmp268;
double tmp270=tmp249+tmp250+tmp256+tmp261+tmp269+vetUdDD[0][0][2];
double tmp273=1.*tmp10*tmp31*vetUdDD[2][1][2];
double tmp274=tmp213*vetUdD[0][1];
double tmp275=-(tmp206*tmp208*tmp209*tmp213*tmp226*vetU[0]);
double tmp276=tmp235*vetU[0];
double tmp277=tmp274+tmp275+tmp276;
double tmp278=0.5*tmp206*tmp208*tmp209*tmp277;
double tmp279=tmp10*tmp226*vetUdD[1][1];
double tmp281=-(tmp206*tmp209*tmp229*tmp280*vetU[1]);
double tmp282=4.*detgdD[1]*tmp20*tmp207*tmp22;
double tmp283=detgdDD[1][1]*tmp207*tmp25;
double tmp285=2.*tmp207*tmp284;
double tmp286=-2.*tmp207*tmp25;
double tmp287=tmp285+tmp286;
double tmp288=detg*tmp287;
double tmp289=tmp282+tmp283+tmp288;
double tmp290=tmp10*tmp289*vetU[1];
double tmp291=tmp279+tmp281+tmp290;
double tmp292=0.5*tmp206*tmp208*tmp209*tmp291;
double tmp293=-(detgdD[2]*tmp10*tmp206*tmp226*tmp31*vetU[2]);
double tmp294=1.*tmp10*tmp266*tmp31*vetU[2];
double tmp300=detgdD[2]*tmp207*tmp25*tmp299;
double tmp301=tmp293+tmp294+tmp300;
double tmp302=0.5*tmp206*tmp208*tmp209*tmp301;
double tmp303=-(tmp10*tmp297*tmp31*vetUdD[2][2]);
double tmp304=tmp272+tmp273+tmp278+tmp292+tmp302+tmp303+vetUdDD[0][0][1];
double tmp391=tmp10*vetUdD[0][2];
double tmp392=-(tmp20*tmp22*tmp299);
double tmp393=-(tmp10*tmp297*vetUdD[1][2]);
double tmp419=0.5*gammabarUU[0][2]*hDDdD[0][0][1];
double tmp420=0.5*gammabarUU[1][2]*tmp42;
double tmp421=0.5*gammabarUU[2][2]*tmp49;
double tmp422=tmp419+tmp420+tmp421;
double tmp430=0.5*gammabarUU[0][2]*hDDdD[0][0][2];
double tmp431=0.5*gammabarUU[1][2]*tmp106;
double tmp432=0.5*gammabarUU[2][2]*tmp111;
double tmp433=tmp351+tmp430+tmp431+tmp432;
double tmp445=0.5*gammabarUU[1][2]*hDDdD[1][1][2]*tmp12;
double tmp446=0.5*gammabarUU[0][2]*tmp152;
double tmp447=0.5*gammabarUU[2][2]*tmp157;
double tmp448=-tmp297;
double tmp449=tmp445+tmp446+tmp447+tmp448;
double tmp317=-(tmp13*vetU[0]);
double tmp318=tmp10*vetUdD[0][0];
double tmp475=tmp10*vetUdD[0][1];
double tmp481=tmp296*tmp296;
double tmp482=1/tmp481;
double tmp477=1.*tmp10*tmp297*vetUdD[1][1];
double tmp487=tmp20*tmp25;
double tmp488=1/tmp487;
BSSNL2Operatorpart2[0]=2.*alpha*tmp101*tmp113+2.*alpha*tmp148*tmp159+2.*alpha*tmp159*tmp164-2.*tmp169*(alphadD[0]+tmp170-alpha*(0.5*gammabarUU[0][0]*hDDdD[0][0][0]+0.5*gammabarUU[0][1]*tmp175+0.5*gammabarUU[0][2]*tmp180))-2.*tmp197*(alphadD[2]-alpha*tmp113+tmp198)+0.3333333333333333*(gammabarUU[0][0]*tmp247+gammabarUU[0][2]*tmp270+gammabarUU[0][1]*tmp304)-2.*tmp189*(alphadD[1]+tmp190-alpha*tmp51)+2.*alpha*tmp51*tmp67-1.3333333333333333*alpha*gammabarUU[0][0]*trKdD[0]-1.3333333333333333*alpha*gammabarUU[0][1]*trKdD[1]-1.3333333333333333*alpha*gammabarUU[0][2]*trKdD[2]+gammabarUU[0][0]*vetUdDD[0][0][0]+2.*alpha*tmp96*(0.5*gammabarUU[0][1]*hDDdD[1][1][1]*tmp12+0.5*gammabarUU[0][0]*tmp74+0.5*gammabarUU[0][2]*tmp79+x1)+2.*gammabarUU[0][1]*(-(tmp10*vetUdD[0][1])+vetUdDD[0][0][1]-tmp16*x1)+2.*alpha*tmp128*(0.5*gammabarUU[0][0]*tmp135+0.5*gammabarUU[0][1]*tmp141+0.5*gammabarUU[0][2]*hDDdD[2][2][2]*tmp12*tmp25+tmp25*x1)+2.*gammabarUU[1][2]*(-(tmp297*vetUdD[0][2])-vetUdD[1][2]+vetUdDD[0][1][2]-tmp25*tmp299*x1)+2.*gammabarUU[0][2]*(-(tmp10*vetUdD[0][2])+vetUdDD[0][0][2]-tmp25*tmp34*x1)+gammabarUU[1][1]*(-vetU[0]-2.*vetUdD[1][1]+vetUdDD[0][1][1]+vetUdD[0][0]*x1)+gammabarUU[2][2]*(-(tmp25*vetU[0])-2.*tmp20*tmp22*vetU[1]+tmp20*tmp22*vetUdD[0][1]-2.*tmp20*vetUdD[2][2]+vetUdDD[0][2][2]+tmp25*vetUdD[0][0]*x1);
BSSNL2Operatorpart2[1]=x1*(2.*alpha*tmp169*(0.5*gammabarUU[0][1]*hDDdD[0][0][0]+0.5*gammabarUU[1][1]*tmp175+0.5*gammabarUU[1][2]*tmp180)+2.*alpha*tmp128*(0.5*gammabarUU[0][1]*tmp135+0.5*gammabarUU[1][1]*tmp141+tmp20*tmp22+0.5*gammabarUU[1][2]*hDDdD[2][2][2]*tmp12*tmp25)+0.3333333333333333*(gammabarUU[0][1]*tmp247+gammabarUU[1][2]*tmp270+gammabarUU[1][1]*tmp304)+2.*gammabarUU[0][1]*(tmp202+tmp203+tmp317+tmp318)+2.*alpha*tmp189*tmp354+2.*alpha*tmp101*tmp359+2.*alpha*tmp197*tmp359+2.*alpha*tmp148*tmp371-2.*tmp164*(alphadD[2]+tmp198-alpha*tmp371)+gammabarUU[1][2]*(tmp249+tmp391+tmp392+tmp393)-2.*(alphadD[0]+tmp170-alpha*tmp354)*tmp67-2.*(alphadD[1]+tmp190-alpha*(0.5*gammabarUU[1][1]*hDDdD[1][1][1]*tmp12+0.5*gammabarUU[0][1]*tmp74+0.5*gammabarUU[1][2]*tmp79))*tmp96-1.3333333333333333*alpha*gammabarUU[0][1]*trKdD[0]-1.3333333333333333*alpha*gammabarUU[1][1]*trKdD[1]-1.3333333333333333*alpha*gammabarUU[1][2]*trKdD[2]+gammabarUU[1][2]*(tmp249+tmp391+tmp392+tmp393-tmp10*tmp20*vetU[2]+1.*tmp10*tmp22*tmp297*vetU[2]+1.*tmp10*(tmp25-tmp284)*tmp31*vetU[2])+gammabarUU[0][0]*(2.*tmp10*tmp16+2.*tmp321*vetU[1]-2.*tmp13*vetUdD[1][0]+tmp10*vetUdDD[1][0][0])+2.*gammabarUU[0][2]*(-(tmp20*tmp22*tmp34)-tmp13*vetUdD[1][2]+tmp10*vetUdDD[1][0][2])+gammabarUU[1][1]*(tmp272+2.*tmp10*vetUdD[0][1]+tmp16*x1)+gammabarUU[2][2]*(tmp10*tmp25*vetU[1]-tmp10*tmp284*vetU[1]+tmp10*tmp20*tmp22*vetUdD[1][1]-2.*tmp10*tmp22*vetUdD[2][2]+tmp10*vetUdDD[1][2][2]+tmp16*tmp25*x1));
BSSNL2Operatorpart2[2]=tmp20*x1*(2.*alpha*tmp169*(0.5*gammabarUU[0][2]*hDDdD[0][0][0]+0.5*gammabarUU[1][2]*tmp175+0.5*gammabarUU[2][2]*tmp180)-2.*tmp128*(alphadD[2]+tmp198-alpha*(0.5*gammabarUU[0][2]*tmp135+0.5*gammabarUU[1][2]*tmp141+0.5*gammabarUU[2][2]*hDDdD[2][2][2]*tmp12*tmp25))+0.3333333333333333*(gammabarUU[0][2]*tmp247+gammabarUU[2][2]*tmp270+gammabarUU[1][2]*tmp304)+2.*gammabarUU[0][2]*(tmp204+tmp205+1.*tmp16*tmp297+tmp317+tmp318)+2.*alpha*tmp189*tmp422+2.*alpha*tmp197*tmp433-2.*tmp101*(alphadD[0]+tmp170-alpha*tmp433)+2.*alpha*tmp164*tmp449-2.*tmp148*(alphadD[1]+tmp190-alpha*tmp449)+2.*alpha*tmp422*tmp67+2.*alpha*(0.5*gammabarUU[1][2]*hDDdD[1][1][1]*tmp12+0.5*gammabarUU[0][2]*tmp74+0.5*gammabarUU[2][2]*tmp79)*tmp96-1.3333333333333333*alpha*gammabarUU[0][2]*trKdD[0]-1.3333333333333333*alpha*gammabarUU[1][2]*trKdD[1]-1.3333333333333333*alpha*gammabarUU[2][2]*trKdD[2]+gammabarUU[1][2]*(tmp273+tmp303+tmp475+tmp477-tmp10*tmp209*vetU[1])+gammabarUU[1][2]*(tmp273+tmp303+tmp475+tmp477-tmp10*vetU[1]-tmp10*tmp482*vetU[1])+gammabarUU[0][0]*(2.*tmp10*tmp34+2.*tmp31*tmp321*vetU[2]-2.*tmp13*tmp31*vetUdD[2][0]+1.*tmp10*tmp31*vetUdDD[2][0][0])+2.*gammabarUU[0][1]*(1.*tmp297*tmp34+1.*tmp13*tmp297*tmp31*vetU[2]-tmp10*tmp297*tmp31*vetUdD[2][0]-tmp13*tmp31*vetUdD[2][1]+1.*tmp10*tmp31*vetUdDD[2][0][1])+gammabarUU[1][1]*(2.*tmp297*tmp299+1.*tmp10*tmp31*vetU[2]+1.*tmp10*tmp31*tmp482*vetU[2]-tmp10*tmp488*vetU[2]+(1.*tmp10*tmp31*tmp482+1.*tmp10*tmp488)*vetU[2]-2.*tmp10*tmp297*tmp31*vetUdD[2][1]+1.*tmp10*tmp31*vetUdDD[2][1][1]+tmp34*x1)+gammabarUU[2][2]*(tmp250+tmp20*tmp22*tmp299+2.*tmp10*vetUdD[0][2]+2.*tmp10*tmp297*vetUdD[1][2]+tmp25*tmp34*x1));
