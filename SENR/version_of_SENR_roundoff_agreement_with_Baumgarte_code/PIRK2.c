// PIRK2 Equation 1
void PIRK2_Eq1(const int gf, double **gfs_n, double **gfs_L1_n, double **gfs_1)
{
  LOOP_NOGZFILL(i, j, k)
    {
      const int idx = GFIDX(i, j, k);

      // u^{(1)} = u^{n} + dt * L1(u^{n}, v^{n})
      gfs_1[gf][idx] = gfs_n[gf][idx] + dt * gfs_L1_n[gf][idx];
    }

  return;
}

// PIRK2 Equation 2
void PIRK2_Eq2(const int gf, double **gfs_n, double **gfs_L2_n, double **gfs_L2_1, double **gfs_L3_n, double **gfs_1)
{
  LOOP_NOGZFILL(i, j, k)
    {
      const int idx = GFIDX(i, j, k);

      // v^{(1)} = v^{n} + dt * (0.5 * (L2(u^{n}) + L2(u^{(1)})) + L3(u^{n}, v^{n}))
      gfs_1[gf][idx] = gfs_n[gf][idx] + dt * (0.5 * (gfs_L2_n[gf][idx] + gfs_L2_1[gf][idx]) + gfs_L3_n[gf][idx]);
    }

  return;
}

// PIRK2 Equation 3
void PIRK2_Eq3(const int gf, double **gfs_n, double **gfs_1, double **gfs_L1_1, double **gfs_np1)
{
  LOOP_NOGZFILL(i, j, k)
    {
      const int idx = GFIDX(i, j, k);

      // u^{n+1} = 0.5 * (u^{n} + u^{(1)} + dt * L3(u^{(1)}, v^{(1)}))
      gfs_np1[gf][idx] = 0.5 * (gfs_n[gf][idx] + gfs_1[gf][idx] + dt * gfs_L1_1[gf][idx]);
    }

  return;
}

// PIRK Equation 4
void PIRK2_Eq4(const int gf, double **gfs_n, double **gfs_L2_n, double **gfs_L2_np1, double **gfs_L3_n, double **gfs_L3_1, double **gfs_np1)
{
    LOOP_NOGZFILL(i, j, k)
    {
      const int idx = GFIDX(i, j, k);

      // v^{n+1} = v^{n} + 0.5 * dt * (L2(u^{n}) + L2(u^{n+1}) + L3(u^{n}, v^{n}) + L3(u^{(1)}, v^{(1)}))
      gfs_np1[gf][idx] = gfs_n[gf][idx] + 0.5 * dt * (gfs_L2_n[gf][idx] + gfs_L2_np1[gf][idx] + gfs_L3_n[gf][idx] + gfs_L3_1[gf][idx]);
    }

  return;
}
