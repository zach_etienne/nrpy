// SymTP coordinates (x, rho, ph) = (yy[0], yy[1], yy[2])
#define bScale  1.0
#define AMAX   10.0
const double del[DIM] = {AMAX / Nx1, 2.0 / Nx2, 2.0 * M_PI / Nx3};
const double slope[DIM] = {AMAX, 2.0, 2.0 * M_PI};
const double intercept[DIM] = {0.0, -1.0, 0.0};

#define REFERENCE_METRIC {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, yy[1][idx] * yy[1][idx]}}
#define RESCALING_MATRIX {{1.0, 1.0, yy[1][idx]}, {1.0, 1.0, yy[1][idx]}, {yy[1][idx], yy[1][idx], yy[1][idx] * yy[1][idx]}}
#define RESCALING_VECTOR {1.0, 1.0, 1.0 / yy[1][idx]}

// -xmax < x(x1, x2) < xmax
double y1_of_t_x(const double t, const double x1, const double x2, const double x3)
{
  const double A = x1;
  const double B = x2;
  const double x = bScale * sqrt(1.0 + A * A) * sin(0.5 * M_PI * B);
  return x;
}

// 0 < rho(x1, x2) < rhomax
double y2_of_t_x(const double t, const double x1, const double x2, const double x3)
{
  const double A = x1;
  const double B = x2;
  const double rho = bScale * A * cos(0.5 * M_PI * B);
  return rho;
}

// 0 < ph(x3) < 2 * pi
double y3_of_t_x(const double t, const double x1, const double x2, const double x3)
{
  return x3;
}

void Apply_out_sym_bcs(const int gf, const double t, double **xx, double **yy, double **func)
{
  // Vector and tensor parity conditions.
  int parity_axis = 1;

#if defined EVOLVE_BSSN
  if(gf == VET2 || gf == LAMB2 || gf == BET2 || gf == H12 || gf == A12 || gf == H23 || gf == A23)
    { 
      // Vector component rho or tensor components x-rho or rho-ph
      parity_axis = -1;
    }
#endif

#if defined OUTER_BOUNDARY_EXACT
  // Exact outer boundary conditions, on region d6
  for(int k = 0; k < Npts[2]; k++)
    {
      for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
	{
	  for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++)
	    {
	      double soln[2];
	      Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // Use exact outer boundary condition
	      func[gf][GFIDX(i, j, k)] = soln[gf]; // Returns u if gf == 0, and v if gf == 1
	    }
	}
    }

  // Exact outer boundary conditions, on regions d5 and d7
  for(int k = 0; k < Npts[2]; k++)
    {
      const int kp = NGHOSTS + (k + Npts[2] / 2 - 2 * NGHOSTS) % (Npts[2] - 2 * NGHOSTS); // ph -> ph + pi
      
      for(int j = 0; j < NGHOSTS; j++)
	{
	  const int jm = 2 * NGHOSTS - 1 - j;
	  const int jp = Npts[1] - 2 * NGHOSTS + j;
	  const int jg = Npts[1] - 1 - j;
	  
	  for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++)
	    {
	      double soln[2];

	      // Region d5
	      Exact_soln(t, xx[0][i], xx[1][jm], xx[2][kp], soln); // Use exact outer boundary condition
	      func[gf][GFIDX(i, j, k)] = soln[gf]; // Returns u if gf == 0, and v if gf == 1

	      // Region d7
	      Exact_soln(t, xx[0][i], xx[1][jp], xx[2][kp], soln); // Use exact outer boundary condition
	      func[gf][GFIDX(i, jg, k)] = soln[gf]; // Returns u if gf == 0, and v if gf == 1
	    }
	}
    }
#elif defined OUTER_BOUNDARY_LIN_EXTRAP
  // Linear extrapolation outer boundary conditions
  for(int k = 0; k < Npts[2]; k++)
    { 
      for(int j = 0; j < Npts[1]; j++)
        { 
          for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++)
            { 
              func[gf][GFIDX(i, j, k)] = 2.0 * func[gf][GFIDX(i - 1, j, k)] - func[gf][GFIDX(i - 2, j, k)];
            }
        }
    }
#endif

  
  // Region d2
  // x1 / Amax < 0 and 0 < x2 < 1, so that rho(x1, x2) < 0
  // Employ parity condition x1 -> -x1
  for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++)
    {
      const int kp = NGHOSTS + (k + Npts[2] / 2 - 2 * NGHOSTS) % (Npts[2] - 2 * NGHOSTS); // ph -> ph + pi

      for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
	{
	  for(int i = 0; i < NGHOSTS; i++)
	    {
	      const int im = 2 * NGHOSTS - 1 - i;

	      // f(-eps, B, ph) = f(+eps, B, ph + pi)
	      func[gf][GFIDX(i, j, k)] = parity_axis * func[gf][GFIDX(im, j, kp)];
	    }
	}
    }

  // Regions d4 and d8
  for(int j = 0; j < NGHOSTS; j++)
    {
      const int jm = 2 * NGHOSTS - 1 - j;
      const int jp = Npts[1] - 2 * NGHOSTS + j;
      const int jg = Npts[1] - 1 - j;

      for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++)
	{
	  const int kp = NGHOSTS + (k + Npts[2] / 2 - 2 * NGHOSTS) % (Npts[2] - 2 * NGHOSTS); // ph -> ph + pi
	      
	  for(int i = 0; i < Npts[0]; i++)
	    {
	      // f(A, -1 - eps, ph) = f(A, -1 + eps, ph + pi)
	      func[gf][GFIDX(i, j, k)] = parity_axis * func[gf][GFIDX(i, jm, kp)];

	      // f(A, 1 + eps, ph) = f(A, 1 - eps, ph + pi)
	      func[gf][GFIDX(i, jg, k)] = parity_axis * func[gf][GFIDX(i, jp, kp)];
	    }
	}
    }

  // Regions d1 and d3
  for(int k = 0; k < Npts[2]; k++)
    {
      for(int j = 0; j < NGHOSTS; j++)
	{
	  const int jm = 2 * NGHOSTS - 1 - j;
	  const int jp = Npts[1] - 2 * NGHOSTS + j;
	  const int jg = Npts[1] - 1 - j;
	  
	  for(int i = 0; i < NGHOSTS; i++)
	    {
	      const int im = 2 * NGHOSTS - 1 - i;

	      // Region d3
	      func[gf][GFIDX(i, j, k)] = func[gf][GFIDX(im, jm, k)];

	      // Region d1
	      func[gf][GFIDX(i, jg, k)] = func[gf][GFIDX(im, jp, k)];
	    }
	}
    }
  
  // Apply periodic BC in ph direction
  for(int k = 0; k < NGHOSTS; k++)
    {
      const int km = 2 * NGHOSTS - 1 - k;
      const int kp = Npts[2] - 2 * NGHOSTS + k;
      const int kg = Npts[2] - 1 - k;

      for(int j = 0; j < Npts[1]; j++)
	{
	  for(int i = 0; i < Npts[0] - NGHOSTS; i++)
	    {
	      // f(A, B, -eps) = f(A, B, 2 * pi - eps)
	      func[gf][GFIDX(i, j, k)] = func[gf][GFIDX(i, j, kp)];

	      // f(A, B, 2 * pi + eps) = f(A, B, +eps)
	      func[gf][GFIDX(i, j, kg)] = func[gf][GFIDX(i, j, km)];
	    }
	}
    }
  
  return;
}

// (x, rho, ph) = (y1, y2, y3)
double Find_Time_Step(double **yy, double y_min[3], double y_max[3])
{
  // Initialize minimum time step
  double dt = DBL_MAX;

  // Initialize bounds
  for(int i = 0; i < 3; i++)
    {
      y_min[i] = DBL_MAX;
      y_max[i] = 0.0;
    }
  
  // Find physical domain extent, smallest step sizes
  for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
    {
      for(int i = NGHOSTS; i < Npts[0] - NGHOSTS; i++)
	{
	  const int idx = GFIDX(i, j, NGHOSTS);
	  const int ip1 = GFIDX(i + 1, j, NGHOSTS);
	  const int im1 = GFIDX(i - 1, j, NGHOSTS);
	  const int jp1 = GFIDX(i, j + 1, NGHOSTS);
	  const int jm1 = GFIDX(i, j - 1, NGHOSTS);

	  const double dxip1 = yy[0][idx] - yy[0][ip1];
	  const double dxim1 = yy[0][idx] - yy[0][im1];
	  const double dxjp1 = yy[0][idx] - yy[0][jp1];
	  const double dxjm1 = yy[0][idx] - yy[0][jm1];

	  const double drhoip1 = yy[1][idx] - yy[1][ip1];
	  const double drhoim1 = yy[1][idx] - yy[1][im1];
	  const double drhojp1 = yy[1][idx] - yy[1][jp1];
	  const double drhojm1 = yy[1][idx] - yy[1][jm1];

	  const double drip1 = sqrt(dxip1 * dxip1 + drhoip1 * drhoip1);
	  const double drim1 = sqrt(dxim1 * dxim1 + drhoim1 * drhoim1);
	  const double drjp1 = sqrt(dxjp1 * dxjp1 + drhojp1 * drhojp1);
	  const double drjm1 = sqrt(dxjm1 * dxjm1 + drhojm1 * drhojm1);

	  const double dri = (drip1 < drim1) ? drip1 : drim1;
	  const double drj = (drjp1 < drjm1) ? drjp1 : drjm1;
	  const double dr = (dri < drj) ? dri : drj;

	  // xmin
	  if(y_min[0] > yy[0][idx])
	    {
	      y_min[0] = yy[0][idx];
	    }

	  // xmax
	  if(y_max[0] < yy[0][idx])
	    {
	      y_max[0] = yy[0][idx];
	    }

	  // rhomin
	  if(y_min[1] > yy[1][idx])
	    {
	      y_min[1] = yy[1][idx];
	    }

	  // rhomax
	  if(y_max[1] < yy[1][idx])
	    {
	      y_max[1] = yy[1][idx];
	    }

	  if(dt > dr)
	    {
	      dt = dr;
	    }
	}
    }

  // phmin < ph < phmax
  y_min[2] = yy[2][GFIDX(NGHOSTS, NGHOSTS, NGHOSTS)]; // phmin
  y_max[2] = yy[2][GFIDX(NGHOSTS, NGHOSTS, Npts[2] - NGHOSTS - 1)]; // phmax

  const double dph = fabs(yy[2][GFIDX(NGHOSTS, NGHOSTS, NGHOSTS + 1)] - y_min[2]);

  if(dt > y_min[1] * dph)
    {
      dt = y_min[1] * dph;
    }  

  return dt;
}
