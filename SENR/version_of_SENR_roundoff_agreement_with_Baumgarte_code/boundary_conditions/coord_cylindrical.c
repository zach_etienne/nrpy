// Clyindrical coordinates (rho, ph, z) = (yy[0], yy[1], yy[2])
#define RHOMAX  5.0
#define ZMAX    5.0
#define ZMIN   -5.0
const double del[DIM] = {RHOMAX / Nx1, 2.0 * M_PI / Nx2, (ZMAX - ZMIN) / Nx3};
const double slope[DIM] = {RHOMAX, 2.0 * M_PI, ZMAX - ZMIN};
const double intercept[DIM] = {0.0, 0.0, ZMIN};

#define REFERENCE_METRIC {{1.0, 0.0, 0.0}, {0.0, yy[0][idx] * yy[0][idx], 0.0}, {0.0, 0.0, 1.0}}
#define RESCALING_MATRIX {{1.0, yy[0][idx], 1.0}, {yy[0][idx], yy[0][idx] * yy[0][idx], yy[0][idx]}, {1.0, yy[0][idx], 1.0}}
#define RESCALING_VECTOR {1.0, 1.0 / yy[0][idx], 1.0}

// 0 < rho(x1) < rhomax
double y1_of_t_x(const double t, const double x1, const double x2, const double x3)
{
  return x1;
}

// 0 < ph(x2) < 2 * pi
double y2_of_t_x(const double t, const double x1, const double x2, const double x3)
{
  return x2;
}

// zmin < z(x3) < zmax
double y3_of_t_x(const double t, const double x1, const double x2, const double x3)
{
  return x3;
}

void Apply_out_sym_bcs(const int gf, const double t, double **xx, double **yy, double **func)
{
  // Vector and tensor parity conditions.
  int parity_axis = 1;

#if defined EVOLVE_BSSN
  if(gf == VET1 || gf == LAMB1 || gf == BET1 || gf == H12 || gf == A12 || gf == H13 || gf == A13)
    { 
      // Vector component rho or tensor components rho-ph or rho-z
      parity_axis = -1;
    }
#endif


  // Exact outer rho boundary conditions
  for(int k = 0; k < Npts[2]; k++)
    {
      for(int j = 0; j < Npts[1]; j++)
	{
	  for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++)
	    {
	      double soln[2];
	      Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // Use exact outer boundary condition
	      func[gf][GFIDX(i, j, k)] = soln[gf]; // Returns u if gf == 0, and v if gf == 1
	    }
	}
    }

  // Exact outer -z boundary conditions
  for(int k = 0; k < NGHOSTS; k++)
    {
      for(int j = 0; j < Npts[1]; j++)
	{
	  for(int i = 0; i < Npts[0]; i++)
	    {
	      double soln[2];
	      Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // Use exact outer boundary condition
	      func[gf][GFIDX(i, j, k)] = soln[gf]; // Returns u if gf == 0, and v if gf == 1
	    }
	}
    }

  // Exact outer +z boundary conditions
  for(int k = Npts[2] - NGHOSTS; k < Npts[2]; k++)
    {
      for(int j = 0; j < Npts[1]; j++)
	{
	  for(int i = 0; i < Npts[0]; i++)
	    {
	      double soln[2];
	      Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // Use exact outer boundary condition
	      func[gf][GFIDX(i, j, k)] = soln[gf]; // Returns u if gf == 0, and v if gf == 1
	    }
	}
    }

  // Apply rho => -rho parity condition, near rho = 0
  for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++)
    {
      for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
	{
	  const int jp = NGHOSTS + (j + Npts[1] / 2 - 2 * NGHOSTS) % (Npts[1] - 2 * NGHOSTS); // ph -> ph + pi

	  for(int i = 0; i < NGHOSTS; i++)
	    {
	      const int im = 2 * NGHOSTS - 1 - i;

	      // f(-eps, ph, z) = f(+eps, ph + pi, z)
	      func[gf][GFIDX(i, j, k)] = parity_axis * func[gf][GFIDX(im, jp, k)];
	    }
	}
    }

  // Apply periodic boundary condition in phi direction
  for(int k = 0; k < Npts[2]; k++)
    {
      for(int j = 0; j < NGHOSTS; j++)
	{
	  const int jm = 2 * NGHOSTS - 1 - j;
	  const int jp = Npts[1] - 2 * NGHOSTS + j;
	  const int jg = Npts[1] - 1 - j;

	  for(int i = 0; i < Npts[0]; i++)
	    {
	      // f(rho, -eps, z) = f(rho, 2 * pi - eps, z)
	      func[gf][GFIDX(i, j, k)] = func[gf][GFIDX(i, jp, k)];

	      // f(rho, 2 * pi + eps, z) = f(rho, +eps, z)
	      func[gf][GFIDX(i, jg, k)] = func[gf][GFIDX(i, jm, k)];
	    }
	}
    }

  return;
}

double Find_Time_Step(double **yy, double y_min[3], double y_max[3])
{
  const int idx = GFIDX(NGHOSTS, NGHOSTS, NGHOSTS);

  y_min[0] = yy[0][idx];
  y_min[1] = yy[1][idx];
  y_min[2] = yy[2][idx];
  y_max[0] = yy[0][GFIDX(Npts[0] - NGHOSTS - 1, NGHOSTS, NGHOSTS)];
  y_max[1] = yy[1][GFIDX(NGHOSTS, Npts[1] - NGHOSTS - 1, NGHOSTS)];
  y_max[2] = yy[2][GFIDX(NGHOSTS, NGHOSTS, Npts[2] - NGHOSTS - 1)];
  
  const double drho = fabs(yy[0][idx] - yy[0][GFIDX(NGHOSTS + 1, NGHOSTS, NGHOSTS)]);
  const double dph = fabs(yy[1][idx] - yy[1][GFIDX(NGHOSTS, NGHOSTS + 1, NGHOSTS)]);
  const double dz = fabs(yy[2][idx] - yy[2][GFIDX(NGHOSTS, NGHOSTS, NGHOSTS + 1)]);

  // dt = min(drho, rho * dph, dz)
  double dt = (drho < y_min[0] * dph) ? drho : y_min[0] * dph;
  dt = (dz < dt) ? dz : dt;
  
  return dt;
}
