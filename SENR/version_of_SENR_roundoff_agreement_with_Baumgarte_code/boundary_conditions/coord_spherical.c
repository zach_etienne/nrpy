// Spherical coordinates (r, th, ph) = (yy[0], yy[1], yy[2])
#define RMAX 12.8
const double del[DIM] = {RMAX / Nx1, M_PI / Nx2, 2.0 * M_PI / Nx3};
const double slope[DIM] = {RMAX, M_PI, 2.0 * M_PI};
const double intercept[DIM] = {0.0, 0.0, 0.0};

#define REFERENCE_METRIC {{1.0, 0.0, 0.0}, {0.0, yy[0][idx] * yy[0][idx], 0.0}, {0.0, 0.0, yy[0][idx] * yy[0][idx] * sin(yy[1][idx]) * sin(yy[1][idx])}}
#define RESCALING_MATRIX {{1.0, yy[0][idx], yy[0][idx] * sin(yy[1][idx])}, {yy[0][idx], yy[0][idx] * yy[0][idx], yy[0][idx] * yy[0][idx] * sin(yy[1][idx])}, {yy[0][idx] * sin(yy[1][idx]), yy[0][idx] * yy[0][idx] * sin(yy[1][idx]), yy[0][idx] * yy[0][idx] * sin(yy[1][idx]) * sin(yy[1][idx])}}
#define RESCALING_VECTOR {1.0, 1.0 / yy[0][idx], 1.0 / (yy[0][idx] * sin(yy[1][idx]))}

// 0 < r(x1) < rmax
double y1_of_t_x(const double t, const double x1, const double x2, const double x3) { return x1; }

// 0 < th(x2) < pi
double y2_of_t_x(const double t, const double x1, const double x2, const double x3) { return x2; }

// 0 < ph(x3) < 2 * pi
double y3_of_t_x(const double t, const double x1, const double x2, const double x3) { return x3; }

void Apply_out_sym_bcs(const int gf, const double t, double **xx, double **yy, double **func)
{
  // Vector and tensor parity conditions. See Table 1 in Baumgarte et al (2012) arXiv:1211.6632.
  int parity_center = 1;
  int parity_axis = 1;
#if defined EVOLVE_BSSN
  if(gf == VET1 || gf == LAMB1 || gf == BET1 || gf == H23 || gf == A23)
    {
      // Vector component r or tensor component th-ph
      parity_center = -1;
    }
  else if(gf == VET2 || gf == LAMB2 || gf == BET2 || gf == H13 || gf == A13)
    {
      // Vector component th or tensor component r-ph
      parity_axis = -1;
    }
  else if(gf == VET3 || gf == LAMB3 || gf == BET3 || gf == H12 || gf == A12)
    {
      // Vector component ph or tensor component r-th
      parity_center = -1;
      parity_axis = -1;
    }
#endif

  // Apply r => -r parity condition, near r = 0
  for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++)
    {
      const int kp = NGHOSTS + (k + Npts[2] / 2 - 2 * NGHOSTS) % (Npts[2] - 2 * NGHOSTS); // ph -> ph + pi

      for(int j = NGHOSTS; j < Npts[1] - NGHOSTS; j++)
	{
	  const int jg = Npts[1] - 1 - j;

	  for(int i = 0; i < NGHOSTS; i++)
	    {
	      const int im = 2 * NGHOSTS - 1 - i;

	      // f(-eps, th, ph) = f(+eps, pi - th, ph + pi)
	      func[gf][GFIDX(i, j, k)] = parity_center * func[gf][GFIDX(im, jg, kp)];
	    }
	}
    }

  // Apply periodic BC in theta direction
  for(int j = 0; j < NGHOSTS; j++)
    {
      const int jm = 2 * NGHOSTS - 1 - j;
      const int jp = Npts[1] - 2 * NGHOSTS + j;
      const int jg = Npts[1] - 1 - j;

      for(int k = NGHOSTS; k < Npts[2] - NGHOSTS; k++)
	{
	  const int kp = NGHOSTS + (k + Npts[2] / 2 - 2 * NGHOSTS) % (Npts[2] - 2 * NGHOSTS); // ph -> ph + pi
	      
	  for(int i = 0; i < Npts[0]; i++)
	    {
	      // f(r, -eps, ph) = f(r, +eps, ph + pi)
	      func[gf][GFIDX(i, j, k)] = parity_axis * func[gf][GFIDX(i, jm, kp)];

	      // f(r, pi + eps, ph) = f(r, pi - eps, ph + pi)
	      func[gf][GFIDX(i, jg, k)] = parity_axis * func[gf][GFIDX(i, jp, kp)];
	    }
	}
    }

  // Apply periodic BC in phi direction
  for(int k = 0; k < NGHOSTS; k++)
    {
      const int km = 2 * NGHOSTS - 1 - k;
      const int kp = Npts[2] - 2 * NGHOSTS + k;
      const int kg = Npts[2] - 1 - k;

      for(int j = 0; j < Npts[1]; j++)
	{
	  for(int i = 0; i < Npts[0]; i++)
	    {
	      // f(r, th, -eps) = f(r, th, 2 * pi - eps)
	      func[gf][GFIDX(i, j, k)] = func[gf][GFIDX(i, j, kp)];

	      // f(r, th, 2 * pi + eps) = f(r, th, +eps)
	      func[gf][GFIDX(i, j, kg)] = func[gf][GFIDX(i, j, km)];
	    }
	}
    }

  for(int k = 0; k < Npts[2]; k++)
    {
      for(int j = 0; j < Npts[1]; j++)
	{
	  for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++)
	    {
#if defined OUTER_BOUNDARY_EXACT
              // Exact outer boundary conditions
	      double soln[2];
	      Exact_soln(t, xx[0][i], xx[1][j], xx[2][k], soln); // Use exact outer boundary condition
	      func[gf][GFIDX(i, j, k)] = soln[gf];
#elif defined OUTER_BOUNDARY_LIN_EXTRAP
              // Linear extrapolation outer boundary conditions
	      func[gf][GFIDX(i, j, k)] = 2.0 * func[gf][GFIDX(i - 1, j, k)] - func[gf][GFIDX(i - 2, j, k)];
#elif defined OUTER_BOUNDARY_QUAD_EXTRAP
              // Linear extrapolation outer boundary conditions
	      func[gf][GFIDX(i, j, k)] = 3.0 * func[gf][GFIDX(i - 1, j, k)] - 3.0 * func[gf][GFIDX(i - 2, j, k)] + func[gf][GFIDX(i - 3, j, k)];
#endif
	    }
	}
    }

  return;
}

// Sommerfeld outgoing wave condition, copied from the Baumgarte et al code fill_outerboundary() in gridfunction.h
void Sommerfeld_Outer_BC(const int gf, double **xx, double **yy, double **func_old, double **func)
{
  const double background = (gf == ALPHA) ? 1.0 : 0.0;
  //const double wave_speed = (gf == ALPHA) ? sqrt(2.0) : 1.0;
  const double wave_speed = 1.0;

  int fall_off = 1;

  if(gf == VET1 || gf == VET2 || gf == VET3 || gf == LAMB1 || gf == LAMB2 || gf == LAMB3 || gf == BET1 || gf == BET2 || gf == BET3)
    {
      fall_off = 2;
    }

  for(int k = 0; k < Npts[2]; k++)
    {
      for(int j = 0; j < Npts[1]; j++)
	{
	  for(int i = Npts[0] - NGHOSTS; i < Npts[0]; i++)
	    {
	      const double Delta_r = fabs(yy[0][GFIDX(i, j, k)] - yy[0][GFIDX(i - 1, j, k)]);
	      const double Courant = wave_speed * dt / Delta_r;
	      const double r0 = yy[0][GFIDX(i - 2, j, k)];
	      const double r1 = yy[0][GFIDX(i - 1, j, k)];
	      const double r2 = yy[0][GFIDX(i, j, k)];
	      const double r_last = Courant * r1 + (1.0 - Courant) * r2;

	      double factor = r_last / r2;
	      
	      if(fall_off == 2)
		{
		  factor *= factor;
		}
	      /*else if(fall_off == 3)
		{
		  factor *= factor * factor;
		  }*/

	      const double P0 = func_old[gf][GFIDX(i - 2, j, k)];
	      const double P1 = func_old[gf][GFIDX(i - 1, j, k)];
	      const double P2 = func_old[gf][GFIDX(i, j, k)];
	      const double P01 = ((r_last - r1) * P0 + (r0 - r_last) * P1) / (r0 - r1);
	      const double P12 = ((r_last - r2) * P1 + (r1 - r_last) * P2) / (r1 - r2);
	      const double P012 = ((r_last - r2) * P01 + (r0 - r_last) * P12) / (r0 - r2);
	      func[gf][GFIDX(i, j, k)] = factor * (P012 - background) + background;
	    }
	}
    }

  return;
}

double Find_Time_Step(double **yy, double y_min[3], double y_max[3])
{
  const int idx = GFIDX(NGHOSTS, NGHOSTS, NGHOSTS);

  y_min[0] = yy[0][idx];
  y_min[1] = yy[1][idx];
  y_min[2] = yy[2][idx];
  y_max[0] = yy[0][GFIDX(Npts[0] - NGHOSTS - 1, NGHOSTS, NGHOSTS)];
  y_max[1] = yy[1][GFIDX(NGHOSTS, Npts[1] - NGHOSTS - 1, NGHOSTS)];
  y_max[2] = yy[2][GFIDX(NGHOSTS, NGHOSTS, Npts[2] - NGHOSTS - 1)];
  
  const double dr = fabs(yy[0][idx] - yy[0][GFIDX(NGHOSTS + 1, NGHOSTS, NGHOSTS)]);
  const double dth = fabs(yy[1][idx] - yy[1][GFIDX(NGHOSTS, NGHOSTS + 1, NGHOSTS)]);
  const double dph = fabs(yy[2][idx] - yy[2][GFIDX(NGHOSTS, NGHOSTS, NGHOSTS + 1)]);

  //double dt = (dr < 0.5 * y_min[0] * dth) ? dr : 0.5 * y_min[0] * dth;
  //dt = (0.25 * y_min[0] * y_min[1] * dph < dt) ? 0.25 * y_min[0] * y_min[1] * dph : dt;
  
  double dt = (dr < y_min[0] * dth) ? dr : y_min[0] * dth;
  dt = (y_min[0] * sin(0.5 * dth) * dph < dt) ? y_min[0] * sin(0.5 * dth) * dph : dt;

  return dt;
}
