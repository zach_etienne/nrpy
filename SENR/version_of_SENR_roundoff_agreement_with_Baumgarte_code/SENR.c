#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
//#include <sys/time.h>
#include "SENR.h"

#if defined EVOLVE_SCALAR_WAVE
#include "Scalar_Wave__grid_and_gridfunction_setup.h"
#elif defined EVOLVE_BSSN
#include "BSSN__grid_and_gridfunction_setup.h"
#endif

#include "grid_setup.h"
#include "loop_setup.h"
#include "PIRK2.c"

// Include coordinate-specific functions
// Set curvilinear coordinates y1, y2, and y3
// Set boundary conditions
// Set CFL-satisfying timestep dt
#if defined COORD_CARTESIAN
#include "boundary_conditions/coord_cartesian.c"
#elif defined COORD_CYLINDRICAL
#include "boundary_conditions/coord_cylindrical.c"
#elif defined COORD_SPHERICAL
#include "boundary_conditions/coord_spherical.c"
#elif defined COORD_SYMTP
#include "boundary_conditions/coord_symtp.c"
#endif

// Set PIRK2 L1, L2, and L3 operators
// Set initial data
#if defined EVOLVE_SCALAR_WAVE
#include "evolution_equations/Scalar_Wave_Operators.c"
#elif defined EVOLVE_BSSN
#include "metric_inverse_and_determinant.h"
#include "evolution_equations/BSSN-L1.c"
#include "evolution_equations/BSSN-ExplicitLambda.c"
#include "evolution_equations/BSSN-L2part1.c"
#include "evolution_equations/BSSN-L3part1.c"
#include "evolution_equations/BSSN-L2part2.c"
#include "evolution_equations/BSSN-L3part2.c"
#include "evolution_equations/BSSN-L2part3.c"
#include "evolution_equations/BSSN-L3part3.c"
#include "diagnostics/Hamiltonian_Constraint.c"
#endif

#include "initial_data/Set_Initial_Data.c"
#include "diagnostics/Grid_Function_Reduction.c"
#include "diagnostics/Data_File_Output.c"

int main(int argc, char *argv[])
{
  // Total number of grid points, including ghost zones
  const int Ntot = Npts[0] * Npts[1] * Npts[2];

  // Allocate grid function storage

  // Evolved fields
  ALLOCATE_2D_GENERIC(double, gfs_n,   NUM_GFS, Ntot);
  ALLOCATE_2D_GENERIC(double, gfs_1,   NUM_GFS, Ntot);
  ALLOCATE_2D_GENERIC(double, gfs_np1, NUM_GFS, Ntot);

  // PIRK2 operators
  ALLOCATE_2D_GENERIC(double, gfs_L1_n,   NUM_GFS, Ntot);
  ALLOCATE_2D_GENERIC(double, gfs_L1_1,   NUM_GFS, Ntot);
  ALLOCATE_2D_GENERIC(double, gfs_L2_n,   NUM_GFS, Ntot);
  ALLOCATE_2D_GENERIC(double, gfs_L2_1,   NUM_GFS, Ntot);
  ALLOCATE_2D_GENERIC(double, gfs_L2_np1, NUM_GFS, Ntot);
  ALLOCATE_2D_GENERIC(double, gfs_L3_n,   NUM_GFS, Ntot);
  ALLOCATE_2D_GENERIC(double, gfs_L3_1,   NUM_GFS, Ntot);

  // Auxiliary grid functions
  ALLOCATE_2D_GENERIC(double, gfs_aux, NUM_AUX_GFS, Ntot);

  // Curvilinear coordinates
  ALLOCATE_2D_GENERIC(double, yy, DIM, Ntot); // (y1, y2, y3)

  // Uniform coordinates
  double *xx[DIM];

  for(int gf = 0; gf < DIM; gf++)
    {
      xx[gf] = (double *)malloc(sizeof(double) * Npts[gf]);
    }

  // Poison the grid functions with NaNs
#if defined POISON_GFS
  LOOP_GZFILL(i, j, k)
    {
      const int idx = GFIDX(i, j, k);

      for(int gf = 0; gf < NUM_GFS; gf++)
	{
	  gfs_n[gf][idx] = 1.0 / 0.0;
	  //gfs_1[gf][idx] = 1.0 / 0.0;
	  //gfs_np1[gf][idx] = 1.0 / 0.0;
	}
    }
#endif

  // Initialize static (uniform) cell-centered coordinates on the chosen coordinate domain
  for(int gf = 0; gf < DIM; gf++)
    {
      for(int i = 0; i < Npts[gf]; i++)
	{
	  xx[gf][i] = intercept[gf] + del[gf] * ((double)i - (double)NGHOSTS + 0.5); 
	}
    }

  // Static coordinate domains set by the grid resolution
  const double x_min[3] = {xx[0][NGHOSTS], xx[1][NGHOSTS], xx[2][NGHOSTS]};
  const double x_max[3] = {xx[0][Npts[0] - NGHOSTS - 1], xx[1][Npts[1] - NGHOSTS - 1], xx[2][Npts[2] - NGHOSTS - 1]};

  // Store coordinate range
  LOOP_GZFILL(i, j, k)
    {
      const int idx = GFIDX(i, j, k);

      yy[0][idx] = y1_of_t_x(0.0, xx[0][i], xx[1][j], xx[2][k]);
      yy[1][idx] = y2_of_t_x(0.0, xx[0][i], xx[1][j], xx[2][k]);
      yy[2][idx] = y3_of_t_x(0.0, xx[0][i], xx[1][j], xx[2][k]);
    }

  // Physical coordinate bounds
  double y_min[DIM], y_max[DIM];

  // Time step, satisfying the CFL condition
  dt = CFL * Find_Time_Step(yy, y_min, y_max);

  // Display grid info
  for(int gf = 0; gf < DIM; gf++)
    {
      printf("x%dmin = %f, x%dmax = %f, Dx%d = %.2e\n", gf, x_min[gf], gf, x_max[gf], gf, del[gf]);
      //printf("x%dmin = %f, x%dmax = %f, Dx%d = %.15e\n", gf, x_min[gf], gf, x_max[gf], gf, del[gf]);
    }

  for(int gf = 0; gf < DIM; gf++)
    {
      printf("y%dmin = %f, y%dmax = %f\n", gf, y_min[gf], gf, y_max[gf]);
    }

  // Print time step size and number of frames, used by the plotting script
  printf("frame dt = %e, final frame index = %i\n", dt * FRAME_JUMP, N_FRAMES);

  // Set initial data
  Set_Initial_Data(xx, yy, gfs_n, gfs_aux);

  // Fill grid functions with random initial data (except vetU = 0 and betU = 0)
  Set_Random_Initial_Data(yy, gfs_n, gfs_aux);

#if defined EVOLVE_BSSN
  // Evaluate detg
  BSSN_Det(gfs_n);

  // Store initial detg
  LOOP_GZFILL(i, j, k)
    {
      const int idx = GFIDX(i, j, k);
      gfs_aux[INITDETG][idx] = gfs_n[DETG][idx];
    }
#endif

  // Rescale gammabarDD to "unit" determinant
  Rescale_Metric_Det(yy, gfs_n, gfs_aux);

  // Enforce trace-free condition on AbarDD
  Remove_Trace(yy, gfs_n);

  // Boundary conditions on the initial data
  for(int gf = 0; gf < NUM_GFS; gf++)
    {
      Apply_out_sym_bcs(gf, 0.0, xx, yy, gfs_n);
    }

#if defined EVOLVE_BSSN
  // Useful for comparing against Thomas' code.
  if(1==0) {
  //  LOOP_GZFILL(ii, jj, kk) {
  for(int ii=1;ii<Npts[0]-NGHOSTS;ii++) for(int jj=1;jj<Npts[1]-1;jj++)  for(int kk=1;kk<Npts[2]-1;kk++) {
        const int idx = GFIDX(ii, jj, kk);
        //printf("%d %d %d | %e | %.15e ggg\n",ii,jj,kk,xx[0][ii],gfs_n[PHI][idx]);
        printf("lam1 %d %d %d | %e | %.15e ggg\n",ii-1,jj-1,kk-1,xx[0][ii],gfs_n[LAMB1][idx]);
        printf("lam2 %d %d %d | %e | %.15e ggg\n",ii-1,jj-1,kk-1,xx[0][ii],gfs_n[LAMB2][idx]);
        printf("lam3 %d %d %d | %e | %.15e ggg\n",ii-1,jj-1,kk-1,xx[0][ii],gfs_n[LAMB3][idx]);
      } exit(1);
  }
#endif

  // Output file names
  char out0Dname[100], out1Dname[100], out2Dname[100], out3Dname[100];
  sprintf(out0Dname, "output/out_0D_N%d_FD%d_CFL%.4f.txt", Nx1, ORDER, CFL);
  sprintf(out1Dname, "output/out_1D_N%d_FD%d_CFL%.4f.txt", Nx1, ORDER, CFL);
  sprintf(out2Dname, "output/out_2D_N%d_FD%d_CFL%.4f.txt", Nx1, ORDER, CFL);
  sprintf(out3Dname, "output/out_3D_N%d_FD%d_CFL%.4f.txt", Nx1, ORDER, CFL);

  // Total number of PIRK iterations
  const int N_iters = FRAME_JUMP * N_FRAMES;

  // Output frame index
  int frame_ind = -1;

  printf("ID %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e %.15e\n", gfs_n[H11][GFIDX(17, 17, 17)], gfs_n[H12][GFIDX(17, 17, 17)], gfs_n[H13][GFIDX(17, 17, 17)], gfs_n[H22][GFIDX(17, 17, 17)], gfs_n[H23][GFIDX(17, 17, 17)], gfs_n[H33][GFIDX(17, 17, 17)], gfs_n[PHI][GFIDX(17, 17, 17)], gfs_n[ALPHA][GFIDX(17, 17, 17)], gfs_n[TRK][GFIDX(17, 17, 17)], gfs_n[A11][GFIDX(17, 17, 17)], gfs_n[A12][GFIDX(17, 17, 17)], gfs_n[A13][GFIDX(17, 17, 17)], gfs_n[A22][GFIDX(17, 17, 17)], gfs_n[A23][GFIDX(17, 17, 17)], gfs_n[A33][GFIDX(17, 17, 17)], gfs_n[LAMB1][GFIDX(17, 17, 17)], gfs_n[LAMB2][GFIDX(17, 17, 17)], gfs_n[LAMB3][GFIDX(17, 17, 17)], gfs_n[VET1][GFIDX(17, 17, 17)], gfs_n[VET2][GFIDX(17, 17, 17)], gfs_n[VET3][GFIDX(17, 17, 17)], gfs_n[BET1][GFIDX(17, 17, 17)], gfs_n[BET2][GFIDX(17, 17, 17)], gfs_n[BET3][GFIDX(17, 17, 17)]);

  // Iterate through time
  for(int n = 0; n <= N_iters; n++)
    {
      // Current time
      const double t = n * dt;

      ////////////////////////////////////////////////////////////////////////

      // PIRK2 integration
#if defined EVOLVE_SCALAR_WAVE
#include "evolution_equations/Scalar_Wave_PIRK_Steps.c"
#elif defined EVOLVE_BSSN
#include "evolution_equations/BSSN_PIRK_Steps.c"
#endif

      ////////////////////////////////////////////////////////////////////////

      // Progress indicator printing to stdout
      printf("Iteration: %d | Time: %.4f ( dt= %.6f ) | %.2f%% Complete | (dumping every %d iters)\r", n, n*dt, dt, 100.0 * (double)n / (double)N_iters, FRAME_JUMP); // \r is carriage return, move cursor to the beginning of the line
      //fflush(stdout); // Flush the stdout buffer

      // Write data to files
      if(n % FRAME_JUMP == 0)
	{
	  frame_ind++; // frame index, zero inclusive

          // Open file output
          FILE *out0D, *out1D, *out2D, *out3D;
          out0D = fopen(out0Dname, "a");
          out1D = fopen(out1Dname, "a");
          out2D = fopen(out2Dname, "a");
          out3D = fopen(out3Dname, "a");

	  // Output 0D, 1D, 2D, and 3D data to files
	  Data_File_Output(out0D, out1D, out2D, out3D, frame_ind, t, xx, yy, gfs_n, gfs_aux);

          // Close file IO
          fclose(out0D);
          fclose(out1D);
          fclose(out2D);
          fclose(out3D);
	}

      ////////////////////////////////////////////////////////////////////////

      // Update grid functions for next time step
      LOOP_GZFILL(i, j, k)
	{
	  const int idx = GFIDX(i, j, k);
	  
	  // Update time step
	  for(int gf = 0; gf < NUM_GFS; gf++)
	    {
	      if(!isfinite(gfs_n[gf][idx]))
		{
		  printf("BAD n = %d, t = %e, (x1,x2,x3)=(%e , %e , %e) gfs_n[%s%s][(%d,%d,%d)] = %e\n", n, t, xx[0][i], xx[1][j], xx[2][k],  gf_name[gf*2],gf_name[gf*2+1] , i,j,k, gfs_n[gf][i,j,k]);
		  exit(1);
		}

	      // u^{n} -> u^{n+1}
	      gfs_n[gf][idx] = gfs_np1[gf][idx];
	    }
	}
    } // END for(n <= N_iters)

  // Free allocated memory
  FREE_2D_GENERIC(double, gfs_n,   NUM_GFS, Ntot);
  FREE_2D_GENERIC(double, gfs_1,   NUM_GFS, Ntot);
  FREE_2D_GENERIC(double, gfs_np1, NUM_GFS, Ntot);

  FREE_2D_GENERIC(double, gfs_L1_n,   NUM_GFS, Ntot);
  FREE_2D_GENERIC(double, gfs_L1_1,   NUM_GFS, Ntot);
  FREE_2D_GENERIC(double, gfs_L2_n,   NUM_GFS, Ntot);
  FREE_2D_GENERIC(double, gfs_L2_1,   NUM_GFS, Ntot);
  FREE_2D_GENERIC(double, gfs_L2_np1, NUM_GFS, Ntot);
  FREE_2D_GENERIC(double, gfs_L3_n,   NUM_GFS, Ntot);
  FREE_2D_GENERIC(double, gfs_L3_1,   NUM_GFS, Ntot);

  FREE_2D_GENERIC(double, gfs_aux, NUM_AUX_GFS, Ntot);

  FREE_2D_GENERIC(double, yy, DIM, Ntot);

  for(int gf = 0; gf < DIM; gf++)
    {
      free(xx[gf]);
    }

  return 0;
}
