void Hamiltonian_Constraint(double **xx, double **yy, double **in_gfs, double **gfs_aux)
{
  LOOP_NOGZFILL(ii, jj, kk)
    {
      const int idx = GFIDX(ii, jj, kk);

      const double x1 = xx[0][ii];
      const double x2 = xx[1][jj];
      const double x3 = xx[2][kk];

      // Declare array that stores the gridfunction data on the stencils
      double locGF[2*NGHOSTS+1][2*NGHOSTS+1][2*NGHOSTS+1]; // About 10KB for NGHOSTS=5.

      // Declare local variable arrays
      double hDD[DIM][DIM], hDDdD[DIM][DIM][DIM], hDDdDD[DIM][DIM][DIM][DIM];
      double phi, phidD[DIM], phidDD[DIM][DIM];
      double aDD[DIM][DIM];
      double lambdaU[DIM], lambdaUdD[DIM][DIM];
      double trK;
      double detg;
  
      /*
	DVGENSTART Hamiltonian_Constraint__gfs_and_derivs.h
	hDD      	ZERO	FIRST   SECOND
	phi		ZERO    FIRST   SECOND
	aDD		ZERO
	trK		ZERO
	lambdaU         ZERO    FIRST
	detg            ZERO
	DVGENEND
      */

#include "../autogenFDcode/Hamiltonian_Constraint__gfs_and_derivs.h"

      //double gammabarDD[DIM][DIM];

      //BSSN_gammabarDD(ii, jj, kk, in_gfs, yy, gammabarDD);

      double gammabarUU[DIM][DIM];
      //double oneoverdetgammabar = 1.0 / detgammabar;
      //Invertgammabar(gammabarUU, gammabarDD, oneoverdetgammabar);

      Compute_Inverse_Metric(ii, jj, kk, yy, in_gfs, gammabarUU);

      double Ham[20];
  
#include "../autogenMathcode/Hamiltonian.txt-parsed.h"

      gfs_aux[HAM][idx] = Ham[0];

      //if(ii == 17 && jj == 17 && kk == 17)
      /*if(ii == NGHOSTS && jj == NGHOSTS && kk == NGHOSTS)
	{
	  printf("traceRbar = %.15e\n", Ham[2]);
	  printf("DphiDphi = %.15e\n", Ham[3]);
	  printf("D2_phi = %.15e\n", Ham[4]);
	  printf("trK = %.15e\n", trK);
	  printf("psim4 = %.15e\n", Ham[1]);
	  printf("A2 = %.15e\n", Ham[5]);
	  printf("gup %.15e %.15e %.15e %.15e %.15e %.15e\n", gammabarUU[0][0], gammabarUU[0][1], gammabarUU[0][2], gammabarUU[1][1], gammabarUU[1][2], gammabarUU[2][2]);
	  }*/
    }

  return;
}
