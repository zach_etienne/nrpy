#include "grid_and_gridfunction_setup.h"

double interpolate_1d(double x_i[INTERP_ORDER], double f_i[INTERP_ORDER], const double x, int bdry)
{
  // Check for extrapolation
  // We assume the independent variables are ordered: x_i[0] < x_i[1] < ... < x_i[INTERP_ORDER-1]
  // Check if x is outside the stencil bounds (resulting in extrapolation)
  // Check if x is farther from the stencil than the stencil's internal dx
  if((fabs(x - x_i[0]) > fabs(x_i[1] - x_i[0]) && x < x_i[0]) ||
     (fabs(x - x_i[INTERP_ORDER-1]) > fabs(x_i[INTERP_ORDER-1] - x_i[INTERP_ORDER-2]) && x > x_i[INTERP_ORDER-1]))
    {
      printf("WARNING: Extrapolating in interpolate_1d() on boundary d%d; %e > %e (x = %e ; x_i[0] = %e) or %e > %e\n", bdry, fabs(x - x_i[0]), fabs(x_i[1] - x_i[0]), x, x_i[0], x, x_i[INTERP_ORDER - 1]);

      for(int i = 0; i < INTERP_ORDER; i++)
	{
	  printf("x_i[%d] = %f\n", i, x_i[i]);
	}

      printf("x = %f\n", x);
    }

  // https://en.wikipedia.org/w/index.php?title=Lagrange_polynomial&oldid=716780760
  double f = 0;
  
  for(int j = 0; j < INTERP_ORDER; j++)
    {
      double l_j = 1.0;
      for(int m = 0; m < j; m++)
	{
	  l_j *= (x - x_i[m]) / (x_i[j] - x_i[m]);
	  /*printf("%e: %d %d %e %e\n",l_j, j,m,x_i[j],x_i[m]); }*/
	}

      for(int m = j + 1; m < INTERP_ORDER; m++)
	{
	  l_j *= (x - x_i[m]) / (x_i[j] - x_i[m]);
	}

      f += f_i[j] * l_j;
    }

  return f;
}

/* To do a 2D interpolation on an N*N grid to get point near center f(x,y)
   1) perform a set of N 1D interpolations along y-direction first, to obtain
   f(x[i],y), for i = [1,N]
   2) then do a single interpolation in x-direction to get f(x,y).
*/
double interpolate_2d(double x_i[INTERP_ORDER], double y_j[INTERP_ORDER], double f_ij[INTERP_ORDER][INTERP_ORDER], const double x, const double y, int bdry)
{
  // Check for extrapolation
  // We assume the independent variables are ordered: x_i[0] < x_i[1] < ... < x_i[INTERP_ORDER-1]
  /*if(x < x_i[0] || x > x_i[INTERP_ORDER-1] || y < y_j[0] || y > y_j[INTERP_ORDER-1])
    {
      printf("WARNING: Extrapolating in interpolate_2d()\n");
      }*/

  // f_ij[i][j] <- 2nd index is the y-component

  double f_i[INTERP_ORDER];

  for(int i = 0; i < INTERP_ORDER; i++)
    {
      // First interpolate to common y-coordinate. Will need to interpolate in x-direction next.
      f_i[i] = interpolate_1d(y_j, f_ij[i], y, bdry);
    }

  // Then interpolate output to common x-coordinate
  return interpolate_1d(x_i, f_i, x, bdry);
}

/* Similarly, to do a 3D interpolation on an N*N*N grid to get point near center f(x,y,z)
   1) perform a set of N 2D interpolations along x-direction first, to obtain
   f(x[i],y,z), for i = [1,N]
   2) then do a single interpolation in x-direction to get f(x,y,z).
*/
double interpolate_3d(double x_i[INTERP_ORDER], double y_j[INTERP_ORDER], double z_k[INTERP_ORDER], double f_ijk[INTERP_ORDER][INTERP_ORDER][INTERP_ORDER], const double x, const double y, const double z, int bdry)
{
  // Check for extrapolation
  // We assume the independent variables are ordered: x_i[0] < x_i[1] < ... < x_i[INTERP_ORDER-1]
  /*if(x < x_i[0] || x > x_i[INTERP_ORDER-1] || y < y_j[0] || y > y_j[INTERP_ORDER-1] || z < z_k[0] || z > z_k[INTERP_ORDER-1])
    {
      printf("WARNING: Extrapolating in interpolate_3d()\n");
      }*/

  // f_ijk[i][j][k] <- 2nd index is the y-component, 3rd index is z-component

  double f_i[INTERP_ORDER];
  
  for(int i = 0; i < INTERP_ORDER; i++)
    {
      // First interpolate to common (y,z)-coordinate
      f_i[i] = interpolate_2d(y_j, z_k, f_ijk[i], y, z, bdry);
    }

  // Then interpolate output to common x-coordinate
  return interpolate_1d(x_i, f_i, x, bdry);
}
