#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]) {
  /** BH PARAMETERS **/
  double a=0.9375,M=1.0;
  /** COORD PARAMETERS **/
  double r=8.13542074363992,th=1.21967714786427;
  /** DISK PARAMETERS **/
  // Hydrodynamic parameters
  double r_in=6.0,r_at_max_density=12.0,gamma=4.0/3.0,kappa=4.249572342033184e-03; //1.0e-3;
  // Magnetic field parameters
  double A_b=sqrt(5.85119/6.954);
  if(argc!=5) {
    printf("*** If you want other parameters, use as follows:\n*** ./FishboneMoncrief [a=BH spin parameter] [M=BH mass] [r] [theta]\n");
    printf("Using DEFAULT INPUT parameters of\n a = %e,\n M = %e, \n r = %e, \nth = %e\n",a,M,r,th);
  } else {
    a=strtod(argv[1],NULL); //0.935;
    M=strtod(argv[2],NULL); //1.0;

    r =strtod(argv[3],NULL); //1.0;
    th=strtod(argv[4],NULL); //2.0;

    printf("Using input parameters of\n a = %e,\n M = %e,\n r = %e,\nth = %e\n",a,M,r,th);
    //double x3=3.0;
  }

  // Following script generates below line of code.
  double alp,betaUr,betaUth,betaUph,grr,grth,grph,gthth,gthph,gphph,krr,krth,krph,kthth,kthph,kphph, sqrtgamma4DET;
  {
#include "NRPy_codegen/KerrSchild.h"
  }

  double LorentzFactor,uUr,uUth,uUph;
  {
#include "NRPy_codegen/FMdisk_Lorentz_uUs.h"
  }

  double hm1,rho0,Pressure0;
  {
#include "NRPy_codegen/FMdisk_hm1_rho_P.h"
  }

  double BtildeUr,BtildeUth,BtildeUph, smallbUt, smallbUr, smallbUth, smallbUph, smallb2, uKS4Ut,uKS4Ur,uKS4Uth,uKS4Uph, uKS4Dt,uKS4Dr,uKS4Dth,uKS4Dph,udotu,g4DD00,g4DD01,g4DD02,g4DD03,Bur,Buth,Buph;
  {
#include "NRPy_codegen/FMdisk_Btildes.h"
  }

  printf("alpha sqrt gamma: r = %.15e th = %.15e al sqrt gamma: %.15e\n",r,th,sqrtgamma4DET);
  printf("gphysDDs: r = %.15e th = %.15e || %.15e %.15e %.15e %.15e\n",r,th,g4DD00,g4DD01,g4DD02,g4DD03);
  printf("uKS4t: r = %.15e th = %.15e uKS4t: %.15e | %.15e %.15e %.15e %.15e\n",r,th,uKS4Ut,uKS4Dt,uKS4Dr,uKS4Dth,uKS4Dph);
  printf("uKS4t: r = %.15e th = %.15e uKS4t: %.15e | %.15e %.15e %.15e %.15e\n",r,th,uKS4Ut,uKS4Ut,uKS4Ur,uKS4Uth,uKS4Uph);
  printf("udotu: r = %.15e th = %.15e udotu: %.15e\n",r,th,udotu);
  printf("Bus: r = %.15e th = %.15e Bus: %.15e %.15e %.15e\n",r,th,Bur,Buth,Buph);

  printf("=====   OUTPUTS   =====\n");
  printf("alp: %.15e, betai: %.15e %.15e %.15e\n",alp,betaUr,betaUth,betaUph);
  printf("gij: %.15e %.15e %.15e %.15e %.15e %.15e\n",grr,grth,grph,gthth,gthph,gphph);
  printf("Kij: %.15e %.15e %.15e %.15e %.15e %.15e\n",krr,krth,krph,kthth,kthph,kphph);
  printf("hm1: %.15e\nrho: %.15e\nPressure: %.15e\n",hm1,rho0,Pressure0);
  printf("LorentzFactor: %.15e\nur: %.15e\nuth: %.15e\nuph: %.15e\n",LorentzFactor,uUr,uUth,uUph);
  printf("Btilder: %.15e\nBtildeth: %.15e\nBtildeph: %.15e\n",BtildeUr,BtildeUth,BtildeUph);
  printf("smallb: %.15e %.15e %.15e %.15e\n",smallbUt,smallbUr,smallbUth,smallbUph);

  printf("%.15e %.15e %.15e %.15e %.15e compare\n",r, rho0,Pressure0,smallb2,LorentzFactor);


  return 0;
}
