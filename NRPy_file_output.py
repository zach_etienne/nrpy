from sympy import sympify,cse,numbered_symbols,ccode,srepr
from finite_difference_stencil_generator import *
from NRPy_functions import read_sympy_expr__return_ccode_string, get_parameter_value
from parameters_compiletime import params_type, params_varname, params_value
from NRPy_SIMD import expr_convert_to_SIMD_funcs

def nrpy_ccode_file_output(filename,def_vars,vars,
                           SIMD="None", SIMD_const_varnme_array=[], SIMD_const_values_array=[]):
    PRECISION = get_parameter_value("PRECISION", params_varname, params_value)
    DATATYPE = PRECISION
    if SIMD=="256bit":
        DATATYPE = "__m256d"
    elif SIMD=="512bit":
        DATATYPE = "__m512d"

    len_def_vars = int(len(def_vars) / 2)
    already_set_defvar = [False for i in range(len_def_vars)]

    len_vars = int(len(vars) / 2)

    input_variable_array = [sympify(0) for i in range(len_def_vars+len_vars)]
    for i in range(len_def_vars):
        input_variable_array[i]              = def_vars[2 * i]
    for i in range(len_vars):
        input_variable_array[len_def_vars+i] = vars[2 * i]

    CSE_results = cse(input_variable_array,numbered_symbols("tmp"), order='canonical')

    # Store all variable names for output.
    output_all_variable_names     = ["dummyvarname" for i in range(len_def_vars+len_vars)]
    counter=0
    # First intermediate variable definitions
    for i in range(len_def_vars):
        output_all_variable_names[counter] = def_vars[2 * i + 1]
        counter+=1
    # Then output variable definitions
    for i in range(len_vars):
        output_all_variable_names[counter]     = vars[2 * i + 1]
        counter+=1

    list_of_all_vars_defined = ["dummyvarname" for i in range(len_def_vars + len_vars)]
    for i in range(len_def_vars):
        list_of_all_vars_defined[i] = output_all_variable_names[i]
    counter=0

    # CSE = common subexpression elimination.
    # C code output should take the following form:
    # tmp0 = [something depending on input values only]
    # tmp1 = [something depending on input values and tmp0 only]
    # tmp2 = [something depending on input values, tmp0, and tmp1 only]
    # ...
    # vars[0] = [something depending on input values & tmp0 through tmpN only]
    # vars[1] = [something depending on input values & tmp0 through tmpN, and maybe vars[0]]
    # ...
    # vars[N] = [something depending on input values & tmp0 through tmpN, and maybe vars[0] through vars[N-1]]
    # ####
    # The lines that start tmp... = we call PART1
    # The lines that start vars[...] = we call PART2
    with open(filename, "a") as output:
        line_of_code__var_output = ["dummyvarname" for i in range(len_def_vars + len_vars)]
        # Output vars[...] (PART2)
        for i,result in enumerate(CSE_results[1]):
            if SIMD=="None":
                line_of_code__var_output[i] = nrpy_ccode_parser(str(ccode(result,output_all_variable_names[i])))+"\n"
            elif SIMD=="256bit" or SIMD=="512bit":
                if i < len_def_vars:
                    line_of_code__var_output[i]  = str(output_all_variable_names[i])+" = "
                    line_of_code__var_output[i] += str(expr_convert_to_SIMD_funcs(result, SIMD_const_varnme_array,
                                                                                  SIMD_const_values_array,debug=True)) + ";\n"
                else:
                    if SIMD=="256bit":
                        line_of_code__var_output[i] = "_mm256_storeu_pd(&" + str(output_all_variable_names[i]) + ","
                    elif SIMD=="512bit":
                        line_of_code__var_output[i] = "_mm512_storeu_pd(&" + str(output_all_variable_names[i]) + ","
                    line_of_code__var_output[i] += str(expr_convert_to_SIMD_funcs(result,SIMD_const_varnme_array,
                                                                                  SIMD_const_values_array,debug=True))+");\n"

        # Output tmp variables (PART1)
        for helper in CSE_results[0]:
            line_of_code__tmpvar_output = "const " + DATATYPE + " "
            if SIMD=="None":
                line_of_code__tmpvar_output += nrpy_ccode_parser(str(ccode(helper[1],helper[0]))) + "\n"
            elif SIMD=="256bit" or SIMD=="512bit":
                line_of_code__tmpvar_output += str(helper[0])+" = "+\
                                               str(expr_convert_to_SIMD_funcs(helper[1],SIMD_const_varnme_array,
                                                                              SIMD_const_values_array,debug=True))+";\n"

            for i in range(len_def_vars):
                if output_all_variable_names[i] in line_of_code__tmpvar_output:
                    if not already_set_defvar[i]:
                        output.write("const "+DATATYPE+" "+line_of_code__var_output[i])
                        already_set_defvar[i] = True

            output.write(line_of_code__tmpvar_output)

        for i in range(len_def_vars):
            if not already_set_defvar[i]:
                output.write("const "+DATATYPE+" "+line_of_code__var_output[i])
                already_set_defvar[i] = True

        for i in range(len_vars):
            output.write(line_of_code__var_output[len_def_vars+i])
    return

def list_unset_variables(expression,protected_varnames_list, unset_varnames_list):
    strings = srepr(expression).replace("Symbol('", "\nvar ").replace("')", "\n").replace("',", "\n").splitlines()
    for i in range(len(strings)):
        if strings[i].startswith('var '):
            newvarname = strings[i].replace("var ", "").replace(" ", "")
            duplicate_or_tmp_or_protected = False
            if newvarname.startswith('tmp') or newvarname.endswith('tmp'):
                duplicate_or_tmp_or_protected = True
            for j in range(len(unset_varnames_list)):
                if unset_varnames_list[j] == newvarname:
                    duplicate_or_tmp_or_protected = True
                    break
            for j in range(len(protected_varnames_list)):
                if newvarname == protected_varnames_list[j]:
                    duplicate_or_tmp_or_protected = True
            if not duplicate_or_tmp_or_protected:
                unset_varnames_list.extend([newvarname])
    return

# MASTER FILE OUTPUT INTERFACE
def NRPy_file_output(filename, func_gf_define_alias,list_of_gfs, upwind_vector,
                     list_of_protected_variables, list_of_defs,list_of_expressions,
                     SIMD="None",SIMD_const_varnme_array=[], SIMD_const_values_array=[]):
    # File is output in four parts, corresponding to evaluating at a single point i,j,k:
    #   Step 1) Read precomputed quantities from memory, set to local variable
    #   Step 2) Read gridfunctions at all needed neighboring points from memory, compute finite difference derivatives if needed.
    #   Step 3) Apply upwinding operators if needed
    #   Step 4) Evaluate expressions and set output variables.

    # Step 0) Create file.
    with open(filename, "w") as output:
        pass

    # Step 0.5) If the list of expressions is an empty list, return empty file.
    if len(list_of_expressions)==0:
        if SIMD=="256bit" or SIMD=="512bit":
            with open(filename+"-SIMDconstants.h", "w") as output:
                pass
        return

    # Step 1) Read precomputed quantities from memory, set to local variable
    # [ unsupported at moment ]

    # Step 2) Read gridfunctions at all needed neighboring points from memory,
    #         compute finite difference derivatives where needed.

    # First construct a list of all variables that must be either read in from main memory
    #   (i.e., gridfunctions) OR computed from finite differences of gridfunctions.

    # Create a temp copy of "list_of_protected_variables" array so that it's not overwritten.
    list_of_protected_variables_tmp = []
    list_of_protected_variables_tmp += list_of_protected_variables
    # Then add all defined quantities to the list of "already_defined_noevol_variables_tmp"
    for i in range(len(list_of_defs)):
        # list of defs follows pattern: [expression, "name", expression, "name",...], so
        #   zeroth, second, and all even elements are expressions, and odd elements are variable names.
        # Store all variable names:
        if (i+1) % 2 == 0:
            list_of_protected_variables_tmp += [list_of_defs[i]]

    # All other quantities must be derivatives of gridfunctions.
    #   We store the list to "list_of_unset_variables".
    list_of_unset_variables = []
    for i in range(len(list_of_expressions)):
        if i % 2 == 0:
            list_unset_variables(list_of_expressions[i],list_of_protected_variables_tmp, list_of_unset_variables)
    for i in range(len(list_of_defs)):
        if i % 2 == 0:
            list_unset_variables(list_of_defs[i], list_of_protected_variables_tmp, list_of_unset_variables)

    # list_unset_variables() stores only unique unset variables, so no need to do anything but sort the result.
    list_of_unset_variables.sort()

    fd_codegen__process_list_of_gridfunction_derivs(filename, list_of_unset_variables, func_gf_define_alias,list_of_gfs,
                                                    stencl_1stderiv, coeffs_1stderiv,
                                                    stencl_1stderivup, coeffs_1stderivup,
                                                    stencl_1stderivdn, coeffs_1stderivdn,
                                                    stencl_2ndderiv, coeffs_2ndderiv,
                                                    SIMD,SIMD_const_varnme_array,SIMD_const_values_array)
    # Step 3) Apply upwinding operators if needed
    list_of_upwind_variables = []
    for i in range(len(list_of_unset_variables)):
        dummybasegf = ["dummy"]
        if base_gf_name_and_type(list_of_unset_variables[i],dummybasegf) == "Upwind":
            list_of_upwind_variables.extend([list_of_unset_variables[i]])
    superfast_uniq(list_of_upwind_variables)
    list_of_upwind_variables.sort()

    with open(filename, "a") as output:
        if SIMD == "256bit":
            output.write("const double ZEROd = 0.0; const __m256d ZEROvec = _mm256_set1_pd(ZEROd);\n")
            output.write("const double ONEd  = 1.0; const __m256d ONEvec  = _mm256_set1_pd(ONEd);\n")
        elif SIMD == "512bit":
            output.write("const double ZEROd = 0.0; const __m512d ZEROvec = _mm512_set1_pd(ZEROd);\n")
            output.write("const double ONEd  = 1.0; const __m512d ONEvec  = _mm512_set1_pd(ONEd);\n")
        if len(list_of_upwind_variables) > 0:
            for i in range(3):
                # Up/Down-winding works as follows:
                # 0) Define the comoving vector v^i as the upwind vector. Could be a fluid velocity for example, or
                #    in the case of solving Einstein's equations in the 3+1 decomposition, the *shift* vector
                #    (i.e., the vector governing the proper coordinates for the spatial points at each 3D slice).
                #    Store the upwind vector to upwind_vector[].
                # 1) The goal is to adjust advection terms v^i \partial_i A, where A can be any non-indexed or indexed
                #    gridfunction, so that \partial_i A is up or downwinded according to the motion of v^i. This
                #    up/downwinding scheme significantly reduces truncation error, as it forces the finite difference
                #    stencils to "look ahead" at the direction of motion .
                # 2) We compute both up- and down-winded finite difference derivatives of the gridfunction A.
                # 3) Looping over directions i=[0,1,2], if upwind_vector[i] > 0, then use the upwinded finite
                #    difference to compute \partial_i A. Otherwise use the downwinded finite difference to compute.

                # Input: upwind_vector[].
                # Output: upwind[i] for directions i=0,1,2. upwind[i] = 1 if v^i > 0, and upwind[i] = 0 otherwise.
                #         WARNING: this is written with the BSSN shift vector \beta^i in mind, which has the
                #         *opposite* behavior of a traditional velocity vector v^i.
                if SIMD=="None":
                    output.write("const "+PRECISION+" upwind" + str(i) + " = " + nrpy_ccode_parser(read_sympy_expr__return_ccode_string(upwind_vector[i])) + " > 0.0 ? 1.0 : 0.0;\n")
                elif SIMD=="256bit" or SIMD=="512bit":
                    SIMD_upwind_vector_string = str(expr_convert_to_SIMD_funcs(upwind_vector[i], SIMD_const_varnme_array,SIMD_const_values_array, debug=True))
                    # The _mm[256|512]_cmp_pd(a,b,OP) intrinsic compares __mm[256|512]d variables a and b using operation OP.

                    # In the case of 256-bit SIMD:
                    #     The result from this comparison is: result = (a OP b) ? NaN : 0.
                    #     Thus if OP is >, then: if a > b then the result is NaN, and if a <= b then the result is 0.
                    #     We want the result to be 1 if a>b and 0 otherwise, so we simply perform a logical AND operation
                    #     on the result, against the number 1, because AND(NaN,1)=1, and AND(0,1)=0,
                    #     where NaN=0xffffff... in double precision.

                    # In the case of 512-bit SIMD:
                    #     The result from this comparison is: result[i] = (a OP b) ? 1 : 0, stored in an 8-bit mask array.
                    #     Then if result==1 we set upwind = 0+1, and if result==0 we set upwind = 0
                    if SIMD=="256bit":
                        output.write("const __m256d upwind" + str(i) + " = _mm256_and_pd(_mm256_cmp_pd("+SIMD_upwind_vector_string+", ZEROvec, _CMP_GT_OQ),ONEvec);\n")
                    elif SIMD=="512bit":
                        # Pretty clunky way to force upwind = 1 if shift > 0 and upwind = 0 otherwise:
                        output.write("const __mmask8 upwindmask" + str(i) + " = _mm512_cmp_pd_mask("+SIMD_upwind_vector_string+", ZEROvec, _CMP_GT_OQ);\n")
                        output.write("const __m512d upwind" + str(i) + " = _mm512_mask_add_pd(ZEROvec,upwindmask"+str(i)+", ZEROvec,ONEvec);\n")

            for j in range(len(list_of_upwind_variables)):
                varnameup = list_of_upwind_variables[j]
                varnamedn = list_of_upwind_variables[j].replace("dupD", "ddnD")
                derivdirn1 = int(varnameup[-1:])  # The last character contains derivdirn1. E.g., 2 in vetUdD02
                # Output: Out = upwind*Aup + dnwind*Adn, where dnwind = 1-upwind
                #      -> Out = upwind*Aup + (1-upwind)*Adn <-- 4 floating-point operations
                #             = upwind*(Aup - Adn) + Adn    <-- 3 floating-point operations.
                if SIMD=="None":
                    output.write(varnameup + " = upwind" + str(derivdirn1) + " * ("
                                 + varnameup + " - " + varnamedn + ") + " + varnamedn + ";\n")
                elif SIMD=="256bit" or  SIMD=="512bit":
                    #     Out = upwind*(Aup - Adn) + Adn    <-- 3 floating-point operations.
                    #         = FMA(upwind,SubSIMD(Aup,Adn),Adn) <-- 2 floating-point operations!
                    output.write(varnameup + " = FusedMulAddSIMD(upwind" + str(derivdirn1) + " , SubSIMD("
                                 + varnameup + " , " + varnamedn + ") , " + varnamedn + ");\n")

    # Step 4) Evaluate expressions and set output variables.
    nrpy_ccode_file_output(filename, list_of_defs,list_of_expressions,
                           SIMD,SIMD_const_varnme_array, SIMD_const_values_array)

    # Step 5) Output SIMD code to define all the constant expressions. This should be #include'ed prior to the
    #         SIMD'ed loop.
    if SIMD=="256bit" or SIMD=="512bit":
        # Step 5a) Sort the list of definitions. Idea from:
        # https://stackoverflow.com/questions/9764298/is-it-possible-to-sort-two-listswhich-reference-each-other-in-the-exact-same-w
        SIMD_const_varnme_array, SIMD_const_values_array = \
            (list(t) for t in zip(*sorted(zip(SIMD_const_varnme_array, SIMD_const_values_array))))
        # Step 5b) Remove duplicates
        uniq_varnme_array = superfast_uniq(SIMD_const_varnme_array)
        uniq_values_array = superfast_uniq(SIMD_const_values_array)
        SIMD_const_varnme_array = uniq_varnme_array
        SIMD_const_values_array = uniq_values_array
        if len(SIMD_const_varnme_array) != len(SIMD_const_values_array):
            print("Error: SIMD constant declaration array sizes are not the same!")
            exit(1)
        else:
            with open(filename+"-SIMDconstants.h", "w") as output:
                for i in range(len(SIMD_const_varnme_array)):
                    if SIMD=="256bit":
                        output.write("const double tmp"+SIMD_const_varnme_array[i]+" = "+SIMD_const_values_array[i]+"; "+
                                     "const __m256d "+SIMD_const_varnme_array[i]+" = _mm256_set1_pd(tmp"+SIMD_const_varnme_array[i]+");\n")
                    elif SIMD=="512bit":
                        output.write("const double tmp"+SIMD_const_varnme_array[i]+" = "+SIMD_const_values_array[i]+"; "+
                                     "const __m512d "+SIMD_const_varnme_array[i]+" = _mm512_set1_pd(tmp"+SIMD_const_varnme_array[i]+");\n")
