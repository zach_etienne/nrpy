import time
from sympy import sqrt, Matrix, sympify, symbols, sin, cos, Rational, diff, log
from parameters_compiletime import params_type, params_varname, params_value
from NRPy_functions import get_parameter_value, set_parameter
from reference_metric import ReU, ReDD, ghatDD, xx, r as r_refmetric, th as th_refmetric, ph as ph_refmetric, protected_varnames
from NRPy_functions import declarerank2, symm_matrix_inverter3x3, generic_matrix_inverter3x3, print_diff
from NRPy_file_output import NRPy_file_output

def sympify_integers__replace_rthph(obj,r,th,ph,r_refmetric,th_refmetric,ph_refmetric):
    if isinstance(obj, int):
        return sympify(obj)
    else:
        # Do not enable precomputation of transcendental functions in initial data routine. Unlikely to positively impact performance.
        return obj.subs(r, r_refmetric).subs(th, th_refmetric).subs(ph, ph_refmetric)
        # However, precomputation can very well be useful
        # return clean_transcendental(obj.subs(r, r_refmetric).subs(th, th_refmetric).subs(ph, ph_refmetric),
        #                             list_of_precomp_names, counter)

OUTDIR = get_parameter_value("Evol_scheme", params_varname, params_value)

##########################
# INITIAL DATA PARAMETERS
##########################

# Initial data parameters: choose between BoostedSchwarzshild, BrillLindquist, Minkowski, StaticTrumpet, UIUC
# Validated against Mathematica: StaticTrumpet (with stationary gauge), UIUC (with stationary & PunctureR2/ShiftZero gauge), BoostedSchwarzschild (with PunctureR2 &StationaryBoostedSchwarzschild gauge), BrillLindquist & Minkowski (PunctureR2+Zero gauge)
ID_scheme = get_parameter_value("ID_scheme", params_varname, params_value)

# Initial lapse: choose between Unit, Trumpet, IsoSchw, PunctureR2, StationaryStaticTrumpet, StationaryUIUC
InitialLapseType = "PunctureR2"
set_parameter("char", "InitialLapseType", "PunctureR2", params_type, params_varname, params_value)

# Initial shift: choose between Zero, StationaryBoostedSchwarzschild, StationaryStaticTrumpet, and StationaryUIUC
InitialShiftType = "Zero"
set_parameter("char", "InitialShiftType", "Zero", params_type, params_varname, params_value)

DebugIDagainstMathematica = "False"

#************************
#************************
# --={ INITIAL DATA }=--
# --={  TRANSLATION }=--
#************************
#************************
# Apply appropriate coordinate transformations to convert
# initial data in a given coord system to our grid

# TIP: If your solution blows up early, you may need to be
#      a bit more careful about coordinate singularities in
#      the Jacobian!

print("Generating C code for BSSN-based -={ "+ID_scheme+" }=- initial data. This may take a few minutes...")
starttimer_bssnID = time.time()

# Variables needed for initial data given in spherical basis
r       = symbols('r', positive=True)
th, ph = symbols('th ph', real=True)

ID_protected_variables = protected_varnames

# Quantities that must be set by initial data routines below:
# Physical (as opposed to conformal or reference) metric:
gPhys0DD = [[sympify(0) for i in range(3)] for j in range(3)]
# Physical (as opposed to conformal) extrinsic curvature:
APhys0DD = [[sympify(0) for i in range(3)] for j in range(3)]
IDtrK = symbols('IDtrK', real=True)

# The BoostedSchwarzschild represent a Schwarzschild black hole with mass M,
# Lorentz boosted in the z - direction. SEE https://arxiv.org/abs/1410.8607
if ID_scheme == "BoostedSchwarzschild":
    # Input parameters:
    vz,M = symbols('vz,M', real=True)
    ID_protected_variables += ["vz","M"]
    # Auxiliary variables:
    zOffset, xB, yB, zB, LorentzFactor, rB, psiB = symbols('zOffset xB yB zB LorentzFactor rB psiB', real=True)

    zOffset = 0
    xB = r * sin(th) * cos(ph)
    yB = r * sin(th) * sin(ph)
    zB = r * cos(th) - zOffset
    LorentzFactor = 1 / sqrt(1 - vz**2)
    rB = sqrt(xB**2 + yB**2 + LorentzFactor**2*zB**2)
    psiB = 1 + M / (2*rB)
    BB,CC,betaUz,BUz,gtzz,Atxx,Atxz,Atyz,Atzz = symbols('BB CC betaUz BUz gtzz Atxx Atxz Atyz Atzz',real=True)
    BB = sqrt( (M + 2*rB)**6 - 16*(M - 2*rB)**2 * rB**4 * vz**2 )
    CC =       (M + 2*rB)**6 -  8*(M - 2*rB)**2 * rB**4 * vz**2
    betaUz = -((M * vz * (M**2 + 6*M * rB + 16*rB**2)*(M**3 + 6*M**2 * rB + 8*M * rB**2 + 16*rB**3)) / BB**2)
    BUz = -((64*M*(M - 4*rB)*(M - 2*rB)*rB**2*(M + 2*rB)**5*vz**2*zB)/
            ((M + 2*rB)**12 - 32*(M - 2*rB)**2 * rB**4 * (M + 2*rB)**6 * vz**2 + 256*(M - 2*rB)**4 * rB**8 * vz**4))
    gtzz = (LorentzFactor**2*BB**2) / (M + 2*rB)**6
    Atxx =  (64 *LorentzFactor   *M*(M - 4*rB)*rB**2*vz*CC*zB) / (3 * (M + 2*rB)**3 * BB**3)
    Atxz = -(32 *LorentzFactor   *M*(M - 4*rB)*rB**2*vz*xB   ) / (    (M + 2*rB)**3 * BB   )
    Atyz = -(32 *LorentzFactor   *M*(M - 4*rB)*rB**2*vz*yB   ) / (    (M + 2*rB)**3 * BB   )
    Atzz = -(128*LorentzFactor**3*M*(M - 4*rB)*rB**2*vz*CC*zB) / (3 * (M + 2*rB)**9 * BB   )
    # *** The physical spatial metric in spherical basis ***
    # Set the upper-triangle of the matrix...
    gPhys0DD[0][0] = psiB**4 * (1 + gtzz + (-1 + gtzz)*cos(2*th))/2
    gPhys0DD[0][1] = psiB**4 * (1 - gtzz)*r*cos(th)*sin(th)
    gPhys0DD[1][1] = psiB**4 * r**2*(1 + gtzz - (-1 + gtzz)*cos(2*th))/2
    gPhys0DD[2][2] = psiB**4 * r**2*sin(th)**2
    # ... then apply symmetries to get the other components
    declarerank2(gPhys0DD,"dummy", "sym12","NoDecl")

    # *** The physical trace-free extrinsic curvature in spherical basis ***
    # Set the upper-triangle of the matrix...
    APhys0DD[0][0] = psiB**4 * ((Atxx + Atzz + (-Atxx + Atzz)*cos(2*th) + 2*(Atxz*cos(ph) + Atyz*sin(ph))*sin(2*th))/2)
    APhys0DD[0][1] = psiB**4 * (r*cos(2*th)*(Atxz*cos(ph) + Atyz*sin(ph)) + (Atxx - Atzz)*r*cos(th)*sin(th))
    APhys0DD[0][2] = psiB**4 * (r*cos(th)*(Atyz*cos(ph) - Atxz*sin(ph))*sin(th))
    APhys0DD[1][1] = psiB**4 * (r**2*(Atxx*cos(th)**2 + Atzz*sin(th)**2 - (Atxz*cos(ph) + Atyz*sin(ph))*sin(2*th)))
    APhys0DD[1][2] = psiB**4 * (r**2*(-Atyz*cos(ph) + Atxz*sin(ph))*sin(th)**2)
    APhys0DD[2][2] = psiB**4 * (Atxx*r**2*sin(th)**2)
    # ... then apply symmetries to get the other components
    declarerank2(APhys0DD, "dummy", "sym12", "NoDecl")

    # *** Trace of extrinsic curvature in spherical basis ***
    IDtrK = (32*LorentzFactor*M*vz*((M + 2*rB)**7 - 32*(M - 2*rB)**2*(M - rB)*rB**4*vz**2)*rB**2*zB)/((M + 2*rB)**3*BB**3)

# Brill-Lindquist initial data represent a pair of nonspinning black
# holes at rest at z = +/- bScale, with masses M1 and M2, respectively.
elif ID_scheme == "BrillLindquist":
    # Input parameters:
    bScaleBL, M1, M2 = symbols('bScaleBL M1 M2', real=True)
    ID_protected_variables += ["bScaleBL", "M1","M2"]
    # Auxiliary variables:
    psi0 = symbols('psi0', real=True)

    # *** The Brill - Lindquist conformal factor ***
    psi0 = 1 + Rational(1,2)*(M1 / sqrt(bScaleBL**2 + r**2 - 2*bScaleBL*r*cos(th)) + M2/sqrt(bScaleBL**2 + r**2 + 2*bScaleBL*r*cos(th)))

    # *** The physical spatial metric in spherical basis ***
    # Set the upper-triangle of the matrix...
    gPhys0DD[0][0] = psi0**4
    gPhys0DD[1][1] = psi0**4 * r**2
    gPhys0DD[2][2] = psi0**4 * r**2*sin(th)**2
    # ... then apply symmetries to get the other components
    declarerank2(gPhys0DD, "dummy", "sym12", "NoDecl")

    # *** The physical trace-free extrinsic curvature in spherical basis ***
    # APhys is ZERO for Brill-Lindquist initial data (set to zero above)
    # *** Trace of extrinsic curvature in spherical basis ***
    # trK is ZERO for Brill-Lindquist initial data
    IDtrK = 0

# Minkowski == Flat spacetime
elif ID_scheme == "Minkowski":
    # *** The physical spatial metric in spherical basis ***
    # Set the upper-triangle of the matrix...
    gPhys0DD[0][0] = 1
    gPhys0DD[1][1] = r**2
    gPhys0DD[2][2] = r**2*sin(th)**2
    # ... then apply symmetries to get the other components
    declarerank2(gPhys0DD, "dummy", "sym12", "NoDecl")
    # *** The physical trace-free extrinsic curvature in spherical basis ***
    # APhys is ZERO for Minkowski initial data (set to zero above)
    # *** Trace of extrinsic curvature in spherical basis ***
    # trK is ZERO for Minkowski initial data
    IDtrK = 0

# StaticTrumpet initial data represent a nonspinning black hole
#   with mass M in static trumpet coordinates,
#   see https://arxiv.org/abs/1403.5484
elif ID_scheme == "StaticTrumpet":
    # Input parameters:
    M = symbols('M', real=True)
    ID_protected_variables += ["M"]
    # Auxiliary variables:
    psi0 = symbols('psi0', real=True)

    # *** The StaticTrumpet conformal factor ***
    psi0 = sqrt(1 + M/r)

    # *** The physical spatial metric in spherical basis ***
    # Set the upper-triangle of the matrix...
    gPhys0DD[0][0] = psi0**4
    gPhys0DD[1][1] = psi0**4 * r**2
    gPhys0DD[2][2] = psi0**4 * r**2*sin(th)**2
    # ... then apply symmetries to get the other components
    declarerank2(gPhys0DD, "dummy", "sym12", "NoDecl")

    # *** The physical trace-free extrinsic curvature in spherical basis ***
    # Set the upper-triangle of the matrix...
    APhys0DD[0][0] = psi0**4 * (-(4*M)                 /(3*(M + r)**2))
    APhys0DD[1][1] = psi0**4 * ((2*M*r**2            )/(3*(M + r)**2))
    APhys0DD[2][2] = psi0**4 * ((2*M*r**2*sin(th)**2)/(3*(M + r)**2))
    # ... then apply symmetries to get the other components
    declarerank2(APhys0DD, "dummy", "sym12", "NoDecl")
    # StaticTrumpet extrinsic curvature
    IDtrK = M / (M + r)**2

# The UIUC initial data represent a Kerr black hole with mass M
#  and dimensionless spin chi in UIUC quasi-isotropic coordinates,
#   see https://arxiv.org/abs/1001.4077
elif ID_scheme == "UIUC":
    # Input parameters:
    M,chi = symbols('M chi', real=True)
    ID_protected_variables += ["M","chi"]
    # Auxiliary variables:
    a,rp,rm,rBL,SIG,DEL,AA = symbols('a rp rm rBL SIG DEL AA', real=True)
    # Spin per unit mass
    a = M*chi

    # Boyer - Lindquist outer horizon
    rp = M + sqrt(M**2 - a**2)
    # Boyer - Lindquist inner horizon
    rm = M - sqrt(M**2 - a**2)

    # Boyer - Lindquist radius in terms of UIUC radius
    rBL = r*(1 + rp / (4*r))**2

    # UIUC definitions
    SIG = rBL**2 + a**2*cos(th)**2
    DEL = rBL**2 - 2*M*rBL + a**2
    AA = (rBL**2 + a**2)**2 - DEL*a**2*sin(th)**2

    # *** The physical spatial metric in spherical basis ***
    # Set the upper-triangle of the matrix...
    gPhys0DD[0][0] = ((SIG*(r + rp/4)**2)/(r**3*(rBL - rm)))
    gPhys0DD[1][1] = SIG
    gPhys0DD[2][2] = AA/SIG*sin(th)**2
    # ... then apply symmetries to get the other components
    declarerank2(gPhys0DD, "dummy", "sym12", "NoDecl")

    # *** The physical trace-free extrinsic curvature in spherical basis ***
    # Set the upper-triangle of the matrix...
    APhys0DD[0][2] = (M*a*sin(th)**2)/(SIG*sqrt(AA*SIG))*\
                     (3*rBL**4 + 2*a**2*rBL**2 - a**4- a**2*(rBL**2 - a**2)*sin(th)**2)*(1 + rp/(4*r))*1/sqrt(r*(rBL - rm))
    APhys0DD[1][2] = -((2*a**3*M*rBL*cos(th)*sin(th)**3)/(SIG*sqrt(AA*SIG)))*(r - rp/4)*sqrt((rBL - rm)/r)
    #APhys0DD[1][2] = simplify(APhys0DD[1][2])

    # ... then apply symmetries to get the other components
    declarerank2(APhys0DD, "dummy", "sym12", "NoDecl")
    # UIUC is a maximal slice of Kerr
    IDtrK = 0

else:
    print("ERROR: You chose an unsupported ID_scheme. Check for spelling issues or special characters and try again.")
    exit(1)

# Apply conformal transformations:
#  Define the BSSN conformal factor in the spherical basis.
ghat0DD = [[sympify(0) for i in range(3)] for j in range(3)]
ghat0DD[0][0] = 1
ghat0DD[1][1] = r**2
ghat0DD[2][2] = r**2 * sin(th)**2

IDphi = symbols('IDphi', real=True)
ghatDET, gPhysDET, gPhysINV, IDpsi, IDcf = symbols('ghatDET gPhysDET gPhysINV IDpsi IDcf', positive=True)
ghatDET  = r**4 * sin(th)**2
#ghatDETm4o3  = r**(-Rational(16,3)) * sin(th)**(-Rational(8,3))

# It is far more efficient to write out the matrix determinant and inverse by hand instead of using built-in functions,
#   since the matrix is symmetric.
gPhys0UU = [[sympify(0) for i in range(3)] for j in range(3)]
gPhysDET = symm_matrix_inverter3x3(gPhys0DD, gPhys0UU)

psim4_0 = symbols('psim4_0', positive=True)
psim4_0 = (gPhysDET/ghatDET)**(-Rational(1,3))
coords0sph = [r,th,ph]

CFEvolution = get_parameter_value("CFEvolution", params_varname, params_value)
if   CFEvolution == "EvolvePhi":
    IDcf = Rational(1,12)*log(gPhysDET/ghatDET)
elif CFEvolution == "EvolveChi":
    IDcf = psim4_0
elif CFEvolution == "EvolveW":
    IDcf = (gPhysDET/ghatDET)**Rational(-1,6)
else:
    print("CFEvolution == ",CFEvolution,"is not supported in initial data routine. Check for typos, special characters, or stray spaces.")
    exit(1)

# Define barred quantities
gbar0DD = [[sympify(0) for i in range(3)] for j in range(3)]
Abar0DD = [[sympify(0) for i in range(3)] for j in range(3)]

for i in range(3):
    for j in range(3):
        gbar0DD[i][j] = psim4_0*gPhys0DD[i][j]
        Abar0DD[i][j] = psim4_0*APhys0DD[i][j]

# Nice property of determinants:
# det(c A) = c^n det(A), for n x n matrix A, scalar c
# https://en.wikipedia.org/wiki/Determinant#Properties_of_the_determinant
gbar0DET = gPhysDET*psim4_0**3 # det(gbar0DDmatrix) = det(psim4_0*gPhys0DD) = psim4_0**3*det(gPhys0DD)

# Evaluate inverse metric and contracted Christoffel symbols
gbar0UU = [[sympify(0) for i in range(3)] for j in range(3)]
# Metric inverses: First gammabarUU
gbar0UU = [[sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        # Properties of matrix inverses: (k A)^(-1) = k^(-1) A^(-1), for scalar k and invertible matrix A
        # https://en.wikipedia.org/wiki/Invertible_matrix#Properties
        # Therefore, gbar = psim4*gPhys -> inv(gbar) = inv(gPhys)/psim4
        gbar0UU[i][j] = gPhys0UU[i][j]/psim4_0 #gbar0UUmatrix[(i,j)]

# Metric inverses: Then gammahatUU
ghat0UU = [[sympify(0) for i in range(3)] for j in range(3)]
# Metric inverses: First gammabarUU
ghatDET = symm_matrix_inverter3x3(ghat0DD,ghat0UU)

# Compute Christoffel symbols and LambdaU for initial data:
ghat0DDdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
gbar0DDdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            ghat0DDdD[i][j][k] = diff(ghat0DD[i][j],coords0sph[k])
            #gbar0DDdD[i][j][k] = psim4_0*diff(gPhys0DD[i][j],coords0sph[k]) + psim4_0dD[k]*gPhys0DD[i][j]
            gbar0DDdD[i][j][k] = diff(gbar0DD[i][j],coords0sph[k])
            #print("Print["+str(i)+" , "+str(j)+" , "+str(k)+" ,\" , \", ("+ mathematica_code(gbar0DDdD[i][j][k]) + " - gbar0DDdD[[" + str(i + 1) + "," + str(j + 1) + "," + str(k + 1) + "]])]")
        #print("Print["+str(i)+" , "+str(j) + " ,\" , \", ("+ mathematica_code(gbar0DD[i][j]) + " - gbar0DD[[" + str(i + 1) + "," + str(j + 1) + "]])]")

# Now that all derivatives of ghat and gbar have been computed,
# we may now substitute the definitions r = r_refmetric, th=th_refmetric,...
# WARNING: Substitution only works when the variable is not an integer. Hence the if not isinstance(...,...) stuff.
# If the variable isn't an integer, we revert transcendental functions inside to normal variables. E.g., sin(x2) -> sinx2
#  Reverting to normal variables in this way makes expressions simpler in NRPy, and enables transcendental functions
#  to be pre-computed in SENR.
psim4_0 = sympify_integers__replace_rthph(psim4_0,r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
IDcf    = sympify_integers__replace_rthph(IDcf   ,r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
IDtrK   = sympify_integers__replace_rthph(IDtrK  ,r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
for i in range(3):
    for j in range(3):
        ghat0UU[i][j] = sympify_integers__replace_rthph(ghat0UU[i][j],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
        gbar0UU[i][j] = sympify_integers__replace_rthph(gbar0UU[i][j],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
        ghat0DD[i][j] = sympify_integers__replace_rthph(ghat0DD[i][j],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
        gbar0DD[i][j] = sympify_integers__replace_rthph(gbar0DD[i][j],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
        Abar0DD[i][j] = sympify_integers__replace_rthph(Abar0DD[i][j],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
        for k in range(3):
            ghat0DDdD[i][j][k] = sympify_integers__replace_rthph(ghat0DDdD[i][j][k],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)
            gbar0DDdD[i][j][k] = sympify_integers__replace_rthph(gbar0DDdD[i][j][k],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)

r = r_refmetric
th = th_refmetric
ph = ph_refmetric

Chrishat0UDD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
Chrisbar0UDD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
for i in range(3):
    for k in range(3):
        for l in range(3):
            for m in range(3):
                Chrishat0UDD[i][k][l] += (Rational(1,2))*ghat0UU[i][m]*(ghat0DDdD[m][k][l] + ghat0DDdD[m][l][k] - ghat0DDdD[k][l][m])
                Chrisbar0UDD[i][k][l] += (Rational(1,2))*gbar0UU[i][m]*(gbar0DDdD[m][k][l] + gbar0DDdD[m][l][k] - gbar0DDdD[k][l][m])
            #print("("+mathematica_code(Chrisbar0UDD[i][k][l])+" - Chrisbar0UDD[["+str(i+1)+","+str(k+1)+","+str(l+1)+"]])")
            #print("("+mathematica_code(Chrishat0UDD[i][k][l])+" - Chrishat0UDD[["+str(i+1)+","+str(k+1)+","+str(l+1)+"]])")

Lambda0U = [ sympify(0) for i in range(3)]
for k in range(3):
    for i in range(3):
        for j in range(3):
            Lambda0U[k] += gbar0UU[i][j]*(Chrisbar0UDD[k][i][j] - Chrishat0UDD[k][i][j])
# Next set initial lapse, which is a free parameter.
#  While you are not *required* to use any particular initial lapse,
#  you will find that some work better than others for a given set
#  of initial data. Bottom line: It can be useful to try multiple initial
#  lapse conditions to find which one yields the best behavior
#  (e.g., lowest constraint violation, largest BH coordinate size, etc)
IDalpha = symbols('IDalpha', real=True)
IDpsi = symbols('IDpsi', positive=True)
IDpsi = psim4_0**(-Rational(1,4))
if InitialLapseType == "Unit":
    IDalpha = 1
elif InitialLapseType == "Trumpet":
    IDalpha = 1/(2*IDpsi - 1)
elif InitialLapseType == "IsoSchw":
    IDalpha = 2/(1 + IDpsi**4)
elif InitialLapseType == "PunctureR2":
    IDalpha = IDpsi**(-2)
# Next some initial data-specific choices for lapse, which depend on auxiliary quantities defined in initial data routine above.
elif InitialLapseType == "StationaryStaticTrumpet":
    if ID_scheme != "StaticTrumpet":
        print("StationaryStaticTrumpet lapse is defined in terms of \"StaticTrumpet\" initial data quantities...")
        print("... so you must choose InitialDatatype=\"StaticTrumpet\" to define this lapse condition.")
        exit(1)
    IDalpha = r/(M + r)
elif InitialLapseType == "StationaryUIUC":
    if ID_scheme != "UIUC":
        print("StationaryUIUC lapse is defined in terms of \"UIUC\" initial data quantities...")
        print("... so you must choose InitialDatatype=\"UIUC\" to define this lapse condition.")
        exit(1)
    IDalpha = sqrt((DEL*SIG) / AA)

else:
    print("InitialLapseType=",InitialLapseType,"is unsupported.")
    print(" Check for typos, special characters, or stray spaces.")
    exit(1)
IDalpha = sympify_integers__replace_rthph(IDalpha,r,th,ph,r_refmetric,th_refmetric,ph_refmetric)

# Next set initial shift, which is a free parameter.
#  Usually we set initial shift to Zero.
beta0U = [sympify(0) for i in range(3)]
if InitialShiftType == "Zero":
    beta0U = [0,0,0]
elif InitialShiftType == "StationaryBoostedSchwarzschild":
    if ID_scheme != "BoostedSchwarzschild":
        print("StationaryBoostedSchwarzschild shift is defined in terms of \"BoostedSchwarzschild\" initial data quantities...")
        print("... so you must choose InitialDatatype=\"BoostedSchwarzschild\" to define this shift condition.")
        exit(1)
    beta0U = [betaUz * cos(th), -((betaUz * sin(th)) / r), 0]

elif InitialShiftType == "StationaryStaticTrumpet":
    if ID_scheme != "StaticTrumpet":
        print("StationaryStaticTrumpet shift is defined in terms of \"StaticTrumpet\" initial data quantities...")
        print("... so you must choose InitialDatatype=\"StaticTrumpet\" to define this shift condition.")
        exit(1)
    beta0U = [(M*r)/(M + r)**2, 0, 0]
elif InitialShiftType == "StationaryUIUC":
    if ID_scheme != "UIUC":
        print("StationaryUIUC shift is defined in terms of \"UIUC\" initial data quantities...")
        print("... so you must choose InitialDatatype=\"UIUC\" to define this shift condition.")
        exit(1)
    beta0U = [0, 0, -((2 * M * a * rBL) / AA)]
else:
    print("InitialShiftType=",InitialShiftType,"is unsupported.")
    print(" Check for typos, special characters, or stray spaces.")
    exit(1)

for i in range(3):
    beta0U[i] = sympify_integers__replace_rthph(beta0U[i],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)

# Transform initial data to our coordinate system:
# First compute Jacobian and its inverse
# drrefmetric__dx_0UDmatrix = Matrix([[diff( r_refmetric,xx[0]), diff( r_refmetric,xx[1]), diff( r_refmetric,xx[2])],
#                                     [diff(th_refmetric,xx[0]), diff(th_refmetric,xx[1]), diff(th_refmetric,xx[2])],
#                                     [diff(ph_refmetric,xx[0]), diff(ph_refmetric,xx[1]), diff(ph_refmetric,xx[2])]])
# dx__drrefmetric_0UDmatrix = drrefmetric__dx_0UDmatrix.inv()
drrefmetric__dx_0UDmatrix = [[diff( r_refmetric,xx[0]), diff( r_refmetric,xx[1]), diff( r_refmetric,xx[2])],
                             [diff(th_refmetric,xx[0]), diff(th_refmetric,xx[1]), diff(th_refmetric,xx[2])],
                             [diff(ph_refmetric,xx[0]), diff(ph_refmetric,xx[1]), diff(ph_refmetric,xx[2])]]
dx__drrefmetric_0UDmatrix = [[sympify(0) for i in range(3)] for j in range(3)]
dummyDET = generic_matrix_inverter3x3(drrefmetric__dx_0UDmatrix, dx__drrefmetric_0UDmatrix)

gbarDD = [[sympify(0) for i in range(3)] for j in range(3)]
AbarDD = [[sympify(0) for i in range(3)] for j in range(3)]
betaU   = [sympify(0) for i in range(3)]
LambdaU = [sympify(0) for i in range(3)]
for i in range(3):
    for j in range(3):
        # Matrices are stored in row, column format, so (i,j) <-> (row,column)
        # betaU[i]   += dx__drrefmetric_0UDmatrix[(i,j)]*beta0U[j]
        # LambdaU[i] += dx__drrefmetric_0UDmatrix[(i,j)]*Lambda0U[j]
        betaU[i]   += dx__drrefmetric_0UDmatrix[i][j]*beta0U[j]
        LambdaU[i] += dx__drrefmetric_0UDmatrix[i][j]*Lambda0U[j]
        for k in range(3):
            for l in range(3):
                gbarDD[i][j] += drrefmetric__dx_0UDmatrix[k][i]*drrefmetric__dx_0UDmatrix[l][j]*gbar0DD[k][l]
                AbarDD[i][j] += drrefmetric__dx_0UDmatrix[k][i]*drrefmetric__dx_0UDmatrix[l][j]*Abar0DD[k][l]

# Compute rescaled metric hDD, extrinsic curvature aDD, shift vetU, and auxiliary variables lambdaU
IDhDD    = [[sympify(0) for i in range(3)] for j in range(3)]
IDaDD    = [[sympify(0) for i in range(3)] for j in range(3)]
IDvetU    = [sympify(0) for i in range(3)]
IDbetU    = [sympify(0) for i in range(3)]
IDlambdaU = [sympify(0) for i in range(3)]

for i in range(3):
    IDvetU[i]    = betaU[i]   / ReU[i]
    IDlambdaU[i] = LambdaU[i] / ReU[i]
    for j in range(3):
        IDhDD[i][j] = (gbarDD[i][j] - ghatDD[i][j]) / ReDD[i][j]
        IDaDD[i][j] =  AbarDD[i][j] / ReDD[i][j]

# For time-dependent shift initial data, we want to make sure bet is set properly.
#for i in range(3):
#    IDbetU[i] = diff(IDvetU[i], t)

# Set time derivative of beta for Boosted Schwarzschild stationary shift. \partial_t vet = bet.
# WARNING: will be incorrect if ReU[1] depends on time (e.g., rotating coordinate system)!
if InitialShiftType == "StationaryBoostedSchwarzschild":
    IDbetU = [BUz*cos(th)/ReU[0], -BUz*sin(th)/r / ReU[1], 0]
    for i in range(3):
        IDbetU[i] = sympify_integers__replace_rthph(IDbetU[i],r,th,ph,r_refmetric,th_refmetric,ph_refmetric)

# -={ C code generation: output to codegen_output/NRPy_Initial_Data_*.h }=-
NRPy_file_output(OUTDIR+"/initial_data/NRPy_codegen/Initial_Data.h", [],[],[], ID_protected_variables,
                 [],[IDvetU[0],"in_gfs[IDX4pt(VET1,idx)]",IDvetU[1],"in_gfs[IDX4pt(VET2,idx)]",IDvetU[2],"in_gfs[IDX4pt(VET3,idx)]",
                 IDhDD[0][0],"in_gfs[IDX4pt(H11,idx)]",IDhDD[0][1],"in_gfs[IDX4pt(H12,idx)]",IDhDD[0][2],"in_gfs[IDX4pt(H13,idx)]",
                 IDhDD[1][1],"in_gfs[IDX4pt(H22,idx)]",IDhDD[1][2],"in_gfs[IDX4pt(H23,idx)]",IDhDD[2][2],"in_gfs[IDX4pt(H33,idx)]",
                 IDcf,"in_gfs[IDX4pt(CF,idx)]", IDalpha,"in_gfs[IDX4pt(ALPHA,idx)]",
                 IDaDD[0][0],"in_gfs[IDX4pt(A11,idx)]",IDaDD[0][1],"in_gfs[IDX4pt(A12,idx)]",IDaDD[0][2],"in_gfs[IDX4pt(A13,idx)]",
                 IDaDD[1][1],"in_gfs[IDX4pt(A22,idx)]",IDaDD[1][2],"in_gfs[IDX4pt(A23,idx)]",IDaDD[2][2],"in_gfs[IDX4pt(A33,idx)]",
                 IDtrK,"in_gfs[IDX4pt(TRK,idx)]",
                 IDlambdaU[0],"in_gfs[IDX4pt(LAMB1,idx)]",IDlambdaU[1],"in_gfs[IDX4pt(LAMB2,idx)]",IDlambdaU[2],"in_gfs[IDX4pt(LAMB3,idx)]",
                 IDbetU[0],"in_gfs[IDX4pt(BET1,idx)]",IDbetU[1],"in_gfs[IDX4pt(BET2,idx)]",IDbetU[2],"in_gfs[IDX4pt(BET3,idx)]"])
# For validation against Mathematica-based code:
if DebugIDagainstMathematica == "True":
    print("Print[\"ReDDs:\"]")
    for i in range(3):
        for j in range(i, 3):
            print_diff(ReDD[i][j], "ReDD[" + str(i) + "][" + str(j) + "]")

    print("Print[\"Jacobian:\"]")
    for i in range(3):
        for j in range(i,3):
            print_diff(drrefmetric__dx_0UDmatrix[(i,j)],"drdxUD[["+str(i+1)+","+str(j+1)+"]]")

    print("Print[\"gbarDDs:\"]")
    for i in range(3):
        for j in range(i,3):
            print_diff(gbarDD[i][j],"gbarDD[["+str(i+1)+","+str(j+1)+"]]")

    print("Print[\"ghatDDs:\"]")
    for i in range(3):
        for j in range(i,3):
            print_diff(ghatDD[i][j],"ghatDD["+str(i)+"]["+str(j)+"]")


    print("Print[\"gPhys0DDs:\"]")
    for i in range(3):
        for j in range(i,3):
            print_diff(gPhys0DD[i][j],"gPhys0DD[["+str(i+1)+","+str(j+1)+"]]")

    print("Print[\"vets:\"]")
    for i in range(3):
        print_diff(IDvetU[i],"vetU[["+str(i+1)+"]]")

    print("Print[\"hDDs:\"]")
    for i in range(3):
        for j in range(i,3):
            print_diff(IDhDD[i][j],"hDD[["+str(i+1)+","+str(j+1)+"]]")

    print("Print[\"cf,alp:\"]")
    print_diff(IDcf, "IDcf")
    print_diff(IDalpha, "alpha")

    print("Print[\"Aijs:\"]")
    for i in range(3):
        for j in range(i,3):
            print_diff(IDaDD[i][j],"aDD[["+str(i+1)+","+str(j+1)+"]]")

    print("Print[\"trK:\"]")
    print_diff(IDtrK, "trK")

    print("Print[\"lambs:\"]")
    for i in range(3):
        print_diff(IDlambdaU[i], "lambdaU[[" + str(i + 1) + "]]")

    print("Print[\"bets:\"]")
    for i in range(3):
        print_diff(IDbetU[i], "betU[[" + str(i + 1) + "]]")

stoptimer_bssnID = time.time()
print("Completed BSSN initial data code generation in \t\t"+str(round(stoptimer_bssnID-starttimer_bssnID,2))+" seconds")
