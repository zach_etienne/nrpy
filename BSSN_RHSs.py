import time
from sympy import sqrt, exp, sympify, symbols, Rational, pi, LeviCivita, sin, diff
# Generate finite difference stencils
#from finite_difference_stencil_generator import *
# Define the reference metric ghatDD & rescaling quantities ReU and ReDD.
from reference_metric import ReU,ReDD,ghatDD,protected_varnames
# Define reference metric derived ("hatted") quantities:
from ref_metric__hatted_quantities import detgammahat,detgammahatdD,ReUdD,detgammahatdDD,\
    ghatUU,GammahatUDD,ghatDDdD,ReDDdD,ReUdDD,GammahatUDDdD, ghatDDdDD,ReDDdDD
from parameters_compiletime import Optimize, EnableKreissOligerDissipation, EnableKO_direction
from NRPy_file_output import NRPy_file_output
from NRPy_functions import declarerank1, declarerank2, declarerank3, declarerank4, \
    symm_matrix_inverter3x3, compute_cost, set_parameter, get_parameter_value
from parameters_compiletime import params_type, params_varname, params_value, runtime_params_type, runtime_params_varname

def func_BSSN_gf_define_alias(gfname):
    # FIXME: compatibility mode:
    return gfname.replace("D", "").replace("U", "").replace("2", "3").replace("1", "2").replace("0", "1").upper().replace("LAMBDA","LAMB").replace("RBAR","AUXR").replace("DETG","AUXDETG")

#    ____ __________ _   __#
#   / __ ) ___/ ___// | / /#
#  / __  \__ \\__ \/  |/ / #
# / /_/ /__/ /__/ / /|  /  #
#/_____/____/____/_/ |_/   #

# Set precision to, e.g., double or long double
PRECISION = get_parameter_value("PRECISION", params_varname, params_value)
OUTDIR = get_parameter_value("Evol_scheme", params_varname, params_value)
RunMode = get_parameter_value("NRPyRunMode", params_varname, params_value)

########################
#   BSSN RHS PARAMETERS
########################

# BSSN RHS parameters:
CFEvolution = "EvolveW" # EvolveW, EvolveChi, EvolvePhi are currently supported
set_parameter("char", "CFEvolution", "EvolveW", params_type, params_varname, params_value)

# Advect the lapse, \partial_t \alpha -> \partial_0 \alpha ?
AdvectAlpha = "True"
set_parameter("char", "AdvectAlpha", "True", params_type, params_varname, params_value)

#  A second-order shift condition is implemented. Should we advect the shift?
#   I.e., should \partial_t \beta^i -> \partial_0 \beta^i?
AdvectShift = "True"
set_parameter("char", "AdvectShift", "True", params_type, params_varname, params_value)
# Similarly, should \partial_t B^i -> \partial_0 B^i?
AdvectB     = "True"
set_parameter("char", "AdvectB", "True", params_type, params_varname, params_value)
# Covariant shift? Eqs. 20a-c in https://arxiv.org/pdf/0902.3652.pdf
# Note that this only modifies shift advection terms, so AdvectB & AdvectShift should be enabled.
CovariantShift = "False"
set_parameter("char", "CovariantShift", "False", params_type, params_varname, params_value)
# Evolve shift equation that uses Lie derivatives
LieShift = "False"
set_parameter("char", "LieShift", "False", params_type, params_varname, params_value)

BSSN_evolved_variable_list = [ "vetU0","vetU1","vetU2","hDD00","hDD01","hDD02","hDD11","hDD12","hDD22","cf","alpha",\
                               "trK","lambdaU0","lambdaU1","lambdaU2","betU0","betU1","betU2",\
                               "aDD00","aDD01","aDD02","aDD11","aDD12","aDD22"]
BSSN_evolved_variable_list.sort()


BSSN_aux_gridfunctions = ["detg","RbarDD00","RbarDD01","RbarDD02","RbarDD11","RbarDD12","RbarDD22"]  # Set gridfunction detg as auxiliary, so that it is not evolved


# Write the file gridfunction_defines.h, which stores #define aliases to gridfunctions.
#  This makes NRPy-generated C codes easier to read and understand.
if RunMode == "Everything" or RunMode == "RHSonly":
    with open(OUTDIR+"/gridfunction_defines-NRPyGEN.h", "w") as output:
        output.write("#define NUM_EVOL_GFS "+str(len(BSSN_evolved_variable_list))+"\n")
        for i in range(len(BSSN_evolved_variable_list)):
            output.write("#define "+func_BSSN_gf_define_alias(BSSN_evolved_variable_list[i])+" "+str(i)+"\n")
        output.write("// AUXILIARY GRIDFUNCTIONS: (These could in principle re-use space stored for other gridfunction RHSs, though they do not.)\n")
        for i in range(len(BSSN_aux_gridfunctions)):
            output.write("#define "+func_BSSN_gf_define_alias(BSSN_aux_gridfunctions[i])+" "+str(i)+"\n")

BSSN_runtime_parameters = ["eta", "diss_ko_strength"]  # eta = BSSN Gamma-freezing shift eta parameter,
                                                 # diss_ko_strength = BSSN Kreiss-Oliger strength parameter
                                                 # We set these as runtime parameters so they are not considered
                                                 #   a gridfunction or evolved variable.


########################
#   DEFINE BSSN RHSs
########################
# Start timer, for benchmarking
starttimer_bssnRHSs = time.time()
print("Generating algebraic expressions for BSSN RHSs...")

BSSN_protected_variables = protected_varnames + BSSN_runtime_parameters

# Declare evolved, rescaled BSSN variables h_{ij} and a_{ij} and their spatial derivatives
hDD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(hDD,"hDD","sym12","DeclTrue")
aDD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(aDD,"aDD","sym12","DeclTrue")
hDDdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(hDDdD,"hDDdD","sym12","DeclTrue")
aDDdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(aDDdD,"aDDdD","sym12","DeclTrue")
hDDdupD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(hDDdupD,"hDDdupD","sym12","DeclTrue")
aDDdupD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(aDDdupD,"aDDdupD","sym12","DeclTrue")
hDDdDD = [[[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)] for l in range(3)]; declarerank4(hDDdDD,"hDDdDD","sym12_sym34","DeclTrue")

# Using rescaling matrix, compute rank - 2 tensors eps, gammabar, and Abar (Eq. 2 in notes.pdf)
epsDD         = [[ sympify(0) for i in range(3)] for j in range(3)]
tmpgammabarDD = [[ sympify(0) for i in range(3)] for j in range(3)] #was testing; declarerank2(gammabarDD,"gammabarDD","sym12","DeclTrue")
AbarDD        = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        epsDD[i][j] = hDD[i][j]*ReDD[i][j]
        tmpgammabarDD[i][j] = ghatDD[i][j] + epsDD[i][j]
        AbarDD[i][j] = aDD[i][j]*ReDD[i][j]


gammabarDD = [[sympify(0) for i in range(3)] for j in range(3)]; declarerank2(gammabarDD,"tmpgammabarDD","sym12","DeclTrue")

tmpgammabarUU = [[sympify(0) for i in range(3)] for j in range(3)];
gammabarDET = symbols('gammabarDET', positive="True")
gammabarDET = symm_matrix_inverter3x3(gammabarDD, tmpgammabarUU)

gammabarUU = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(gammabarUU,"tmpgammabarUU","sym12","DeclTrue")

# Compute Dhat {gammabar_ {ij} (Eq. 10 in notes.pdf)
epsDDdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            epsDDdD[j][k][i] = hDD[j][k]*ReDDdD[j][k][i] + ReDD[j][k]*hDDdD[j][k][i]
DhatgammabarDDdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            DhatgammabarDDdD[j][k][i] = epsDDdD[j][k][i]
for i in range(3):
    for j in range(3):
        for k in range(3):
            for d in range(3):
                DhatgammabarDDdD[j][k][i] += -GammahatUDD[d][i][j]*epsDD[d][k] - GammahatUDD[d][i][k]*epsDD[j][d]

# Next compute second partial derivatives of eps_ {jk} (Eq. 5 in notes.pdf)
epsDDdDD = [[[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)] for l in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                epsDDdDD[j][k][i][l] = ReDDdDD[j][k][i][l] * hDD[j][k] + ReDD[j][k] * hDDdDD[j][k][i][l] + hDDdD[j][k][l] * ReDDdD[j][k][i] + hDDdD[j][k][i] * ReDDdD[j][k][l]

# Then compute \partial_l Dhat_i {gammabar_ {jk} (Eq. 19 in notes.pdf)
DhatgammabarDDdDdD = [[[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)] for l in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                DhatgammabarDDdDdD[j][k][i][l] = epsDDdDD[j][k][i][l]
for d in range(3):
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    DhatgammabarDDdDdD[j][k][i][l] +=-(GammahatUDDdD[d][i][j][l] * epsDD[d][k] + GammahatUDDdD[d][i][k][l] * epsDD[d][j]) \
                                                      - (GammahatUDD[d][i][j] * epsDDdD[d][k][l] + GammahatUDD[d][i][k] * epsDDdD[d][j][l])
# Finally compute Dhat_l Dhat_i {gammabar_ {jk} (Eq. 27 in https://arxiv.org/pdf/1211.6632.pdf)
Dhat2gammabarDDdDD = [[[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)] for l in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                Dhat2gammabarDDdDD[i][j][k][l] = DhatgammabarDDdDdD[i][j][k][l]
for m in range(3):
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    Dhat2gammabarDDdDD[i][j][k][l] += - DhatgammabarDDdD[i][j][m] * GammahatUDD[m][l][k] \
                                                      - DhatgammabarDDdD[m][j][l] * GammahatUDD[m][i][k] \
                                                      - DhatgammabarDDdD[i][m][l] * GammahatUDD[m][j][k]

if Optimize == "True":
    costorig = 0
    costopt = 0
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    if i<=j:
                        costorig += compute_cost(Dhat2gammabarDDdDD[i][j][k][l])
                        Dhat2gammabarDDdDD[i][j][k][l] = ((Dhat2gammabarDDdDD[i][j][k][l]))
                        costopt  += compute_cost(Dhat2gammabarDDdDD[i][j][k][l])
                        print(i,j,k,l,"Dhat2gammabarDDdDD optimized cost:",costopt,"Orig cost:",costorig,"Speed-up factor: ",costorig/(costopt+1e-16))
    declarerank4(Dhat2gammabarDDdDD,"dummy","sym12","NoDecl")

# Then focus on hatted quantities related to vectors

# Set up vetU, betU, lambdaU, and all their needed derivatives
vetU = [sympify(0) for i in range(3)]; declarerank1(vetU,"vetU")
betU = [sympify(0) for i in range(3)]; declarerank1(betU,"betU")
alphadupD = [sympify(0) for i in range(3)]; declarerank1(alphadupD,"alphadupD")
cfdupD = [sympify(0) for i in range(3)]; declarerank1(cfdupD,"cfdupD")
trKdupD = [sympify(0) for i in range(3)]; declarerank1(trKdupD,"trKdupD")
lambdaU = [sympify(0) for i in range(3)]; declarerank1(lambdaU,"lambdaU")
vetUdD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(vetUdD,"vetUdD","nosym","DeclTrue")
vetUdupD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(vetUdupD,"vetUdupD","nosym","DeclTrue")
betUdupD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(betUdupD,"betUdupD","nosym","DeclTrue")
vetUdDD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(vetUdDD,"vetUdDD","sym23","DeclTrue")
lambdaUdD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(lambdaUdD,"lambdaUdD","nosym","DeclTrue")
lambdaUdupD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(lambdaUdupD,"lambdaUdupD","nosym","DeclTrue")

# Using rescaling vector, compute beta^i, B^i, and Lambdabar^i (Eq. 22 in https://arxiv.org/pdf/1211.6632.pdf )
betaU = [sympify(0) for i in range(3)]
BU = [sympify(0) for i in range(3)]
LambarU = [sympify(0) for i in range(3)]
for i in range(3):
    betaU[i]   = ReU[i] * vetU[i]
    BU[i]      = ReU[i] * betU[i]
    LambarU[i] = ReU[i] * lambdaU[i]

# Compute 1st derivs of Lambar, and 1st & 2nd derivs of beta (straightforward limit of Eqs 3 & 5 in notes.pdf )
LambarUdD = [[ sympify(0) for i in range(3)] for j in range(3)]
betaUdD = [[ sympify(0) for i in range(3)] for j in range(3)]
BUdupD = [[ sympify(0) for i in range(3)] for j in range(3)]
betaUdupD = [[ sympify(0) for i in range(3)] for j in range(3)]
LambarUdupD = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        LambarUdD[i][j] = ReUdD[i][j] * lambdaU[i] + ReU[i] * lambdaUdD[i][j]
        betaUdD[i][j] = ReUdD[i][j] * vetU[i] + ReU[i] * vetUdD[i][j]
        BUdupD[i][j] = ReUdD[i][j] * betU[i] + ReU[i] * betUdupD[i][j]
        betaUdupD[i][j] = ReUdD[i][j] * vetU[i] + ReU[i] * vetUdupD[i][j]
        LambarUdupD[i][j] = ReUdD[i][j] * lambdaU[i] + ReU[i] * lambdaUdupD[i][j]
betaUdDD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
for i in range(3):
    for j in range(3):
        for l in range(3):
            betaUdDD[j][i][l] = ReUdDD[j][i][l]*vetU[j] + ReU[j]*vetUdDD[j][i][l] + ReUdD[j][i]*vetUdD[j][l] + ReUdD[j][l]*vetUdD[j][i]
if Optimize=="True":
    costorig = 0
    costopt = 0
    for i in range(3):
        for j in range(3):
            for l in range(3):
                if(j>=l):
                    costorig += compute_cost(betaUdDD[j][i][l])
                    betaUdDD[j][i][l] = (betaUdDD[j][i][l])
                    costopt += compute_cost(betaUdDD[j][i][l])
                    print(i,j,l,"betaUdDD optimized cost:", costopt, "Orig cost:", costorig, "Speed-up factor: ",costorig / (costopt+1e-16))
declarerank3(betaUdDD,"dummy","sym23","NoDecl")

# Compute Dhat_i Lambar^j , according to Eq. 11 in notes. It is needed in computation of Ricci tensor (Eq 12 in https://arxiv.org/pdf/1211.6632.pdf)
DhatLambarUdD = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        DhatLambarUdD[j][i] = LambarUdD[j][i]
        for k in range(3):
            DhatLambarUdD[j][i] += GammahatUDD[j][i][k] * LambarU[k]

# Compute Dhat_k Dhat_i beta^j , according to Eq. 16 in notes. It is needed in RHS of Lambar in BSSN eqs. (Eq 9 e in https://arxiv.org/pdf/1211.6632.pdf)
Dhat2betaUdDD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            Dhat2betaUdDD[j][i][k] = betaUdDD[j][i][k]
            for d in range(3):
                Dhat2betaUdDD[j][i][k] += GammahatUDDdD[j][i][d][k] * betaU[d] + GammahatUDD[j][i][d] * betaUdD[d][k] \
                                            + GammahatUDD[j][k][d] * betaUdD[d][i] - GammahatUDD[d][k][i] * betaUdD[j][d]
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                for d in range(3):
                    Dhat2betaUdDD[j][i][k] += (GammahatUDD[j][l][k]*GammahatUDD[l][i][d] - GammahatUDD[j][l][d]*GammahatUDD[l][i][k])*betaU[d]

# Next define BSSN quantities

# Declare upwinded and centered derivatives
trKdD = [sympify(0) for i in range(3)]; declarerank1(trKdD,"trKdD")
alphadD = [sympify(0) for i in range(3)]; declarerank1(alphadD,"alphadD")
cfdD = [sympify(0) for i in range(3)]; declarerank1(cfdD,"cfdD")
detgdD = [sympify(0) for i in range(3)]; declarerank1(detgdD,"detgdD")
alphadDD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(alphadDD,"alphadDD","sym12","DeclTrue")
cfdDD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(cfdDD,"cfdDD","sym12","DeclTrue")
detgdDD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(detgdDD,"detgdDD","sym12","DeclTrue")

AbarDDdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(AbarDDdD,"AbarDDdD","sym12","DeclTrue")
AbarDDdupD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(AbarDDdupD,"AbarDDdupD","sym12","DeclTrue")
gammabarDDdD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(gammabarDDdD,"gammabarDDdD","sym12","DeclTrue")
gammabarDDdupD = [[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(gammabarDDdupD,"gammabarDDdupD","sym12","DeclTrue")
# gammabarDDdDD = [[[[ sympify(0) for i in range(3)] for j in range(3)] for k in range(3)] for l in range(3)]; declarerank4(gammabarDDdDD,"gammabarDDdDD","sym12_sym34","DeclTrue") # FIXME: WHY IS gammabarDDdDD NOT NEEDED?!

for i in range(3):
    for j in range(3):
        for k in range(3):
            # AbarDD = aDD ReDD
            # -> AbarDDdD = aDDdD ReDD + aDD ReDDdD
            AbarDDdD[i][j][k] = aDDdD[i][j][k] * ReDD[i][j] + aDD[i][j] * ReDDdD[i][j][k]
            AbarDDdupD[i][j][k] = aDDdupD[i][j][k] * ReDD[i][j] + aDD[i][j] * ReDDdD[i][j][k]
            #gammabarDD = ghatDD + epsDD
            #           = ghatDD + hDD ReDD
            # -> gammabarDDdD = ghatDDdD + hDDdD ReDD + hDD ReDDdD
            # -> gammabarDDdDD =ghatDDdDD + hDDdDD ReDD + hDDdD ReDDdD + hDDdD ReDDdD + hDD ReDDdDD
            gammabarDDdD[i][j][k] = ghatDDdD[i][j][k] + hDDdD[i][j][k] * ReDD[i][j] + hDD[i][j] * ReDDdD[i][j][k]
            gammabarDDdupD[i][j][k] = ghatDDdD[i][j][k] + hDDdupD[i][j][k] * ReDD[i][j] + hDD[i][j] * ReDDdD[i][j][k]
            # for l in range(3): # FIXME: WHY IS gammabarDDdDD NOT NEEDED?!
            #     gammabarDDdDD[i][j][k][l] = ghatDDdDD[i][j][k][l] + \
            #                                 hDDdDD[i][j][k][l] * ReDD[i][j] + \
            #                                 hDDdD[i][j][k] * ReDDdD[i][j][l] + \
            #                                 hDDdD[i][j][l] * ReDDdD[i][j][k] + \
            #                                 hDD[i][j] * ReDDdDD[i][j][k][l]

GammabarDDD = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
GammabarUDD = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
DGammaDDD = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]
DGammaUDD = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]

for i in range(3):
    for j in range(3):
        for k in range(3):
            GammabarDDD[i][j][k] = (Rational(1,2)) * (gammabarDDdD[i][j][k] + gammabarDDdD[i][k][j] - gammabarDDdD[j][k][i])
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                GammabarUDD[i][j][k] += gammabarUU[i][l] * GammabarDDD[l][j][k]
for i in range(3):
    for j in range(3):
        for k in range(3):
            #print("jj1",i,j,k,compute_cost(GammabarUDD[i][j][k] - GammahatUDD[i][j][k]))
            DGammaUDD[i][j][k] = GammabarUDD[i][j][k] - GammahatUDD[i][j][k]
            #print("jj2",i,j,k,compute_cost(DGammaUDD[i][j][k]))
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                DGammaDDD[i][j][k] += gammabarDD[i][l] * DGammaUDD[l][j][k]

# increases computational cost of CSE-generated code
# if Optimize == "True":
#     for i in range(3):
#         for j in range(3):
#             for k in range(3):
#                 beforeUDD = compute_cost(DGammaUDD[i][j][k])
#                 beforeDDD = compute_cost(DGammaDDD[i][j][k])
#                 DGammaUDD[i][j][k] = (DGammaUDD[i][j][k],ratio=1.0, measure=compute_cost)
#                 DGammaDDD[i][j][k] = (DGammaDDD[i][j][k],ratio=1.0, measure=compute_cost)
#                 afterUDD  = compute_cost(DGammaUDD[i][j][k])
#                 afterDDD  = compute_cost(DGammaDDD[i][j][k])
#                 print(i, j, k, beforeUDD/afterUDD, beforeDDD/afterDDD)

DGammaU = [sympify(0) for i in range(3)]
AbarUD = [[ sympify(0) for i in range(3)] for j in range(3)]
AbarUU = [[ sympify(0) for i in range(3)] for j in range(3)]

for i in range(3):
    for j in range(3):
        for k in range(3):
            DGammaU[i] += gammabarUU[j][k]*DGammaUDD[i][j][k]
            AbarUD[i][j] += gammabarUU[i][k] * AbarDD[k][j]
for i in range(3):
    for j in range(3):
        for l in range(3):
            AbarUU[i][j] += gammabarUU[i][l] * AbarUD[j][l]

# *******************
# * LIE DERIVATIVES *
# *******************
# Next we compute Lie derivatives of gammabar and Abar.
#   * Note that gammabar and Abar are ordinary tensors, with density weight 0,
#   * unlike orig. BSSN (Eqs.24 and 25 in http://arxiv.org/pdf/1205.5111.pdf).
#   * Recall that in the original BSSN equations, \tilde{\gamma}_{ij}
#   * and\tilde {A} _ {ij} have tensor weight -2/3. Thus according to the
#   * definition of Lie derivative (https://en.wikipedia.org/wiki/Lie_derivative),
#   * the term with the (-2/3) multiplying the divergence of the shift in
#   * Eqs. 24 and 25 in http://arxiv.org/pdf/1205.5111.pdf DISAPPEARS.
LbetagammabarDD = [[ sympify(0) for i in range(3)] for j in range(3)]
LbetaAbarDD = [[ sympify(0) for i in range(3)] for j in range(3)]

for i in range(3):
    for j in range(3):
        for k in range(3):
            LbetagammabarDD[i][j] += betaU[k] * gammabarDDdupD[i][j][k] # First term
            LbetagammabarDD[i][j] += gammabarDD[k][i]*betaUdD[k][j] + gammabarDD[k][j]*betaUdD[k][i] # Second (symmetrized) term

            LbetaAbarDD[i][j] += betaU[k]*AbarDDdupD[i][j][k] # First term
            LbetaAbarDD[i][j] += AbarDD[k][i]*betaUdD[k][j] + AbarDD[k][j]*betaUdD[k][i] # Second (symmetrized) term

# Eq. 11.52 in Shapiro & Baumgarte:
LbetatrK = sympify(0)
for i in range(3):
    LbetatrK += betaU[i] * trKdupD[i]

cf = symbols('cf',real=True)
phidD = [sympify(0) for i in range(3)]; declarerank1(phidD,"phidD")
phidupD = [sympify(0) for i in range(3)]; declarerank1(phidupD,"phidupD")
phidDD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(phidDD,"phidDD","sym12","DeclTrue")
# Convert from generic conformal factor (cf) to phi
# Default case: phi evolution:
# phi = cf
if CFEvolution == "EvolvePhi" : # cf = phi
    psim4 = exp(-4 * cf)
    for i in range(3):
        phidD[i] = cfdD[i]
        phidupD[i] = cfdupD[i]
        for j in range(3):
            phidDD[i][j] = cfdDD[i][j]
elif CFEvolution == "EvolveChi" : # chi = Exp[-4 phi]
    # phi = -(1/4) Log[cf]
    psim4 = cf
    for i in range(3):
        phidD[i] = -cfdD[i]/(4*cf)
        phidupD[i] = -cfdupD[i]/(4*cf)
        for j in range(3):
            phidDD[i][j] = 1/(4*cf)*( cfdD[i]*cfdD[j]/cf - cfdDD[i][j])
elif CFEvolution == "EvolveW": # W = Exp[-2 phi]
    # phi = -(1 / 2)Log[cf]
    psim4 = cf*cf
    for i in range(3):
        phidD[i] = -cfdD[i] / (2 * cf)
        phidupD[i] = -cfdupD[i] / (2 * cf)
        for j in range(3):
            phidDD[i][j] = 1/(2 * cf) * ( cfdD[i]*cfdD[j]/cf - cfdDD[i][j])
else:
    print("CFEvolution == ",CFEvolution,"is not supported in BSSN RHS. Check for typos, special characters, or stray spaces.")
    exit(1)

# phi is simply a scalar with tensor density zero.
Lbetaphi = sympify(0)
for i in range(3):
    Lbetaphi += betaU[i] * phidupD[i]

# For advecting lapse gauge
Lbetaalpha = sympify(0)
if AdvectAlpha == "True":
    for i in range(3):
        Lbetaalpha+=betaU[i]*alphadupD[i]

# Lie derivatives of\bar {\Lambda}^i.
# *Again, \bar {\Lambda}^i is an ordinary tensor with density weight 0, unlike
# *     \tilde {\Gamma}^i in original BSSN.Thus the Lie derivative will be given by
# *(see https:// en.wikipedia.org/wiki/Lie_derivative) :
# * Lbeta_LambarU^i = \beta^j\partial_j\Lambar^i - \partial_j\beta^i\Lambar^j
LbetaLambarU = [sympify(0) for i in range(3)]
for i in range(3):
    for j in range(3):
        LbetaLambarU[i] += betaU[j] * LambarUdupD[i][j] - betaUdD[i][j] * LambarU[j]

# For LieShift shift gauge
LbetaBU = [sympify(0) for i in range(3)]
for i in range(3):
    for j in range(3):
        LbetaBU[i] += betaU[j] * BUdupD[i][j] - betaUdD[i][j] * BU[j]

# *************************************
# COVARIANT DERIVATIVES OF PHI & ALPHA
# *************************************

# lapse is a scalar, so \bar D_i' s are easy to compute:
DbaralphaD = [sympify(0) for i in range(3)]
DbaralphaU = [sympify(0) for i in range(3)]
for i in range(3):
    DbaralphaD[i] = alphadD[i]
    for j in range(3):
        DbaralphaU[i] += gammabarUU[i][j] * alphadD[j]

# Bottom of page 56 in Shapiro & Baumgarte: "phi behaves as a scalar under\bar D_i's" :
DbarphiD = [sympify(0) for i in range(3)]
DbarphiU = [sympify(0) for i in range(3)]
for i in range(3):
    DbarphiD[i] = phidD[i]
    for j in range(3):
        DbarphiU[i] += gammabarUU[i][j] * phidD[j]

# Dbar2phiDD = Dbar_i Dbar_j\phi:
Dbar2phiDD = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        Dbar2phiDD[i][j] = phidDD[i][j]
        for l in range(3):
            Dbar2phiDD[i][j] -= phidD[l] * GammabarUDD[l][i][j]

# Dbar_phi2 = Dbarphi^i Dbarphi_i
Dbarphi2 = sympify(0)
for i in range(3):
    Dbarphi2 += DbarphiD[i] * DbarphiU[i]

# Derivatives of the lapse
Dbar2alphaDD = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        Dbar2alphaDD[i][j] = alphadDD[i][j]
        for l in range(3):
            Dbar2alphaDD[i][j] -= alphadD[l] * GammabarUDD[l][i][j]
Dbar2alpha = sympify(0)
for i in range(3):
    for j in range(3):
        Dbar2alpha += gammabarUU[i][j]*Dbar2alphaDD[i][j]

# All terms involving derivatives of phi in Eq.9 b, http://arxiv.org/pdf/1211.6632.pdf
alpha = symbols('alpha',real=True)
phitermsDD = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        phitermsDD[i][j]  = -2 * alpha * Dbar2phiDD[i][j]     # 1st phi term
        phitermsDD[i][j] += +4 * alpha * DbarphiD[i]*DbarphiD[j]  # 2nd phi term
        phitermsDD[i][j] += +2 * (DbaralphaD[i]*DbarphiD[j] + DbaralphaD[j]*DbarphiD[i]) # 3rd phi term

# *****************************************
# COVARIANT DERIVATIVES OF GAMMABAR & SHIFT
# *****************************************

# Rescale the metric determinant
detg = symbols('detg',real=True)
detgammabar = detgammahat*detg

detgammabardD = [sympify(0) for i in range(3)]
detgammabardDD = [[ sympify(0) for i in range(3)] for j in range(3)]

for i in range(3):
    detgammabardD[i] = detgammahatdD[i] * detg + detgammahat * detgdD[i]
    for j in range(3):
        detgammabardDD[i][j] = detgammahatdDD[i][j]*detg + \
                               detgammahatdD[i]*detgdD[j] + detgammahatdD[j]*detgdD[i] + detgammahat*detgdDD[i][j]

# ************************************
# Used for covariant Gamma-driver condition
# \bar D_j\beta^i = \partial_j\beta^i + \Gammabar^i_ {jl}\beta^l
DbarbetaUD    = [[ sympify(0) for i in range(3)] for j in range(3)]
DbarBUD       = [[ sympify(0) for i in range(3)] for j in range(3)]
DbarLambarUD = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        DbarbetaUD[i][j]   =   betaUdupD[i][j] # First term
        DbarBUD[i][j]      =      BUdupD[i][j] # First term
        DbarLambarUD[i][j] = LambarUdupD[i][j] # First term
        for l in range(3):
            DbarbetaUD[i][j]   += GammabarUDD[i][j][l] *   betaU[l] # Second term
            DbarBUD[i][j]      += GammabarUDD[i][j][l] *      BU[l] # Second term
            DbarLambarUD[i][j] += GammabarUDD[i][j][l] * LambarU[l] # Second term

# OLD WAY OF COMPUTING Dbarbetacontraction:
# \bar D_i\beta^i
# Dbarbetacontraction = sympify(0)
# Using Christoffel symbols
# Dbarbetacontraction += DbarbetaUD[i][i]
# ************************************

# ***************************************
# NEW WAY, CONSISTENT WITH BAUMGARTE CODE
# \bar D_i\beta^i
# Using the metric determinant
Dbarbetacontraction = sympify(0)
for i in range(3):
    Dbarbetacontraction += betaUdD[i][i] + betaU[i]*detgammabardD[i]/(2*detgammabar)
# ***************************************

# \bar {D}^i\bar {D} _j\beta^j = gammabar^{ik}[beta^j_{,jk} +1/2*(gammabar_{,jk} beta^j - gammabar_{,j} gammabar_{,k} beta^j/gammabar + gammabar_{,j} beta^j_{,k})/gammabar]
# (See notes.tex for derivation of full expression.)

# First compute\bar{D}_i \bar{D}_j\beta^j:
Dbar2betacontractionD = [sympify(0) for i in range(3)]
for j in range(3):
    for k in range(3):
        Dbar2betacontractionD[k] += betaUdDD[j][j][k] # First term
        Dbar2betacontractionD[k] += (Rational(1,2))*(detgammabardDD[j][k]*betaU[j] -
                                             detgammabardD[j]*detgammabardD[k]*betaU[j]/detgammabar +
                                             detgammabardD[j]*betaUdD[j][k])/detgammabar # Second term
Dbar2betacontractionU = [sympify(0) for i in range(3)]
for i in range(3):
    for k in range(3):
        Dbar2betacontractionU[i] += gammabarUU[i][k]*Dbar2betacontractionD[k]


# *******************************
# RICCI TENSOR, WRT BARRED METRIC
# *******************************
tmpRbarDD = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                for m in range(3):
                    tmpRbarDD[i][j] += gammabarUU[k][l]*((DGammaUDD[m][k][i] * DGammaDDD[j][m][l] + DGammaUDD[m][k][j] * DGammaDDD[i][m][l]) + DGammaUDD[m][i][k] * DGammaDDD[m][j][l]) # Fourth term

# Super slow, no real improvement.
#         if i>=j:
#             print(i,j,"before tmpRbarDD",compute_cost(tmpRbarDD[i][j]))
#             tmpRbarDD[i][j] = (tmpRbarDD[i][j], ratio=1.0,measure=compute_cost)
#             print(i,j,"after  tmpRbarDD",compute_cost(tmpRbarDD[i][j]))
# declarerank2(tmpRbarDD,"sym12","NoDecl")


for i in range(3):
    for j in range(3):
        for k in range(3):
            for l in range(3):
                tmpRbarDD[i][j] += -(Rational(1,2)) * gammabarUU[k][l] * Dhat2gammabarDDdDD[i][j][k][l] # First term
for i in range(3):
    for j in range(3):
        for k in range(3):
            tmpRbarDD[i][j] += (Rational(1,2)) * (gammabarDD[k][i] * DhatLambarUdD[k][j] + gammabarDD[k][j] * DhatLambarUdD[k][i]) # Second term
            tmpRbarDD[i][j] += DGammaU[k]*(Rational(1,2))*(DGammaDDD[i][j][k] + DGammaDDD[j][i][k]) # Third term

# Kronecker Delta:
deltaUD = [[ sympify(0) for i in range(3)] for j in range(3)]
deltaDD = [[sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(3):
        deltaUD[i][j] = 0
        if i==j:
            deltaUD[i][j] = 1
        deltaDD[i][j] = deltaUD[i][j]

# *********************************************************
# TRACE-FREE PARTS OF RbarDD, phitermsDD, and Dbar2_alphaDD
# *********************************************************

# For a tensor P_{ij}, TF(P)_{ij} = P_{ij} - gammabar_{ij} gammabar^{lm} P_{lm}*(1/3)
RbarTFDD = [[ sympify(0) for i in range(3)] for j in range(3)]
phitermsTFDD = [[ sympify(0) for i in range(3)] for j in range(3)]
Dbar2alphaTFDD = [[ sympify(0) for i in range(3)] for j in range(3)]
RbarDD = [[ sympify(0) for i in range(3)] for j in range(3)]; declarerank2(RbarDD,"RbarDD","sym12","DeclTrue")
for i in range(3):
    for j in range(3):
        RbarTFDD[i][j] = RbarDD[i][j]
        phitermsTFDD[i][j] = phitermsDD[i][j]
        Dbar2alphaTFDD[i][j] = Dbar2alphaDD[i][j]

for i in range(3):
    for j in range(3):
        for l in range(3):
            for m in range(3):
                RbarTFDD[i][j]       -= gammabarDD[i][j] * gammabarUU[l][m] * RbarDD[l][m] * (Rational(1,3)) # The "3" in 1/3 comes from dimension!
                phitermsTFDD[i][j]   -= gammabarDD[i][j] * gammabarUU[l][m] * phitermsDD[l][m] * (Rational(1,3)) # The "3" in 1/3 comes from dimension!
                Dbar2alphaTFDD[i][j] -= gammabarDD[i][j] * gammabarUU[l][m] * Dbar2alphaDD[l][m] * (Rational(1,3)) # The "3" in 1/3 comes from dimension!

# *********
# BSSN RHSs
# *********

# Set RHS variables

# Trace correction term in gammabarrhsDD
trAbar = sympify(0)
for i in range(3):
    for j in range(3):
        trAbar += gammabarUU[i][j] * AbarDD[i][j]

# Eq. 9a in http://arxiv.org/pdf/1211.6632.pdf:
gammabarrhsDD = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(i,3): # No need to compute symmetric components
        gammabarrhsDD[i][j] += LbetagammabarDD[i][j] # Zeroth term
        gammabarrhsDD[i][j] += (-Rational(2,3)) * gammabarDD[i][j] * Dbarbetacontraction # First term
        gammabarrhsDD[i][j] += (-2) * alpha * AbarDD[i][j]  # Second term
        gammabarrhsDD[i][j] += (Rational(2,3)) * alpha * gammabarDD[i][j] * trAbar # Drive trAbar to zero (See third term in Eq 21a in Brown (2009))

# Eq. 9b in http://arxiv.org/pdf/1211.6632.pdf, split for use by PIRK2. Add up L2 and L3 terms to get full RHS expression:

# L2 operator for Abar_{ij}, as defined in Eq. B1 of https://arxiv.org/pdf/1211.6632.pdf
L2AbarrhsDD = [[ sympify(0) for i in range(3)] for j in range(3)]
for i in range(3):
    for j in range(i,3): # No need to compute symmetric components
        L2AbarrhsDD[i][j] = psim4 * (phitermsTFDD[i][j] - Dbar2alphaTFDD[i][j] + alpha * RbarTFDD[i][j]) # Fourth term

# L3 operator for Abar_ {ij}, as defined in Eq. B2 of https://arxiv.org/pdf/1211.6632.pdf .
#   Also, "Lie derivative terms and matter source terms are always included in the explicitly treated parts."
L3AbarrhsDD = [[ sympify(0) for i in range(3)] for j in range(3)]
trK = symbols('trK',real=True)
for i in range(3):
    for j in range(3): # No need to compute symmetric components
        L3AbarrhsDD[i][j] += LbetaAbarDD[i][j] # Zeroth term
        L3AbarrhsDD[i][j] += (-Rational(2,3))*AbarDD[i][j]*Dbarbetacontraction # First term
        L3AbarrhsDD[i][j] += alpha*AbarDD[i][j]*trK # Third term
for i in range(3):
    for j in range(3): # No need to compute symmetric components
        for l in range(3):
            L3AbarrhsDD[i][j] += (-2) * alpha * AbarDD[i][l] * AbarUD[l][j] # Second term

# Eq. 9c in http://arxiv.org/pdf/1211.6632.pdf

# Note that Lie derivative can be identified from http://arxiv.org/pdf/1205.5111.pdf, Eq 23.
#  Also, chi=exp(-4*phi) -> \partial_t chi=-4*exp(-4*phi)*(\partial_t phi)=-4*chi*(\partial_t phi) -> chi_rhs=-4*chi*phi_rhs.
cfrhs  = Lbetaphi # Zeroth term; Lie derivative of a scalar
cfrhs += (Rational(1,6)) * Dbarbetacontraction # First term
cfrhs += (Rational(1,6)) * -alpha*trK # Second term
if CFEvolution == "EvolveChi":
    cfrhs *= -4 * cf
elif CFEvolution == "EvolveW":
    cfrhs *= -2 * cf

# Eq. 9d in http://arxiv.org/pdf/1211.6632.pdf, split for use by PIRK2. Add up L2 and L3 terms to get full RHS expression:
L2trKrhs = -psim4*Dbar2alpha # Third term
for i in range(3):
    L2trKrhs += -psim4 * 2 * DbaralphaU[i] * DbarphiD[i] # Fourth term, FIXME : missing matter source terms

# L3 operator for trK, as defined in Eq. B4 of https://arxiv.org/pdf/1211.6632.pdf
#  NOTE THERE IS A TYPO IN B3 & B4; THE AIJ CONTRACTION SHOULD BE IN L3 NOT L2.
#  Also, "Lie derivative terms and matter source terms are always included in the explicitly treated parts."
L3trKrhs  = LbetatrK # Zeroth term
L3trKrhs += alpha / 3*trK*trK # First term
for i in range(3):
    for j in range(3):
        L3trKrhs += alpha * (AbarDD[i][j] * AbarUU[i][j]) # Second term


# Eq. 9e in http://arxiv.org/pdf/1211.6632.pdf, split for use by PIRK2. Add up L2 and L3 terms to get full RHS expression:
L2LambarrhsU = [ sympify(0) for i in range(3)]
for i in range(3):
    L2LambarrhsU[i] += (Rational(1,3)) * Dbar2betacontractionU[i] # Third term
    for j in range(3):
        L2LambarrhsU[i] += -(Rational(4,3))*alpha*gammabarUU[i][j]*trKdD[j] # Fifth term, FIXME: missing matter source terms
        for k in range(3):
            L2LambarrhsU[i] += gammabarUU[j][k]*Dhat2betaUdDD[i][j][k] # First term
            L2LambarrhsU[i] += -2*AbarUU[j][k]*(deltaUD[i][j]*alphadD[k] - 6*alpha*deltaUD[i][j]*phidD[k] - alpha*DGammaUDD[i][j][k]) # Fourth term

# L3 operator for Lambar^i, as defined in Eq. B6 of https://arxiv.org/pdf/1211.6632.pdf .
#   Also, "Lie derivative terms and matter source terms are always included in the explicitly treated parts."
L3LambarrhsU = [ sympify(0) for i in range(3)]
for i in range(3):
    L3LambarrhsU[i] += LbetaLambarU[i] # Zeroth term
    L3LambarrhsU[i] += (Rational(2,3))*DGammaU[i]*Dbarbetacontraction # Second term

# Eq. 15 in https://arxiv.org/pdf/1211.6632.pdf, except with advecting lapse:
alpharhs = -2*alpha*trK # First term
if AdvectAlpha == "True":
    alpharhs += Lbetaalpha

# Eq. 16a: non-advecting, Gamma-driving shift:
betarhsU = [sympify(0) for i in range(3)]
for i in range(3):
    betarhsU[i] = BU[i]
    if AdvectShift == "True" and LieShift == "False":
        if CovariantShift == "True":
            for j in range(3):
                # First term, Eq. 20b in https://arxiv.org/pdf/0902.3652.pdf
                betarhsU[i] += betaU[j]*DbarbetaUD[i][j]
        else:
            for j in range(3):
                betarhsU[i] += betaU[j]*betaUdupD[i][j]

# Eq. 16b: non-advecting, Gamma-driving shift, split for use by PIRK2. Add up L2 and L3 terms to get full RHS expression:
# L2 operator for B^i, as defined in Eq. B7 of https://arxiv.org/pdf/1211.6632.pdf
L2BrhsU = [sympify(0) for i in range(3)]
for i in range(3):
    if CovariantShift == "True":
        # Covariant shift condition: Eq 20c in https://arxiv.org/pdf/0902.3652.pdf :
        L2BrhsU[i] = (Rational(3, 4)) * (L2LambarrhsU[i] + L3LambarrhsU[i]) # Second term
        for j in range(3):
            L2BrhsU[i] -= (Rational(3, 4)) * betaU[j] * DbarLambarUD[i][j] # Third term
    elif LieShift == "True":
        L2BrhsU[i] = (Rational(3, 4)) * (L2LambarrhsU[i] + L3LambarrhsU[i] - LbetaLambarU[i])  # Second term
    else:
        L2BrhsU[i] = (Rational(3, 4)) * (L2LambarrhsU[i] + L3LambarrhsU[i]) # FIXME: which term?
        for j in range(3):
            L2BrhsU[i] -= (Rational(3, 4)) * betaU[j] * LambarUdupD[i][j] # FIXME: which term?

# L3 operator for B^i, as defined in Eq. B8 of https://arxiv.org/pdf/1211.6632.pdf .
#   Also, "Lie derivative terms and matter source terms are always included in the explicitly treated parts."
L3BrhsU = [sympify(0) for i in range(3)]
eta = symbols('eta',positive=True)
for i in range(3):
    L3BrhsU[i] = - eta * BU[i]
    if AdvectB == "True":
        if CovariantShift == "True":
            for j in range(3):
                # First term, Eq. 20c in https://arxiv.org/pdf/0902.3652.pdf
                L3BrhsU[i] += betaU[j] * DbarBUD[i][j]
        else:
            for j in range(3):
                L3BrhsU[i] += betaU[j] * BUdupD[i][j]
    elif LieShift == "True":
        L3BrhsU[i] += LbetaBU[i]

# Finally, compute rescaled RHS variables for rank-1 and rank-2 tensors
vetrhsU = [sympify(0) for i in range(3)]
hrhsDD = [[ sympify(0) for i in range(3)] for j in range(3)]
L2betrhsU = [sympify(0) for i in range(3)]
L3betrhsU = [sympify(0) for i in range(3)]
L2lambdarhsU = [sympify(0) for i in range(3)]
L3lambdarhsU = [sympify(0) for i in range(3)]
L2arhsDD = [[ sympify(0) for i in range(3)] for j in range(3)]
L3arhsDD = [[ sympify(0) for i in range(3)] for j in range(3)]

for i in range(3):
    vetrhsU[i] = betarhsU[i] / ReU[i]
    L2betrhsU[i] = L2BrhsU[i] / ReU[i]
    L3betrhsU[i] = L3BrhsU[i] / ReU[i]
    L2lambdarhsU[i] = L2LambarrhsU[i] / ReU[i]
    L3lambdarhsU[i] = L3LambarrhsU[i] / ReU[i]
    for j in range(3):
        hrhsDD[i][j] = gammabarrhsDD[i][j] / ReDD[i][j]
        L2arhsDD[i][j] = L2AbarrhsDD[i][j] / ReDD[i][j]
        L3arhsDD[i][j] = L3AbarrhsDD[i][j] / ReDD[i][j]


if EnableKreissOligerDissipation == "True":
    cfdDKO    = [sympify(0) for i in range(3)]; declarerank1(cfdDKO, "cfdDKO")
    alphadDKO = [sympify(0) for i in range(3)]; declarerank1(alphadDKO, "alphadDKO")
    trKdDKO   = [sympify(0) for i in range(3)]; declarerank1(trKdDKO, "trKdDKO")
    vetUdDKO  = [[sympify(0) for i in range(3)] for j in range(3)];   declarerank2(vetUdDKO, "vetUdDKO", "NoSym", "DeclTrue")
    betUdDKO  = [[sympify(0) for i in range(3)] for j in range(3)];   declarerank2(betUdDKO, "betUdDKO", "NoSym", "DeclTrue")
    lambdaUdDKO = [[sympify(0) for i in range(3)] for j in range(3)]; declarerank2(lambdaUdDKO, "lambdaUdDKO", "NoSym", "DeclTrue")
    hDDdDKO = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(hDDdDKO, "hDDdDKO", "sym12", "DeclTrue")
    aDDdDKO = [[[sympify(0) for i in range(3)] for j in range(3)] for k in range(3)]; declarerank3(aDDdDKO, "aDDdDKO", "sym12", "DeclTrue")
    diss_ko_strength = symbols('diss_ko_strength', positive=True)

    for dirn in range(3):
        if EnableKO_direction[dirn] == "True":
            cfrhs    += diss_ko_strength*cfdDKO[dirn]
            alpharhs += diss_ko_strength*alphadDKO[dirn]
            L3trKrhs += diss_ko_strength*trKdDKO[dirn]
            for i in range(3):
                vetrhsU[i]      += diss_ko_strength*vetUdDKO[i][dirn]
                L3betrhsU[i]    += diss_ko_strength*betUdDKO[i][dirn]
                L3lambdarhsU[i] += diss_ko_strength*lambdaUdDKO[i][dirn]
                for j in range(3):
                    hrhsDD[i][j]   += diss_ko_strength*hDDdDKO[i][j][dirn]
                    L3arhsDD[i][j] += diss_ko_strength*aDDdDKO[i][j][dirn]

stoptimer_bssnRHSs = time.time()
print("Finished generating BSSN RHS algebraic expressions in \t"+str(round(stoptimer_bssnRHSs-starttimer_bssnRHSs,2))+" seconds")

# We split the BSSN RHSs into two separate parts, to give the compiler a chance of optimizing/vectorizing both.
#   Otherwise it gives up on such a large chunk of code, and we're left with ~2x slower runtimes!
def_tmpgammabarsDD = [tmpgammabarDD[0][0],'tmpgammabarDD00',tmpgammabarDD[0][1],"tmpgammabarDD01",tmpgammabarDD[0][2],"tmpgammabarDD02",
                      tmpgammabarDD[1][1],"tmpgammabarDD11",tmpgammabarDD[1][2],"tmpgammabarDD12",tmpgammabarDD[2][2],"tmpgammabarDD22"]
def_tmpgammabarsUU = [tmpgammabarUU[0][0],"tmpgammabarUU00",tmpgammabarUU[0][1],"tmpgammabarUU01",tmpgammabarUU[0][2],"tmpgammabarUU02",
                      tmpgammabarUU[1][1],"tmpgammabarUU11",tmpgammabarUU[1][2],"tmpgammabarUU12",tmpgammabarUU[2][2],"tmpgammabarUU22"]
def_tmpgammabarsall = def_tmpgammabarsDD + def_tmpgammabarsUU

BSSN_rhs_vars = [sympify(0) for i in range(4)]
BSSN_aux_vars = [sympify(0) for i in range(4)]

for i in range(4):
    BSSN_aux_vars[i] = def_tmpgammabarsall
BSSN_rhs_vars[0] = [L2lambdarhsU[0]+L3lambdarhsU[0],"gfs_rhs[IDX4pt(LAMB1,idx)]",L2betrhsU[0]+L3betrhsU[0],"gfs_rhs[IDX4pt(BET1,idx)]"]
BSSN_rhs_vars[1] = [L2lambdarhsU[1]+L3lambdarhsU[1],"gfs_rhs[IDX4pt(LAMB2,idx)]",L2betrhsU[1]+L3betrhsU[1],"gfs_rhs[IDX4pt(BET2,idx)]"]
BSSN_rhs_vars[2] = [L2lambdarhsU[2]+L3lambdarhsU[2],"gfs_rhs[IDX4pt(LAMB3,idx)]",L2betrhsU[2]+L3betrhsU[2],"gfs_rhs[IDX4pt(BET3,idx)]"]
BSSN_rhs_vars[3] = [vetrhsU[0],"gfs_rhs[IDX4pt(VET1,idx)]",vetrhsU[1],"gfs_rhs[IDX4pt(VET2,idx)]", vetrhsU[2],"gfs_rhs[IDX4pt(VET3,idx)]",
                    hrhsDD[0][0],"gfs_rhs[IDX4pt(H11,idx)]",hrhsDD[0][1],"gfs_rhs[IDX4pt(H12,idx)]",hrhsDD[0][2],"gfs_rhs[IDX4pt(H13,idx)]",
                    hrhsDD[1][1],"gfs_rhs[IDX4pt(H22,idx)]",hrhsDD[1][2],"gfs_rhs[IDX4pt(H23,idx)]",hrhsDD[2][2],"gfs_rhs[IDX4pt(H33,idx)]",
                    cfrhs,"gfs_rhs[IDX4pt(CF,idx)]",alpharhs,"gfs_rhs[IDX4pt(ALPHA,idx)]",
                    L2trKrhs+L3trKrhs,"gfs_rhs[IDX4pt(TRK,idx)]",
                    L2arhsDD[0][0]+L3arhsDD[0][0],"gfs_rhs[IDX4pt(A11,idx)]",
                    L2arhsDD[0][1]+L3arhsDD[0][1],"gfs_rhs[IDX4pt(A12,idx)]",
                    L2arhsDD[0][2]+L3arhsDD[0][2],"gfs_rhs[IDX4pt(A13,idx)]",
                    L2arhsDD[1][1]+L3arhsDD[1][1],"gfs_rhs[IDX4pt(A22,idx)]",
                    L2arhsDD[1][2]+L3arhsDD[1][2],"gfs_rhs[IDX4pt(A23,idx)]",
                    L2arhsDD[2][2]+L3arhsDD[2][2],"gfs_rhs[IDX4pt(A33,idx)]"]

BSSN_Ricci_vars = [sympify(0) for i in range(2)]
BSSN_Ricci_vars[0] = [tmpRbarDD[0][0], "gfs_aux[IDX4pt(AUXR11,idx)]",tmpRbarDD[1][1], "gfs_aux[IDX4pt(AUXR22,idx)]",tmpRbarDD[2][2], "gfs_aux[IDX4pt(AUXR33,idx)]"]
BSSN_Ricci_vars[1] = [tmpRbarDD[0][1], "gfs_aux[IDX4pt(AUXR12,idx)]",tmpRbarDD[0][2], "gfs_aux[IDX4pt(AUXR13,idx)]",tmpRbarDD[1][2], "gfs_aux[IDX4pt(AUXR23,idx)]"]

# Next generate the C code for declaring and allocating memory for gridfunctions
#   Gridfunctions come in two classes:
#   (1) evolved quantities, and (2) not evolved quantities. The latter are defined
#   inside BSSN_aux_gridfunctions[] and the former can be teased out of the
#   expressions that have been generated, using get_full_list_evol_gfs()
# The full list of evolved
# NRPy_file_output_define_gridfunctions(OUTDIR+"/diagnostics/NRPy_codegen/list_of_gridfunctions.h",func_BSSN_gf_define_alias,
#                                       BSSN_evolved_variable_list,BSSN_aux_gridfunctions)
# Append auxiliary (not-evolved) gridfunctions to end of list of BSSN_gfs
list_of_BSSN_gfs = BSSN_evolved_variable_list + BSSN_aux_gridfunctions

BSSN_upwind_vector = [sympify(0) for i in range(3)]
for i in range(3):
    BSSN_upwind_vector[i] = vetU[i]*ReU[i]

FD_CENTDERIVS_ORDER = get_parameter_value("FDCENTERDERIVS_FDORDER", params_varname, params_value)
SIMD = get_parameter_value("SIMD", params_varname, params_value)
if RunMode=="Everything" or RunMode=="RHSonly":
    print("Converting BSSN RHS expressions to C code. This may take a few minutes...")
    starttimer_bssnRHS_CCode = time.time()
    # Below we break up the BSSN RHSs so that even 8th order evolutions are fully vectorized by the compiler,
    #   yielding a big speedup for these slowest runs.
    if SIMD=="256bit" or SIMD=="512bit" or PRECISION == "long double" or (FD_CENTDERIVS_ORDER <= 4 or FD_CENTDERIVS_ORDER > 8): # 10th+ order cannot be vectorized by compiler, so don't bother splitting up RHSs
        BSSN_rhs_vars[0] = BSSN_rhs_vars[0] + BSSN_rhs_vars[1] + BSSN_rhs_vars[2] + BSSN_rhs_vars[3]
        for i in [1, 2, 3]:
            BSSN_aux_vars[i] = []
            BSSN_rhs_vars[i] = []
    elif FD_CENTDERIVS_ORDER == 6:
        BSSN_rhs_vars[0] = BSSN_rhs_vars[0] + BSSN_rhs_vars[1] + BSSN_rhs_vars[2]
        for i in [1, 2]:
            BSSN_aux_vars[i] = []
            BSSN_rhs_vars[i] = []

    for i in range(4):
        NRPy_file_output(OUTDIR+"/evolution_equations/NRPy_codegen/NRPy_BSSN_RHS_pt"+str(i+1)+".h",
                         func_BSSN_gf_define_alias, list_of_BSSN_gfs, BSSN_upwind_vector, BSSN_protected_variables,
                         BSSN_aux_vars[i],BSSN_rhs_vars[i],SIMD)

    stoptimer_bssnRHS_CCode = time.time()
    print("Finished generating BSSN RHS C Code in \t\t\t"+str(round(stoptimer_bssnRHS_CCode-starttimer_bssnRHS_CCode,2))+" seconds")
if RunMode == "Everything" or RunMode == "Riccionly":
    print("Converting BSSN Ricci expressions to C code. This may take a few minutes...")
    starttimer_bssnRicci_CCode = time.time()
    if SIMD=="256bit" or SIMD=="512bit" or PRECISION == "long double" or (FD_CENTDERIVS_ORDER != 8):  # only 8th order benefits from splitting up the Ricci tensor calculation
        BSSN_Ricci_vars[0] += BSSN_Ricci_vars[1]
        BSSN_Ricci_vars[1]  = []
    NRPy_file_output(OUTDIR+"/evolution_equations/NRPy_codegen/NRPy_BSSN_Ricci_pt1.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                     BSSN_upwind_vector, BSSN_protected_variables,def_tmpgammabarsall,   BSSN_Ricci_vars[0],SIMD)
    NRPy_file_output(OUTDIR+"/evolution_equations/NRPy_codegen/NRPy_BSSN_Ricci_pt2.h",func_BSSN_gf_define_alias,list_of_BSSN_gfs,
                     BSSN_upwind_vector, BSSN_protected_variables,def_tmpgammabarsall,   BSSN_Ricci_vars[1],SIMD)

    stoptimer_bssnRicci_CCode = time.time()
    print("Finished generating BSSN Ricci C Code in \t\t\t"+str(round(stoptimer_bssnRicci_CCode-starttimer_bssnRicci_CCode,2))+" seconds")
