#!/bin/bash

COORDSYSTEM="Spherical"
EVOLUTIONTYPE="finite_diff_demo"
INITIALDATA="dummy"

# Clean up any previous files that were there.
rm -f ../output/*

# Keeping the grid structure fixed, do we obtain exponential convergence with increased finite difference order?
cd ../../
for FDORDER in 2 4 6 8 10 12; do 
    python NRPy_main.py Everything $COORDSYSTEM $EVOLUTIONTYPE $INITIALDATA $FDORDER
    cd finite_diff_demo
    make -j
    rm -f test/outputFD$FDORDER.txt
    for j in `seq -6 0.025 1.2`; do
        RMAX=`echo e\($j*l\(10.\)\) | bc -l `
        echo `OMP_NUM_THREADS=1 ./finite_diff_demo 2 2 2 $RMAX | grep -v "FD order"` >> test/outputFD$FDORDER.txt
    done
    cd ..
done
cd finite_diff_demo/test/

paste outputFD2.txt outputFD4.txt outputFD6.txt outputFD8.txt outputFD10.txt outputFD12.txt > outallFDorders.txt
rm -f outputFD?.txt outputFD??.txt

gnuplot gnuplot_script
# convert plots to pdf files
ps2pdf FD_rel_error__vs__delta_x__loglog.ps && ps2pdf delta_FD_rel_error__vs__delta_x__loglog.ps  #&& rm -f *.ps

